package com.finger.tigo.remittance;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by Finger-kjh on 2017-05-25.
 */

public class RemittanceBroadcastReceiver extends BroadcastReceiver {
    public static final String ACTION_SEND_MONEY = "com.finger.tigo.ACTION_SEND_MONEY";

    public static final String KEY_MONEY_AMOUNT = "com.finger.tigo.KEY_MONEY_AMOUNT";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ACTION_SEND_MONEY)) {
            int money = intent.getIntExtra(KEY_MONEY_AMOUNT, 0);

            Toast.makeText(context, ACTION_SEND_MONEY + " received, money = " + money, Toast.LENGTH_LONG).show();

        }
    }

}
