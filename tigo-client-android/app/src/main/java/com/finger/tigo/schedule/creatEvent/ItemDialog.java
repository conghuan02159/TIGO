package com.finger.tigo.schedule.creatEvent;


public class ItemDialog {

    private String color;
    private String color_text;

    public ItemDialog(){

    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor_text() {
        return color_text;
    }

    public void setColor_text(String color_text) {
        this.color_text = color_text;
    }
}