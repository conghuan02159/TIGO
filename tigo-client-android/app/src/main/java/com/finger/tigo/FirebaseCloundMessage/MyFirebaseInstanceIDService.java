package com.finger.tigo.FirebaseCloundMessage;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


/**
 * Created by duong on 11/08/2016.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {
        // Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        // Displaying token on logcat


    }
}
