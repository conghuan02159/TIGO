package com.finger.tigo.friend.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.finger.tigo.R;
import com.finger.tigo.items.ItemNoti;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Duong on 3/13/2017.
 */

public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.MyViewHolder> {

    private Activity activity;
    public static List<ItemNoti> feedItems;

    public HorizontalAdapter(Activity activity, List<ItemNoti> persons){
        this.feedItems = persons;
        this.activity = activity;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_horizontal_list_new_friend, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return feedItems.size();
    }

    @Override
    public void onBindViewHolder(MyViewHolder viewHolder, int position) {
        final ItemNoti item = feedItems.get(position);

        viewHolder.name.setText(item.nameCut);
        Glide.with(activity).load(item.getProfilePic())
                .placeholder(R.drawable.avatar).error(R.drawable.avatar)
                .into(viewHolder.profilePic);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        CircleImageView profilePic;

        public MyViewHolder(View rowView) {
            super(rowView);
            name = (TextView)rowView.findViewById(R.id.name_horizon);
            profilePic = (CircleImageView)rowView.findViewById(R.id.profilePic);

        }
    }
}
