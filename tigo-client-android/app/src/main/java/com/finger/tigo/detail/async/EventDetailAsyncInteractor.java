package com.finger.tigo.detail.async;

import com.finger.tigo.app.ApiExecuter;
import com.finger.tigo.app.AppConfig;

/**
 * Created by Duong on 4/5/2017.
 */

public class EventDetailAsyncInteractor implements IEventDetailAsyncInteractor {

    @Override
    public void InteractorGetEventDetail(ApiExecuter.ApiProgressListener apiProgressListener, ApiExecuter.ApiResultListener apiResultListener, String idUser, String idSc) {
        sendDetails(apiProgressListener, apiResultListener, idUser, idSc);
    }

    @Override
    public void interactorJoinOrNotJoin(ApiExecuter.ApiProgressListener apiProgressListener, String progressMsg,  ApiExecuter.ApiResultListener apiResultListener, String idUser, String idSc, String state) {
        postJoinOrNotJoin(apiProgressListener, progressMsg, apiResultListener, idUser, idSc, state);
    }

    @Override
    public void IterractorInvite(ApiExecuter.ApiProgressListener apiProgressListener, ApiExecuter.ApiResultListener apiResultListener, String idUser, String id_sc, String state) {
        postaccept(apiProgressListener, apiResultListener, idUser,id_sc,state);
    }

    private void postaccept(ApiExecuter.ApiProgressListener apiProgressListener, ApiExecuter.ApiResultListener apiResultListener, final String idUser, final String id_sc, final String status_like){
        ApiExecuter.requestApi(apiProgressListener, AppConfig.URL_ACCEPT_INVITE, null, apiResultListener,
                "idsc", id_sc, "id_user", idUser, "state", status_like);
    }

    // post state join or not join
    private void postJoinOrNotJoin(ApiExecuter.ApiProgressListener apiProgressListener, String progressMsg, ApiExecuter.ApiResultListener apiResultListener, final String idUser, final String id_sc, final String status_like){
        ApiExecuter.requestApi(apiProgressListener, AppConfig.URL_JOIN_OR_NOT_JOIN, progressMsg, apiResultListener,
                "id_sc", id_sc, "id_user_like", idUser, "status_like", status_like);
    }

    // get infomation details event
    private void sendDetails(ApiExecuter.ApiProgressListener apiProgressListener, ApiExecuter.ApiResultListener apiResultListener, final  String idUser, final String idSc) {
        ApiExecuter.requestApi(apiProgressListener, AppConfig.URL_GET_EVENT_DETAIL, null, apiResultListener, "id_sc", idSc, "id_user", idUser);
    }
}
