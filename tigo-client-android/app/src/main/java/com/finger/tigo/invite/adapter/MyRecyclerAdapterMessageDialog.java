package com.finger.tigo.invite.adapter;



import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.finger.tigo.R;
import com.finger.tigo.schedule.creatEvent.Person;

import java.util.List;


public class MyRecyclerAdapterMessageDialog extends RecyclerView.Adapter<MyRecyclerAdapterMessageDialog.PersonViewHolder>  {

    OnItemClickListener mItemClickListener;
    List<Person> people;


    public OnItemClickListener getmItemClickListener() {
        return mItemClickListener;
    }

    public void setmItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public MyRecyclerAdapterMessageDialog(List<Person> persons){
        this.people = persons;

    }

    @Override
    public MyRecyclerAdapterMessageDialog.PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_send_message_dialog, parent, false);
        return new PersonViewHolder(v);
    }


    @Override
    public void onBindViewHolder(MyRecyclerAdapterMessageDialog.PersonViewHolder holder, int position) {

        holder.personName.setText(people.get(position).name);
        holder.personAge.setText(people.get(position).id);
        holder.cbSelected.setChecked(people.get(position).isSelected());
    }

    public void setItemSelected(int position, boolean isSelected){
        if (position != -1) {
            people.get(position).setSelected(isSelected);
            notifyDataSetChanged();
        }
    }



    public interface OnItemClickListener{
        void onItemClick(View v, int position);
    }

    @Override
    public int getItemCount() {
        return people.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class PersonViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener{

        public final View mView;
        public TextView personName;
        public TextView personAge;
        public Person mItem;
        public CheckBox cbSelected;

        public PersonViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            itemView.setOnClickListener(this);

            personName = (TextView) itemView.findViewById(R.id.phone_name_message_dialog);
            personAge = (TextView) itemView.findViewById(R.id.phone_number_message_dialog);
            cbSelected = (CheckBox) itemView.findViewById(R.id.cbSelectedMessageDialog);


        }
        @Override
        public String toString(){
            return super.toString()+" '"+personName +"'";
        }

        @Override
        public void onClick(View v) {
            if(mItemClickListener != null){
                mItemClickListener.onItemClick(v, getPosition());

            }
        }
    }



}
