package com.finger.tigo.login.async;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.login.FacebookVerificode;
import com.finger.tigo.login.ForgotPassword;
import com.finger.tigo.login.LoginActivity;
import com.finger.tigo.login.model.itemLogin;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Duong on 2/27/2017.
 */

public class AsynchLoginInteractor implements IAsynchLoginInteractor{

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    public static final int KEY_ERROR_TWO = 2;
    public static final int KEY_ERROR_ONE = 1;
    // not connect
    public static final int KEY_ERROR_THREE = 3;
    public static final int KEY_ERROR_FOUR = 4;

    ArrayList<itemLogin> listLogin;
    private Bitmap bitmapProfile;
    @Override
    public void validateCredentailsAsync(final OnLoginFinishedListener listener, final String username, final String password) {

                if( password.length() >=8){
                     listLogin  = new ArrayList<itemLogin>();
                     checkLogin(listener, username, password);
                }else{
                    listener.onError(LoginActivity.LOGIN_TIGON, KEY_ERROR_ONE);
                }

    }

    @Override
    public void validateCreadentailsAsyncFacebook(final OnLoginFinishedListener listener, final String idFace, final String name, final int sex) {

                 listLogin  = new ArrayList<itemLogin>();
                 bitmapProfile = getFacebookProfilePicture(idFace);

                registerfacebook(listener, idFace, name, sex);

    }

    @Override
    public void validateFacebookVerificode(OnLoginFinishedListener listener, String idUser, String phone) {
        listLogin  = new ArrayList<itemLogin>();

        UpdatePhoneFacebook(listener, idUser, phone);
    }

    @Override
    public void validateVerificodeAsync(OnLoginFinishedListener listener, String verificode) {
        listLogin = new ArrayList<>();
        verifyOtp(listener, verificode);
    }

    @Override
    public void validateSearchPhone(OnLoginFinishedListener listener, String phone) {
        listLogin = new ArrayList<itemLogin>();
        ConfirmPhone(listener, phone);
    }

    @Override
    public void validateForgotOtp(OnLoginFinishedListener listener, String Otp) {
        listLogin = new ArrayList<>();
        phoneOtpForgotPass(listener, Otp);

    }

    @Override
    public void validateResetPass(OnLoginFinishedListener listener, String idUser, String password) {
        listLogin = new ArrayList<>();
        ResetPassword(listener, idUser, password);
    }
    public void ResetPassword(final OnLoginFinishedListener listener, final String iduser, final String password) {

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_RESET_PASS, new Response.Listener<String>(){

            @Override
            public void onResponse(String response) {
                listener.onNetWorkSuccess(ForgotPassword.RESET_PASSWORD, listLogin);

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError(ForgotPassword.RESET_PASSWORD, KEY_ERROR_ONE);

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_user", iduser);
                params.put("password", password);


                return params;

            }

        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq);

    }
    //phone forgot

    private void phoneOtpForgotPass(final OnLoginFinishedListener listener, final String otp) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_VERIFY_OTP_FORGOT_PASS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject responseObj = new JSONObject(response);

                    boolean error = responseObj.getBoolean("error");
                    if (!error) {
                        listener.onNetWorkSuccess(ForgotPassword.FORGOT_OTP, listLogin);

                    } else {
                       listener.onError(ForgotPassword.FORGOT_OTP, KEY_ERROR_ONE);
                    }

                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError(ForgotPassword.FORGOT_OTP, KEY_ERROR_TWO);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("otp", otp);

                return params;
            }

        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }

    public void ConfirmPhone(final  OnLoginFinishedListener listener, final String strPhone) {

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_CONFIRM_PHONE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    response = response.replace("NULL", "");
                    response = response.trim();
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    if (!error) {
                        JSONObject user = jObj.getJSONObject("user");
                        listLogin.add(new itemLogin(user.getInt("id_user")));
                        // setid = item.getId();

                        listener.onNetWorkSuccess(ForgotPassword.SEARCH_PHONE, listLogin);
                    } else {

                        listener.onError(ForgotPassword.SEARCH_PHONE, KEY_ERROR_ONE);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError(ForgotPassword.SEARCH_PHONE, KEY_ERROR_TWO);

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("numberPhone", strPhone);

                return params;
            }

        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq);


    }

// Verificode
    private void verifyOtp(final OnLoginFinishedListener listener, final String otp) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_VERIFY_OTP, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject responseObj = new JSONObject(response);

                    // Parsing json object response
                    // response will be a json object
                        boolean error = responseObj.getBoolean("error");

                    if (!error) {
                        // parsing the user profile information

                        listener.onNetWorkSuccess(FacebookVerificode.VERIFICODE, listLogin);

                    } else {

                        listener.onError(FacebookVerificode.VERIFICODE, KEY_ERROR_ONE);
                    }

                } catch (JSONException e) {

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                listener.onError(FacebookVerificode.VERIFICODE, KEY_ERROR_TWO);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("otp", otp);


                return params;
            }

        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }


    private void UpdatePhoneFacebook(final  OnLoginFinishedListener listener, final String idUser,  final String phonenumber) {

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_PHONE_FACEBOOK, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    response = response.replace("NULL", "");
                    response = response.trim();
                    JSONObject responseObj = new JSONObject(response);
                    // Parsing json object response
                    // response will be a json object
                    boolean error = responseObj.getBoolean("error");

                    if (!error) {
                        listener.onNetWorkSuccess(FacebookVerificode.FACEBOOK_VERIFICODE, listLogin);

                    } else {
                        listener.onError(FacebookVerificode.FACEBOOK_VERIFICODE, KEY_ERROR_ONE);
                    }

                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                listener.onError(FacebookVerificode.FACEBOOK_VERIFICODE, KEY_ERROR_TWO);

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url

                Map<String, String> params = new HashMap<String, String>();

                params.put("iduser", idUser);
                params.put("phonenumber", phonenumber);

                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);

    }


    private void registerfacebook(final OnLoginFinishedListener listener, final String id_face, final String name,
                                   final int sex) {


        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_LOGIN_FACEBOOK, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                //  hideDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    JSONObject user = jObj.getJSONObject("user");

                    if (user != null) {

                        String profilePic  = user.isNull("profilepic") ? null : user.getString("profilepic");
                        String phone  = user.isNull("phone") ? null : user.getString("phone");

                        itemLogin item = new itemLogin(user.getInt("state"), user.getInt("id_user"), user.getString("name"), phone,
                                user.getString("sex"), profilePic, user.getString("created_at"));

                        listLogin.add(item);
                        //updateToken(listener, idUser);
                        listener.onNetWorkSuccess( LoginActivity.LOGIN_FACEBOOK,listLogin);

                    }
                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                listener.onError(LoginActivity.LOGIN_FACEBOOK, KEY_ERROR_ONE);

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                String token = FirebaseInstanceId.getInstance().getToken();
                // Displaying token on logcat
                String image = getStringImage(bitmapProfile);

                params.put("Pid_facebook", id_face);
                params.put("Pname", name);
                params.put("Psex", String.valueOf(sex));
                params.put("PprofilePic", image);
                params.put("Pid_token", token);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }

    public static Bitmap getFacebookProfilePicture(String userID) {

        Bitmap bitmap4 = null;
        try {
            URL imageURL = new URL("https://graph.facebook.com/" + userID + "/picture?type=large");
            bitmap4 = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bitmap4;
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void checkLogin(final OnLoginFinishedListener listener, final String email, final String password) {
        // Tag used to cancel the request

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {

                        JSONObject user = jObj.getJSONObject("user");
                        String idUser = user.getInt("id_user")+"";
                        String profilePic = user.isNull("profilePic") ? null : user.getString("profilePic");

                        itemLogin item = new itemLogin(user.getInt("id_user") ,user.getString("name"), user.getString("phone"),
                                user.getString("birthday"), user.getString("sex"), profilePic, user.getString("created_at"));

                        updateToken(listener, idUser);

                        listLogin.add(item);
                        listener.onNetWorkSuccess(LoginActivity.LOGIN_TIGON, listLogin);


                    } else {
                        // Error in login. Get the error message
                        listener.onError(LoginActivity.LOGIN_TIGON, KEY_ERROR_TWO);

                    }
                } catch (JSONException e) {

                    e.printStackTrace();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                listener.onError(LoginActivity.LOGIN_TIGON, KEY_ERROR_THREE);

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("password", password);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }

    private void updateToken(final OnLoginFinishedListener listener, final String id_user) {
        // Tag used to cancel the request
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPDATE_TOKEN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                listener.onError(LoginActivity.LOGIN_TIGON, KEY_ERROR_FOUR);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                String token = FirebaseInstanceId.getInstance().getToken();
                params.put("id_user", id_user);
                params.put("id_token", token);
                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }

}
