package com.finger.tigo.addfriend.async;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.finger.tigo.addfriend.DetailFriendActivity;
import com.finger.tigo.addfriend.fragment.TigonPhoneFragment;
import com.finger.tigo.addfriend.model.ItemAddfriend;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.finger.tigo.addfriend.DetailFriendActivity.GET_DATA_EVENT;
import static com.finger.tigo.addfriend.fragment.TigonPhoneFragment.CHECK_NEW_FRIEND;

/**
 * Created by Duong on 3/30/2017.
 */

public class AddfriendInteractor implements IAddfriendInteractor {

    public static  final  int   KEY_ERROR_ONE = 1;
    public static final int KEY_ERROR_TWO = 2;

    List<ItemAddfriend> lisAddfriend;
    @Override
    public void validatePhone(OnNewFrFinishedListener listener, String phone) {
        lisAddfriend = new ArrayList<>();
        searchPhone(listener, phone);
    }

    @Override
    public void validateSyncContact(OnNewFrFinishedListener listener, List<ItemAddfriend> listContact, String idUser) {
        lisAddfriend = new ArrayList<>();
        AddFriend(listener, listContact, idUser);
    }

    @Override
    public void validateCheckFriend(OnNewFrFinishedListener listener, String idUser, String idFriend) {
        lisAddfriend = new ArrayList<>();
        checkFriend(listener, idUser, idFriend);
    }

    @Override
    public void validateAcceptFriend(OnNewFrFinishedListener listener, String iduser, String idfriend, String name, String phone, String state) {
        lisAddfriend = new ArrayList<>();
        loadAdded(listener, iduser, idfriend, name, phone, state);
    }

    @Override
    public void validateAddFriend(OnNewFrFinishedListener listener, String idUser, String idFriend, String name, String phone) {
        addNewFriend(listener, idUser, idFriend, name, phone);
    }

    @Override
    public void validateDataTigon(OnNewFrFinishedListener listener, String idTigon) {
        lisAddfriend = new ArrayList<>();
        getId_qr(listener, idTigon);

    }

     /*
       *********** get qr profile*********************
        */

    public void getId_qr( final OnNewFrFinishedListener listener, final String idTigon) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GET_USER_QR, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);

                   String  strName = jObj.isNull("name") ? null : jObj.getString("name");
                   String  strPhone = jObj.isNull("phone") ? null : jObj.getString("phone");
                    String   strProfilePic = jObj.isNull("profilePic") ? null : jObj.getString("profilePic");
                    lisAddfriend.add(new ItemAddfriend(strPhone, strName, strProfilePic));
                   listener.listenerSuccess(DetailFriendActivity.GET_DATA_TIGON, lisAddfriend);
                } catch (Exception ex) {

                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_user", idTigon);
                return params;
            }
        };
        strReq.setTag(this.getClass().getName());
        AppController.getInstance().addToRequestQueue(strReq);
    }


    // post id friend get data event
    public void postId(final OnNewFrFinishedListener listener, final String idUser, final int pageDowload) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_EVENT_FRIEND, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jObj = new JSONObject(response);
                    JSONArray feedArray = jObj.getJSONArray("feed");
                    if(feedArray != null) {
                        for (int i = 0; i < feedArray.length(); i++) {

                            JSONObject feedObj = (JSONObject) feedArray.get(i);
                            String image = feedObj.isNull("image") ? null : feedObj
                                    .getString("image");

                            // url might be null sometimes
                            String strDestription = feedObj.isNull("description") ? null : feedObj
                                    .getString("description");

                            lisAddfriend.add(new ItemAddfriend(feedObj.getString("id_sc"), feedObj.getString("status"), strDestription, image, feedObj.getString("daystart")));
                        }
                        listener.listenerSuccess(GET_DATA_EVENT, lisAddfriend);
                    }else{
                        //Log.d("tttt : ", "null");
                    }

                } catch (Exception ex) {
                    // JSON parsing error

                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                listener.listenerError(GET_DATA_EVENT, KEY_ERROR_ONE);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("idUser", idUser);
                params.put("page", pageDowload + "");
                return params;
            }
        };
        strReq.setTag(this.getClass().getName());
        AppController.getInstance().addToRequestQueue(strReq);
    }


    // add friend
    private void addNewFriend(final OnNewFrFinishedListener listener, final String iduser, final String idfriend, final String name, final String phone) {
        // Tag used to cancel the request

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADDNEWFRIEND, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                listener.listenerSuccess( DetailFriendActivity.ADD_NEW_FRIEND,lisAddfriend);

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                listener.listenerError(DetailFriendActivity.ADD_NEW_FRIEND, KEY_ERROR_ONE);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("iduser", iduser);
                params.put("idfriend", idfriend);
                params.put("name", name);
                params.put("phone", phone);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }

    // accept or rufust friend
    private void loadAdded(final OnNewFrFinishedListener listener, final String iduser, final String idfriend, final String name, final String phone,
                           final String state) {
        // Tag used to cancel the request

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_INSERT_FRIEND, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                lisAddfriend.add(new ItemAddfriend(state));
                listener.listenerSuccess( DetailFriendActivity.ACCEPT_OR_RUFUST,lisAddfriend);

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

              listener.listenerError(DetailFriendActivity.ACCEPT_OR_RUFUST, KEY_ERROR_ONE);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("iduser", iduser);
                params.put("idfriend", idfriend);
                params.put("name", name);
                params.put("phone", phone);
                params.put("state", state);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }

    // Check friend
    private void checkFriend(final OnNewFrFinishedListener listener, final String idUser, final String idFriend) {

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_CHECK_FRIEND, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        JSONObject confirm = jObj.getJSONObject("confirm");

                        lisAddfriend.add(new ItemAddfriend(confirm.getInt("confirm"), confirm.getString("countEvent"), confirm.getString("countFriend")));
                        listener.listenerSuccess(DetailFriendActivity.CHECKFRIEND, lisAddfriend);

                    } else {
                        listener.listenerError(DetailFriendActivity.CHECKFRIEND, KEY_ERROR_ONE);

                    }

                } catch (Exception ex) {

                    ex.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                listener.listenerError(DetailFriendActivity.CHECKFRIEND, KEY_ERROR_ONE);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<>();

                params.put("iduser", idUser);
                params.put("idfriend",idFriend);
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(strReq);

    }


    // Addfriend from contact
    private void AddFriend(final OnNewFrFinishedListener listener, final  List<ItemAddfriend> listContact, final String idUser) {
        // Tag used to cancel the request

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADDFRIEND, new Response.Listener<String>(){

            @Override
            public void onResponse(String response) {

                listener.listenerSuccess(TigonPhoneFragment.SYNC_CONTACT_NEW_FRIEND, lisAddfriend);

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                listener.listenerError(TigonPhoneFragment.SYNC_CONTACT_NEW_FRIEND, KEY_ERROR_ONE);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                JSONArray arrPhone = new JSONArray();
                JSONArray arrName = new JSONArray();
                Map<String, String> params = new HashMap<String, String>();
                params.put("iduser", idUser);

                for(ItemAddfriend object: listContact){

                    arrPhone.put(object.phone);
                    arrName.put(object.name);

                }

                params.put("friendPhone", arrPhone.toString());
                params.put("friendName", arrName.toString());

                return params;

            }

        };
        AppController.getInstance().addToRequestQueue(strReq);
    }

    private void searchPhone(final OnNewFrFinishedListener listener, final String phone) {

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_CHECK_NEW_FRIEND, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        JSONObject user = jObj.getJSONObject("user");

                        lisAddfriend.add(new ItemAddfriend(user.getString("iduser"), user.getString("name"), user.getString("profilePic")));
                        listener.listenerSuccess(CHECK_NEW_FRIEND, lisAddfriend);

                    } else {
                        listener.listenerError(CHECK_NEW_FRIEND, KEY_ERROR_ONE);

                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                listener.listenerError(CHECK_NEW_FRIEND, KEY_ERROR_TWO);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                // Displaying token on logcat
                params.put("phone", phone);


                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }

}
