package com.finger.tigo.menu.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.finger.tigo.BaseFragment;
import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.app.ApiExecuter;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppIntent;
import com.finger.tigo.items.FeedItem;
import com.finger.tigo.menu.ProfileAvatarActivity;
import com.finger.tigo.menu.SettingActivity;
import com.finger.tigo.menu.UpdateUserActivity;
import com.finger.tigo.menu.adapter.MenuRecyclerAdapter;
import com.finger.tigo.session.SessionManager;
import com.finger.tigo.util.DateUtil;
import com.finger.tigo.util.MyLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class MenuFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    public SessionManager session;

    private List<FeedItem> feedItems;
    private MenuRecyclerAdapter listAdapter;

    int pageToDownload;
    String iduser;

    RecyclerView recyclerViewMenu;
    TextView username;
    TextView tvBitrhDay;
    TextView phone;
    TextView tvEvent;
    Spinner spinEventFilter;
    CircleImageView imageView;
    SwipeRefreshLayout swipeRefreshLayout;
    ImageButton  update_user;
    TextView setting;
    LinearLayout lnNotData;
    LinearLayout lnNotConnect;
    Button reptryConnect;

    Handler handler;
    private boolean mUpdateProfileImage;

    public String URL;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.menu_person_detail, container, false);

        recyclerViewMenu = (RecyclerView)v.findViewById(R.id.recycler_menu);
        username = (TextView)v.findViewById(R.id._name);
        tvBitrhDay = (TextView)v.findViewById(R.id.txt_birthday);
        phone = (TextView)v.findViewById(R.id._phone);
        tvEvent = (TextView)v.findViewById(R.id.tvEventCount);
        spinEventFilter = (Spinner)v.findViewById(R.id.spin_event_filter);
        imageView = (CircleImageView)v.findViewById(R.id.ivPhoto);
        swipeRefreshLayout = (SwipeRefreshLayout)v.findViewById(R.id.id_swiRefresh);
        reptryConnect = (Button) v.findViewById(R.id.bt_reptry_connect);

        update_user = (ImageButton)v.findViewById(R.id.update_user);
        setting = (TextView)v.findViewById(R.id.setting);
        lnNotData = (LinearLayout) v.findViewById(R.id.ln_not_data_menu);
        lnNotConnect = (LinearLayout) v.findViewById(R.id.layout_not_connect);

        // spinner triangle color change
        Drawable spinnerDrawable = spinEventFilter.getBackground().getConstantState().newDrawable();
        spinnerDrawable.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            spinEventFilter.setBackground(spinnerDrawable);
        }else{
            spinEventFilter.setBackgroundDrawable(spinnerDrawable);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.simple_spinner_item_gravity_right,
                getResources().getStringArray(R.array.event_filter_entries));
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        spinEventFilter.setAdapter(adapter);

        session = new SessionManager(getActivity());

        iduser = AppAccountManager.getAppAccountUserId(getActivity());

        infoUser();
        countEvent();

        swipeRefreshLayout.setColorSchemeColors(Color.BLUE, Color.GREEN, Color.YELLOW, Color.RED);
        swipeRefreshLayout.setDistanceToTriggerSync(100);
        swipeRefreshLayout.setSize(SwipeRefreshLayout.DEFAULT);
        swipeRefreshLayout.setOnRefreshListener(this);

        initRecyclerView();
        handler = new Handler();
        swipeRefreshLayout.setRefreshing(true);
//        pageToDownload = 1;
//        getEventFeedList(iduser, URL);

        URL =  AppConfig.URL_EVENT_USER;
//        pageToDownload = 1;
//        getEventFeedList(iduser);

        update_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editUser();
            }
        });

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setting();
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ProfileAvatarActivity.class);
                intent.putExtra("idUser", iduser);
                if (session.getLpic_session() == null) {
                } else {
                    intent.putExtra("profilePicUrl", session.getLpic_session());
                }
                startActivityForResult(intent, AppIntent.REQUEST_CHANGE_AVATAR);
            }
        });

        spinEventFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                MyLog.i("position=" + position);
                if(position ==0){
                    showProgress(getActivity().getResources().getString(R.string.loading));
                    URL =  AppConfig.URL_EVENT_USER;
                    pageToDownload = 1;
                    getEventFeedList(iduser, URL);
                }else if(position ==1){
                    showProgress(getActivity().getResources().getString(R.string.loading));
                    URL =  AppConfig.URL_EVENT_USER_PROGRESSING;
                    pageToDownload = 1;
                    getEventFeedList(iduser, URL);
                }else if(position ==2){
                    showProgress(getActivity().getResources().getString(R.string.loading));
                    URL =  AppConfig.URL_EVENT_USER_LAST;
                    pageToDownload = 1;
                    getEventFeedList(iduser, URL);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinEventFilter.setSelection(0);
        reptryConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pageToDownload =1;
                getEventFeedList(iduser, URL);
            }
        });

//        if(!BuildConfig.IS_CAN_EDIT_PROFILE) {
//            update_user.setVisibility(View.GONE);
//            setting.setVisibility(View.GONE);
//        }

        return v;
    }



    private void initRecyclerView() {
        feedItems = new ArrayList<>();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewMenu.setLayoutManager(mLayoutManager);
//        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerViewMenu.setItemAnimator(new DefaultItemAnimator());
        listAdapter = new MenuRecyclerAdapter(getActivity(), feedItems, recyclerViewMenu);
        recyclerViewMenu.setAdapter(listAdapter);

        listAdapter.setOnLoadMoreListener(new MenuRecyclerAdapter.OnLoadMoreListener() {
            @Override
            public void onLoadmore() {
                Log.d("error loadmore: ", ": ");
                    feedItems.add(null);
                    recyclerViewMenu.post(new Runnable() {
                        @Override
                        public void run() {
                            listAdapter.notifyDataSetChanged();
                            //listAdapter.notifyItemInserted(feedItems.size() - 1);
                        }
                    });

                    if (feedItems.size() >= 10) {
                        ++pageToDownload;

                    }
                getEventFeedList(iduser, URL);



            }
        });
    }


    private void infoUser() {
        username.setText(session.getLName_session());
        tvBitrhDay.setText(DateUtil.getDateString(session.getbirhtday_session()));
        phone.setText(session.getphone_session());

        MyLog.d("profileImage url=" + session.getLpic_session());

        Glide.with(getActivity()).load(session.getLpic_session())
                .placeholder(R.drawable.avatar)
                .error(R.drawable.avatar)
                .dontAnimate()      // if you dont write this, CircleImageView is not displayed.
                .into(imageView);

    }


    // chinh sua user

    public void editUser() {
        Intent in = new Intent(getActivity(), UpdateUserActivity.class);
        startActivityForResult(in, AppIntent.REQUEST_EDIT_PROFILE);
    }


    public void setting() {
        Intent i = new Intent(getActivity(), SettingActivity.class);
        startActivity(i);
    }


    public void getEventFeedList(final String idUser, final String url) {
        ApiExecuter.requestApiNoProgress(url, new ApiExecuter.ApiResultListener() {
            @Override
            public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {

                try {
                    JSONObject jObj = new JSONObject(response);
                    JSONArray feedArray = jObj.getJSONArray("feed");
                    hideProgress();
                    if (feedArray != null) {
                        if (pageToDownload > 1) {
                            feedItems.remove(feedItems.size() - 1);
                            listAdapter.notifyItemRemoved(feedItems.size());
                        } else {
                            feedItems.clear();
                            listAdapter.notifyDataSetChanged();
                        }

                        for (int i = 0; i < feedArray.length(); i++) {
                            JSONObject feedObj = (JSONObject) feedArray.get(i);
                            String image = feedObj.isNull("image") ? null : feedObj
                                    .getString("image");
                            // url might be null sometimes
                            String strDestription = feedObj.isNull("description") ? null : feedObj
                                    .getString("description");

                            FeedItem item = new FeedItem();

                            item.setId_sc(feedObj.getString("id_sc"));

                            item.setImge(image);
                            item.setdaystart(feedObj.getString("daystart"));
                            item.setDayend(feedObj.getString("dayfinish"));

                            item.setStatus(feedObj.getString("status"));
                            item.setAddress(feedObj.getString("location"));
                            item.setPosition(feedObj.getString("position"));

                            item.setColor(feedObj.getString("color"));
                            item.setMode(feedObj.getString("mode"));
                            item.setPhoneEvent(feedObj.getString("phone_event"));
                            item.setLg1(feedObj.getDouble("lg1"));
                            item.setLg2(feedObj.getDouble("lg2"));
                            item.setCategory(feedObj.getString("category"));
                            item.setAllowInvite(feedObj.getString("allowinvite"));
                            item.setDescription(strDestription);
                            item.setAllDay(feedObj.getString("allday"));
                            item.setAlarm(feedObj.getString("alarm"));

                            feedItems.add(item);

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    listAdapter.notifyItemInserted(feedItems.size());

                                }
                            });

                        }

                        if (swipeRefreshLayout.isRefreshing()) {
                            swipeRefreshLayout.setRefreshing(false);
                        }
                        if(feedArray.length() >= 10)
                            listAdapter.setLoaded();
                        if(feedItems.size() ==0){
                            lnNotData.setVisibility(View.VISIBLE);
                            lnNotConnect.setVisibility(View.GONE);

                        }else{
                            lnNotData.setVisibility(View.GONE);
                            lnNotConnect.setVisibility(View.GONE);
                        }

                    }

                } catch (Exception ex) {
                    // JSON parsing error

                    ex.printStackTrace();
                    onFail(apiUrl, params, ex.getMessage(), ex);
                }

            }

            @Override
            public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {
                hideProgress();

                if(feedItems.size() <=0){
                    lnNotConnect.setVisibility(View.VISIBLE);
                }else {
                    showToast(errMessage);
                }
                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }

            }
        }, "idUser", idUser, "page", pageToDownload + "", "currentTime", Calendar.getInstance().getTimeInMillis() + "");
    }

    private void countEvent() {
        ApiExecuter.requestApiNoProgress(AppConfig.URL_COUNT_EVENT_FRIEND, new ApiExecuter.ApiResultListener() {
            @Override
            public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                try {
                    JSONObject jObj = new JSONObject(response);

                    JSONObject confirm = jObj.getJSONObject("like");
                    if(confirm.getString("countEvent").equals("1"))
                    {
                        tvEvent.setText(confirm.getString("countEvent"));
                    }
                    else
                    {
                        tvEvent.setText(confirm.getString("countEvent"));
                    }
                    //tvEvent.setText(confirm.getString("countEvent"));
//                    tvFriend.setText(confirm.getString("countFriend"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    onFail(apiUrl, params, e.getMessage(), e);
                }
            }

            @Override
            public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {

            }
        }, "iduser", iduser);
    }

    @Override
    public void onResume() {
        super.onResume();

        MyLog.d("imageView.getTag()=" + imageView.getTag() + ", mUpdateProfileImage=" + mUpdateProfileImage);

        if (mUpdateProfileImage) {
            infoUser();
            mUpdateProfileImage = false;
        }
        countEvent();
    }



    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public void onRefresh() {

        pageToDownload = 1;
        getEventFeedList(iduser, URL);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == AppIntent.REQUEST_CHANGE_AVATAR) {
            if(resultCode == Activity.RESULT_OK) {
                infoUser();
            }
        } else if(requestCode == AppIntent.REQUEST_EDIT_PROFILE) {
            if (resultCode == Activity.RESULT_OK) {
                infoUser();
            }

        }
    }
}
