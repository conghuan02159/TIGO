package com.finger.tigo.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.HashMap;

public class SQLiteHandler  extends SQLiteOpenHelper {

    private static final String TAG = SQLiteHandler.class.getSimpleName();

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "dataTigo";

    // Login table name
    private static final String TABLE_USER = "users";
    private static final String TABLE_SCHEDULE = "schedule";

    // Login Table Columns names
    private static final String KEY_ID = "id_user";
    private static final String KEY_ID_FACEBOOK ="id_facebook";
    private static final String KEY_NAME = "name";
    private static final String KEY_PHONE = "phone";
    private static final String KEY_SEX = "sex";
    private static final String KEY_BIRTHDAY = "birthday";
    private static final String KEY_CREATED_AT = "created_at";
    private static final String KEY_PROFILE_PIC = "profilePic";

    // Schedule table Collumns event
    private static final String KEY_ID_SC = "id_sc";
    private static final String KEY_ID_USER = "id_user";
    private static final String KEY_NUMBER_VIEW ="number_view";
    private static final String KEY_STATUS = "status";
    private static final String KEY_LOCATION = "location";
    private static final String KEY_POSITION ="position";
    private static final String KEY_MILLIS_START ="millis_start";
    private static final String KEY_MILLIS_END = "millis_end";
    private static final String KEY_MODE = "mode";
    private static final String KEY_PHONE_EVENT   = "phone_event";
    private static final String KEY_LG1 = "lg1";
    private static final String KEY_LG2 = "lg2";
    private static final String KEY_COLOR = "color";
    private static final String KEY_CATEGORY = "category";
    private static final String KEY_ALLOW_INVITE ="allow_invite";
    private static final String KEY_IMAGE = "image";
    private static final String KEY_ALARM = "alarm";
    private static final String KEY_DESCRIPTION ="position";
    private static final String KEY_ALL_DAY ="allday";
    private static final String KEY_DAY_UP_DATE = "dayupdate";


    public SQLiteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_USER + "("
                + KEY_ID + " INTEGER PRIMARY KEY," +KEY_ID_FACEBOOK +" TEXT,"+ KEY_NAME + " TEXT,"
                + KEY_PHONE + " TEXT,"
                + KEY_SEX + "INT,"
                + KEY_BIRTHDAY + "TEXT,"
                + KEY_PROFILE_PIC + "TEXT,"
                + KEY_CREATED_AT + " TEXT" + ")";
        db.execSQL(CREATE_LOGIN_TABLE);
        Log.d(TAG, "Database tables created");

        String CREATE_SCHEDULE = "CREATE TABLE " + TABLE_SCHEDULE + "("
                + KEY_ID_SC + " TEXT PRIMARY KEY," +KEY_ID_USER +" TEXT,"+ KEY_NUMBER_VIEW + " INTEGER,"
                + KEY_STATUS + " TEXT,"
                + KEY_LOCATION + "TEXT,"
                + KEY_POSITION + "TEXT,"
                + KEY_MILLIS_START + "TEXT,"
                + KEY_MILLIS_END + " TEXT,"
                + KEY_MODE + "TEXT,"
                + KEY_PHONE_EVENT + "TEXT,"
                + KEY_LG1 + "TEXT,"
                + KEY_LG2 + " TEXT"
                + KEY_COLOR + "TEXT,"
                + KEY_CATEGORY + " TEXT,"
                + KEY_ALLOW_INVITE + "TEXT,"
                + KEY_IMAGE + "TEXT,"
                + KEY_ALARM + "INTEGER,"
                + KEY_DESCRIPTION + " TEXT"
                + KEY_ALL_DAY + "INTEGER,"
                + KEY_DAY_UP_DATE + " TEXT"+ ")";
        db.execSQL(CREATE_SCHEDULE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        db.execSQL("DROP TABLE IF EXISTS" + TABLE_SCHEDULE);
        // Create tables again
        onCreate(db);
    }



    /**
     * Storing user details in database
     * */
    public void addUser(int iduser, String name,  String phone, int sex, String profilePic, String birthday, String created_at) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, iduser);

        values.put(KEY_NAME, name); // Name
        values.put(KEY_PHONE, phone);//phone
        values.put(KEY_SEX, sex);//sex
        values.put(KEY_PROFILE_PIC, profilePic); // avatar
        values.put(KEY_BIRTHDAY, birthday);//birthday
        values.put(KEY_CREATED_AT, created_at); // Created At
        // Inserting Row
        long id = db.insert(TABLE_USER, null, values);
        db.close(); // Closing database connection
        Log.d(TAG, "New user inserted into sqlite: " + id);
    }

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_USER;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            user.put(KEY_ID, cursor.getString(0));
            user.put(KEY_ID_FACEBOOK, cursor.getString(1));
            user.put("name", cursor.getString(2));
            user.put("phone", cursor.getString(3));
            user.put("sex", cursor.getString(4));
            user.put("birthday", cursor.getString(5));
            user.put(KEY_PROFILE_PIC, cursor.getString(6));
            user.put("created_at", cursor.getString(7));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching user from Sqlite: " + user.toString());
        return user;
    }



    public void deleteUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_USER, null, null);
        db.close();
        Log.d(TAG, "Deleted all user info from sqlite");
    }

/*
* Storing schedule details in database
*
* */

    public void addSchedule(String idSc,  String idUser, int numberView, String status, String location, String position, String millisStart, String millisEnd,
                            String mode, String phoneEvent, String lg1, String lg2, String color, String category, String allowInvite, String image, int alarm,
                            String description, int allday, String dayupdate ) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID_SC, idSc); //
        values.put(KEY_ID_USER, idUser);//
        values.put(KEY_NUMBER_VIEW, numberView);//
        values.put(KEY_STATUS, status); //
        values.put(KEY_LOCATION, location);//
        values.put(KEY_POSITION, position); //
        values.put(KEY_MILLIS_START, millisStart); //
        values.put(KEY_MILLIS_END, millisEnd);//
        values.put(KEY_MODE, mode);//
        values.put(KEY_PHONE_EVENT, phoneEvent); //
        values.put(KEY_LG1, lg1);//
        values.put(KEY_LG2, lg2); //
        values.put(KEY_COLOR, color);//
        values.put(KEY_CATEGORY, category); //
        values.put(KEY_ALLOW_INVITE, allowInvite); //
        values.put(KEY_IMAGE, image);//
        values.put(KEY_ALARM, alarm);//
        values.put(KEY_DESCRIPTION, description); //
        values.put(KEY_ALL_DAY, allday);//
        values.put(KEY_DAY_UP_DATE, dayupdate); //
        // Inserting Row
        long id = db.insert(TABLE_SCHEDULE, null, values);
        db.close(); // Closing database connection
        Log.d(TAG, "New user inserted into sqlite: " + id);
    }

    public HashMap<String, String> getEvent() {
        HashMap<String, String> user = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_SCHEDULE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            user.put(KEY_ID_SC, cursor.getString(0));
            user.put(KEY_ID_USER, cursor.getString(1));
            user.put(KEY_NUMBER_VIEW, cursor.getString(2));
            user.put(KEY_STATUS, cursor.getString(3));
            user.put(KEY_LOCATION, cursor.getString(4));
            user.put(KEY_POSITION, cursor.getString(5));
            user.put(KEY_MILLIS_START, cursor.getString(6));
            user.put(KEY_MILLIS_END, cursor.getString(7));
            user.put(KEY_MODE, cursor.getString(8));
            user.put(KEY_PHONE_EVENT, cursor.getString(9));
            user.put(KEY_LG1, cursor.getString(10));
            user.put(KEY_LG2, cursor.getString(11));
            user.put(KEY_COLOR, cursor.getString(12));
            user.put(KEY_CATEGORY, cursor.getString(13));
            user.put(KEY_ALLOW_INVITE, cursor.getString(14));
            user.put(KEY_IMAGE, cursor.getString(15));
            user.put(KEY_ALARM, cursor.getString(16));
            user.put(KEY_DESCRIPTION, cursor.getString(17));
            user.put(KEY_ALL_DAY, cursor.getString(18));
            user.put(KEY_DAY_UP_DATE, cursor.getString(19));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching user from Sqlite: " + user.toString());
        return user;
    }



    public void deleteSchedule() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_SCHEDULE, null, null);
        db.close();
        Log.d(TAG, "Deleted all user info from sqlite");
    }
}

