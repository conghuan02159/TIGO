package com.finger.tigo.login;

import com.finger.tigo.login.model.itemLogin;

import java.util.List;

/**
 * Created by Duong on 2/27/2017.
 */

public interface ILoginView {

    void onSuccess(String success, List<itemLogin> list);
    void onError(String error, int key);

}
