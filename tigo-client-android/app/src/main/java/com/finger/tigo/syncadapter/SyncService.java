/**
 * Copyright (c) 2012-2013, David Wiesner
 * 
 * This file is part of Andoid Caldav Sync Adapter Free.
 *
 * Andoid Caldav Sync Adapter Free is free software: you can redistribute 
 * it and/or modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3 of the 
 * License, or at your option any later version.
 *
 * Andoid Caldav Sync Adapter Free is distributed in the hope that 
 * it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Andoid Caldav Sync Adapter Free.  
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package com.finger.tigo.syncadapter;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.finger.tigo.session.SessionManager;
import com.finger.tigo.util.MyLog;

import me.leolin.shortcutbadger.ShortcutBadger;

public class SyncService extends Service {
    private static final Object sSyncAdapterLock = new Object();

    private static SyncAdapter sSyncAdapter = null;
    SessionManager session;

    @Override
    public void onCreate() {
        session = new SessionManager(getApplicationContext());
        synchronized (sSyncAdapterLock) {
            if (sSyncAdapter == null) {
                sSyncAdapter = new SyncAdapter(getApplicationContext(), true);
            }
        }
        badgeIconScreen();
    }

    public void badgeIconScreen(){
        int badgeCount = 0;
        int count = session.getCountFriendNotification()+session.getCountNotification();
        session.setCountNotificationScreen(count);
        if(session.getCountNotificationScreen() >0) {
            try {
                badgeCount = session.getCountNotificationScreen();
            } catch (NumberFormatException e) {
                MyLog.d("badge Count screen: ",e.getMessage());
            }

            ShortcutBadger.applyCount(getApplicationContext(), badgeCount);
        }else{
            ShortcutBadger.removeCount(getApplicationContext());
        }

    }

    @Override
    public IBinder onBind(Intent intent) {
        return sSyncAdapter.getSyncAdapterBinder();
    }
}
