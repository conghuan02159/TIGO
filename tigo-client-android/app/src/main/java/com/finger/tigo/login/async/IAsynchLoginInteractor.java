package com.finger.tigo.login.async;

/**
 * Created by Duong on 2/27/2017.
 */

public interface IAsynchLoginInteractor {
    void validateCredentailsAsync(OnLoginFinishedListener listener, String username, String password);

    void validateCreadentailsAsyncFacebook(OnLoginFinishedListener listener, String idFace, String name, int sex);

    void validateFacebookVerificode(OnLoginFinishedListener listener, String idUser, String phone);

    void validateVerificodeAsync(OnLoginFinishedListener listener, String verificode);

    void validateSearchPhone(OnLoginFinishedListener listener, String phone);

    void validateForgotOtp(OnLoginFinishedListener listener, String Otp);

    void validateResetPass(OnLoginFinishedListener listener, String idUser, String password);
}
