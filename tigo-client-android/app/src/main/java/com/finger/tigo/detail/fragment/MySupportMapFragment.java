package com.finger.tigo.detail.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.finger.tigo.R;
import com.google.android.gms.maps.SupportMapFragment;

/**
 * This class is enable free scroll google map in NestedScrollView
 */
public class MySupportMapFragment extends SupportMapFragment {

    private OnTouchListener mListener;
    private boolean mFocused;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        View layout = super.onCreateView(inflater, parent, savedInstanceState);

        TouchableWrapper frameLayout = new TouchableWrapper(getActivity());
        frameLayout.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        ((ViewGroup) layout).addView(frameLayout,
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        return layout;
    }

    public void setListener(OnTouchListener listener) {
        mListener = listener;
    }

    public void setFocused(boolean focused) {
        if(focused) {
            View mapView = getView();
            if(mapView != null) {
                mapView.setBackgroundResource(R.drawable.bg_border_main_color);
            }
        } else {
            View mapView = getView();
            if(mapView != null) {
                mapView.setBackgroundResource(android.R.color.transparent);
            }
        }
        mFocused = focused;
    }

    public interface OnTouchListener {
        void onTouch();
    }


    public class TouchableWrapper extends FrameLayout {

        public TouchableWrapper(Context context) {
            super(context);
        }

        @Override
        public boolean dispatchTouchEvent(MotionEvent event) {
            if(mFocused) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (mListener != null)
                            mListener.onTouch();
                        break;
                    case MotionEvent.ACTION_UP:
                        if (mListener != null)
                            mListener.onTouch();
                        break;
                }
            }

            return super.dispatchTouchEvent(event);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            if(event.getAction() == MotionEvent.ACTION_DOWN) {
                setFocused(true);
            }
            return super.onTouchEvent(event);
        }
    }

    public boolean isFocused() {
        return mFocused;
    }
}