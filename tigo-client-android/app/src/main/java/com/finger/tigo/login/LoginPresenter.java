package com.finger.tigo.login;

import com.finger.tigo.login.async.AsynchLoginInteractor;
import com.finger.tigo.login.async.OnLoginFinishedListener;
import com.finger.tigo.login.model.itemLogin;

import java.util.List;

/**
 * Created by Duong on 2/27/2017.
 */

public class LoginPresenter implements OnLoginFinishedListener {

    private ILoginView view;
    private AsynchLoginInteractor interactor;

    public LoginPresenter(ILoginView loginView){
        this.view = loginView;
        this.interactor = new AsynchLoginInteractor();
    }

    public void attemptLogin(String username, String password) {
        interactor.validateCredentailsAsync(this, username, password);
    }

    public void attemptLoginFacebook( String idFace, String nameFace, int sex ){
        interactor.validateCreadentailsAsyncFacebook(this,  idFace, nameFace, sex);
    }

    public void attemptFacebookVerificode(String idUser, String phone){

        interactor.validateFacebookVerificode(this, idUser, phone);
    }

    public void attemptVerificode(String Otp){

        interactor.validateVerificodeAsync(this, Otp);

    }
    // For got password
    public void attemptCheckPhone(String phone){
        interactor.validateSearchPhone(this, phone);
    }
    public void attemptForgotOtp(String Otp){
        interactor.validateForgotOtp(this, Otp);

    }

    public void attemptResetPass(String idUser, String pass){
        interactor.validateResetPass(this, idUser, pass);
    }


    @Override
    public void onError(String error, int key) {
        view.onError(error, key);
    }

    @Override
    public void onNetWorkSuccess(String success, List<itemLogin> list) {
        view.onSuccess(success, list);
    }


}
