package com.finger.tigo.detail;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.finger.tigo.BaseActivity;
import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.app.ApiExecuter;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.app.AppIntent;
import com.finger.tigo.detail.adapter.CommentAdapter;
import com.finger.tigo.items.FeedItem;
import com.finger.tigo.session.SessionManager;
import com.finger.tigo.util.MyLog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CommentActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {
    private ListView listView;
    private TextView count_comment;
    private EditText commentEdit;
    private ProgressBar progressBar_post;
    private ImageView submitButton;


    private List<FeedItem> feedItemslist;
    private CommentAdapter listAdapter;

    private FeedItem item;
    private SessionManager session;

    private String id_sc;
    private String newText;
    private String poss;
    private String idUser;
    Toolbar toolbar;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        toolbar=(Toolbar)findViewById(R.id.toolbarComment);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.comment));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setTitleTextColor(Color.WHITE);


        idUser = AppAccountManager.getAppAccountUserId(this);
//        emojiButton = (ImageView) findViewById(R.id.emoji_btn);
        listView = (ListView) findViewById(R.id.listcommnet);
        submitButton = (ImageView) findViewById(R.id.submit_btn);
        commentEdit = (EditText) findViewById(R.id.emojicon_edit_text);
        progressBar_post = (ProgressBar) findViewById(R.id.prosserbar_post_comment);

        feedItemslist = new ArrayList<>();
        listView.setVisibility(View.INVISIBLE);

        count_comment = (TextView) findViewById(R.id.countcomment);
        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        if (b == null) {
            finish();
        } else {
            String count = b.getString("countcomment");
            poss = b.getString("positem");
            id_sc = b.getString("mDetailsHomeActivity");
            //   Toast.makeText(this, "error load data", Toast.LENGTH_SHORT).show();
            String checkWatch = b.getString("clickNotification");
            if(checkWatch !=null){
                String typeNoti = b.getString(AppIntent.ACTION_TYPE_NOTI);
                String idNoti =  b.getString(AppIntent.ACTION_ID_NOTI);
                MyLog.d("watched: ", typeNoti+" idnoty: "+idNoti);
                if(checkWatch.equalsIgnoreCase("watched_2_3_6_5"))
                    postwatching(idUser,idNoti, AppConfig.URL_WATCHING_DETAIL,typeNoti);
                else{
                    postwatching(idUser,  idNoti, AppConfig.URL_WATCHING_TBLIKE, typeNoti);
                }
            }

            if(count != null && id_sc != null ) {
                count_comment.setText(count);
                downloadComment(getApplicationContext(),id_sc, true);
            } else if (poss == null && count == null && id_sc != null) {
                downloadComment(getApplicationContext(),id_sc, true);
            } else {
                listView.setVisibility(View.VISIBLE);
            }
        }
        submitButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                newText = commentEdit.getText().toString();
                //    text = txtcomment.getText().toString().trim();

                if (!newText.isEmpty()) {
                    String iduser = AppAccountManager.getAppAccountUserId(CommentActivity.this);
                    progressBar_post.setVisibility(View.VISIBLE);
                    submitButton.setVisibility(View.VISIBLE);
                    addComment(id_sc,iduser,newText);

                } else {
                    Toast.makeText(CommentActivity.this, R.string.dit_not_comment, Toast.LENGTH_LONG).show();
                    progressBar_post.setVisibility(View.GONE);

                }

            }
        });



    }


    //function add comment
    public void addComment(final String id_sc, final String id_user, final String contentcomment) {
        // Hide all views

        ApiExecuter.requestApiNoProgress(AppConfig.URL_ADDCOMMENT, new ApiExecuter.ApiResultListener() {
            @Override
            public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                if (!feedItemslist.isEmpty()) {
                    session = new SessionManager(CommentActivity.this.getApplicationContext());
                    long time= System.currentTimeMillis();
                    FeedItem items = new FeedItem();
                    items.setNamecomment(session.getLName_session());
                    items.setAvatarComment(session.getLpic_session());
                    items.setStatusComment(contentcomment);
                    items.setCountComment("0");
                    items.setTimecomment(time+"");
                    System.out.println(session.getLName_session());
                    System.out.println(session.getLpic_session());
                    listAdapter.feedItems.add(0, items);
                    listView.setAdapter(listAdapter);
                    downloadComment(getApplicationContext(),id_sc, false);
                } else {
                    downloadComment(getApplicationContext(),id_sc, false);
                }

                commentEdit.setText("");
                progressBar_post.setVisibility(View.GONE);
                submitButton.setVisibility(View.VISIBLE);

            }

            @Override
            public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {
                showToast(errMessage);
            }
        }, "id_sc", id_sc, "iduser", id_user, "containerComment", contentcomment);
    }

    private void downloadComment(Context a,final String id_sc, boolean showCenterProgress) {
        ApiExecuter.requestApi(this, AppConfig.URL_COMMENT, showCenterProgress, a.getString(R.string.loading), new ApiExecuter.ApiResultListener() {
            @Override
            public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                try {
                    if(response !=null) {
                        JSONObject jObj = new JSONObject(response);
                        JSONArray feedArray = jObj.getJSONArray("comments");

                        feedItemslist.clear();
                        for (int i = 0; i < feedArray.length(); i++) {
                            final JSONObject feedComment = (JSONObject) feedArray.get(i);
                            item = new FeedItem();
                            item.setIdComment(feedComment.getString("ID_USER"));
                            item.setNamecomment(feedComment.getString("name"));
                            item.setAvatarComment(feedComment.getString("profilePic"));
                            item.setStatusComment(feedComment.getString("ContainerComment"));
                            item.setTimecomment(feedComment.getString("Currendate"));
                            item.setCountComment(feedComment.getString("count_comment"));
                            item.setPhone(feedComment.getString("phone"));
                            item.setId_user(feedComment.getString("iduser"));
                            // get repaly comment
                            item.setCountcommentplay(feedComment.getString("CountComment"));
                            item.setIdRComment(feedComment.getString("ID_COMMENT"));

                            String idUserReplayComment = feedComment.isNull("iduserReplay") ? null : feedComment.getString("iduserReplay");
                            String nameReplay = feedComment.isNull("nameReplay") ? null : feedComment.getString("nameReplay");
                            String profilePic = feedComment.isNull("profilePicReplay") ? null : feedComment.getString("profilePicReplay");
                            String ContainerCommentRply = feedComment.isNull("ContainerCommentRply") ? null : feedComment.getString("ContainerCommentRply");
                            String currentdateRply = feedComment.isNull("currentdateRply") ? null : feedComment.getString("currentdateRply");

                            item.setIdUserReplayComment(idUserReplayComment);
                            item.setNameReplay(nameReplay);
                            item.setProfilePicReplay(profilePic);
                            item.setContainerCommentRply(ContainerCommentRply);
                            item.setCurrendateCommentRply(currentdateRply);


                            count_comment.setText(item.getCountComment());
                            MyLog.e("metroi",item.getIdComment());
                            feedItemslist.add(item);
                        }


                        Intent a = new Intent();
                        a.setAction(AppIntent.ACTION_RELOAD_COUNT_COMMENT);
                        a.putExtra("reloadcountcomment", true);
                        a.putExtra("countcomment", item == null ? "0" : item.getCountComment());
                        a.putExtra("postions", poss);
                        getApplication().sendBroadcast(a);

                        doawloadsuccess();

                        commentEdit.setText("");
                        submitButton.setVisibility(View.VISIBLE);
                    }else{
                        onFail(apiUrl, params, "response is null", new IllegalStateException("response is null"));
                    }
                } catch (Exception ex) {
                    // JSON parsing error
                    onFail(apiUrl, params, ex.getMessage(), ex);
                }
            }

            @Override
            public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {
                showToast(errMessage);
            }
        }, "id_sc", id_sc);
    }

    private void doawloadsuccess() {
        listView.setVisibility(View.VISIBLE);

        listAdapter = new CommentAdapter(CommentActivity.this, feedItemslist);
        listView.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();
    }

//    / post watched notification
    public void postwatching(final String idUser, final String id_noti, final String url, final String type_noti) {
        // Hide all views

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Intent intent = new Intent();
                intent.setAction(AppIntent.ACTION_PUSH_RECEIVED);
                intent.putExtra(AppIntent.ACTION_PUSH_WATCHED, "watched");
                intent.putExtra(AppIntent.ACTION_ID_NOTI, id_noti);
                intent.putExtra(AppIntent.ACTION_TYPE_NOTI, type_noti);
                sendBroadcast(intent);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyLog.d("error when postWatching", error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<>();
                params.put("id_user", idUser);
                params.put("id_noti", id_noti);

                return params;
            }

        };

        // Adding request to request queue

        AppController.getInstance().addToRequestQueue(strReq);
    }

    @Override
    public void onRefresh() {
        downloadComment(getApplicationContext(),id_sc, true);
    }
    BroadcastReceiver appendChatScreenMsgReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();
            if (b != null) {
                if (b.getBoolean("reload")) {
                    downloadComment(getApplicationContext(),id_sc, true);
                }
            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(appendChatScreenMsgReceiver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {//Write your logic here
            //      feedItems.clear();
            this.finish();
            return true;
        } else {
        }
        return false;
    }

    @Override
    protected void onResume() {
        downloadComment(getApplicationContext(),id_sc, false);
        registerReceiver(this.appendChatScreenMsgReceiver, new IntentFilter(AppIntent.ACTION_PUSH_RECEIVED));
        super.onResume();
    }


}
