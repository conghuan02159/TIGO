package com.finger.tigo.detail;

import com.finger.tigo.app.ApiExecuter;
import com.finger.tigo.common.model.EventItem;
import com.finger.tigo.detail.async.EventDetailAsyncInteractor;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by Duong on 4/5/2017.
 */

public class EventDetailPresenter {

    EventDetailView IView;
    EventDetailAsyncInteractor interactor;

    public EventDetailPresenter(EventDetailView view){
        IView = view;
        interactor = new EventDetailAsyncInteractor();
    }

    public void attemptGetEventDetail(ApiExecuter.ApiProgressListener apiProgressListener, String idUser, String idSc){
        interactor.InteractorGetEventDetail(apiProgressListener, new ApiExecuter.ApiResultListener() {
            @Override
            public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                try {
                    JSONObject jObj = new JSONObject(response);

                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        JSONObject event = jObj.getJSONObject("info");
                        String image = event.isNull("image") ? null : event.getString("image");
                        String description = event.isNull("description") ? null : event.getString("description");
                        String phone = event.isNull("phone") ? null : event.getString("phone");
                        String status = event.isNull("status") ? null : event.getString("status");
                        String checkinvite = event.isNull("checkinvite") ? null : event.getString("checkinvite");
                        String location = event.isNull("location") ? null : event.getString("location");
                        String postision = event.isNull("position") ? null : event.getString("position");


                        EventItem eventItem = new EventItem(event.getString("iduser"), status, event.getString("color"),postision
                                ,location , event.getDouble("lg1"),
                                event.getDouble("lg2"), event.getString("daystart"), event.getString("dayfinish"),
                                event.getString("allday").equals("1") ? true : false, image, phone,description,
                                event.getString("state"), event.getString("countJoin"), event.getString("username"),
                                event.getString("countComment"), checkinvite);

                        IView.onSuccess(EventDetailActivity.INFO_EVENT, eventItem);

                    } else {
                        IView.onError(EventDetailActivity.INFO_EVENT, "Can not load event detail." , null);
                    }

                } catch (JSONException e) {

                    e.printStackTrace();

                    onFail(apiUrl, params, e.getMessage(), e.getCause());
                }
            }

            @Override
            public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {

                IView.onError(EventDetailActivity.INFO_EVENT, errMessage, cause);
            }
        }, idUser, idSc);
    }

    public void attemptJoinOrNotJoin(ApiExecuter.ApiProgressListener apiProgressListener, String idUser, String idSc, final String state, String progressMsg){
        interactor.interactorJoinOrNotJoin(apiProgressListener, progressMsg, new ApiExecuter.ApiResultListener() {
            @Override
            public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                IView.onSuccess(EventDetailActivity.POST_JOIN_OR_NOT_JOIN, new EventItem(state));
            }

            @Override
            public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {
                IView.onError(EventDetailActivity.POST_JOIN_OR_NOT_JOIN, errMessage, cause);
            }
        }, idUser, idSc, state);
    }

    // accept or rufust
    public void attemptaccepivite(ApiExecuter.ApiProgressListener apiProgressListener, String idUser , String id_sc, final String state){
        interactor.IterractorInvite(apiProgressListener, new ApiExecuter.ApiResultListener() {
            @Override
            public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                String status_likes;
                if (state.equalsIgnoreCase(EventDetailActivity.CANCEL)) {
                    status_likes = "0";

                } else {
                    status_likes = "1";
                }

                IView.onSuccess(EventDetailActivity.POST_ACCEPT, new EventItem(status_likes));
            }

            @Override
            public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {
                IView.onError(EventDetailActivity.POST_ACCEPT, errMessage, cause);
            }
        }, idUser, id_sc, state);
    }
}
