package com.finger.tigo.addfriend.presenter;

import com.finger.tigo.addfriend.async.AddfriendInteractor;
import com.finger.tigo.addfriend.async.OnNewFrFinishedListener;
import com.finger.tigo.addfriend.model.ItemAddfriend;

import java.util.List;

/**
 * Created by Duong on 3/30/2017.
 */

public class AddNewFriendPresenter implements OnNewFrFinishedListener{

    IAddNewFriendView iRegisterView;
    AddfriendInteractor addInter;

    public AddNewFriendPresenter(IAddNewFriendView iRegisterView ){
        this.iRegisterView = iRegisterView;
        addInter = new AddfriendInteractor();

    }

    // check phone and intent detail friend
    public void attemptAddNewFriend(String phone){
        addInter.validatePhone(this, phone);

    }



    // Check friend
    public void attemptCheckFriend(String idUser, String idFriend){
        addInter.validateCheckFriend(this, idUser, idFriend);
    }
    public void attemptAcceptFriend(final String iduser, final String idfriend, final String name, final String phone,
                                    final String state){
        addInter.validateAcceptFriend(this, iduser, idfriend, name, phone, state);
    }

    public void attemptAddNewFriend(final String idUser, final String idFriend, final String name, final String phone){
        addInter.validateAddFriend(this, idUser, idFriend, name, phone);
    }
    // get data for user friend from qrcode

    public void attempDataTigon(final  String idFriend){
        addInter.validateDataTigon(this, idFriend);
    }

    @Override
    public void listenerSuccess(String success, List<ItemAddfriend> listRegister) {
        iRegisterView.onSuccess(success, listRegister);
    }


    @Override
    public void listenerError(String error, int keyError) {
        iRegisterView.onError(error, keyError);
    }
}
