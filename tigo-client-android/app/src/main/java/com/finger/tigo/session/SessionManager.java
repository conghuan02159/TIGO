package com.finger.tigo.session;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SessionManager {

    private static final String PREFERENCES = "preferences";
    private  static String TAG = SessionManager.class.getSimpleName();
    private SharedPreferences mSharedPreferences;

    private Editor mEditor;
    private Context mContext;

    private static String KEY_SOUND_EVENT = "soundEvent";
    private static String KEY_SHOW_FRIEND = "showFriend";
    private static String KEY_SHOW_DETAIL_HOME= "ShowDetailHome";
    private static String KEY_SHOW_PARTICI= "particion";
    private static String KEY_SHOW_ALL= "notifiall";
    private static String KEY_SHOW_INVITE= "invite";
    private static String KEY_SHOW_COMMENT= "comment";
    private static String KEY_IS_LOGGED_IN = "isLoggedIn";
    private static String KEY_ID_USERFB = "idface";
    private static String KEY_ID_USER = "iduser";
    private static String KEY_EMAIL = "email";
    private static String KEY_NAME = "name";
    private static String KEY_PROFILEPIC = "profilePic";
    private static String KEY_PHONE = "phone";
    private static String KEY_BIRTHDAY = "birthday";
    private static String KEY_SEX = "sex";
    private static String KEY_IS_SHOW_HOME = "showhome";
    private static String KEY_CODE_COUNTRY = "codecountr";
    private static String KEY_IS_ALARM = "alarm";
    private static String KEY_CODE_QRcode= "qrcodes";
    private static String KEY_SYNC_CALENDAR_ID = "SyncCalendarId";
    private static String KEY_SYNC_DATE = "syncDate";
    private static String KEY_IS_LOGIN_FACE = "keyLoginFace";
    private static String KEY_IS_Particion = "keyPaticion";
    private static String KEY_IS_ALL = "keyall";
    private static String KEY_IS_INVITE = "keyinvite";
    private static String KEY_IS_COMMENT = "keycomment";
    private static String KEY_BADGE_ICON_TAB= "KeyBadge";
    private static String KEY_COUNT_NOTIFICATION_FRIEND = "keyCountFriendNotification";
    private static String KEY_COUNT_NOTIFICATION_SCREEN = "keyCountScreen";


//
    public  SessionManager(Context context){
        this.mContext = context;
        this.mSharedPreferences = mContext.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        this.mEditor = mSharedPreferences.edit();

    }
    // save value friend notification __($_^)|__|()|/|@
    public void setCountFriendNotification(int count) {
        mEditor.putInt(KEY_COUNT_NOTIFICATION_FRIEND, count);
        mEditor.commit();
    }


    public int getCountFriendNotification(){
        return mSharedPreferences.getInt(KEY_COUNT_NOTIFICATION_FRIEND, 0);
    }
    // save value notification screen __($_^)__
    public void setCountNotificationScreen(int count) {
        mEditor.putInt(KEY_COUNT_NOTIFICATION_SCREEN, count);
        mEditor.commit();
    }


    public int getCountNotificationScreen(){
        return mSharedPreferences.getInt(KEY_COUNT_NOTIFICATION_SCREEN, 0);
    }

    // save value notification __($_^)__
    public void setCountNotification(int count) {
        mEditor.putInt(KEY_BADGE_ICON_TAB, count);
        mEditor.commit();
    }


    public int getCountNotification(){
        return mSharedPreferences.getInt(KEY_BADGE_ICON_TAB, 0);
    }

    // save value sync contact date

    public void setSyncDate(long millisDate) {
        mEditor.putLong(KEY_SYNC_DATE, millisDate);
        mEditor.commit();
    }


    public long getSyncDate(){
        return mSharedPreferences.getLong(KEY_SYNC_DATE, 0);
    }
    // IS LOGIN FACE
    public void setIsLogerFb(boolean isLogerFb){
        mEditor.putBoolean(KEY_IS_LOGIN_FACE, isLogerFb);
        mEditor.commit();
    }
    public boolean getIsLogerFb(){
        return mSharedPreferences.getBoolean(KEY_IS_LOGIN_FACE, false);
    }

    public void setIsAlarm(boolean alarm) {
        mEditor.putBoolean(KEY_IS_ALARM, alarm);
        mEditor.commit();
    }


    public boolean getIsAlarm(){
        return mSharedPreferences.getBoolean(KEY_IS_ALARM, true);
    }

    public void setSyncCalendarId(int calendarId){
        mEditor.putInt(KEY_SYNC_CALENDAR_ID, calendarId);
        mEditor.commit();
    }



    public int getSyncCalendarId(){
        return mSharedPreferences.getInt(KEY_SYNC_CALENDAR_ID, 0);
    }

    public void setDetailHome (boolean name){
        mEditor.putBoolean(KEY_SHOW_DETAIL_HOME, name);
        mEditor.commit();
    }
    public boolean getDetailHome(){
        return mSharedPreferences.getBoolean(KEY_SHOW_DETAIL_HOME, false);
    }
    public void setShowFriend (boolean name){
        mEditor.putBoolean(KEY_SHOW_FRIEND, name);
        mEditor.commit();
    }
    public boolean getShowFriend(){
        return mSharedPreferences.getBoolean(KEY_SHOW_FRIEND, false);
    }

    public void setShowHome (boolean is){
        mEditor.putBoolean(KEY_IS_SHOW_HOME, is);
        mEditor.commit();
    }
    public boolean getShowHome(){
        return mSharedPreferences.getBoolean(KEY_IS_SHOW_HOME, false);
    }

    public void setSound( String sound){
        mEditor.putString(KEY_SOUND_EVENT, sound);
        mEditor.commit();
    }
    public String getKeySoundEvent(){
        return mSharedPreferences.getString(KEY_SOUND_EVENT, null);
    }

    public  void setLogin(boolean isLoggedIn){
        mEditor.putBoolean(KEY_IS_LOGGED_IN, isLoggedIn);
        mEditor.commit();
    }
    public boolean getLoggedIn() {
        return mSharedPreferences.getBoolean(KEY_IS_LOGGED_IN, false);
    }
    //    public int getLogin(){
//        return pref.getInt(KEY_ID_USER, -1);
//    }
    public void set_id_user_session (int id_user){
        mEditor.putInt(KEY_ID_USER, id_user);
        mEditor.commit();
    }
    public int get_id_user_session(){
        return mSharedPreferences.getInt(KEY_ID_USER, -1);
    }

    public void setName_session (String name){
        mEditor.putString(KEY_NAME, name);
        mEditor.commit();
    }
    public String getLName_session(){
        return mSharedPreferences.getString(KEY_NAME, null);
    }
    public void setmail_session (String mail){
        mEditor.putString(KEY_EMAIL, mail);
        mEditor.commit();
    }
    public String getmail_session(){
        return mSharedPreferences.getString(KEY_EMAIL, null);
    }
    public void setpic_session (String pic){
        mEditor.putString(KEY_PROFILEPIC, pic);
        mEditor.commit();
    }
    public String getLpic_session(){
        return mSharedPreferences.getString(KEY_PROFILEPIC, null);
    }

    public void setbirhtday_session (String pic){
        mEditor.putString(KEY_BIRTHDAY, pic);
        mEditor.commit();
    }
    public String getbirhtday_session(){
        return mSharedPreferences.getString(KEY_BIRTHDAY, null);
    }

    public void setphone_session (String pic){
        mEditor.putString(KEY_PHONE, pic);
        mEditor.commit();
    }
    public String getphone_session(){
        return mSharedPreferences.getString(KEY_PHONE, null);
    }

    public void setsex_session (String pic){
        mEditor.putString(KEY_SEX, pic);
        mEditor.commit();
    }
    public String getsex_session(){
        return mSharedPreferences.getString(KEY_SEX, null);
    }

    public void setqrcodesesion (String qrcode){
        mEditor.putString(KEY_CODE_QRcode, qrcode);
        mEditor.commit();
    }
    public String getqrcodesesion(){
        return mSharedPreferences.getString(KEY_CODE_QRcode, null);
    }

    public void setKeyCodeCountry( String codeCountry){
        mEditor.putString(KEY_CODE_COUNTRY, codeCountry);
        mEditor.commit();
    }
    public String getKeyCodeCountry(){
        return mSharedPreferences.getString(KEY_CODE_COUNTRY, null);
    }


    public void clearqr() {
        // Clearing all data from Shared Preferences
        mEditor.clear();
        mEditor.commit();
    }

    public void setPref_setting_parti(boolean settingParti){
        mEditor.putBoolean(KEY_IS_Particion, settingParti);
        mEditor.commit();
    }
    public boolean getPref_setting_parti(){
        return mSharedPreferences.getBoolean(KEY_IS_Particion, true);
    }

    public void setPref_setting_all(boolean settingall){
        mEditor.putBoolean(KEY_IS_ALL, settingall);
        mEditor.commit();
    }
    public boolean getPref_setting_all(){
        return mSharedPreferences.getBoolean(KEY_IS_ALL, false);
    }

    public void setPref_setting_invite(boolean settinginvite){
        mEditor.putBoolean(KEY_IS_INVITE, settinginvite);
        mEditor.commit();
    }
    public boolean getPref_setting_invite(){
        return mSharedPreferences.getBoolean(KEY_IS_INVITE, true);
    }

    public void setPref_setting_comment(boolean settingcomment){
        mEditor.putBoolean(KEY_IS_COMMENT, settingcomment);
        mEditor.commit();
    }
    public boolean getPref_setting_comment(){
        return mSharedPreferences.getBoolean(KEY_IS_COMMENT, true);
    }
}
