package com.finger.tigo.common.contact;

/**
 * Created by Finger-kjh on 2017-05-24.
 */

public class Contact {

    public String name;
    public String phone;

    public Contact(String name, String phone){
        this.name = name;
        this.phone = phone;
    }
}
