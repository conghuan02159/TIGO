package com.finger.tigo.detail;

/**
 * Created by Duong on 4/10/2017.
 */

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.finger.tigo.R;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.session.SessionManager;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by DELL on 4/10/2017.
 */

public class UpdateCommentRplyActivity extends AppCompatActivity {
    private CircleImageView image_comment_rly;
    private EditText edtcommentRply;
    private Button btnCancel,btnUpdate;
    private SessionManager session;
    private ProgressDialog pDialog;
    private String idUser;
    private String id_r_comment_update;
    public static final String TAG = "DetailsPersons";
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_commentply_activity);
        toolbar= (Toolbar) findViewById(R.id.toolbarUpdate);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setTitleTextColor(Color.WHITE);
        image_comment_rly=(CircleImageView)findViewById(R.id.image_comment_update);
        edtcommentRply=(EditText)findViewById(R.id.edt_comment_update);
        btnCancel=(Button)findViewById(R.id.btnHuy);
        btnUpdate=(Button)findViewById(R.id.btnOk);
        session = new SessionManager(getApplicationContext());

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        idUser= session.get_id_user_session()+"";
        Intent intent=getIntent();
        id_r_comment_update =intent.getStringExtra("id_r_comment");
        String containercommentRply=intent.getStringExtra("ContainerCommentRply");
        edtcommentRply.setText(containercommentRply);


        Glide.with(UpdateCommentRplyActivity.this).load(session.getLpic_session())
                .placeholder(R.drawable.avatar).error(R.drawable.avatar)
                .into(image_comment_rly);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customActionBar();
            }
        });

    }
    public void customActionBar(){

        String id_r_commentrply = id_r_comment_update.trim();
        String comment_status = edtcommentRply.getText().toString().trim();

        //Log.e("hhh",id_r_commentrply+"/"+comment_status);
        if (!id_r_commentrply.isEmpty() && !comment_status.isEmpty()) {

            UpdateComment(id_r_commentrply,comment_status);
            edtcommentRply.setText("");

        } else {
            Toast.makeText(getApplicationContext(), R.string.error_enter_detail, Toast.LENGTH_LONG).show();
        }
    }

    private void UpdateComment(final String comment_r_id, final String comment) {

        pDialog.setMessage(getResources().getString(R.string.upload));
        showDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPDATE_COMMENT_RPLAY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Log.d(TAG, "Register Response: " + response.toString());
                hideDialog();

                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("kkkk", "Updatestration Error: " + error.getMessage());

                hideDialog();
                finish();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                //String profilePic = getStringImage(bitmap);

                Map<String, String> params = new HashMap<String, String>();
                params.put("ID_R_comment", comment_r_id);
                params.put("ContainerCommentRply", comment);

                //Log.e("kkkkk",comment_r_id+"-"+comment);

                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);

    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {//NavUtils.navigateUpFromSameTask(this);
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
