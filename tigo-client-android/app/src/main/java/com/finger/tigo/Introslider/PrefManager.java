package com.finger.tigo.Introslider;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by DUYEN on 7/12/2016.
 */

public class PrefManager {
    SharedPreferences pref, prefFace;
    SharedPreferences.Editor editor, editorFace;
    Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;
    int  PRIVATE_MODE_FACE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "androidhive-welcome";
    private static final String PREF_NAME_FACE = "androidhive-welcome";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    private static final String IS_ADD_FRIEDNS_LAUNCH ="IsAddFriendsLaunch";

    private static final String IS_ADD_FRIEDNS_LAUNCH_FACE ="IsAddFriendsFace";


    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        prefFace = _context.getSharedPreferences(PREF_NAME_FACE, PRIVATE_MODE_FACE);
        editor = pref.edit();
        editorFace = prefFace.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setIsAddFriednsLaunch(boolean isFirstTime){
        editor.putBoolean(IS_ADD_FRIEDNS_LAUNCH, isFirstTime);
        editor.commit();
    }
    public boolean isAddFriendLaunch(){
        return pref.getBoolean(IS_ADD_FRIEDNS_LAUNCH, true);
    }

    public void setIsAddFriednsFace(boolean isFirstTime){
        editorFace.putBoolean(IS_ADD_FRIEDNS_LAUNCH_FACE, isFirstTime);
        editorFace.commit();
    }
    public boolean isAddFriendFace(){
        return prefFace.getBoolean(IS_ADD_FRIEDNS_LAUNCH_FACE, true);
    }
}
