package com.finger.tigo.app;


public class AppConfig {

    public static final String URL_COUNTRY_CODE = "https://restcountries.eu/rest/v1/all";

    public static final String HTTP = "http://tigotech.tk/schedule/";
//    public static final String HTTP = "http://ec2-54-255-164-197.ap-southeast-1.compute.amazonaws.com/";

    public static final String URL_CONFIRM_PHONE = HTTP + "confirmPhone.php";
    public static final String URL_CHECK_FRIEND = HTTP + "check_friend.php";
    public static final String URL_CHECK_NEW_FRIEND = HTTP + "checkNewFriend.php";
    public static final String URL_COMMENT = HTTP + "comment.php";
    public static final String URL_TOP_COMMENT = HTTP + "top_comment.php";
    public static final String URL_ADDCOMMENT = HTTP + "addComment.php";
    public static final String URL_DELETECOMMENT = HTTP + "delete_comment.php";
    public static final String URL_UPDATE_COMMENT = HTTP + "update_comment.php";
    public static final String URL_LOGIN = HTTP + "login.php";
    // Server user register url
    public static final String URL_REGISTER = HTTP + "register.php";
    public static final String URL_ADDFRIEND = HTTP +"syncContact.php";
    public static final String URL_ADDNEWFRIEND = HTTP + "addnewfriend.php";
    public static final String URL_LIST_FRIEND_INVITED = HTTP +"listinvitefriend.php";
    public static final String URL_UPDATE_WATCHED_FRIEND = HTTP + "UpdateWatchedFriend.php";
    public static final String URL_NOTIFICATION_FRIEND =HTTP + "notificationFriend.php";

    public static final String URL_LIST_NEW_FRIEND = HTTP + "listNewFriend.php";
    public static final String URL_NOTIFICATION_LIST_OLD_FRIEND = HTTP + "NotificationListOldFriend.php";
    public static final String URL_INSERT_FRIEND = HTTP + "insertfr.php";
    public static final String URL_CREATE_EVENT = HTTP +"schedule.php";
    public static final String URL_GET_EVENT_DETAIL = HTTP + "DetailHome.php";
    public static final String URL_UPDATE_IMAGE = HTTP + "update_profile_image.php";
    public static final String URL_GET_EVENTS_HOME = HTTP + "home_more.php";
    public static final String URL_INVITE_FRIEND = HTTP + "invite.php";
    public static final String URL_AGENDA = HTTP + "agendar.php";
    public static final String URL_EVENT_USER = HTTP + "eventUser.php";
    public static final String URL_UPDATE_EVENT = HTTP+"update_schedule.php";
    public static final String URL_DELETE_EVENT = HTTP + "delete_schedule.php";
    public static final String URL_JOIN_OR_NOT_JOIN = HTTP + "addlike.php";
    public static final String URL_WATCHING_TBLIKE = HTTP + "Watchingtb_like.php";
    public static final String URL_WATCHING_DETAIL = HTTP + "Watching_Detail_noti.php";

// tinh xoa

    public static final String URL_COUNT_EVENT_FRIEND = HTTP +"count_event_friend.php";
    public static final String URL_UPDATE_USER = HTTP +"update_user.php";
    public static final String URL_UPDATE_PASS = HTTP +"updateuserpassword.php";

    public static final String URL_LOGOUT_TOKEN = HTTP + "Logout_token.php";
    public static final String URL_UPDATE_TOKEN = HTTP + "update_token.php";
    public static final String URL_ACCEPTED = HTTP + "list_invite_accepted.php";
    public static final String URL_SEARCH_EVENT = HTTP + "list_search_event.php";
    public static final String URL_VERIFY_OTP = HTTP + "verify_otp.php";
    public static final String URL_PHONE_FACEBOOK = HTTP +"update_phone_facebook.php";
    public static final String URL_VERIFY_OTP_FORGOT_PASS = HTTP + "verify_otp_forget_pass.php";
    public static final String URL_RESET_PASS = HTTP +"resetpass.php";
    public static final String URL_CHECK_PASSWORD = HTTP + "checkoldpassworld.php";
    public static final String URL_RESEND_VERIFICODE = HTTP + "resendVerificode.php";
    public static final String URL_LOGIN_FACEBOOK = HTTP + "loginfacebook.php";
    public static final String URL_COUNT_VIEW = HTTP + "count_view.php";
    public static final String URL_HIS_NOTI = HTTP + "history_notification.php";
    public static final String URL_GET_USER_QR = HTTP + "get_user_qr.php";

    // replaycomment
    public static final String URL_ADDCOMMENTRPLY = HTTP + "addCommentRply.php";
    public static  final String URL_COMMENT_RPLY = HTTP +"commentrply.php";
    public static final String URL_DELETECOMMENT_RPLY = HTTP + "deletecommentRply.php";
    public static final String URL_UPDATE_COMMENT_RPLAY = HTTP +"updatecommentRply.php";
    public static final String URL_COUNT_COMMENT_RPLY = HTTP + "countcommentRply.php";

    // new
    public static final String URL_ACCEPT_INVITE = HTTP + "Accept_invite.php";
    public static final String URL_SYNC_NEW = HTTP + "listSyncNew.php";
    public static final String URL_EVENT_USER_PROGRESSING = HTTP + "event_user_progressing.php";
    public static final String URL_EVENT_USER_LAST = HTTP + "event_user_last.php";
    public static final String URL_COUNT_HISTORY_NOTIFICATION = HTTP + "histori_count_notification.php";
    public static final String URL_COUNT_NOTIFICATION_FRIEND = HTTP + "count_notification_friend.php";
    public static final String URL_CREATE_COUNT_NOTIFICATION = HTTP + "count_notification.php";

    public static final String URl_EVENT_FRIEND_PROGRESSING = HTTP + "event_friend_progressing.php";
    public static final String URl_EVENT_FRIEND_LAST = HTTP + "event_friend_last.php";
    public static final String URL_EVENT_FRIEND = HTTP +"event_friend_all.php";
    // data


}