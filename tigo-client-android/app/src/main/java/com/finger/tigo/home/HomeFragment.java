package com.finger.tigo.home;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.finger.tigo.BaseActivity;
import com.finger.tigo.BaseFragment;
import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.app.ApiExecuter;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppIntent;
import com.finger.tigo.home.adapter.RecyclerAdapterHome;
import com.finger.tigo.items.FeedItem;
import com.finger.tigo.schedule.creatEvent.CreateEventActivity;
import com.finger.tigo.session.SessionManager;
import com.finger.tigo.util.MyLog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import static com.finger.tigo.detail.EventDetailActivity.POST_ITEM_HOME;
import static com.finger.tigo.detail.EventDetailActivity.RES_LOAD_DETAILS_HOME;
import static com.finger.tigo.detail.EventDetailActivity.STATE_JOIN;

/**
 * Created by Duong on 4/3/2017.
 */

public class HomeFragment extends BaseFragment implements RecyclerAdapterHome.itemParticipationClick, SwipeRefreshLayout.OnRefreshListener {


    RecyclerView recyclerView;

    SwipeRefreshLayout swipeRefreshLayout;

    LinearLayout lnNotEvent;
    LinearLayout lnNotConnect;
    Button bt_create_home;
    Button reptryConnect;


    RecyclerAdapterHome mAdapter;

    private List<FeedItem> feedItems;
    private int pageToDownload;

    SessionManager session;
    String idUser;

    Handler handler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.fragment_home, container, false);
        recyclerView = (RecyclerView)v.findViewById(R.id.recycler_home);
        swipeRefreshLayout = (SwipeRefreshLayout)v.findViewById(R.id.idswwipRefesh);
        lnNotEvent = (LinearLayout)v.findViewById(R.id.activity_not_event);
        lnNotConnect = (LinearLayout) v.findViewById(R.id.layout_not_connect);
        reptryConnect = (Button) v.findViewById(R.id.bt_reptry_connect);
        bt_create_home = (Button)v.findViewById(R.id.bt_create_home);
        bt_create_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), CreateEventActivity.class);
                startActivity(i);
            }
        });

        getActivity().registerReceiver(this.appendChatScreenMsgReceiver, new IntentFilter(AppIntent.ACTION_RELOAD_COUNT_COMMENT));

        idUser = AppAccountManager.getAppAccountUserId(getActivity());


        handler = new Handler();

        session = new SessionManager(getActivity().getApplicationContext());
        swipeRefreshLayout.setColorSchemeColors(Color.BLUE, Color.GREEN, Color.YELLOW, Color.RED);
        swipeRefreshLayout.setDistanceToTriggerSync(100);
        swipeRefreshLayout.setSize(SwipeRefreshLayout.DEFAULT);
        swipeRefreshLayout.setOnRefreshListener(this);
        initRecyclerView();
        // progressBar.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setRefreshing(true);
        pageToDownload = 1;
        getEventFeedList(idUser, false);
        return v;
    }

    private void initRecyclerView() {
        feedItems = new ArrayList<>();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        mAdapter = new RecyclerAdapterHome(getActivity(), feedItems, recyclerView, this);
        recyclerView.setAdapter(mAdapter);


        mAdapter.setOnLoadMoreListener(new RecyclerAdapterHome.OnLoadMoreListener() {
            @Override
            public void onLoadmore() {
                feedItems.add(null);
                mAdapter.notifyItemInserted(feedItems.size() - 1);
                if (feedItems.size() >= 10)
                    ++pageToDownload;
                getEventFeedList(idUser, false);
            }

        });
        reptryConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swipeRefreshLayout.setRefreshing(true);
                pageToDownload =1;
                getEventFeedList(idUser,false );
            }
        });

    }

    public void getEventFeedList(final String strIdUser, boolean showCenterProgress) {
        ApiExecuter.requestApi((BaseActivity) getActivity(), AppConfig.URL_GET_EVENTS_HOME, showCenterProgress,
                null, new ApiExecuter.ApiResultListener() {
                    @Override
                    public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                        MyLog.d("success getEventFeedList at HomeFragment!! response=" + response);

                        try {
                            JSONObject jObj = new JSONObject(response);
                            JSONArray feedArray = jObj.getJSONArray("feed");
                            Log.e("checkArray",feedArray+"");
                            if (swipeRefreshLayout.isRefreshing()) {
                                swipeRefreshLayout.setRefreshing(false);
                            }
                            if (feedArray != null) {
                                if (pageToDownload > 1) {
                                    feedItems.remove(feedItems.size() - 1);
                                    mAdapter.notifyItemRemoved(feedItems.size());
                                } else {
                                    feedItems.clear();
                                    mAdapter.notifyDataSetChanged();
                                }

                                for (int i = 0; i < feedArray.length(); i++) {

                                    JSONObject feedObj = (JSONObject) feedArray.get(i);
                                    FeedItem item = new FeedItem();
                                    item.setName(feedObj.getString("name"));
                                    String image = feedObj.isNull("image") ? null : feedObj
                                            .getString("image");
                                    item.setImge(image);
                                    item.setdaystart(feedObj.getString("daystart"));
                                    item.setDayend(feedObj.getString("dayfinish"));
                                    item.setProfilePic(feedObj.getString("profilePic"));
                                    item.setTimeStamp(feedObj.getString("dayupdate"));
                                    item.setMode(feedObj.getString("mode"));
                                    item.setStatus(feedObj.getString("status"));
                                    item.setAddress(feedObj.getString("location"));
                                    item.setStatus_pass(feedObj.getString("trangthai"));
                                    item.setCount_participation(feedObj.getString("numpar"));
                                    item.setCount_view(feedObj.getString("number_view"));
                                    item.setId_sc(feedObj.getString("id_sc"));
                                    item.setCountComment(feedObj.getString("numcomment"));
                                    item.setColor(feedObj.getString("color"));
                                    item.setAllDay(feedObj.getString("allday"));
                                    item.setId_user(feedObj.getString("iduser"));
                                    item.setPhone(feedObj.getString("phone"));
                                    feedItems.add(item);

                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            mAdapter.notifyItemInserted(feedItems.size());

                                        }
                                    });
                                }


                                if(feedArray.length()>=10)
                                    mAdapter.setLoaded();


                                if (feedItems.size() == 0) {
                                    lnNotEvent.setVisibility(View.VISIBLE);
                                    lnNotConnect.setVisibility(View.GONE);

                                } else {
                                    lnNotEvent.setVisibility(View.GONE);
                                    lnNotConnect.setVisibility(View.GONE);

                                }

                            }

                        } catch (Exception ex) {
                            // JSON parsing error

                            ex.printStackTrace();
                        }
                    }

                    @Override
                    public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {
//                        showToast(errMessage);
                        lnNotConnect.setVisibility(View.VISIBLE);
                        if (swipeRefreshLayout.isRefreshing()) {
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    }
                }, "idUser", strIdUser, "page", pageToDownload + "");
        Log.d("aaaaaaa",pageToDownload+"");
    }

    @Override
    public void onItemClick(int position, int i, RecyclerView.ViewHolder v) {
        if (i == 1) {
            FeedItem item = feedItems.get(position);
            joinOrNotJoin(item.getId_sc(), "0", position);
            ((RecyclerAdapterHome.MyViewHolder) v).btJoin.setVisibility(View.VISIBLE);
            ((RecyclerAdapterHome.MyViewHolder) v).btJoin.setBackgroundResource(R.drawable.ic_tigo_home);
            ((RecyclerAdapterHome.MyViewHolder) v).txtJoin.setTextColor(getResources().getColor(R.color.dot_light_screen1));

        } else {
            FeedItem item = feedItems.get(position);
            joinOrNotJoin(item.getId_sc(), "1", position);
            ((RecyclerAdapterHome.MyViewHolder) v).btJoin.setVisibility(View.VISIBLE);
            ((RecyclerAdapterHome.MyViewHolder) v).btJoin.setBackgroundResource(R.drawable.frame_tigo_animation);
            ((RecyclerAdapterHome.MyViewHolder) v).txtJoin.setTextColor(getResources().getColor(R.color.colorPrimary));
            ((RecyclerAdapterHome.MyViewHolder)v).animation = (AnimationDrawable) ((RecyclerAdapterHome.MyViewHolder)v).btJoin.getBackground();
            ((RecyclerAdapterHome.MyViewHolder)v).animation.start();


        }
    }

    public void createEventWithName(Context ctx, String entryid, String name, String color, int allDay, String location, String startMillis, String endMillis) {

        ContentValues cv = new ContentValues();

        cv.put(CalendarContract.Events.TITLE, name);
        cv.put(Events._ID, entryid);
        cv.put(Events.DTSTART, Long.parseLong(startMillis));
        cv.put(Events.DTEND, Long.parseLong(endMillis));
        cv.put(Events.CALENDAR_ID, session.getSyncCalendarId());
        cv.put(Events.EVENT_COLOR, Color.parseColor(color));
        cv.put(Events.ALL_DAY, allDay);
        cv.put(Events.EVENT_LOCATION, location);
        cv.put(Events.EVENT_TIMEZONE, TimeZone.getDefault().toString());
        //cv.put(Events.RRULE, "FREQ=DAILY;INTERVAL=2");


        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        ctx.getContentResolver().insert(Events.CONTENT_URI, cv);

    }

    private void deleteCalendarEntry(int entryID) {
        Uri eventUri = ContentUris.withAppendedId(Events.CONTENT_URI, entryID);
        getActivity().getContentResolver().delete(eventUri, null, null);
    }

    public void joinOrNotJoin(final String id_sc, final String status_like, final int positon){
        ApiExecuter.requestApiNoProgress(AppConfig.URL_JOIN_OR_NOT_JOIN,
                new ApiExecuter.ApiResultListener() {
                    @Override
                    public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                        FeedItem item = feedItems.get(positon);
                        String[] temp = item.getId_sc().split("-");

                        if(status_like.equalsIgnoreCase("0")){
                            deleteCalendarEntry(Integer.parseInt(temp[0]));
                        }else {
                            int checkAllDay = 0;
                            if(item.allDay.toString().equalsIgnoreCase("true"))
                                checkAllDay = 1;
                            Log.d("aaaaaaaaa", "onResponse: "+item.color+" allday: "+checkAllDay+" sesion: "+session.getSyncCalendarId());
                            createEventWithName(getActivity(), temp[0], item.name, item.color, checkAllDay, item.address, item.daystart, item.dayend);
                        }

                    }

                    @Override
                    public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {
                        showToast(errMessage);
                    }
                }
                , "id_sc", id_sc, "id_user_like", idUser, "status_like", status_like);
    }

    @Override
    public void onRefresh() {
        //feedItems.clear();
        pageToDownload =1;
        getEventFeedList(idUser, false);
    }

    @Override
    public void onStop() {
        mAdapter.notifyDataSetChanged();
        super.onStop();

    }

    @Override
    public void onResume(){
        mAdapter.notifyDataSetChanged();
        getActivity().registerReceiver(this.appendChatScreenMsgReceiver, new IntentFilter(AppIntent.ACTION_RELOAD_COUNT_COMMENT));
        super.onResume();
    }

    BroadcastReceiver appendChatScreenMsgReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();
            if (b != null) {
                if (b.getBoolean("reloadcountcomment")) {
                    String count = b.getString("countcomment");
                    String pos = b.getString(POST_ITEM_HOME);
                    if (pos!=null && count !=null) {
                        FeedItem item = feedItems.get(Integer.parseInt(pos));
                        item.setCountComment(count);
                        mAdapter.notifyItemChanged(Integer.parseInt(pos));

                    }
                }else if(b.getBoolean(RES_LOAD_DETAILS_HOME)){
                    String state = b.getString(STATE_JOIN);

                    String postItem = b.getString(POST_ITEM_HOME);
                    if (postItem!=null  ) {
                        FeedItem item = feedItems.get(Integer.parseInt(postItem));
                        item.setStatus_pass(state);
                        if(state.equalsIgnoreCase("1")){
                            int co = Integer.parseInt(item.getCount_participation()) + 1;
                            item.setCount_participation("" + co);

                        }else{

                            int co = Integer.parseInt(item.getCount_participation());
                            if(co != 0){
                                co = co -1;
                            }
                            item.setCount_participation("" + co);
                        }
                        MyLog.d("dddddddstate: ", state + " count: "+item.getCount_participation());
                        mAdapter.notifyItemChanged(Integer.parseInt(postItem));
                    }
                }
            }
        }
    };

    @Override
    public void onDestroy(){
        getActivity().unregisterReceiver(this.appendChatScreenMsgReceiver);
        super.onDestroy();
    }
}
