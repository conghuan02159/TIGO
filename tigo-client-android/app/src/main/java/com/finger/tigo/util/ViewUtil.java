package com.finger.tigo.util;

import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by Finger-kjh on 2017-06-08.
 */

public class ViewUtil {
    /**
     * set some action for soft keyboard right|bottom key click.
     */
    public static void setEditorActionForButtonClick(EditText editText, final View btn) {
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_DONE:
                    case EditorInfo.IME_ACTION_SEARCH:
                        btn.performClick();
                        break;
                    default:
                        return false;
                }
                return true;
            }
        });
    }
}
