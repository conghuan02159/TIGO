package com.finger.tigo.invite.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.Glide;
import com.finger.tigo.R;
import com.finger.tigo.app.AppController;
import com.finger.tigo.schedule.creatEvent.Person;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class RecyclerAdapterInviteFriend extends RecyclerView.Adapter<RecyclerAdapterInviteFriend.PersonViewHolder> {
    OnItemClickListener mItemClickListener;
    List<Person> people;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    Context mContext;
    // private CustomFilter userFilter;

    public OnItemClickListener getmItemClickListener() {
        return mItemClickListener;
    }

    public void setmItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public RecyclerAdapterInviteFriend(List<Person> persons, Context mContext){
        this.people = persons;
        this.mContext = mContext;
        // userFilter = new CustomFilter(MyRecyclerAdapter.this);
    }


    @Override
    public RecyclerAdapterInviteFriend.PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_item, parent, false);
        return new PersonViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerAdapterInviteFriend.PersonViewHolder holder, int position) {
        holder.personName.setText(people.get(position).name);

        String strPhone = people.get(position).getEmail();
        String strCutPhone = cutPhone(strPhone);
        holder.phoneNumber.setText(strCutPhone+"xxx");

        Glide.with(mContext).load(people.get(position).getPhotoId())
                .error(R.drawable.avatar)
                  //This avoided the OutOfMemoryError
                .into(holder.personPhoto);

        if(people.get(position).getState().equalsIgnoreCase("1")){
            //holder.tvInvited.setVisibility(View.VISIBLE);
            holder.linearLayout.setBackgroundColor(mContext.getResources().getColor(R.color.colorAccent));
            holder.cbSelected.setVisibility(View.GONE);
        }else {
            holder.cbSelected.setVisibility(View.VISIBLE);
            holder.cbSelected.setChecked(people.get(position).isSelected());
            //holder.tvInvited.setVisibility(View.GONE);
        }

    }
    public String cutPhone(String str) {
        if (str != null && str.length() > 0) {
            str = str.substring(0, str.length()-3);
        }
        return str;
    }


    public void setItemSelected(int position, boolean isSelected){
        if (position != -1) {

            people.get(position).setSelected(isSelected);
            notifyItemChanged(position);


        }
    }

    public interface OnItemClickListener{
        void onItemClick(View v, int position);
    }

    @Override
    public int getItemCount() {
        return people.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }



    public class PersonViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener{

        public final View mView;
        public TextView personName;
        public TextView phoneNumber;
        public CircleImageView personPhoto;
        public CheckBox cbSelected;
//        public TextView tvInvited;
        LinearLayout linearLayout;
        public PersonViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            if (imageLoader == null)
                imageLoader = AppController.getInstance().getImageLoader();
            itemView.setOnClickListener(this);
            personName = (TextView) itemView.findViewById(R.id.person_name);
            phoneNumber = (TextView) itemView.findViewById(R.id.phone_number);
            personPhoto = (CircleImageView) itemView.findViewById(R.id.profilePic);
            cbSelected = (CheckBox) itemView.findViewById(R.id.cbSelected);
//            tvInvited = (TextView) itemView.findViewById(R.id.tvInvited);
            linearLayout=(LinearLayout)itemView.findViewById(R.id.layout_item);
        }

        @Override
        public String toString(){
            return super.toString()+" '"+personName +"'";
        }

        @Override
        public void onClick(View v) {
            if(mItemClickListener != null){
                mItemClickListener.onItemClick(v, getPosition());

            }
        }
    }

//    public class CustomFilter extends Filter {
//        private MyRecyclerAdapter mAdapter;
//        private CustomFilter(MyRecyclerAdapter mAdapter) {
//            super();
//            this.mAdapter = mAdapter;
//        }
//        @Override
//        protected FilterResults performFiltering(CharSequence constraint) {
//            InviteFriendFragment.filteredList.clear();
//            final FilterResults results = new FilterResults();
//
//            if (constraint.length() == 0) {
//                InviteFriendFragment.filteredList.addAll(InviteFriendFragment.mPersonList);
//            } else {
//                final String filterPattern = constraint.toString().toLowerCase().trim();
//                for (final Person mWords : InviteFriendFragment.mPersonList) {
//                    if (mWords.getName().toLowerCase().startsWith(filterPattern)) {
//                        InviteFriendFragment.filteredList.add(mWords);
//                    }
//                }
//            }
//            System.out.println("Count Number " + InviteFriendFragment.filteredList.size());
//            results.values = InviteFriendFragment.filteredList;
//            results.count = InviteFriendFragment.filteredList.size();
//            return results;
//        }
//        @Override
//        protected void publishResults(CharSequence constraint, FilterResults results) {
//            System.out.println("Count Number 2 " + ((List<Person>) results.values).size());
//            this.mAdapter.notifyDataSetChanged();
//        }
//    }
}
