package com.finger.tigo.addfriend.async;


import com.finger.tigo.addfriend.model.ItemAddfriend;

import java.util.List;

/**
 * Created by Duong on 3/30/2017.
 */

public interface IAddfriendInteractor {
    void validatePhone(OnNewFrFinishedListener listener, String phone);
    void validateSyncContact(OnNewFrFinishedListener listener, List<ItemAddfriend> listContact, String idUser);
    // check friend
    void validateCheckFriend(OnNewFrFinishedListener listener, String idUser, String idFriend);
    // accept friend

    void validateAcceptFriend(OnNewFrFinishedListener listener, final String iduser, final String idfriend, final String name, final String phone,
                              final String state);
    // Addfriend

    void validateAddFriend(OnNewFrFinishedListener listener, final String idUser, final String idFriend, final String name, final String phone);

    // get data for user friend
    void validateDataTigon(OnNewFrFinishedListener listener, final String idUser);
}
