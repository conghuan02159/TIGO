package com.finger.tigo.detail.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.finger.tigo.R;
import com.finger.tigo.addfriend.DetailFriendActivity;
import com.finger.tigo.app.ApiExecuter;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.detail.CommentAPIListener;
import com.finger.tigo.detail.ReplayCommentActivity;
import com.finger.tigo.detail.UpdateCommentActivity;
import com.finger.tigo.items.FeedItem;
import com.finger.tigo.session.SessionManager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Duong on 4/18/2017.
 */

public class RecyclerAdapterComment extends RecyclerView.Adapter<RecyclerAdapterComment.ViewHolder> {
    public List<FeedItem> feedItems;

    private Context context;
    private String contents;
    private SessionManager session;
    private String idUser;
    private CommentAPIListener mCommentAPIListener;

    public RecyclerAdapterComment(List<FeedItem> mlist, Context context, CommentAPIListener commentAPIListener) {
        super();
        //Getting all the superheroes
        this.feedItems = mlist;
        this.context = context;
        this.mCommentAPIListener = commentAPIListener;
    }

    public interface IOnClickitemListener {
        void onItemClick(int position, int i, ViewHolder v);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_comment, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final  ViewHolder holder, final  int position) {


        final  FeedItem item = feedItems.get(position);
        
        session = new SessionManager(context);
        idUser = session.get_id_user_session() + "";

        holder.ViewAllComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ReplayCommentActivity.class);
                intent.putExtra("id_comment", feedItems.get(position).getIdRComment());
                intent.putExtra("namecomment",feedItems.get(position).getNamecomment());
                intent.putExtra("textComment",feedItems.get(position).getStatusComment());
                intent.putExtra("avartar",feedItems.get(position).getAvatarComment());
                context.startActivity(intent);
            }
        });

        Glide.with(context).load(item.getAvatarComment())
                .placeholder(R.drawable.avatar).error(R.drawable.avatar)
                .into(holder.image_comment);

        Glide.with(context).load(item.getProfilePicReplay())
                .placeholder(R.drawable.avatar).error(R.drawable.avatar)
                .into(holder.imageNameReply);



        holder.comment_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, DetailFriendActivity.class);
                i.putExtra(DetailFriendActivity.ID_USER, item.getIdComment()+"");
                i.putExtra(DetailFriendActivity.NAME, item.getNamecomment());
                i.putExtra(DetailFriendActivity.PHONE, item.getPhone());
                i.putExtra(DetailFriendActivity.PROFILE_PIC, item.getAvatarComment());
                context.startActivity(i);
            }
        });
        holder.txtReplyComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ReplayCommentActivity.class);
                // intent id comment qua replay
                intent.putExtra("id_comment", feedItems.get(position).getIdRComment());
                intent.putExtra("namecomment",feedItems.get(position).getNamecomment());
                intent.putExtra("textComment",feedItems.get(position).getStatusComment());
                intent.putExtra("avartar",feedItems.get(position).getAvatarComment());

                context.startActivity(intent);
            }
        });

        if(idUser.equals(feedItems.get(position).getIdComment()))
        {
            holder.dialogShow.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.dialogShow.setVisibility(View.GONE);
        }
        holder.dialogShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (idUser.equals(feedItems.get(position).getIdComment())) {
                    PopupMenu popup = new PopupMenu(context,holder.dialogShow, Gravity.RIGHT);
                    popup.getMenuInflater().inflate(R.menu.comment_menu, popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem items) {
                            if (items.getItemId() == R.id.delete) {
                                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
                                builder.setMessage(R.string.delete_comment)
                                        .setCancelable(false)
                                        .setPositiveButton(R.string.notification_yes, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int idd) {
                                                deleteComment(feedItems.get(position).getIdRComment(), position);

                                            }
                                        })
                                        .setNegativeButton(R.string.notification_no, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });
                                android.app.AlertDialog alert = builder.create();
                                //Setting the title manually
                                alert.setTitle(R.string.title_comment);
                                alert.show();
//
                            } else if (items.getItemId() == R.id.update) {
                                Intent it = new Intent(context, UpdateCommentActivity.class);
//                              String idScc = item.getId() + "";
//                              it.putExtra("id_user", idScc);
                                it.putExtra("statuscomment", feedItems.get(position).getStatusComment());
                                it.putExtra("id_comment", feedItems.get(position).getIdRComment());
                                context.startActivity(it);
                                Log.e("kkukuku",feedItems.get(position).getStatusComment()+"----"+feedItems.get(position).getIdRComment());
                            } else {

                            }
                            return true;
                        }
                    });
                    popup.show();
                }

            }
        });

        if (!TextUtils.isEmpty(item.getStatusComment())) {
            holder.textView.setText(item.getStatusComment());
            holder.textView.setVisibility(View.VISIBLE);
        } else {
            // status is empty, remove from view
            holder.textView.setVisibility(View.GONE);
        }
        CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
                Long.parseLong(item.getTimecomment()),
                System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);


        holder.comment_time.setText(timeAgo);
        holder.comment_name.setText(item.getNamecomment());
        countmentRply(item.getId_sc());
        holder.commentNameReply.setText(item.getNameReplay());
        holder.commentFirstReply.setText(item.getContainerCommentRply());
        holder.TimeCommentReply.setText(item.getCurrendateCommentRply());

        holder.ViewAllComment.setText(context.getResources().getString(R.string.view_comment) + "(" + item.getCountcommentplay() + ")");


        if (feedItems.get(position).getContainerCommentRply() == null) {
            holder.layoutcommentRplay.setVisibility(View.GONE);
        } else {
            holder.layoutcommentRplay.setVisibility(View.VISIBLE);
        }


    }


    @Override
    public int getItemCount() {

        return feedItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtReplyComment;
        CircleImageView imageNameReply;
        TextView commentNameReply;
        TextView commentFirstReply;
        LinearLayout layoutcommentRplay;
        TextView TimeCommentReply;
        TextView comment_name;
        TextView textView;
        TextView comment_time;
        CircleImageView image_comment;
        TextView ViewAllComment;
        LinearLayout ln;
        TextView dialogShow;


        public ViewHolder(View itemView) {
            super(itemView);
            txtReplyComment = (TextView)itemView.findViewById(R.id.txtreply);
            imageNameReply = (CircleImageView)itemView.findViewById(R.id.imagename); ;
            commentNameReply = (TextView)itemView.findViewById(R.id.commentName);
            commentFirstReply = (TextView)itemView.findViewById(R.id.commentFirstReply);
            layoutcommentRplay = (LinearLayout)itemView.findViewById(R.id.layoutcommentRplay);
            TimeCommentReply = (TextView)itemView.findViewById(R.id.txtTimecomment);
            comment_name = (TextView)itemView.findViewById(R.id.txt_comment_Name);
            textView = (TextView)itemView.findViewById(R.id.textViewchat);
            comment_time = (TextView)itemView.findViewById(R.id.txt_commenttime);
            image_comment = (CircleImageView)itemView.findViewById(R.id.image_comment);
            ViewAllComment = (TextView)itemView.findViewById(R.id.ViewAllComment);
            ln = (LinearLayout)itemView.findViewById(R.id.lin_layout);
            dialogShow=(TextView)itemView.findViewById(R.id.dialogShow);
        }
    }
    private void deleteComment(final String Id, final int position){
        ApiExecuter.ApiProgressListener apiProgressListener = (ApiExecuter.ApiProgressListener) context;

        ApiExecuter.requestApi(apiProgressListener, AppConfig.URL_DELETECOMMENT, null, new ApiExecuter.ApiResultListener() {
            @Override
            public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                feedItems.remove(position);
                notifyDataSetChanged();

                if(mCommentAPIListener != null)
                    mCommentAPIListener.onSuccessCommentAPI(apiUrl);
            }

            @Override
            public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {
                Toast.makeText(context, errMessage, Toast.LENGTH_LONG).show();
            }
        }, "id_sc", Id);

    }

    private void countmentRply(final String id_sc) {
        StringRequest strReq=new StringRequest(Request.Method.POST, AppConfig.URL_COUNT_COMMENT_RPLY,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                    }
                },new Response.ErrorListener()
        {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        })
        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("id_sc", id_sc);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(strReq);

    }

}