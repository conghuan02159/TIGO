package com.finger.tigo.notification.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.finger.tigo.R;
import com.finger.tigo.addfriend.DetailFriendActivity;

import com.finger.tigo.items.ItemNoti;
import com.finger.tigo.session.SessionManager;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Duong on 3/17/2017.
 */

public class NotificationFriendAdapter extends RecyclerView.Adapter<NotificationFriendAdapter.MyViewHolder> {

    private Activity activity;
    private List<ItemNoti> feedItems;
    SessionManager session;
    String idUser;
    public itemClickNotificationFriend itemClick;

    public NotificationFriendAdapter(Activity activity, List<ItemNoti> persons, itemClickNotificationFriend itemClick){
        this.feedItems = persons;
        this.activity = activity;
        this.itemClick = itemClick;
        session = new SessionManager(activity);

    }
    public interface itemClickNotificationFriend{
        void updateFriend(int postion, String state, MyViewHolder v);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_listview_notifi_addfriend, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return feedItems.size();
    }

    @Override
    public void onBindViewHolder(final  MyViewHolder viewHolder, final  int position) {
        final ItemNoti item = feedItems.get(position);
        idUser = session.get_id_user_session() + "";
        viewHolder.name.setText(item.getName());
        viewHolder.Stt.setText(item.getPhone());
        CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
                Long.parseLong(item.getTimeStamp()),
                System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
        viewHolder.timeStamp.setText(timeAgo);

        viewHolder.layout_bt.setVisibility(View.VISIBLE);
        viewHolder.Stt.setText(item.getPhone());
        Glide.with(activity).load(item.getProfilePic())
                .placeholder(R.drawable.avatar).error(R.drawable.avatar)
                .into(viewHolder.profilePic);

        viewHolder.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClick.updateFriend(position, "accepted", viewHolder);
            }
        });
        viewHolder.refused.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClick.updateFriend(position, "refust", viewHolder);
            }
        });

        viewHolder.butItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(activity, DetailFriendActivity.class);
                i.putExtra(DetailFriendActivity.ID_USER, item.getIdFriend());
                i.putExtra(DetailFriendActivity.NAME,item.getName());
                i.putExtra(DetailFriendActivity.PROFILE_PIC, item.getProfilePic());
                i.putExtra(DetailFriendActivity.PHONE, item.getPhone());
                activity.startActivity(i);
            }
        });

    }
    public class MyViewHolder extends RecyclerView.ViewHolder {


        public TextView name;
        CircleImageView profilePic;
        public TextView Stt;
        public TextView timeStamp;
        public ImageButton accept;
        public ImageButton refused;
        public LinearLayout layout_bt;
        public LinearLayout butItem;

        public MyViewHolder(View rowView) {
            super(rowView);
            name = (TextView)rowView.findViewById(R.id.name);
            profilePic = (CircleImageView)rowView.findViewById(R.id.profilePic);
            Stt = (TextView)rowView.findViewById(R.id.txtStt);
            timeStamp = (TextView)rowView.findViewById(R.id.timestamp);
            accept = (ImageButton)rowView.findViewById(R.id.accepted);
            refused = (ImageButton)rowView.findViewById(R.id.refused);
            layout_bt = (LinearLayout)rowView.findViewById(R.id.layout_bt);
            butItem = (LinearLayout) rowView.findViewById(R.id.but_item);



        }
    }
}
