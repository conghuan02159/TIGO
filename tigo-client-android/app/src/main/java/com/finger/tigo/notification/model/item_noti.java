package com.finger.tigo.notification.model;

/**
 * Created by Conghuan on 3/10/2017.
 */

public class item_noti {
    private String name1;
    private String name2;
    private String id_noti;
    private String id_sc;
    private String messages_data;
    private String type_noti;
    private String name_even;
    private String number_user;
    private String time_noti;

    public String getWatching() {
        return watching;
    }

    public void setWatching(String watching) {
        this.watching = watching;
    }

    private String watching;

    public String getName_user_even() {
        return name_user_even;
    }

    public void setName_user_even(String name_user_even) {
        this.name_user_even = name_user_even;
    }

    private String name_user_even;
    private String profilePic;
    private String image;

    public item_noti() {
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getId_noti() {
        return id_noti;
    }

    public void setId_noti(String id_noti) {
        this.id_noti = id_noti;
    }

    public String getId_sc() {
        return id_sc;
    }

    public void setId_sc(String id_sc) {
        this.id_sc = id_sc;
    }

    public String getMessages_data() {
        return messages_data;
    }

    public void setMessages_data(String messages_data) {
        this.messages_data = messages_data;
    }

    public String getType_noti() {
        return type_noti;
    }

    public void setType_noti(String type_noti) {
        this.type_noti = type_noti;
    }

    public String getName_even() {
        return name_even;
    }

    public void setName_even(String name_even) {
        this.name_even = name_even;
    }

    public String getNumber_user() {
        return number_user;
    }

    public void setNumber_user(String number_user) {
        this.number_user = number_user;
    }

    public String getTime_noti() {
        return time_noti;
    }

    public void setTime_noti(String time_noti) {
        this.time_noti = time_noti;
    }


    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }



}
