package com.finger.tigo.items;

/**
 * Created by duong on 26/10/2016.
 */


/**
 * Created by duong on 25/10/2016.
 */
public class itemCountry {

    public String code;
    public String iso;
    public String country;

    public itemCountry(String code, String iso, String country) {
        this.code = code;
        this.iso = iso;
        this.country = country;
    }

    public itemCountry(){

    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
