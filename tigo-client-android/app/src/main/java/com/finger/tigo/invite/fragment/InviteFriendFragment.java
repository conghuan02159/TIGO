package com.finger.tigo.invite.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.invite.adapter.MyRecyclerAdapterDialog;
import com.finger.tigo.invite.adapter.RecyclerAdapterInviteFriend;
import com.finger.tigo.libautolabel.AutoLabelUI;
import com.finger.tigo.schedule.creatEvent.Person;
import com.finger.tigo.session.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InviteFriendFragment extends Fragment {



    public static List<Person> mPersonList;
    public static List<Person> filteredList;
    public static List<Person> filteredListDialog;
    public static List<String> list;
    AlertDialog.Builder dialog;

    ProgressBar progress;
    ProgressDialog pDialog;
    TextView tvInvite;

    private RecyclerAdapterInviteFriend adapter;
    private MyRecyclerAdapterDialog adapterDialog;
    private RecyclerView recyclerView,recyclerView1;

    private String id;
    private TextView txtFrCount;

    private String idUser;
    private EditText searchView;
    //    private TextView tvConnect;
    private AutoLabelUI mAutoLabel;
    LinearLayout linearlayoutlabel;
    View dialogView;
    int i=0;
    private int count=0;

    SessionManager session;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.activity_invite, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        Intent intent = getActivity().getIntent();
        Bundle b = intent.getExtras();
        String id_sc = b.getString("detailsHomeActivity");
        if(id_sc != null){
            id = id_sc+"";
        }

        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);

        session = new SessionManager(getActivity());
        idUser = AppAccountManager.getAppAccountUserId(getActivity());

        progress = (ProgressBar) inflatedView.findViewById(R.id.progressinvite);
        txtFrCount=(TextView)inflatedView.findViewById(R.id.txtFrCount);
        linearlayoutlabel=(LinearLayout)inflatedView.findViewById(R.id.linearlayoutlabel);

        mAutoLabel=(AutoLabelUI)inflatedView.findViewById(R.id.label_view_img);
        mAutoLabel.setBackgroundResource(R.drawable.round_corner_background);

        recyclerView = (RecyclerView) inflatedView.findViewById(R.id.recyclerView);
        recyclerView.setVisibility(View.GONE);

        setRecyclerView();

        Button sent = (Button) inflatedView.findViewById(R.id.sent);
        sent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(filteredList !=null && !filteredList.isEmpty()) {
                    pDialog.setMessage(getResources().getString(R.string.dialog_sending));
                    showDialogg();
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            list = new ArrayList<String>();
                            for (int i = 0; i < filteredList.size(); i++) {
                                if (filteredList.get(i).isSelected()) {
                                    list.add(filteredList.get(i).getId());
                                    //      InviteFriendFragment(id + "", idUser, itema.getIdUser(), "invite");

                                }
                            }
                            InviteFriend(id + "",idUser , list, "invite");
                        }
                    });

                }
            }
        });
        searchView = (EditText) inflatedView.findViewById(R.id.searchView);

        searchView.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                s = s.toString().toLowerCase();
                final  List<Person> filltedList = new ArrayList<Person>();
                for(int i =0; i< mPersonList.size(); i++){
                    final String text = mPersonList.get(i).name.toLowerCase();
                    if(text.contains(s)){
                        filltedList.add(mPersonList.get(i));
                    }
                }
                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                adapter = new RecyclerAdapterInviteFriend(filltedList, getActivity());
                recyclerView.setAdapter(adapter);
                //  adapter.notifyDataSetChanged();
                adapter.setmItemClickListener(new RecyclerAdapterInviteFriend.OnItemClickListener() {

                    @Override
                    public void onItemClick(View v, int position) {
                        itemListClicked(position);
                    }
                });// data set changed

            }

        });
        linearlayoutlabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });

        return inflatedView;
    }

    private void showDialogCommplete(){
        dialog=new AlertDialog.Builder(getActivity());
        dialog.setTitle(R.string.list_selected);

        dialog.setNegativeButton(R.string.ok_password, null);

        LayoutInflater inflater=getActivity().getLayoutInflater();
        dialogView=inflater.inflate(R.layout.dialog_send_complete,null);
        dialog.setView(dialogView);

        TextView tvComplete = (TextView) dialogView.findViewById(R.id.tvComplete);
        if(list.size() >0){
            String strComplete = getResources().getString(R.string.sending_success);
            tvComplete.setText(strComplete+" "+list.size());
        }
        final AlertDialog alertDialog=dialog.create();
        alertDialog.show();
        Button negativeButton=alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        negativeButton.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                getActivity().finish();
            }
        });
    }

    private void showDialog() {
        dialog=new AlertDialog.Builder(getActivity());
        dialog.setTitle(R.string.list_selected);
        dialog.setPositiveButton(R.string.cancelnot, null);
        dialog.setNegativeButton(R.string.ok_password, null);

        LayoutInflater inflater=getActivity().getLayoutInflater();
        dialogView=inflater.inflate(R.layout.fragment_invite_friend_dialog,null);
        dialog.setView(dialogView);
        recyclerView1 = (RecyclerView)dialogView.findViewById(R.id.recyclerViewdialog);
        recyclerView1.setLayoutManager(new LinearLayoutManager(dialogView.getContext()));
        tvInvite = (TextView) dialogView.findViewById(R.id.tv_not_invite);
        final AlertDialog alertDialog=dialog.create();
        alertDialog.show();
        Button positiveButton=alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        positiveButton.setTextColor(getResources().getColor(android.R.color.black));
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPositiveButtonClicked(alertDialog);
            }
        });
        Button negativeButton=alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        negativeButton.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNegativeButtonClicked(alertDialog);
            }
        });
        showlog();

    }

    private void showlog()
    {
        if(mPersonList!=null&&!mPersonList.isEmpty())
        {
            filteredListDialog = new ArrayList<>();
            for(int k=0;k<mPersonList.size();k++) {
                if(mPersonList.get(k).isSelected()) {
                    Person per=new Person();
                    per.setId(mPersonList.get(k).getId());
                    per.setName(mPersonList.get(k).getName());
                    per.setEmail(mPersonList.get(k).getEmail());
                    per.setPhotoId(mPersonList.get(k).getPhotoId());
                    per.setState(mPersonList.get(k).getState());
                    filteredListDialog.add(new Person(per.getId(), per.getName(), per.getEmail(), per.getPhotoId()
                            , true, per.getState(), k));

                }
            }
            if(filteredListDialog.size() >0){
                tvInvite.setVisibility(View.GONE);
                adapterDialog = new MyRecyclerAdapterDialog(filteredListDialog,getActivity());
                recyclerView1.setAdapter(adapterDialog);
                adapterDialog.setmItemClickListener(new MyRecyclerAdapterDialog.OnItemClickListener() {
                    @Override
                    public void onItemClick(View v, int position) {
                        OnItemClickListenerDialog(position);

                    }
                });
                adapterDialog.notifyDataSetChanged();

            }else{
                tvInvite.setText(getResources().getString(R.string.dialog_no_have_invite_list));
                tvInvite.setVisibility(View.VISIBLE);
            }

        }
    }

    private void onPositiveButtonClicked(AlertDialog alertDialog) {

        alertDialog.cancel();
}

    private void onNegativeButtonClicked(AlertDialog alertDialog) {

        if(filteredListDialog!=null && !filteredListDialog.isEmpty()){
            for(int i =0; i< filteredListDialog.size();i++){
                if(!filteredListDialog.get(i).isSelected()){
                    itemListClicked(filteredListDialog.get(i).position);
                }
            }
        }
        alertDialog.dismiss();
    }

    public void setRecyclerView() {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm);
        // checkListInvite();
        running();
    }
    //**********************AUTOLABEL**************************

    private void itemListClicked(int position){
        Person per = filteredList.get(position);
        boolean isSelected = per.isSelected();
        boolean success;
        if(per.getState().equalsIgnoreCase("0")) {

            if (isSelected) {
                adapter.setItemSelected(position, false);
                success = mAutoLabel.removeLabel(position);
                i--;
                if (i >1 ) {
                    txtFrCount.setText("+"+(i-1)+ getResources().getString(R.string.invite_send));
                    txtFrCount.setVisibility(View.VISIBLE);
                } else {
                    txtFrCount.setVisibility(View.GONE);
                }
            } else {
                adapter.setItemSelected(position, true);
                success = mAutoLabel.addLabel(position, per.getPhotoId(), 1);

                i++;
                if (i >1) {
                    txtFrCount.setText("+"+(i-1)+ getResources().getString(R.string.invite_send));
                    txtFrCount.setVisibility(View.VISIBLE);
                } else {
                    txtFrCount.setVisibility(View.GONE);
                }
            }
            if (success) {
                adapter.setItemSelected(position, !isSelected);
            }
        }
    }
    // Dialog
    private void OnItemClickListenerDialog(int position)
    {
        Person list = filteredListDialog.get(position);
        boolean isSelected = list.isSelected();
        if(isSelected){
            adapterDialog.setItemSelected(position, false);
        }
        else {
            adapterDialog.setItemSelected(position, true);
        }
    }
    //************************AUTOLABEL**************************

    private void  running(){
        mPersonList = new ArrayList<>();
        progress.setVisibility(View.VISIBLE);
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_LIST_FRIEND_INVITED, new Response.Listener<String>(){

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    JSONArray feedArray = jObj.getJSONArray("feed");

                    for (int i = 0; i < feedArray.length(); i++) {
                        JSONObject feedObj = (JSONObject) feedArray.get(i);
                        String idfriend = feedObj.getString("idfriend");
                        String name  =feedObj.getString("name");
                        String email = feedObj.getString("phone");
                        String profile = feedObj.getString("profilePic");
                        String state = feedObj.getString("state");

                        mPersonList.add(new Person(idfriend, name, email, profile
                                , false, state));

                    }
                    if(mPersonList!=null && !mPersonList.isEmpty()) {
                        filteredList = new ArrayList<>();
                        filteredList.addAll(mPersonList);
                        adapter = new RecyclerAdapterInviteFriend(filteredList, getActivity());
                        recyclerView.setAdapter(adapter);
                        progress.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        adapter.setmItemClickListener(new RecyclerAdapterInviteFriend.OnItemClickListener() {
                            @Override
                            public void onItemClick(View v, int position) {
                                itemListClicked(position);
                            }
                        });
                    }else {
                        progress.setVisibility(View.GONE);
                        //Toast.makeText(getActivity(), "k co du lieu", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    progress.setVisibility(View.GONE);
//                    tvConnect.setVisibility(View.VISIBLE);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                progress.setVisibility(View.GONE);
//               tvConnect.setVisibility(View.VISIBLE);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("iduser", idUser);
                params.put("idsc", id+"");
                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);

    }

    //  final String idsc, final String iduser, final List<String> idfriend, final String state
    private void InviteFriend(final String idsc, final String iduser ,final  List<String> idfriend, final String state) {
        // Tag used to cancel the request
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_INVITE_FRIEND, new Response.Listener<String>(){
            @Override
            public void onResponse(String response) {
                hideDialog();
                showDialogCommplete();
//                Intent i = new Intent(AddFriendActivity.this, SlideMenuActivity.class);
//                startActivity(i);
//                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideDialog();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                JSONArray arrid_user = new JSONArray();
                Map<String, String> params = new HashMap<String, String>();
                for(String object: idfriend){
                    arrid_user.put(object);
                }
                params.put("idsc", idsc);
                params.put("iduser", iduser);
                params.put("idfriend", arrid_user.toString());
                params.put("state", state);


                return params;

            }

        };
        AppController.getInstance().addToRequestQueue(strReq);
    }

//    private void setListeners() {
//        mAutoLabel.setOnLabelsCompletedListener(new AutoLabelUI.OnLabelsCompletedListener() {
//            @Override
//            public void onLabelsCompleted() {
//                Snackbar.make(recyclerView, R.string.complete, Snackbar.LENGTH_SHORT).show();
//            }
//        });
//
//        mAutoLabel.setOnRemoveLabelListener(new AutoLabelUI.OnRemoveLabelListener() {
//            @Override
//            public void onRemoveLabel(Label removedLabel, int position) {
//                adapter.setItemSelected(position, false);
//                i--;
//                if (i>3) {
//                    count=i+1;
//                    txtFrCount.setText("+"+(count-3)+"");
//                    //txtFrCount.setVisibility(View.VISIBLE);
//                } else {
//                    //txtFrCount.setVisibility(View.GONE);
//                }
//            }
//
//        });
//
//        mAutoLabel.setOnLabelsEmptyListener(new AutoLabelUI.OnLabelsEmptyListener() {
//            @Override
//            public void onLabelsEmpty() {
//                Snackbar.make(recyclerView,R.string.empty, Snackbar.LENGTH_SHORT).show();
//            }
//        });
//
//        mAutoLabel.setOnLabelClickListener(new AutoLabelUI.OnLabelClickListener() {
//            @Override
//            public void onClickLabel(Label labelClicked) {
//                Snackbar.make(recyclerView, labelClicked.getText(), Snackbar.LENGTH_SHORT).show();
//            }
//        });
//
//
//    }


    public void onDestroy(){
        super.onDestroy();
        if (mAutoLabel != null){
            mAutoLabel.destroyDrawingCache();
        }
    }

    private void showDialogg() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public void onResume(){
        super.onResume();

    }

}
