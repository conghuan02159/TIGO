package com.finger.tigo.addfriend.presenter;

import com.finger.tigo.addfriend.model.ItemAddfriend;


import java.util.List;

/**
 * Created by Duong on 4/3/2017.
 */

public interface IAddNewFriendView {
    void onSuccess(String success, List<ItemAddfriend> listRegister);
    void onError(String error, int keyError);
}
