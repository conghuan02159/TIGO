package com.finger.tigo.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Finger-kjh on 2017-05-22.
 */

public class DateUtil {
    public static String getDateString(String timeInMillisStr) {
        try {
            long time = Long.parseLong(timeInMillisStr);
            return getDateString(time);

        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String getDateString(long timeInMillis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeInMillis);

        SimpleDateFormat sdf = new SimpleDateFormat("E dd/MM/yyyy", Locale.getDefault());
        String strDate = sdf.format(calendar.getTime());

        return strDate;
    }

    public static String getDateTimeString(long timeInMillis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeInMillis);

        SimpleDateFormat sdf = new SimpleDateFormat("E dd/MM/yyyy HH:mm:ss", Locale.getDefault());
        String strDateTime = sdf.format(calendar.getTime());

        MyLog.i("timeInMillis=" + timeInMillis + " >> converted " + strDateTime);

        return strDateTime;
    }
}
