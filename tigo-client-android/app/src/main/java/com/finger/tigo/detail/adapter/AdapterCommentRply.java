package com.finger.tigo.detail.adapter;

/**
 * Created by Duong on 4/10/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.finger.tigo.R;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.detail.UpdateCommentRplyActivity;
import com.finger.tigo.items.FeedItem;
import com.finger.tigo.session.SessionManager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by DELL on 4/3/2017.
 */

public class AdapterCommentRply extends BaseAdapter {

    private LayoutInflater inflater;
    private Activity activity;
    private List<FeedItem> feedItems;
    private SessionManager session;
    CircleImageView imageUser;
    private RadioButton showDialog;
    private String idUser;
    String TAG = "FeedListAdapter";

    ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public AdapterCommentRply(Activity activity, List<FeedItem> feedItems)
    {
        this.activity=activity;
        this.feedItems=feedItems;

    }

    @Override
    public int getCount() {
        return feedItems.size();
    }

    @Override
    public Object getItem(int position) {

        return feedItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_item_rply_comment, null);

        TextView TimeDate = (TextView) convertView.findViewById(R.id.timeNowDay);
        TextView commentrplymessage = (TextView) convertView.findViewById(R.id.commentrplymessage);
        TextView usernamerply = (TextView) convertView.findViewById(R.id.usernamerply);
        imageUser = (CircleImageView) convertView.findViewById(R.id.image_comment_rply);
        showDialog=(RadioButton)convertView.findViewById(R.id.showDialog);
        session = new SessionManager(activity);
        idUser= session.get_id_user_session()+"";
        Log.e("",idUser);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();


        final FeedItem item = feedItems.get(position);
        Glide.with(activity).load(item.getAvatarComment())
                .placeholder(R.drawable.avatar).error(R.drawable.avatar)
                .into(imageUser);
        CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
                Long.parseLong(item.getTimecomment()),
                System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
        TimeDate.setText(timeAgo);
        usernamerply.setText(item.getNamecomment());
        commentrplymessage.setText(item.getStatusComment());

        if(idUser.equals(feedItems.get(position).getId_sc()))
        {
            showDialog.setVisibility(View.VISIBLE);
        }
        else
        {
            showDialog.setVisibility(View.GONE);
        }
        showDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (idUser.equals(item.getId_sc())) {
                    PopupMenu popup = new PopupMenu(activity, v);
                    //Inflating the Popup using xml file
                    popup.getMenuInflater().inflate(R.menu.comment_menu, popup.getMenu());

                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem items) {
                            if (items.getItemId() == R.id.delete) {
                                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
                                //Uncomment the below code to Set the message and title from the strings.xml file
                                //builder.setMessage(R.string.dialog_message) .setTitle(R.string.dialog_title);
                                //Setting message manually and performing action on button click
                                builder.setMessage(R.string.delete_comment)
                                        .setCancelable(false)
                                        .setPositiveButton(R.string.notification_yes, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int idd) {
                                                deletecomment(item.getIdRComment() + "");
                                                feedItems.remove(position);
                                                notifyDataSetChanged();
                                            }
                                        })
                                        .setNegativeButton(R.string.notification_no, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                //  Action for 'NO' Button
                                                dialog.cancel();
                                            }
                                        });

                                //Creating dialog box
                                android.app.AlertDialog alert = builder.create();
                                //Setting the title manually
                                alert.setTitle(R.string.title_comment);
                                alert.show();
//
                            } else if (items.getItemId() == R.id.update) {

                                Intent it = new Intent(activity, UpdateCommentRplyActivity.class);

                                it.putExtra("ContainerCommentRply", item.getStatusComment());
                                it.putExtra("id_r_comment", item.getIdRComment());
                                activity.startActivity(it);

                            } else {

                            }
                            return true;
                        }
                    });
                    popup.show();
                }
            }

        });
        Log.e("idsckkk",item.getId_sc());
        return convertView;
    }
    private void deletecomment(final String Id){
        String tag_string_req = "req_idsc";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DELETECOMMENT_RPLY, new Response.Listener<String>(){
            @Override
            public void onResponse(String response) {
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Registration Error: " + error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("ID_R_comment", Id);
                //Log.e("kokoko",Id);
                return params;
            }
        };
        // Adding request to request queue

        AppController.getInstance().addToRequestQueue(strReq);

    }


}
