package com.finger.tigo.app;

/**
 * Created by Finger-kjh on 2017-05-31.
 */

public class AppIntent {
    public static final String ACTION_ADD_FRIEND_FINISHED = "com.finger.tigo.action.ADD_FRIEND_FINISHED";
    public static final String ACTION_RELOAD_COUNT_COMMENT = "com.finger.tigo.action.RELOAD_COUNT_COMMENT";
    public static final String ACTION_CHANGE_NOTIFICATION_BADGE_BY_ADD_FRIEND = "com.finger.tigo.action.CHANGE_NOTIFICATION_BADGE_BY_ADD_FRIEND";
    public static final String ACTION_FINISH_ACTIVITY = "com.finger.tigo.action.FINISH_ACTIVITY";
    public static final String ACTION_PUSH_RECEIVED = "com.finger.tigo.action.PUSH_RECEIVED";
    public static final String ACTION_LOGOUT_DIVICE = "com.finger.tigo.action.ACTION_LOGOUT_DIVICE";
    public static final String ACTION_PUSH_WATCHED = "com.finger.tigo.action.ACTION_PUSH_WATCHED";

    public static final String ACTION_ID_NOTI = "id_noti";
    public static final String ACTION_TYPE_NOTI = "type_noti";

    public static final int REQUEST_UPDATE_EVENT = 10000;
    public static final int REQUEST_EDIT_PROFILE = 10001;
    public static final int REQUEST_CHANGE_AVATAR = 10002;
}
