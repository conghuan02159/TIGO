package com.finger.tigo.search;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.finger.tigo.BaseActivity;
import com.finger.tigo.R;
import com.finger.tigo.app.ApiExecuter;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.detail.EventDetailActivity;
import com.finger.tigo.items.FeedItem;
import com.finger.tigo.listener.RecyclerTouchListener;
import com.finger.tigo.schedule.creatEvent.Person;
import com.finger.tigo.session.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * Created by DUYEN on 12/27/2016.
 */
public class SearchEventActivity extends BaseActivity {


    public  List<Person> filteredList;


    private MyRecyclerAdapterSearch adapter;
    private RecyclerView recyclerView;

    private String newText;
    private EditText searchView;
    private ImageButton btnSearch;
    SessionManager session;

    private TextView mTxtEmptyResult;
    private int pageToDownload =0;
    private Handler handler;

    @Nullable
    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_event);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.invite_search));
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        toolbar.setTitleTextColor(Color.WHITE);
        actionBar.setDisplayHomeAsUpEnabled(true);


        session = new SessionManager(this);
        btnSearch = (ImageButton) findViewById(R.id.search);
        searchView = (EditText) findViewById(R.id.searchView_event);

        mTxtEmptyResult = (TextView) findViewById(R.id.txt_empty);

        handler = new Handler();
        setRecyclerView();

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newText = searchView.getText().toString();
                if (!newText.isEmpty()) {
                    pageToDownload =1;
                    runSearch(newText);
                    //search.setVisibility(View.INVISIBLE);
                } else {
                    Toast.makeText(SearchEventActivity.this, R.string.not_information, Toast.LENGTH_LONG).show();
                }
            }
        });



    }


    public void setRecyclerView(){
        filteredList = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter = new MyRecyclerAdapterSearch(filteredList, SearchEventActivity.this, recyclerView);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Person movie = filteredList.get(position);
//                Toast.makeText(SearchEventActivity.this, movie.getName() + " is selected!", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(SearchEventActivity.this, EventDetailActivity.class);
                i.putExtra("mDetailsHomeActivity", movie.getId()+"");
                i.putExtra("countcomment",movie.getCount()+"");
                i.putExtra("count_participation", movie.getParticipation());

                startActivity(i);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        adapter.setOnLoadMoreListener(new MyRecyclerAdapterSearch.OnLoadMoreListener() {
            @Override
            public void onLoadmore() {
//                Log.d("error loadmore: ", ": ");
                filteredList.add(null);
                recyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
//                        adapter.notifyItemInserted(filteredList.size() - 1);
                    }
                });

                if (filteredList.size() >= 10) {
                    ++pageToDownload;

                }
                runSearch(newText);



            }
        });
    }
    private void runSearch(final  String newText){

        ApiExecuter.requestApi(this, AppConfig.URL_SEARCH_EVENT, getString(R.string.searching), new ApiExecuter.ApiResultListener() {
            @Override
            public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                try {
                    hideProgress();
                    JSONObject jObj = new JSONObject(response);
                    JSONArray feedArray = jObj.getJSONArray("feed");
//                    MyLog.d("fffffff", response);
                    if(feedArray != null) {
                        if (pageToDownload > 1) {
                            filteredList.remove(filteredList.size() - 1);
                            adapter.notifyItemRemoved(filteredList.size());
                        } else {
                            filteredList.clear();
                            adapter.notifyDataSetChanged();
                        }
                        for (int i = 0; i < feedArray.length(); i++) {
                            JSONObject feedObj = (JSONObject) feedArray.get(i);
                            FeedItem item = new FeedItem();

                            item.setId_sc(feedObj.getString("id_sc"));
                            item.setStatus(feedObj.getString("status"));
                            String image = feedObj.isNull("image")? null : feedObj.getString("image");
                            item.setCodeComment(feedObj.getString("numcomment"));
                            item.setAvatarComment(feedObj.getString("profilePic"));
                            item.setdaystart(feedObj.getString("daystart"));
                            item.setDayend(feedObj.getString("dayfinish"));
                            String Description = feedObj.isNull("description") ? null : feedObj.getString("description");
                            item.setCount_participation(feedObj.getString("numparticipation"));
                            filteredList.add(new Person(item.getId_sc(), item.getStatus(), image, item.getAvatarComment(), item.getdaystart(),item.getDateend(), Description, item.getCountComment(), item.getCount_participation()));

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    adapter.notifyItemInserted(filteredList.size());

                                }
                            });
                        }

//                        MyLog.d("fffff", filteredList.get(0)+"");
                        if(feedArray.length() >= 10)
                            adapter.setLoaded();
                        adapter.notifyDataSetChanged();


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    hideProgress();
                    onFail(apiUrl, params, e.getMessage(), e);
                }
            }

            @Override
            public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {
                showToast(errMessage);
                hideProgress();
            }
        }, "message", newText, "page", pageToDownload+"" );
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {//Write your logic here
            this.finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

}
