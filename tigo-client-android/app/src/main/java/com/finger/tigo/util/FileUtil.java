package com.finger.tigo.util;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Finger-kjh on 2017-05-25.
 */

public class FileUtil {
    public static String getRealFilePathFromUri(Context context, Uri uri){
        String path = "";
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null );

        if(cursor != null) {
            cursor.moveToNext();
            path = cursor.getString(cursor.getColumnIndex("_data"));
            cursor.close();
        } else {
            String uriStr = uri.toString();

            if(uriStr.startsWith("file://")) {
                path = uriStr.substring(7);
            }
        }

        MyLog.d("path = " + path);

        return path;
    }

    /**
     * 주어진 비트맵을 주어진 파일경로에 outputQuality 품질 jpeg 방식으로 압축 생성한다.
     * @param bitmap
     * @param destFilePath
     * @param outputQuality
     */
    public static void saveBitmapToFile(Bitmap bitmap, String destFilePath, int outputQuality) throws IOException {
        FileOutputStream fos = null;
        fos = new FileOutputStream(destFilePath);
        bitmap.compress(Bitmap.CompressFormat.JPEG, outputQuality, fos);

        if(fos != null) {
            try {
                fos.close();
            } catch(IOException e) { }
        }
    }

    public static String getFileSize(File file) {
        try {
            if (file.isFile()) {
                long length = file.length();

                return getDataSizeString(length);
            }
        } catch (SecurityException e) {
            return "Unknown(permission denied)";
        }

        return null;
    }


    public static String getDataSizeString(long byteLen) {
        String oriSize = "(" + byteLen + ")";
        long kb = byteLen / 1024;

        if(kb < 1000) {
            return kb + " Kbytes" + oriSize;
        } else if(kb >= 1000) {
            double mb = (double) byteLen / 1048576;

            return "" + (Math.round(mb*100d) / 100d) + " MBytes" + oriSize;
        } else {
            return "unknown size";
        }

    }
}
