package com.finger.tigo.register;

import com.finger.tigo.register.model.ItemRegister;

import java.util.List;

/**
 * Created by Duong on 3/23/2017.
 */

public interface IRegisterView {

    void onSuccess(String success, List<ItemRegister> listRegister);
    void onError(String error, int keyError);
}
