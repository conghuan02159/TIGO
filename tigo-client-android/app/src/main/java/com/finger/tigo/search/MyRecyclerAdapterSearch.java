package com.finger.tigo.search;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.finger.tigo.R;
import com.finger.tigo.schedule.creatEvent.Person;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.Calendar;


/**
 * Created by DUYEN on 12/27/2016.
 */
public class MyRecyclerAdapterSearch extends RecyclerView.Adapter  {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    List<Person> filterList;
    Activity context;
    private int visibleThreshold ;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;



    public MyRecyclerAdapterSearch(List<Person> filterList, Activity context, RecyclerView mRecyclerView){
        this.filterList = filterList;
        this.context=context;

        if (mRecyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    visibleThreshold = linearLayoutManager.getChildCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadmore();
                        }
                        loading = true;
                    }
                }

            });
        }
    }

    public interface  OnLoadMoreListener{
        void onLoadmore();
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final Person item = filterList.get(position);

        if(holder instanceof MyViewHolder) {
            ((MyViewHolder)holder).personName.setText(item.name);
            Glide.with(((MyViewHolder) holder).personPhoto.getContext()).load(item.getPhotoId()).animate(android.R.anim.fade_in)
                    .error(R.drawable.background_hoa_tigo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.background_hoa_tigo)
                    .into(((MyViewHolder) holder).personPhoto);

            ((MyViewHolder)holder).description_event.setText(item.description_event);
            long day_event = Long.parseLong(item.getDay_event());
            long dayEnd = Long.parseLong(item.getDateend());
            SimpleDateFormat dsft = new SimpleDateFormat("E, dd MMM yyyy", Locale.getDefault());
            final String strStDate = dsft.format(day_event);
            ((MyViewHolder)holder).day_event.setText(strStDate);
//            Glide.with(context)
//                    .load(item.getAvatar())
//                    .placeholder(R.drawable.avatar)
//                    .into(((MyViewHolder)holder).avatar);
            Calendar curent = Calendar.getInstance();
            if (curent.getTimeInMillis() > dayEnd) {
                //      ((PersonViewHolder) holder).layoutItem.setBackgroundColor(activity.getResources().getColor(R.color.colorBlackTran));
                ((MyViewHolder) holder).img_oldevent.setVisibility(View.VISIBLE);
            } else {
//                ((MyViewHolder) viewHolder).layoutItem.setBackgroundColor(activity.getResources().getColor(R.color.white));
                ((MyViewHolder) holder).img_oldevent.setVisibility(View.GONE);
            }

        }else {
            if (filterList.size() >= 10) {
                ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
                ((ProgressViewHolder) holder).progressBar.setVisibility(View.VISIBLE);
            }
            else {
                ((ProgressViewHolder) holder).progressBar.setIndeterminate(false);
                ((ProgressViewHolder) holder).progressBar.setVisibility(View.GONE);
            }
        }
    }


    public interface OnItemClickListener{
        void onItemClick(View v, int position);
    }
    public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener listener) {
        this.onLoadMoreListener = listener;
    }


    @Override
    public int getItemCount() {
        return filterList.size();
    }
    @Override
    public int getItemViewType(int position) {
        return filterList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_item_search, parent, false);
            vh = new MyViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_event_progess, parent, false);
            vh = new ProgressViewHolder(v);
        }


        return vh;
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView personName;
        public TextView description_event;
        public TextView day_event;
        public LinearLayout img_oldevent;
        ImageView personPhoto;


        public MyViewHolder(View itemView) {
            super(itemView);

            personName = (TextView) itemView.findViewById(R.id.event_name);
            personPhoto = (ImageView) itemView.findViewById(R.id.profilePic);
            description_event = (TextView) itemView.findViewById(R.id.description_event);
            img_oldevent =(LinearLayout) itemView.findViewById(R.id.img_oldevent);
            day_event = (TextView) itemView.findViewById(R.id.day_event);
//            avatar = (CircleImageView) itemView.findViewById(R.id.avatar);

        }

    }
    public  class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progress_more);
        }
    }



}
