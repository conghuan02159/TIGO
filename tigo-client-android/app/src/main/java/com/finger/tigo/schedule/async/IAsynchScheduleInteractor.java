package com.finger.tigo.schedule.async;

/**
 * Created by Duong on 4/17/2017.
 */

public interface IAsynchScheduleInteractor {
    void MonthInteractorAsync(ConfirmDataListener listener, String idUser);
}
