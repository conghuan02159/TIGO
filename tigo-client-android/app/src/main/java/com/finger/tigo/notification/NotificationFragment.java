package com.finger.tigo.notification;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.android.volley.Cache;
import com.finger.tigo.BaseFragment;
import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.app.ApiExecuter;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.app.AppIntent;
import com.finger.tigo.home.adapter.RecyclerAdapterHome;
import com.finger.tigo.notification.adapter.NotificationAdapter;
import com.finger.tigo.notification.model.item_noti;
import com.finger.tigo.session.SessionManager;
import com.finger.tigo.util.MyLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class NotificationFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    private List<item_noti> mlist;
    private NotificationAdapter listAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    LinearLayout lnNotData;
    LinearLayout lnNotConnect;
    Button reptryConnect;

    private int pageToDownload;
    Handler handler;
    //Creating Views
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private String iduser;
    private boolean runs = false;
    SessionManager session;
    private NotificationBadgeListener mNotificationBadgeListener;

    private static final String TAG = NotificationFragment.class.getSimpleName();

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (Build.VERSION.SDK_INT < 23) {
            onAttachToContext(context);
        }
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
       onAttachToContext(context);

    }

    public void onAttachToContext(Context context) {
        if (context instanceof NotificationBadgeListener) {
            mNotificationBadgeListener = (NotificationBadgeListener) context;

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.notification_tigo, container, false);
        iduser = AppAccountManager.getAppAccountUserId(getActivity());
        handler = new Handler();
        mRecyclerView = (RecyclerView) v.findViewById(R.id.recyclerView_noti);
        mRecyclerView.setHasFixedSize(true);
        session = new SessionManager(getActivity());

        initRecyclerView();

        lnNotData = (LinearLayout) v.findViewById(R.id.layout_not_data);
        lnNotConnect = (LinearLayout) v.findViewById(R.id.layout_not_connect);
        reptryConnect = (Button) v.findViewById(R.id.bt_reptry_connect);
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.idswwipRefesh_noti);
        swipeRefreshLayout.setColorSchemeColors(Color.BLUE, Color.GREEN, Color.YELLOW, Color.RED);
        swipeRefreshLayout.setDistanceToTriggerSync(100);
        swipeRefreshLayout.setSize(SwipeRefreshLayout.DEFAULT);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);
        pageToDownload = 1;
        getNotificationFeedList();
        countNotification();

        reptryConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pageToDownload =1;
                getNotificationFeedList();
            }
        });

        return v;
    }

    private void initRecyclerView() {
        mlist = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        listAdapter = new NotificationAdapter(mlist, mRecyclerView, getActivity(), mNotificationBadgeListener);
        mRecyclerView.setAdapter(listAdapter);
        listAdapter.setOnLoadMoreListener(new RecyclerAdapterHome.OnLoadMoreListener() {
            @Override
            public void onLoadmore() {
                mlist.add(null);
                listAdapter.notifyItemInserted(mlist.size() - 1);
                if (mlist.size() >= 10)
                    ++pageToDownload;
                getNotificationFeedList();
            }
        });
    }

    public void add_noti(String id_noti, String id_sc, String messages_data, String type_noti, String name_even,
                         String number_user,
                         String time_noti, String name, String profilePic, String image, String name1, String name2) {
        item_noti item = new item_noti();
        item.setId_noti(id_noti);
        item.setId_sc(id_sc);
        item.setMessages_data(messages_data);
        item.setType_noti(type_noti);
        item.setName_even(name_even);
        item.setNumber_user(number_user);
        item.setTime_noti(time_noti);
        item.setName_user_even(name);
        item.setProfilePic(profilePic);
        item.setImage(image);
        item.setWatching("0");
        item.setName1(name1);
        item.setName2(name2);

        //check list trung
        Iterator<item_noti> ite = listAdapter.mlists.iterator();

        int count= session.getCountNotification();

        count = count+1;
        session.setCountNotification(Integer.parseInt(count+""));
        if (mNotificationBadgeListener != null) {
            MyLog.d("ddddd", count+"");
            mNotificationBadgeListener.onBadgeUpdate(count);
        }

        createCountNotification(iduser, count+"", session.getCountFriendNotification()+"");


        while (ite.hasNext()) {
            item_noti value = ite.next();

            if (value.getId_noti().contains(id_noti) && value.getType_noti().contains(type_noti)) {
                ite.remove();
            }
        }



        //  arrayAdapter.MessengerList.add(temp);
        listAdapter.mlists.add(0, item);
        mRecyclerView.setAdapter(listAdapter);
        //  listAdapter.notifyDataSetChanged();
    }

    public void getNotificationFeedList() {
        AppController.getInstance().getRequestQueue().getCache().clear();
        Cache cache = AppController.getInstance().getRequestQueue().getCache();

        Cache.Entry entry = cache.get(AppConfig.URL_HIS_NOTI);
        if (entry != null) {
            // fetch the data from cache
            try {
                String data = new String(entry.data, "UTF-8");
                try {
                    parseJsonFeed(new String(data));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        } else {
            ApiExecuter.requestApiNoProgress(AppConfig.URL_HIS_NOTI, new ApiExecuter.ApiResultListener() {
                @Override
                public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                    if (response != null) {
                        parseJsonFeed(response);
                    }
                }

                @Override
                public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {

                    if(mlist.size() <=0){
                        lnNotConnect.setVisibility(View.VISIBLE);
                    }else{
                        showToast(errMessage);
                    }
                    if (swipeRefreshLayout.isRefreshing()) {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
            }, "idUser", iduser, "page", pageToDownload + "");
        }
    }


    private void parseJsonFeed(String response) {
        try {
            JSONObject jObj = new JSONObject(response);
            JSONArray feedArray = jObj.getJSONArray("his_noti");
            MyLog.i(response);
            if (swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
            }

            if (feedArray != null) {
                if (pageToDownload > 1) {
                    mlist.remove(mlist.size() - 1);
                    listAdapter.notifyItemRemoved(mlist.size());
                } else {
                    mlist.clear();
                    listAdapter.notifyDataSetChanged();
                }

                for (int i = 0; i < feedArray.length(); i++) {
                    JSONObject feedObj = (JSONObject) feedArray.get(i);
                    item_noti item = new item_noti();
                    item.setId_noti(feedObj.getString("id_noti"));
                    item.setId_sc(feedObj.getString("id_sc"));
                    item.setMessages_data(feedObj.getString("messages_data"));
                    item.setType_noti(feedObj.getString("type_noti"));
                    item.setName_even(feedObj.getString("name_even"));
                    item.setNumber_user(feedObj.getString("number_user"));
                    item.setTime_noti(feedObj.getString("time_noti"));
                    item.setName_user_even(feedObj.getString("name"));
                    item.setProfilePic(feedObj.getString("profilePic"));
                    item.setImage(feedObj.getString("image"));
                    item.setName1(feedObj.getString("name1"));
                    String name = feedObj.isNull("name2") ? null : feedObj
                            .getString("name2");
                    item.setName2(name);

                    String watching = feedObj.isNull("watching") ? null : feedObj
                            .getString("watching");
                    if(watching == null){
                        watching ="0";
                    }
                    item.setWatching(watching);

                    mlist.add(item);

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            listAdapter.notifyItemInserted(mlist.size());
                        }
                    });
                    if (feedArray.length() >= 10)
                        listAdapter.setLoaded();
                    listAdapter.notifyDataSetChanged();
                }


                downloadsuccess();

            }
        } catch (JSONException e) {
            e.printStackTrace();

            if (swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
            }

        }

    }

    private void downloadsuccess() {
        // set value of notification =0

        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }

        if (mlist != null && !mlist.isEmpty()) {
            lnNotData.setVisibility(View.GONE);
            lnNotConnect.setVisibility(View.GONE);
            // Plus value when it's new

        } else {
            lnNotData.setVisibility(View.VISIBLE);
            lnNotConnect.setVisibility(View.GONE);
        }

        listAdapter.notifyDataSetChanged();

    }

    // count number notification not watched
    private void countNotification() {
        ApiExecuter.requestApiNoProgress(AppConfig.URL_COUNT_HISTORY_NOTIFICATION, new ApiExecuter.ApiResultListener() {
            @Override
            public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                try {

                    session.setCountNotification(0);
                    JSONObject jObj = new JSONObject(response);

                    JSONObject confirm = jObj.getJSONObject("countNotification");

                    String countNotifi = confirm.isNull("countNotifi") ? null: confirm.getString("countNotifi");
                    String countFriendNotif = confirm.isNull("countFriendNotifi") ? null: confirm.getString("countFriendNotifi");
                    if(countNotifi !=null)
                     session.setCountNotification(Integer.parseInt(countNotifi));

                    if(mNotificationBadgeListener != null) {
                        mNotificationBadgeListener.onBadgeUpdate(session.getCountNotification());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    onFail(apiUrl, params, e.getMessage(), e);
                }
            }

            @Override
            public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {

            }
        }, "iduser", iduser);
    }

    @Override
    public void onRefresh() {
        pageToDownload = 1;
        getNotificationFeedList();
        countNotification();
    }

    BroadcastReceiver appendChatScreenMsgReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();
            if (b != null) {
                if (b.getBoolean("reload")) {

                    String id_noti = b.getString("id_noti");
                    String name1 = b.getString("name1");
                    String name2 = b.getString("name2");
                    String id_sc = b.getString("id_sc");
                    String messages_data = b.getString("messages_data");
                    String type_noti = b.getString("type_noti");
                    String name_even = b.getString("name_even");
                    String number_user = b.getString("number_user");
                    String time_noti = b.getString("time_noti");
                    String name = b.getString("name");
                    String image_use_event = b.getString("image_use_event");
                    String image_event = b.getString("image_event");

                    add_noti(id_noti, id_sc, messages_data, type_noti, name_even,
                            number_user,
                            time_noti, name, image_use_event, image_event, name1, name2);


                }else if (b.getString(AppIntent.ACTION_PUSH_WATCHED).equalsIgnoreCase("watched")){
                    String idNoti = b.getString(AppIntent.ACTION_ID_NOTI);
                    String typeNoti = b.getString(AppIntent.ACTION_TYPE_NOTI);
                    Iterator<item_noti> ite = listAdapter.mlists.iterator();

                    int count= session.getCountNotification();
                    if(count !=0) {
                        count = count - 1;
                    }

                    session.setCountNotification(count);
                    MyLog.d("watched broadcast:  "+ session.getCountNotification());
                    if (mNotificationBadgeListener != null) {
                        mNotificationBadgeListener.onBadgeUpdate(session.getCountNotification());
                    }
                    while (ite.hasNext()) {
                        item_noti value = ite.next();
                        if (value.getId_noti().contains(idNoti) && value.getType_noti().contains(typeNoti)) {
                            value.setWatching("1");
                            listAdapter.notifyDataSetChanged();
                            break;

                        }
                    }
                    createCountNotification(iduser, count+"", session.getCountFriendNotification()+"");

                }
            }
        }
    };
// craetea notification
    public void createCountNotification(final String idUser, final String countNotifi, final String countNotififriend){

        ApiExecuter.requestApiNoProgress(AppConfig.URL_CREATE_COUNT_NOTIFICATION, new ApiExecuter.ApiResultListener() {
            @Override
            public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {


                        MyLog.d("create Noti: "+ session.getCountNotification());

                    } else {


                    }
                } catch (JSONException ex) {
                    ex.printStackTrace();
                    MyLog.d(ex.getMessage());
                }

            }

            @Override
            public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {

            }
        }, "iduser", idUser, "countNoti", countNotifi, "countFriendNoti", countNotififriend);


    }


    @Override
    public void onResume() {
        getActivity().registerReceiver(this.appendChatScreenMsgReceiver, new IntentFilter(AppIntent.ACTION_PUSH_RECEIVED));
        super.onResume();
    }

    @Override
    public void onDestroy() {

        getActivity().unregisterReceiver(this.appendChatScreenMsgReceiver);
        super.onDestroy();


    }

    public interface NotificationBadgeListener {
        void onBadgeUpdate(int value);
    }

}


