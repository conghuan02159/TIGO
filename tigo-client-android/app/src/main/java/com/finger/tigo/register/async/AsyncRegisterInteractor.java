package com.finger.tigo.register.async;


import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.common.contact.Contact;
import com.finger.tigo.register.RegisterActivity;
import com.finger.tigo.register.model.ItemRegister;
import com.finger.tigo.session.SessionManager;
import com.finger.tigo.util.MyLog;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.finger.tigo.register.RegisterActivity.PHONE_NUMBER;

/**
 * Created by Duong on 3/23/2017.
 */

public class AsyncRegisterInteractor implements IAsynchRegisInteractor {


    // Error program
    public static final int KEY_ERROR_TWO = 2;
    public static final int KEY_ERROR_ONE = 1;
    public static final int KEY_ERROR_THREE = 3;

    /// Error connect
    public static final int KEY_ERROR_FOUR = 4;

    String valiPhone = "^[0-9]{10,15}$";
    String uid;
    String strPhone;
    private List<ItemRegister> listRegister;
    private  SessionManager session;

    @Override
    public void validateCredentailsAsync(final OnRegisFinishedListener listener, final String name, final String password, final String rePassword) {

                if (name.trim().isEmpty()) {
                    listener.listenerError(RegisterActivity.CREATE_INFORMATION, KEY_ERROR_ONE);
                } else if (password.length() < 8) {
                    listener.listenerError(RegisterActivity.CREATE_INFORMATION, KEY_ERROR_TWO);
                } else if (!password.equals(rePassword)) {
                    listener.listenerError(RegisterActivity.CREATE_INFORMATION, KEY_ERROR_THREE);
                } else {
                    listener.listenerSuccess(RegisterActivity.CREATE_INFORMATION, listRegister);
                }

    }

    @Override
    public void validatePhoneAsync(final String countryCode, final OnRegisFinishedListener listener, final String phone, final String name, final String password, String rePassword, final int sex, final long birthday) {

                if (phone.isEmpty()) {
                    listener.listenerError(PHONE_NUMBER, KEY_ERROR_ONE);
                } else if (!phone.matches(valiPhone)) {
                    listener.listenerError(PHONE_NUMBER, KEY_ERROR_TWO);
                } else {

                    strPhone = processPhone(countryCode, phone);
                    registerUser( listener, name, password, strPhone, sex + "", birthday + "");
                }


    }


    @Override
    public void validateVerificodeAsync(OnRegisFinishedListener listener, String otp) {
        listRegister = new ArrayList<>();
        verifyOtp(listener, otp);
    }

    @Override
    public void validateSyncContactAsync(OnRegisFinishedListener listener, List<Contact> listContact, String idUser) {
        AddFriend(listener, listContact, idUser);
    }


    // phone register
    public String processPhone( String strCode, String phone){

        if (phone.charAt(0) == '0') {
            phone = phone.substring(1);
            phone = strCode + phone;
        } else if (phone.charAt(0) == '+') {
        } else {
            phone = strCode + phone;
        }
        return phone;
    }

    private void registerUser(final OnRegisFinishedListener listener, final String name,
                              final String password, final String phone, final String sex, final String birthday) {

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_REGISTER, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                listRegister = new ArrayList<ItemRegister>();
                try {
                    response = response.replace("NULL", "");
                    response = response.trim();

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        JSONObject user = jObj.getJSONObject("user");

                        uid = jObj.getString("id_user");
                        String names = user.getString("name");
                        String sexs = user.getString("sex");
                        String birthday = user.getString("birthday");
                        String phone = user.getString("phone");

                        listRegister.add(new ItemRegister(uid, names, birthday, sexs, phone));
                        listener.listenerSuccess(PHONE_NUMBER, listRegister);

                    } else {
                        listener.listenerError(PHONE_NUMBER, KEY_ERROR_THREE);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MyLog.d(e.getMessage());
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                MyLog.d(error.getMessage());
                listener.listenerError(PHONE_NUMBER, KEY_ERROR_FOUR);

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                String token = FirebaseInstanceId.getInstance().getToken();
                // Displaying token on logcat
                params.put("token", token);
                params.put("name", name);
                params.put("password", password);
                params.put("phone", phone);
                params.put("sex", sex);
                params.put("birthday", birthday);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }

    // Verificode
    private void verifyOtp(final OnRegisFinishedListener listener,  final String otp) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_VERIFY_OTP, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject responseObj = new JSONObject(response);

                    // Parsing json object response
                    // response will be a json object
                    boolean error = responseObj.getBoolean("error");

                    if (!error) {
                        // parsing the user profile information

                        listener.listenerSuccess(RegisterActivity.VERIFICODE, listRegister);

                    } else {

                        listener.listenerError(RegisterActivity.VERIFICODE, KEY_ERROR_ONE);
                    }

                } catch (JSONException e) {

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                listener.listenerError(RegisterActivity.VERIFICODE, KEY_ERROR_TWO);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("otp", otp);


                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }

    // Addfriend from contact
    private void AddFriend(final OnRegisFinishedListener listener, final  List<Contact> listContact, final String idUser) {
        // Tag used to cancel the request

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADDFRIEND, new Response.Listener<String>(){

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        listRegister.add(new ItemRegister(1));
                    } else {
                        listRegister.add(new ItemRegister(0));

                    }
                }catch (Exception ex){

                }
                listener.listenerSuccess(RegisterActivity.SYNC_CONTACT, listRegister);




            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                listener.listenerError(RegisterActivity.SYNC_CONTACT, KEY_ERROR_FOUR);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                JSONArray arrPhone = new JSONArray();
                JSONArray arrName = new JSONArray();
                Map<String, String> params = new HashMap<String, String>();
                params.put("iduser", idUser);

                for(Contact object: listContact){

                    arrPhone.put(object.phone);
                    arrName.put(object.name);

                }

                params.put("friendPhone", arrPhone.toString());
                params.put("friendName", arrName.toString());

                return params;

            }

        };
        AppController.getInstance().addToRequestQueue(strReq);
    }


}
