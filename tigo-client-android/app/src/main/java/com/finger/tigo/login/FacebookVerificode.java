package com.finger.tigo.login;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.login.LoginManager;
import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.addfriend.AddFriendActivity;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.app.AppIntent;
import com.finger.tigo.data.SQLiteHandler;
import com.finger.tigo.libcountrypicker.fragments.CountryPicker;
import com.finger.tigo.libcountrypicker.interfaces.CountryPickerListener;
import com.finger.tigo.login.async.AsynchLoginInteractor;
import com.finger.tigo.login.model.itemLogin;
import com.finger.tigo.service.OnSmsCatchListener;
import com.finger.tigo.service.SmsVerifyCatcher;
import com.finger.tigo.session.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Duong on 1/14/2017.
 */

public class FacebookVerificode extends AppCompatActivity implements ILoginView {

    public static final String FACEBOOK_VERIFICODE = "facebookVerificode";
    public static final String VERIFICODE = "verifcode";
    private static final String TAG = FacebookVerificode.class.getSimpleName();
    private static final int PERMISSIONS_REQUEST_WRITE_CALENDAR = 0;
    Account account;


    EditText inputPhone;
    EditText et_verificode;
    TextView count_time;
    Button bResend,bt_countrycode,btn_next,btVerificode,btn_edit_mobile;
    TextView tvMobile;
    TextView tvCountry;
    LinearLayout lnPhoneNumber;
    LinearLayout layoutVerifiCode;
    TextInputLayout til;

    String strCode;
    private String calendarid;
    private int currentTime ;
    private int waitingTime = 60;
    String phone;
    SessionManager session;
    private CountryPicker mCountryPicker;


    LoginPresenter presenter;
    ProgressDialog progressDialog;
    private SmsVerifyCatcher smsVerifyCatcher;
    private SQLiteHandler sqlite;
    AccountManager mAccountManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook_verificode);
        inputPhone = (EditText)findViewById(R.id.et_phonenumber);
        et_verificode = (EditText)findViewById(R.id.et_verificode);
        count_time = (TextView)findViewById(R.id.count_time);
        bResend = (Button)findViewById(R.id.btnResend);
        tvMobile = (TextView)findViewById(R.id.txt_edit_mobile);
        tvCountry = (TextView)findViewById(R.id.tv_country);
        lnPhoneNumber = (LinearLayout)findViewById(R.id.lnPhoneNumber);
        layoutVerifiCode = (LinearLayout)findViewById(R.id.ln_verificode);
        til = (TextInputLayout)findViewById(R.id.txt_phonenumber_layout);
        bt_countrycode = (Button)findViewById(R.id.bt_countrycode);
        btn_next = (Button)findViewById(R.id.btn_next);
        btVerificode = (Button)findViewById(R.id.btVerificode);
        btn_edit_mobile = (Button)findViewById(R.id.btn_edit_mobile);

        sqlite = new SQLiteHandler(this);
        mAccountManager = AccountManager.get(this);
        session = new SessionManager(FacebookVerificode.this);
        presenter = new LoginPresenter(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        tvCountry.setText(session.getKeyCodeCountry());
        strCode = session.getKeyCodeCountry();
        currentTime = waitingTime;
        count_time.setText("" + currentTime);

        mCountryPicker = CountryPicker.newInstance(getResources().getString(R.string.select_country));

        mCountryPicker.setListener(new CountryPickerListener() {
            @Override public void onSelectCountry(String name, String code, String dialCode,
                                                  int flagDrawableResID) {
                tvCountry.setText(dialCode);
                strCode = dialCode;
                mCountryPicker.dismiss();
            }
        });


        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                String code = parseCode(message);//Parse verification code
                et_verificode.setText(code);//set code in edit text


                //then you can send verification code to server
            }
        });

        bt_countrycode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CountryCode();
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhoneNumber();
            }
        });
        btVerificode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Verificode();
            }
        });
        bResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResendOtp();
            }
        });
        btn_edit_mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditMobile();
            }
        });
    }



    private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{6}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }

    public void CountryCode(){
        mCountryPicker.show(getFragmentManager(), "COUNTRY_PICKER");
    }


    public void PhoneNumber(){
        phone = inputPhone.getText().toString().trim();
        submitForm();
    }


    public void Verificode(){
        String otp = et_verificode.getText().toString().trim();
        if (!otp.isEmpty()) {
            progressDialog.setMessage(getResources().getString(R.string.create_verificode));
            showDialog();
            presenter.attemptVerificode(otp);
        } else {
            Toast.makeText(getApplicationContext(), R.string.error_otp, Toast.LENGTH_SHORT).show();
        }

    }

    public void ResendOtp(){
        currentTime = waitingTime;
        count_time.setText(currentTime+"");
        handler.removeCallbacks(runnable);
        runnable.run();
        bResend.setVisibility(View.GONE);
        putVerifiCode();
    }


    public void EditMobile(){
        handler.removeCallbacks(runnable);
        layoutVerifiCode.setVisibility(View.GONE);
        lnPhoneNumber.setVisibility(View.VISIBLE);
        bResend.setVisibility(View.GONE);

    }

    private void submitForm() {
        if (!validatePhoneNumber()) {
            return;
        }

        if (phone.charAt(0) == '0') {
            phone = phone.substring(1);
            phone = strCode + phone;
        } else if (phone.charAt(0) == '+') {

        } else {
            phone =  strCode + phone;
        }
        progressDialog.setMessage(getResources().getString(R.string.create_phone));
        showDialog();
        String idUser = session.get_id_user_session()+"";

//        Log.d("tttttttt", "verificode: "+phone+" iduser: "+idUser);
        presenter.attemptFacebookVerificode(idUser, phone);

    }


    private boolean validatePhoneNumber() {

        if (inputPhone.getText().toString().trim().isEmpty()) {

            til.setErrorEnabled(true);
            til.setError(getString(R.string.empty_phone));

            requestFocus(til);
            return false;
        }else if (!isValidPhoneNumber(inputPhone.getText().toString())) {

            til.setErrorEnabled(true);
            til.setError(getString(R.string.invalid_phone));
            requestFocus(til);
            return false;
        }else{
            til.setErrorEnabled(false);

        }
        return true;
    }
    private static boolean isValidPhoneNumber(String mobile) {
        String regEx = "^[0-9]{10,15}$";
        return mobile.matches(regEx);
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public void onSuccess(String success, List<itemLogin> list) {

        if(success.equalsIgnoreCase(FACEBOOK_VERIFICODE)) {
            hideDialog();
            layoutVerifiCode.setVisibility(View.VISIBLE);
            lnPhoneNumber.setVisibility(View.GONE);
            currentTime = waitingTime;
            count_time.setText(currentTime + "");
            runnable.run();
            tvMobile.setText(phone);
        }else if(success.equalsIgnoreCase(VERIFICODE)){
            hideDialog();
            account = new Account(session.getLName_session(), AppAccountManager.ACCOUNT_TYPE);

            if (mAccountManager.addAccountExplicitly(account, null, null)) {
                if (Build.VERSION.SDK_INT >= 23 ) {

                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {

                        checkAppPermissions();

                    }else if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED){

                        AppAccountManager.saveAccountUserDataAndSyncCalendar(this, account, String.valueOf(session.get_id_user_session()), session.getLName_session());

                        session.setLogin(true);
                        session.setIsLogerFb(true);
                        Intent intent = new Intent(FacebookVerificode.this, AddFriendActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        session.setphone_session(phone);

                        if (session.getKeySoundEvent() == null) {
                            session.setSound("tigobabysmall");
                        }
                        Intent a = new Intent();
                        a.setAction(AppIntent.ACTION_FINISH_ACTIVITY);
                        a.putExtra("finishFromLogin", true);
                        this.sendBroadcast(a);
                        finish();
                    }

                }else{
                    AppAccountManager.saveAccountUserDataAndSyncCalendar(this, account, String.valueOf(session.get_id_user_session()), session.getLName_session());

                    session.setLogin(true);
                    session.setIsLogerFb(true);
                    Intent intent = new Intent(FacebookVerificode.this, AddFriendActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    session.setIsLogerFb(true);
                    session.setLogin(true);
                    session.setphone_session(phone);
                    if (session.getKeySoundEvent() == null) {
                        session.setSound("tigobabysmall");
                    }

                    Intent a = new Intent();
                    a.setAction(AppIntent.ACTION_FINISH_ACTIVITY);
                    a.putExtra("finishFromLogin", true);
                    this.sendBroadcast(a);
                    finish();
                }

            } else {
                Log.v(TAG,"no new account created");
            }



//            sqlite.addUser(session.get_id_user_session(),  session.getLName_session(),phone, Integer.parseInt(session.getsex_session()),
//                    session.getLpic_session(), session.getbirhtday_session(), getDateTime());



        }
    }


    @Override
    public void onError(String error, int key) {
        if(error.equalsIgnoreCase(FACEBOOK_VERIFICODE)) {
            hideDialog();
            switch (key) {
                case AsynchLoginInteractor.KEY_ERROR_ONE:
                    Toast.makeText(getApplicationContext(), R.string.error_phone, Toast.LENGTH_SHORT).show();
                    break;
                case AsynchLoginInteractor.KEY_ERROR_TWO:

                    Toast.makeText(getApplicationContext(), R.string.notconnect, Toast.LENGTH_SHORT).show();
                    break;
            }
        }else if(error.equalsIgnoreCase(VERIFICODE)){
            hideDialog();
            switch (key) {
                case AsynchLoginInteractor.KEY_ERROR_ONE:

                    Toast.makeText(getApplicationContext(), R.string.error_verificode, Toast.LENGTH_SHORT).show();
                    break;
                case AsynchLoginInteractor.KEY_ERROR_TWO:
                    Toast.makeText(getApplicationContext(), R.string.notconnect, Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }


    public void putVerifiCode() {

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_RESEND_VERIFICODE, new Response.Listener<String>(){

            @Override
            public void onResponse(String response) {

                try{
                    JSONObject jObj = new JSONObject(response);
                    JSONArray feedArray = jObj.getJSONArray("feed");

                }catch (JSONException e){
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Registration Error: " + error.getMessage());

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                String idUser = session.get_id_user_session()+"";
                params.put("id_user", idUser);
                params.put("phone", phone);

                return params;

            }

        };
        AppController.getInstance().addToRequestQueue(strReq);


    }


    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        public void run() {

            currentTime--;
//
            if (currentTime == 0) {
                handler.removeCallbacks(runnable);
                bResend.setVisibility(View.VISIBLE);
                count_time.setText("" + currentTime);
            } else if (currentTime >= 0) {
                count_time.setText("" + currentTime);

                handler.postDelayed(this, 1000);

            }
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        // smsVerifyCatcher.onStop();
    }

    /**
     * need for Android 6 real time permissions
     */
    private void checkAppPermissions() {
        // Here, thisActivity is the current activity

        // No explanation needed, we can request the permission.
        ActivityCompat.requestPermissions(FacebookVerificode.this,
                new String[]{Manifest.permission.WRITE_CALENDAR},
                PERMISSIONS_REQUEST_WRITE_CALENDAR);

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSIONS_REQUEST_WRITE_CALENDAR: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    AppAccountManager.saveAccountUserDataAndSyncCalendar(this, account, String.valueOf(session.get_id_user_session()), session.getLName_session());

                    session.setLogin(true);
                    session.setIsLogerFb(true);
                    Intent intent = new Intent(FacebookVerificode.this, AddFriendActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    session.setIsLogerFb(true);
                    session.setLogin(true);
                    session.setphone_session(phone);
                    if (session.getKeySoundEvent() == null) {
                        session.setSound("tigobabysmall");
                    }

                    Intent a = new Intent();
                    a.setAction(AppIntent.ACTION_FINISH_ACTIVITY);
                    a.putExtra("finishFromLogin", true);
                    this.sendBroadcast(a);
                    finish();

                } else {
                    AccountManager am =  (AccountManager)this.getSystemService(Context.ACCOUNT_SERVICE);
                    Account[] accountsFromFirstApp = am.getAccountsByType(AppAccountManager.ACCOUNT_TYPE);
                    am.removeAccount(accountsFromFirstApp[0], null, null);
                    finish();
                    Toast.makeText(getApplicationContext(), R.string.user_rejected_calendar_write_permission, Toast.LENGTH_LONG).show();

                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }

    }
    private void showDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    private void hideDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(FacebookVerificode.this);
        builder.setMessage(R.string.want_logout)
                .setCancelable(false)
                .setPositiveButton(R.string.notification_yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int idd) {
                        LoginManager.getInstance().logOut();
                        finish();
                    }
                })
                .setNegativeButton(R.string.notification_no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();
                    }
                });

        //Creating dialog box
        android.app.AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle(R.string.error_login_facebook);
        alert.show();

    }

    // broadcast finish
    BroadcastReceiver appendChatScreenMsgReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();
            if (b != null) {

                if(b.getBoolean("finishFromAddfriend")){
                    finish();
                }
            }
        }
    };

    @Override
    public void onResume(){
        registerReceiver(this.appendChatScreenMsgReceiver, new IntentFilter(AppIntent.ACTION_ADD_FRIEND_FINISHED));
        super.onResume();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(appendChatScreenMsgReceiver);

    }
}
