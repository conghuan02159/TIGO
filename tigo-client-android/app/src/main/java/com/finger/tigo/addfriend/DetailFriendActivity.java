package com.finger.tigo.addfriend;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.finger.tigo.BaseActivity;
import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.addfriend.adapter.FriendRecyclerAdapter;
import com.finger.tigo.addfriend.async.AddfriendInteractor;
import com.finger.tigo.addfriend.model.ItemAddfriend;
import com.finger.tigo.addfriend.presenter.AddNewFriendPresenter;
import com.finger.tigo.addfriend.presenter.IAddNewFriendView;
import com.finger.tigo.app.ApiExecuter;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.detail.EventDetailActivity;
import com.finger.tigo.items.FeedItem;
import com.finger.tigo.listener.RecyclerTouchListener;
import com.finger.tigo.session.SessionManager;
import com.finger.tigo.util.MyLog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class DetailFriendActivity extends BaseActivity implements IAddNewFriendView, SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = "DetailsPersons";
    public static final String CHECKFRIEND = "checkfriend";
    public static final String ACCEPT_OR_RUFUST = "acceptOrRufust";
    public static final String ACCEPTED = "accepted";
    public static final String RUFUST = "rufust";
    public static final String ADD_NEW_FRIEND = "addNewFriend";
    public static final String GET_DATA_EVENT = "getDataEvent";
    public static final String ID_USER = "idUser";
    public static final String NAME = "name";
    public static final String PHONE = "phone";
    public static final String PROFILE_PIC = "profilePic";
    public static final String GET_DATA_TIGON = "getDataTigon";


    CircleImageView mAvatarImageView;
    Button addfriend, btn_accept, btn_rufust;
    LinearLayout layout_confirm_friend;

    Toolbar mToolBar;
    RecyclerView recyclerView;
    TextView tvConnect;
    SwipeRefreshLayout swipeRefreshLayout;
    AppBarLayout mAppBarLayout;
    TextView mTitleTextView;
    TextView mPhoneTextView;
    TextView mTotal;
    Spinner spEventTigon;
    LinearLayout lnNotData;
    LinearLayout lnNotConnect;

    ImageView background;

    SessionManager session;

    private String idUser;
    private String idFriend;
    Toolbar toolbar;

    private List<FeedItem> feedItems;
    private FriendRecyclerAdapter listAdapter;

    private int pageToDownload;
    private AddNewFriendPresenter presenter;
    String strName;
    String strPhone;

    private android.os.Handler handler;
    CollapsingToolbarLayout collapsingToolbar;
    private String URL_EVENT;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_persons);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        collapsingToolbar = (CollapsingToolbarLayout)findViewById(R.id.toolbar_layout);


        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        collapsingToolbar.setTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        mAvatarImageView = (CircleImageView)findViewById(R.id.profilePicDetailPerson);
        addfriend = (Button)findViewById(R.id.btn_addfriend);
        layout_confirm_friend = (LinearLayout)findViewById(R.id.layout_confirm_friend);

        mToolBar = (Toolbar)findViewById(R.id.toolbar);
        recyclerView = (RecyclerView)findViewById(R.id.list_person);
        tvConnect = (TextView)findViewById(R.id.tv_connect);
        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.id_swiRefresh);
        mAppBarLayout = (AppBarLayout)findViewById(R.id.appbar_layout);
        mTitleTextView  = (TextView)findViewById(R.id.textView_title);
        mPhoneTextView  = (TextView)findViewById(R.id.textView_phone);
        mTotal = (TextView) findViewById(R.id.tv_total_event_tigon);
        spEventTigon = (Spinner)findViewById(R.id.spin_event_filter_tigon);
        lnNotData = (LinearLayout) findViewById(R.id.ln_not_data_tigon);
        lnNotConnect = (LinearLayout) findViewById(R.id.layout_not_connect);

        background = (ImageView)findViewById(R.id.background_detail);
        btn_accept = (Button)findViewById(R.id.btn_accept);
        btn_rufust = (Button)findViewById(R.id.btn_rufust);

        setUpToolbar();

        presenter = new AddNewFriendPresenter(this);
        session = new SessionManager(this);
        Intent i = getIntent();

        Bundle bundle = i.getExtras();
        idFriend = bundle.getString(ID_USER);
        strName = bundle.getString(NAME);
        strPhone = bundle.getString(PHONE);
        String strProfilePic = bundle.getString(PROFILE_PIC);

        idUser = AppAccountManager.getAppAccountUserId(this);

        if (strName == null && strPhone == null && strProfilePic == null) {

            presenter.attempDataTigon(idFriend);

        } else {

            mTitleTextView.setText(strName);
            mPhoneTextView.setText(strPhone);

            Glide.with(this).load(strProfilePic)
                    .placeholder(R.drawable.avatar).error(R.drawable.avatar)
                    .error(R.drawable.avatar)
                    .dontAnimate()
                    .into(mAvatarImageView);

        }

        Drawable spinnerDrawable = spEventTigon.getBackground().getConstantState().newDrawable();
        spinnerDrawable.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            spEventTigon.setBackground(spinnerDrawable);
        }else{
            spEventTigon.setBackgroundDrawable(spinnerDrawable);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.simple_spinner_item_gravity_right,
                getResources().getStringArray(R.array.event_filter_entries));
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spEventTigon.setAdapter(adapter);

        initRecyclerView();
        swipeRefreshLayout.setColorSchemeColors(Color.BLUE, Color.GREEN, Color.YELLOW, Color.RED);
        swipeRefreshLayout.setDistanceToTriggerSync(100);
        swipeRefreshLayout.setSize(SwipeRefreshLayout.DEFAULT);
        swipeRefreshLayout.setOnRefreshListener(this);


        showProgress();

        handler = new android.os.Handler();

        presenter.attemptCheckFriend(idUser, idFriend);

        if(idUser.equalsIgnoreCase(idFriend)){
            URL_EVENT  = AppConfig.URL_EVENT_FRIEND;
        }else{
            URL_EVENT = AppConfig.URL_EVENT_FRIEND;
        }

        spEventTigon.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                MyLog.i("position=" + position);
                if(position ==0){
                    showProgress(getResources().getString(R.string.loading));
                    if(idUser.equalsIgnoreCase(idFriend)) {
                        URL_EVENT =  AppConfig.URL_EVENT_USER;

                    }else{
                        URL_EVENT = AppConfig.URL_EVENT_FRIEND;
                    }
                    pageToDownload = 1;
                    getEventTigon(idFriend, idUser, URL_EVENT);
                }else if(position ==1){
                    showProgress(getResources().getString(R.string.loading));
                    if(idUser.equalsIgnoreCase(idFriend)) {
                        URL_EVENT =  AppConfig.URL_EVENT_USER_PROGRESSING;
                    }else{
                        URL_EVENT = AppConfig.URl_EVENT_FRIEND_PROGRESSING;
                    }
                    pageToDownload = 1;
                    getEventTigon(idFriend,idUser, URL_EVENT);
                }else if(position ==2){
                    showProgress(getResources().getString(R.string.loading));
                    if(idUser.equalsIgnoreCase(idFriend)) {
                        URL_EVENT =  AppConfig.URL_EVENT_USER_LAST;
                    }else{
                        URL_EVENT = AppConfig.URl_EVENT_FRIEND_LAST;
                    }
                    pageToDownload = 1;
                    getEventTigon(idFriend, idUser, URL_EVENT);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spEventTigon.setSelection(0);

        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accepted();
            }
        });
        btn_rufust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rufust();
            }
        });
        addfriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFriend();
            }
        });
    }

    private void setUpToolbar() {
        setSupportActionBar(mToolBar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initRecyclerView() {
        feedItems = new ArrayList<>();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);
//        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        listAdapter = new FriendRecyclerAdapter(feedItems, recyclerView);
        recyclerView.setAdapter(listAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                FeedItem movie = feedItems.get(position);
                Intent i = new Intent(DetailFriendActivity.this, EventDetailActivity.class);
                i.putExtra(EventDetailActivity.M_DETAILS_HOME_ACTIVITY, movie.getId_sc());

                startActivity(i);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        listAdapter.setOnLoadMoreListener(new FriendRecyclerAdapter.OnLoadMoreListener() {
            @Override
            public void onLoadmore() {
                feedItems.add(null);
                listAdapter.notifyDataSetChanged();
                if(feedItems.size() >=10)
                    ++pageToDownload;
                getEventTigon(idFriend, idUser, URL_EVENT);

            }
        });

    }

    // layout confirm accept or rufust
    // chấp nhận lời mời
    protected void accepted() {


       showProgress();
        presenter.attemptAcceptFriend(idUser, idFriend, strName, strPhone, ACCEPTED);

    }

    // từ chối lời mời
    protected void rufust() {


        showProgress();
        presenter.attemptAcceptFriend(idUser, idFriend, strName, strPhone, RUFUST);
    }

    // add friend
    protected void addFriend() {


        showProgress();

        presenter.attemptAddNewFriend(idUser, idFriend, strName, strPhone);
    }

    /*
    *********** POSt ID SU KIEN*********************
     */



    public void getEventTigon(final String idFriend, final String idUser, final String URL_EVENT) {

        ApiExecuter.requestApiNoProgress(URL_EVENT, new ApiExecuter.ApiResultListener() {
            @Override
            public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    JSONArray feedArray = jObj.getJSONArray("feed");
                    hideProgress();

                    if (swipeRefreshLayout.isRefreshing()) {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                    if (feedArray != null) {
                        if (pageToDownload > 1) {
                            feedItems.remove(feedItems.size() - 1);
                            listAdapter.notifyItemRemoved(feedItems.size());
                        } else {
                            feedItems.clear();
                            listAdapter.notifyDataSetChanged();
                        }

                        for (int i = 0; i < feedArray.length(); i++) {

                            JSONObject feedObj = (JSONObject) feedArray.get(i);
                            String image = feedObj.isNull("image") ? null : feedObj
                                    .getString("image");
                            // url might be null sometimes
                            String strDestription = feedObj.isNull("description") ? null : feedObj
                                    .getString("description");

                            feedItems.add(new FeedItem(feedObj.getString("id_sc"), feedObj.getString("status"), strDestription, image, feedObj.getString("daystart"), feedObj.getString("dayfinish")));
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    listAdapter.notifyItemInserted(feedItems.size());
                                }
                            });

                        }

                        if (feedArray.length() >= 10)
                            listAdapter.setLoaded();

                        if(feedItems.size() ==0){
                            lnNotData.setVisibility(View.VISIBLE);
                            lnNotConnect.setVisibility(View.GONE);

                        }else{
                            lnNotData.setVisibility(View.GONE);
                            lnNotConnect.setVisibility(View.GONE);
                        }
                    }

                } catch (Exception ex) {
                    // JSON parsing error

                    ex.printStackTrace();
                    onFail(apiUrl, params, ex.getMessage(), ex);
                }
            }

            @Override
            public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {
                hideProgress();
                if(feedItems.size() <=0){
                    lnNotConnect.setVisibility(View.VISIBLE);
                }
                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        }, "idUser", idUser, "idFriend", idFriend, "page", pageToDownload + "", "currentTime", Calendar.getInstance().getTimeInMillis() + "");

    }
    /*
     **********ADD FRIEND**************************
    */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {//Write your logic here
            this.finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onSuccess(String success, List<ItemAddfriend> listCheck) {
        if (success.equalsIgnoreCase(CHECKFRIEND)) {
          hideProgress();
            for (ItemAddfriend object : listCheck) {

                switch (object.confirm) {
                    case 1:
                        // da gui loi moi
                        tvConnect.setVisibility(View.VISIBLE);
                        tvConnect.setText(R.string.friend_request_sent);

                        break;
                    case 2:
                        // la ban be
                        break;
                    case 3:
                        // add friend
                        MyLog.d("idUser=" + idUser + ", idFriend=" + idFriend);
                        if(!idUser.equals(idFriend)) {
                            addfriend.setVisibility(View.VISIBLE);

                        }

                        break;
                    case 4:
                        // accept and refust
                        layout_confirm_friend.setVisibility(View.VISIBLE);

                        break;
                }

                // register
            }
        } else if (success.equalsIgnoreCase(ACCEPT_OR_RUFUST)) {
             hideProgress();
            layout_confirm_friend.setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(), R.string.notification_contacts, Toast.LENGTH_SHORT).show();


        } else if (success.equalsIgnoreCase(ADD_NEW_FRIEND)) {
           hideProgress();
            addfriend.setVisibility(View.GONE);
        } else if (success.equalsIgnoreCase(GET_DATA_TIGON)) {

            for (ItemAddfriend object : listCheck) {

                mTitleTextView.setText(object.name);
                mPhoneTextView.setText(object.id);
                strPhone = object.id;
                strName = object.name;
                Glide.with(this).load(object.profilePic)
                        .placeholder(R.drawable.avatar).error(R.drawable.avatar)
                        .dontAnimate()
                        .into(mAvatarImageView);
            }

        }
    }


    @Override
    public void onError(String error, int keyError) {
        if (error.equalsIgnoreCase(CHECKFRIEND)) {
            hideProgress();
            switch (keyError) {
                case AddfriendInteractor.KEY_ERROR_ONE:
                    Toast.makeText(getApplicationContext(), R.string.notconnect, Toast.LENGTH_SHORT).show();

                    break;
                case AddfriendInteractor.KEY_ERROR_TWO:
                    Toast.makeText(getApplicationContext(),R.string.notconnect, Toast.LENGTH_SHORT).show();

                    break;
            }
        } else if (error.equalsIgnoreCase(ACCEPT_OR_RUFUST)) {
           hideProgress();
            switch (keyError) {
                case AddfriendInteractor.KEY_ERROR_ONE:
                    Toast.makeText(getApplicationContext(), R.string.notconnect, Toast.LENGTH_SHORT).show();
                    break;
            }
        } else if (error.equalsIgnoreCase(ADD_NEW_FRIEND)) {
            hideProgress();
            switch (keyError) {
                case AddfriendInteractor.KEY_ERROR_ONE:
                    Toast.makeText(getApplicationContext(), R.string.notconnect, Toast.LENGTH_SHORT).show();
                    break;
            }
        } else if (error.equalsIgnoreCase(GET_DATA_EVENT)) {
          hideProgress();
            switch (keyError) {
                case AddfriendInteractor.KEY_ERROR_ONE:
                    Toast.makeText(getApplicationContext(), R.string.notconnect, Toast.LENGTH_SHORT).show();

                    break;
            }
        }
    }


    @Override
    public void onRefresh() {

            pageToDownload = 1;
            getEventTigon(idFriend, idUser, URL_EVENT);

    }


    @Override
    public void onPause() {
        super.onPause();

    }

}
