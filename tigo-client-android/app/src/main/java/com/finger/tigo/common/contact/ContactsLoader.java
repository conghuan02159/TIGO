package com.finger.tigo.common.contact;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Finger-kjh on 2017-05-24.
 *
 * This class used to load contacts at background.
 */

public class ContactsLoader extends AsyncTask<String, Integer, List<Contact>> {

    private ContentResolver mContentResolver;
    private ContactsLoadListener mContactsLoadListener;
    private boolean showCountryCode;

    public ContactsLoader(Context context, ContactsLoadListener contactsLoadListener) {
        mContentResolver = context.getContentResolver();
        mContactsLoadListener = contactsLoadListener;
    }

    @Override
    protected void onPreExecute() {
        mContactsLoadListener.onLoadStarted();
    }

    @Override
    protected List<Contact> doInBackground(String... params) {
        String countryCode = null;
        if(params != null && params.length > 0) {
            countryCode = params[0];
            showCountryCode = true;
        }

        return getAllContacts(countryCode);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        mContactsLoadListener.onLoading(values[0]);
    }

    @Override
    protected void onPostExecute(List<Contact> contacts) {
        mContactsLoadListener.onLoadCompleted(contacts);
    }

    private List<Contact> getAllContacts(String countryCode) {

        List<Contact> contacts = new ArrayList<>();

        Cursor cursor = mContentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");

        if (cursor != null && cursor.getCount() > 0) {
            int total = cursor.getCount();
            int current = 0;
            int percent;

            while (cursor.moveToNext()) {
                current++;

                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));
                if (hasPhoneNumber > 0) {
                    String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                    String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                    Cursor phoneCursor = mContentResolver.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id},
                            null);

                    while (phoneCursor != null && phoneCursor.moveToNext()) {

                        String phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        String phone = phoneNumber;

                        if(showCountryCode) {
                            if (phone.charAt(0) == '0') {
                                phone = phone.substring(1);
                                phone = countryCode + phone;
                            } else if (phone.charAt(0) == '+') {
                                phone = phoneNumber;
                            } else {
                                phone = countryCode + phone;
                            }
                        }

                        contacts.add(new Contact(name, phone));

                        // update progress UI

                        percent = (int) (current / (float) total * 100f);
//                        MyLog.d("current=" + current + ", total=" + total + ", percent=" + percent);
                        publishProgress(percent);
                    }

                    if(phoneCursor != null) {
                        phoneCursor.close();
                    }
                }
            }
        }

        if(cursor != null) {
            cursor.close();
        }

        return contacts;
    }
}
