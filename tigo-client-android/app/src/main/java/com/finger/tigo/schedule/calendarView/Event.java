package com.finger.tigo.schedule.calendarView;

/**
 * Created by duong on 21/10/2016.
 */

import android.support.annotation.Nullable;

/**
 * Created by duong on 21/10/2016.
 */

public class Event {
    private int color;
    private long timeInMillis;
    private Object data;
    private String idsc;
    private String location;
    private String dayStart;

    public Event(int color, long timeInMillis) {
        this.color = color;
        this.timeInMillis = timeInMillis;
    }

    public Event(int color, long timeInMillis, Object data, String idsc, String location, String dayStart) {
        this.color = color;
        this.timeInMillis = timeInMillis;
        this.data = data;
        this.idsc = idsc;
        this.location = location;
        this.dayStart = dayStart;
    }

    public String getDayStart() {
        return dayStart;
    }

    public void setDayStart(String dayStart) {
        this.dayStart = dayStart;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getIdsc() {
        return idsc;
    }

    public void setIdsc(String idsc) {
        this.idsc = idsc;
    }

    public int getColor() {
        return color;
    }

    public long getTimeInMillis() {
        return timeInMillis;
    }

    @Nullable
    public Object getData() {
        return data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        if (color != event.color) return false;
        if (timeInMillis != event.timeInMillis) return false;
        if (data != null ? !data.equals(event.data) : event.data != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = color;
        result = 31 * result + (int) (timeInMillis ^ (timeInMillis >>> 32));
        result = 31 * result + (data != null ? data.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Event{" +
                "color=" + color +
                ", timeInMillis=" + timeInMillis +
                ", data=" + data +
                '}';
    }
}
