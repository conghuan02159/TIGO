package com.finger.tigo.views;

import android.app.Activity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.TextView;
import android.widget.Toast;

import com.finger.tigo.items.FeedItem;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import com.finger.tigo.R;

class ViewHolder implements OnMapReadyCallback {
    Activity activity;
    MapView mapView;
    TextView title;
    GoogleMap map;
    @Override
    public void onMapReady(GoogleMap googleMap) {
//        MapsInitializer.initialize(getApplication());
        FeedItem feedItem = (FeedItem) mapView.getTag();
        if (feedItem != null) {
            setMapLocation(googleMap, feedItem);
        }
    }
    public void initializeMapView() {
        if (mapView != null) {
            // Initialise the MapView
            mapView.onCreate(null);
            // Set the map ready callback to receive the GoogleMap object
            mapView.getMapAsync(this);
        }}
    AbsListView.RecyclerListener mRecycleListener = new AbsListView.RecyclerListener() {
        @Override
        public void onMovedToScrapHeap(View view) {
            ViewHolder holder = (ViewHolder) view.getTag();
            if (holder != null && holder.map != null) {
                // Clear the map and free up resources by changing the map type to none
                Toast.makeText(activity, R.string.map_click,Toast.LENGTH_SHORT).show();
                holder.map.setMapType(GoogleMap.MAP_TYPE_NONE);
            }}};

    public void setMapLocation(GoogleMap map, FeedItem feedItems) {
        LatLng latLng = new LatLng(feedItems.getLat(),feedItems.getLng());
        // Add a marker for this item and set the camera
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10f));
        map.addMarker(new MarkerOptions().position(latLng));
        // Set the map type back to normal.
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }
}

