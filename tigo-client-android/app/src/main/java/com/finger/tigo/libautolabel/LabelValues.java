package com.finger.tigo.libautolabel;

import android.os.Parcel;
import android.os.Parcelable;

/*
 * Copyright (C) 2015 David Pizarro
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class LabelValues implements Parcelable {

    private int key;
    private String value;
    private int idImage;
    private int idCheck;
    private String urlImage;
    private int id;

    public LabelValues(int key, String value) {
        this.key = key;
        this.value = value;

    }
    public LabelValues(int key,  int idImage) {
        this.key = key;
        this.idImage = idImage;
    }
    public LabelValues(int key, String url, int id){
        this.key = key;
        this.urlImage = url;
        this.id= id;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdImage() {
        return idImage;
    }

    public void setIdImage(int idImage) {
        this.idImage = idImage;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.key);
        dest.writeString(this.value);
        dest.writeInt(this.idImage);
    }

    private LabelValues(Parcel in) {
        this.key = in.readInt();
        this.value = in.readString();
        this.idImage = in.readInt();
    }

    public static final Creator<LabelValues> CREATOR
            = new Creator<LabelValues>() {
        public LabelValues createFromParcel(Parcel source) {
            return new LabelValues(source);
        }

        public LabelValues[] newArray(int size) {
            return new LabelValues[size];
        }
    };
}
