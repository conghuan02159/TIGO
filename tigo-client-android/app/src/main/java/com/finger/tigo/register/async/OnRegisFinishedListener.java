package com.finger.tigo.register.async;

import com.finger.tigo.register.model.ItemRegister;

import java.util.List;

/**
 * Created by Duong on 3/23/2017.
 */

public interface OnRegisFinishedListener {

    void listenerSuccess(String success, List<ItemRegister> listRegister);
    void listenerError(String error, int keyError);
}
