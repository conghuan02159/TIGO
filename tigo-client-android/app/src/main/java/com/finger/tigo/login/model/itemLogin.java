package com.finger.tigo.login.model;

/**
 * Created by Duong on 2/27/2017.
 */

public class itemLogin {

    public int id;
    public String name;
    public String email;
    public String phone;
    public String birthday;
    public String sex;
    public String profilePic;
    public int state;
    public String createAt;

    public itemLogin( int id, String name,  String phone, String birthday, String sex, String profilePic, String createAt){

        this.id = id;
        this.name = name;

        this.phone = phone;
        this.birthday = birthday;
        this.sex = sex;
        this.profilePic = profilePic;
        this.createAt = createAt;
    }

    public itemLogin(int  state, int id, String name, String phone,   String sex, String profilePic, String createAt){
        this.state = state;
        this.id = id;
        this.name = name;
        this.createAt = createAt;
        this.phone = phone;
        this.sex = sex;
        this.profilePic = profilePic;
    }

    public itemLogin( int id){
        this.id = id;

    }
}
