package com.finger.tigo.detail;

import com.finger.tigo.common.model.EventItem;

/**
 * Created by Duong on 4/5/2017.
 */

public interface EventDetailView {
    void onSuccess(String apiCode, EventItem eventItem);
    void onError(String apiCode, String errMessage, Throwable cause);
}
