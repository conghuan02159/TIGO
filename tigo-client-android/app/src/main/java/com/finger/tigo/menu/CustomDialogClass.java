package com.finger.tigo.menu;



import android.app.Activity;
import android.app.Dialog;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.finger.tigo.R;
import com.finger.tigo.session.SessionManager;

import java.util.ArrayList;
import java.util.List;

public class CustomDialogClass extends Dialog implements
        android.view.View.OnClickListener {

    public Activity c;
    public Dialog d;
    public Button yes, no;
    String a = "";
    MediaPlayer mMediaPlayer;
    Resources res;
    SessionManager session;
    String soundSession;


    public CustomDialogClass(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog_sound);
        yes = (Button) findViewById(R.id.btn_yes);
        no = (Button) findViewById(R.id.btn_no);
        yes.setOnClickListener(this);
        no.setOnClickListener(this);
        session = new SessionManager(c);
        soundSession = session.getKeySoundEvent();
        //  String actualSound = session.getKeySoundEvent();
        res = c.getResources();

        final List<String> stringList=new ArrayList<>();

        // here is list
        stringList.add(0, "tigobabysmall");
        stringList.add(1, "tigobatterylow");
        stringList.add(2, "tigochildren");
        stringList.add(3, "tigorobot");
        stringList.add(4, "tigoshrinking");
        stringList.add(5, "tigosqirrel");
        stringList.add(6, "tigozombie");

        final List<String> strNameSound = new ArrayList<>();
        strNameSound.add(0, "Baby Small");
        strNameSound.add(1, "Battery Low");
        strNameSound.add(2, "Helium");
        strNameSound.add(3, "Robot");
        strNameSound.add(4, "Shrinking");
        strNameSound.add(5, "Squirrel");
        strNameSound.add(6, "Zombie");

//        for(int i=0;i<5;i++) {
//            stringList.add("RadioButton " + (i + 1));
//        }
        RadioGroup rg = (RadioGroup) findViewById(R.id.radio_group);

        for(int i=0;i<stringList.size();i++){
            RadioButton rb=new RadioButton(c); // dynamically creating RadioButton and adding to RadioGroup.
            rb.setText(strNameSound.get(i));

            rg.addView(rb);
            if(stringList.get(i).equalsIgnoreCase(soundSession)){
                rb.setChecked(true);
            }
        }


        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int childCount = group.getChildCount();
                for (int x = 0; x < childCount; x++) {
                    RadioButton btn = (RadioButton) group.getChildAt(x);
                    if (btn.getId() == checkedId) {
                        int soundId = res.getIdentifier(stringList.get(x), "raw", c.getPackageName());
                        if(mMediaPlayer!=null) {
                            if (mMediaPlayer.isPlaying()) {
                                mMediaPlayer.release();
                            }
                        }
                        mMediaPlayer = MediaPlayer.create(c, soundId);
                        mMediaPlayer.start();



                        a = stringList.get(x);

                    }
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_yes) {
            if (mMediaPlayer != null) {
                if (mMediaPlayer.isPlaying()) {
                    mMediaPlayer.release();
                }
            }
            SettingActivity.tvSound.setText(a);
            session.setSound(a);

        } else if (i == R.id.btn_no) {
            if (mMediaPlayer != null) {
                if (mMediaPlayer.isPlaying()) {
                    mMediaPlayer.release();
                }
            }
            dismiss();

        } else {
        }
        dismiss();
    }


}