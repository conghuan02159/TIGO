package com.finger.tigo.friend.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.finger.tigo.BaseFragment;
import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.common.contact.Contact;
import com.finger.tigo.common.contact.ContactsLoadListener;
import com.finger.tigo.common.contact.ContactsLoader;
import com.finger.tigo.friend.adapter.SyncTigonAdapter;
import com.finger.tigo.items.ItemNoti;
import com.finger.tigo.session.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.finger.tigo.addfriend.fragment.TigonPhoneFragment.PERMISSION_REQUEST_CONTACT;

/**
 * Created by Duong on 5/17/2017.
 */

public class TigonSyncFragment extends BaseFragment implements SyncTigonAdapter.itemClickNotificationFriend{


    TextView dateUpdateSync;
    RecyclerView recyclerSync;
    Button btConnectSync;
    TextView textSync;
    SessionManager session;
    private List<ItemNoti> listSync;
    SyncTigonAdapter adapter;
    String idUser;
    Activity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_list_synch_tigon, container, false);
        dateUpdateSync = (TextView)rootview.findViewById(R.id.date_update);
        recyclerSync = (RecyclerView)rootview.findViewById(R.id.recycler_sync);
        btConnectSync = (Button)rootview.findViewById(R.id.bt_connect_sync);
        textSync = (TextView)rootview.findViewById(R.id.textSync);
        activity = getActivity();
        idUser = AppAccountManager.getAppAccountUserId(activity);
        session = new SessionManager(getActivity());
//        strCode = session.getKeyCodeCountry();
        initListSync();
        setSyncNewTigon();


        dateUpdateSync.setText(getDate(session.getSyncDate()));
        return rootview;
    }

    private void initListSync() {
        listSync = new ArrayList<>();
        adapter = new SyncTigonAdapter(getActivity(), listSync, this);

        recyclerSync.setHasFixedSize(true);
        RecyclerView.LayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerSync.setLayoutManager(horizontalLayoutManagaer);

        recyclerSync.setItemAnimator(new DefaultItemAnimator());
        recyclerSync.setAdapter(adapter);
        btConnectSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btConnectSync();
            }
        });

//        recyclerSync.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerSync, new RecyclerTouchListener.ClickListener() {
//            @Override
//            public void onClick(View view, int position) {
//                ItemNoti item = feedItems.get(position);
//                updateWatchedFriend( item.getIdFriend());
//                Intent i = new Intent(getActivity(), DetailFriendActivity.class);
//                i.putExtra(LISTFRIEND, item.getIdFriend());
//                i.putExtra(DetailFriendActivity.ID_USER, item.getIdFriend());
//                i.putExtra(DetailFriendActivity.NAME,item.getName());
//                i.putExtra(DetailFriendActivity.PROFILE_PIC, item.getProfilePic());
//                i.putExtra(DetailFriendActivity.PHONE, item.getPhone());
//                startActivity(i);
//
//            }
//
//            @Override
//            public void onLongClick(View view, int position) {
//
//            }
//        }));

    }

    public  void setSyncNewTigon() {

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_SYNC_NEW, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response !=null)
                    parseJsonFeed(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("iduser", idUser);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
        // }
    }
    public void parseJsonFeed(String response){
        try {
            JSONObject jObj = new JSONObject(response);
            JSONArray feedArray = jObj.getJSONArray("feed");
            listSync.clear();
            for (int i = 0; i < feedArray.length(); i++) {
                JSONObject feedObj = (JSONObject) feedArray.get(i);
                ItemNoti item = new ItemNoti();
                item.setName(feedObj.getString("name"));
                item.setIdFriend(feedObj.getString("idfriend"));
                item.setPhone(feedObj.getString("phone"));
                item.setProfilePic(feedObj.getString("profilePic"));
                listSync.add(item);

            }

            adapter.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    private String getDate(long timeMillis){
        DateFormat dfDate = new SimpleDateFormat("EE dd/MM/yyyy", Locale.getDefault());
        String date=dfDate.format(timeMillis);
        DateFormat dfTime = new SimpleDateFormat("HH:mm");
        String time = dfTime.format(timeMillis);
        return time + ", " + date;
    }


    public void btConnectSync(){
        showProgress(getResources().getString(R.string.contact_sync));
        dateUpdateSync.setText(getResources().getString(R.string.contact_sync));
        loadContacts();
    }

    public void loadContacts() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.READ_CONTACTS)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setTitle(R.string.contacts_access);
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setMessage(R.string.please_contacts_access);//TODO put real question
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            requestPermissions(
                                    new String[]
                                            {Manifest.permission.READ_CONTACTS}
                                    ,  PERMISSION_REQUEST_CONTACT);
                        }
                    });
                    builder.show();
                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(activity,
                            new String[]{Manifest.permission.READ_CONTACTS},
                            PERMISSION_REQUEST_CONTACT);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                getAllContacts();
            }
        } else {
            getAllContacts();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CONTACT: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getAllContacts();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    hideProgress();
                }
                return;
            }

        }
    }

    private void getAllContacts() {
        ContactsLoader contactLoader = new ContactsLoader(getActivity(), new ContactsLoadListener() {
            @Override
            public void onLoadStarted() {
                showProgress("loading contacts...");
            }

            @Override
            public void onLoading(int percent) {
                showProgress("loading contacts... " + percent + "%");
            }

            @Override
            public void onLoadCompleted(List<Contact> contacts) {
                hideProgress();

                syncContact(contacts, idUser);
            }
        });

        contactLoader.execute(session.getKeyCodeCountry());
    }

    private void syncContact(final  List<Contact> listContact, final String idUser) {
        // Tag used to cancel the request

        showProgress("syncronizing contacts...");
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADDFRIEND, new Response.Listener<String>(){

            @Override
            public void onResponse(String response) {
                try {
                    hideProgress();

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {

                    } else {
                        textSync.setText(getActivity().getResources().getString(R.string.no_have_sync));
                    }
                    setSyncNewTigon();
                    session.setSyncDate(Calendar.getInstance().getTimeInMillis());
                    dateUpdateSync.setText(getDate(Calendar.getInstance().getTimeInMillis()));
                }catch(Exception ex){

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress();
                dateUpdateSync.setText(getDate(session.getSyncDate()));

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                JSONArray arrPhone = new JSONArray();
                JSONArray arrName = new JSONArray();
                Map<String, String> params = new HashMap<String, String>();
                params.put("iduser", idUser);

                for(Contact object: listContact){

                    arrPhone.put(object.phone);
                    arrName.put(object.name);
                }


                params.put("friendPhone", arrPhone.toString());
                params.put("friendName", arrName.toString());

                return params;

            }

        };
        AppController.getInstance().addToRequestQueue(strReq);
    }


    @Override
    public void addFriend(int postion, String idFriend, String name, String phone, SyncTigonAdapter.MyViewHolder v) {

        addNewFriend(idUser, idFriend, name, phone);
    }

    private void addNewFriend(final String iduser, final String idfriend, final String name, final String phone) {
        // Tag used to cancel the request

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADDNEWFRIEND, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                hideProgress();



            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                hideProgress();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("iduser", iduser);
                params.put("idfriend", idfriend);
                params.put("name", name);
                params.put("phone", phone);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }


}
