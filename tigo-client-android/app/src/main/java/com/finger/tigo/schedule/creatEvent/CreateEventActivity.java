package com.finger.tigo.schedule.creatEvent;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Calendars;
import android.provider.CalendarContract.Events;
import android.support.annotation.IdRes;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.angel.black.baframeworkimagepick.ImagePickExecuter;
import com.angel.black.baframeworkimagepick.ImagePickIntentConstants;
import com.angel.black.baframeworkimagepick.ImagePickOnActivityResult;
import com.bumptech.glide.Glide;
import com.finger.tigo.BaseActivity;
import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.detail.EventDetailActivity;
import com.finger.tigo.items.FeedItem;
import com.finger.tigo.session.SessionManager;
import com.finger.tigo.util.MyLog;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.kosalgeek.android.photoutil.GalleryPhoto;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;


public class CreateEventActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener, View.OnClickListener, ICreateEventView, CustomGridColor.ItemColorClick {

    private static final String TAG = CreateEventActivity.class.getSimpleName();
    public static final String CREATE_EVENT = "create_event";


    private String idUser = "";

    private EditText nameEvent, localEvent, phoneEvent, description_event;
    public TextView lg1, lg2;
    ImageView image_position;
    //    private ColorPickerDialog dialog;
    private String Regime = "true";
    private String description;

    private RadioGroup rgRegime;

    public static ColorSquare square1;
    public static AlertDialog builder;
    private Context context;
    String[] strArray;

    private CustomGridColor listAdapter;
    private List<ItemDialog> feedItemss;
    public static String mSelectedColor = "#fcd059";
    public static String cate = "";
    private ImageView imv = null, imv_take, location;

    private TextView txt_location_creat, txt_location_gone;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 0;
    public Integer number;
    private String alamr = "10";
    String invite = "false";
    private ArrayList<Image> images = new ArrayList<>();
    private int REQUEST_CODE_PICKER = 2000;
    String selectedPhoto;
    GalleryPhoto galleryPhoto;
    private TextView dateStart, dateEnd, timeStart, timeEnd;
    private Calendar cal1, cal2;
    Spinner alarmEvent;
    Switch toggleFullDay, toggle_invite, toggle_joinfee;
    Toolbar toolbar;
    ProgressBar progressBar_location;
    private CreateEventPresenter presenter;
    private ProgressDialog pDialog;
    int checkAllday = 0;
    private SessionManager session;
    String calendarId;
    AccountManager mAccountManager;
    CountDownTimer cTimer = null;

    public static ArrayList<String> eventid = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);
        toolbar = (Toolbar) findViewById(R.id.toolbarCreateEvent);
        // an ban phim
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        ScrollView sv = (ScrollView) findViewById(R.id.scrollViewss);
        sv.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    onClickAway();
                }
            }
        });
        //this second listener is probably unnecessary but I put it in just in case there are weird edge cases where the scrollView is clicked but failed to gain focus
        sv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickAway();
            }
        });

        context = this;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.ic_cancel_create_event);
        session = new SessionManager(this);
        init();


        presenter = new CreateEventPresenter(this);

        imv_take.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImagePickExecuter.executeImagePickWithCameraToOneImage(CreateEventActivity.this, false);
            }
        });

        imv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImagePickExecuter.executeImagePickWithCameraToOneImage(CreateEventActivity.this, false);

            }
        });
    }

    protected void onClickAway() {
        //hide soft keyboard
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    public void init() {

        galleryPhoto = new GalleryPhoto(getApplicationContext());
        imv_take = (ImageView) findViewById(R.id.image_takephoto);
        dateStart = (TextView) findViewById(R.id.but_chonngay1);
        dateEnd = (TextView) findViewById(R.id.but_chonngay2);
        timeStart = (TextView) findViewById(R.id.but_chongio1);
        timeEnd = (TextView) findViewById(R.id.but_chongio2);
        nameEvent = (EditText) findViewById(R.id.nameEvent);
        localEvent = (EditText) findViewById(R.id.localEvent);
        txt_location_gone = (TextView) findViewById(R.id.txt_location_null);

        rgRegime = (RadioGroup) findViewById(R.id.rgRegime);
        phoneEvent = (EditText) findViewById(R.id.phone_event);
        alarmEvent = (Spinner) findViewById(R.id.spinner);
        description_event = (EditText) findViewById(R.id.description);
        progressBar_location = (ProgressBar) findViewById(R.id.progress_bar_location);
        image_position = (ImageView) findViewById(R.id.image_location);
        imv = (ImageView) findViewById(R.id.img_sk);
        txt_location_creat = (TextView) findViewById(R.id.txt_location_home);
        lg1 = (TextView) findViewById(R.id.b1);
        lg2 = (TextView) findViewById(R.id.b2);

        square1 = (ColorSquare) findViewById(R.id.menurow_square);
        square1.setBackgroundColor(Color.parseColor("#fcd059"));
        toggleFullDay = (Switch) findViewById(R.id.toggle_full_day);
        toggle_invite = (Switch) findViewById(R.id.toggle_invite);
        toggle_joinfee = (Switch) findViewById(R.id.toggle_joinfee);




        toggle_joinfee.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked == true) {
                    final Dialog dialog = new Dialog(CreateEventActivity.this);
                    dialog.setContentView(R.layout.dialog_joinfee);
                    WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
                    lp.dimAmount = 0.7f;
                    dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                    Button dialogButton = (Button) dialog.findViewById(R.id.but_ok_joinfee);
                    // if button is clicked, close the custom dialog
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    dialog.show();
                } else if (isChecked == false) {

                }
            }
        });


        cate = getResources().getString(R.string.festival);

        toggle_invite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
                if (isChecked == true) {
                    invite = "true";
                } else if (isChecked == false) {
                    invite = "false";
                }
            }
        });

        toggleFullDay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {


                if (isChecked == true) {
                    timeStart.setVisibility(View.GONE);
                    timeEnd.setVisibility(View.GONE);
                    checkAllday = 1;

                } else if (isChecked == false) {
                    timeEnd.setVisibility(View.VISIBLE);
                    timeStart.setVisibility(View.VISIBLE);
                    checkAllday = 0;

                }

            }
        });
        Regime = "true";
        strArray = getResources().getStringArray(R.array.alarm_array);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, strArray); //selected item will look like a spinner set from XML
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        alarmEvent.setAdapter(arrayAdapter);
        alarmEvent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                // Object item = parentView.getItemAtPosition(position);

                String item = parentView.getItemAtPosition(position).toString();
                switch (position) {
                    case 0:

                        alamr = 10 + "";
                        break;

                    case 1:
                        alamr = 20 + "";
                        break;

                    case 2:
                        alamr = 30 + "";
                        break;

                    case 3:
                        alamr = 60 + "";
                        break;

                    case 4:
                        alamr = 1440 + "";
                        break;

                    case 5:
                        alamr = 10080 + "";
                        break;
                    default:

                }

            }

            public void onNothingSelected(AdapterView<?> arg0) {// do nothing
            }

        });


        AccountManager am = (AccountManager) getSystemService(Context.ACCOUNT_SERVICE);
        Account[] accountsFromFirstApp = am.getAccountsByType(AppAccountManager.ACCOUNT_TYPE);
        idUser = am.getUserData(accountsFromFirstApp[0], AppAccountManager.USER_DATA_ID);
        calendarId = am.getUserData(accountsFromFirstApp[0], AppAccountManager.USER_DATA_CALENDAR);

        phoneEvent.setText(session.getphone_session());

        txt_location_creat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image_position.performClick();
            }
        });
        txt_location_gone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image_position.performClick();
            }
        });
        image_position.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                image_position.setVisibility(View.INVISIBLE);
                progressBar_location.setVisibility(View.VISIBLE);
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    Intent intent = builder.build(CreateEventActivity.this);
                    startActivityForResult(intent, 1);
                    txt_location_creat.setVisibility(View.GONE);


                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }

            }
        });


        square1.setOnClickListener(this);
        rgRegime.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {

                if (checkedId == R.id.rbPuplic) {
                    final Dialog dialog = new Dialog(CreateEventActivity.this);
                    dialog.setContentView(R.layout.dialog_raido_public);
                    WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
                    lp.dimAmount = 0.7f;
                    dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                    Button dialogButton = (Button) dialog.findViewById(R.id.but_ok_joinfee);
                    // if button is clicked, close the custom dialog
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    dialog.show();

                } else if (checkedId == R.id.noPublic) {
                    final Dialog dialog2 = new Dialog(CreateEventActivity.this);
                    dialog2.setContentView(R.layout.dialog_raido_nopublic);
                    WindowManager.LayoutParams lp2 = dialog2.getWindow().getAttributes();
                    lp2.dimAmount = 0.7f;
                    dialog2.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                    Button dialogButton2 = (Button) dialog2.findViewById(R.id.but_ok_joinfee);
                    // if button is clicked, close the custom dialog
                    dialogButton2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog2.dismiss();
                        }
                    });

                    dialog2.show();

                }

            }
        });


        dateStart.setOnClickListener(this);
        dateEnd.setOnClickListener(this);
        timeStart.setOnClickListener(this);
        timeEnd.setOnClickListener(this);
        imv.setOnClickListener(this);
        ArrayList<String> a = readCalendarEvent(this);
        for (int i = 0; i < a.size(); i++) {
            System.out.println(a.get(i));
        }

        Intent intent = getIntent();
        long startTime = intent.getLongExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, -1);
        long endTime = intent.getLongExtra(CalendarContract.EXTRA_EVENT_END_TIME, -1);
        boolean allDay = intent.getBooleanExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, false);
        cal1 = Calendar.getInstance();
        cal2 = Calendar.getInstance();

        MyLog.d("startTime=" + startTime + ", endTime=" + endTime);
        if(startTime > 0) {
            // from calendar empty cell click (+ New Event)
            cal1.setTimeInMillis(startTime);
            cal2.setTimeInMillis(startTime + DateUtils.HOUR_IN_MILLIS);

            if(allDay)
                toggleFullDay.setChecked(true);

        } else {
            // default (current time ~ current time + 1hour)
            cal2.setTimeInMillis(cal1.getTimeInMillis() + DateUtils.HOUR_IN_MILLIS);
        }

        setDateTime(cal1, cal2);
    }






    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.img_sk) {//                showFileChooser();


        } else if (i == R.id.menurow_square) {
            showAlertDialog(context);

        } else if (i == R.id.but_chonngay1) {
            selectDayStart();

        } else if (i == R.id.but_chonngay2) {
            selectDayEnd();

        } else if (i == R.id.but_chongio1) {
            selectHourStart();

        } else if (i == R.id.but_chongio2) {
            selecHourEnd();

        } else {
        }
    }

    public void onClickOK() {

        getAvailableCalendars(CreateEventActivity.this);
        String iduser = null;
        if (idUser != null)
            iduser = idUser.trim();


        if (checkAllday == 1) {

            cal1.set(Calendar.HOUR_OF_DAY, 0);
            cal1.set(Calendar.MINUTE, 0);
            cal1.set(Calendar.SECOND, 0);
            cal1.set(Calendar.MILLISECOND, 0);

//            cal2.setTimeInMillis(cal1.getTimeInMillis());
//            cal2.set(Calendar.DAY_OF_MONTH, cal1.get(Calendar.DAY_OF_MONTH) + 1);

//            cal2.set(Calendar.HOUR_OF_DAY, 23);
//            cal2.set(Calendar.MINUTE, 59);
//            cal2.set(Calendar.SECOND, 59);
//            cal2.set(Calendar.MILLISECOND, 0);

            long endTime = cal1.getTimeInMillis() + java.util.concurrent.TimeUnit.DAYS.toMillis(1);
            cal2.setTimeInMillis(endTime);

            // end time.this is time finish event

        }

        String title = nameEvent.getText().toString().trim();
        String location = localEvent.getText().toString().trim();
        String positionevent = txt_location_creat.getText().toString().trim() != null ? txt_location_creat.getText().toString() : null;
        String daystart = cal1.getTimeInMillis()+"";
        String dayfinish = cal2.getTimeInMillis()+"";
        String mode = Regime.toString().trim();
        String phoneevent = phoneEvent.getText().toString().trim() != null ? phoneEvent.getText().toString() : null;
        String lga = lg1.getText().toString().trim() != null ? lg1.getText().toString() : null;
        String lgb = lg2.getText().toString().trim() != null ? lg2.getText().toString() : null;
        String color = mSelectedColor.trim() !=null ? mSelectedColor.trim() : null;
        String category = cate.toString().trim() != null ? cate.toString().trim() : null ;
        String allowinvite = invite.toString().trim() != null ? invite.toString().trim() : null;
        String alarm = alamr.toString().trim() != null ? alamr.toString().trim() : null;
        String description = description_event.getText().toString().trim() != null ? description_event.getText().toString().trim() : null;
        String image = imv.getContentDescription() != null ? imv.getContentDescription().toString() : null;
        MyLog.d("image new :" + image);

        // long idSc = createEventWithName(CreateEventActivity.this, title, color, checkAllday, location, daystart, dayfinish);

        long idSc = getNewEventId();
        String idevent = idSc + "-" + iduser;

        presenter.attemCreateEvent(this, getString(R.string.create_event), idevent, iduser, title, location, positionevent, daystart, dayfinish, mode,
                phoneevent, lga, lgb, color,category, allowinvite, alarm, description, image , checkAllday);

    }

    public void getAvailableCalendars(Context c) {

        final String[] PROJECTION = new String[]{
                CalendarContract.Calendars._ID, Calendars.CALENDAR_DISPLAY_NAME
        };

        Cursor cur = null;
        ContentResolver cr = c.getContentResolver();
        Uri uri = Calendars.CONTENT_URI;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        cur = cr.query(uri, PROJECTION, null, null, null);

        while (cur.moveToNext()) {
            System.out.println(cur.getString(0) + " + " + cur.getString(1));
        }
    }

    public long getNewEventId() {
        ArrayList<String> a = readCalendarEvent(this);
        if (a != null && !a.isEmpty()) {
            System.out.println(Long.parseLong(a.get(a.size() - 1)) + 1);
            return Long.parseLong(a.get(a.size() - 1)) + 1;
        } else {
            return 1;
        }

    }

    public ArrayList<String> readCalendarEvent(Context context) {

        Cursor cursor = context.getContentResolver()
                .query(
                        Uri.parse("content://com.android.calendar/events"),
                        new String[]{"calendar_id", "_id", "title", "description",
                                "dtstart", "dtend", "eventLocation"},
                        Events.CALENDAR_ID
                                + " = " + calendarId,
                        null, null);
        cursor.moveToFirst();
        // fetching calendars name
        String CNames[] = new String[cursor.getCount()];

        // fetching calendars id
        eventid.clear();

        for (int i = 0; i < CNames.length; i++) {
            System.out.println(cursor.getString(1) + " ");
            System.out.println(cursor.getString(2) + " ");
            System.out.println(cursor.getString(3) + " ");
            eventid.add(cursor.getString(1));
            CNames[i] = cursor.getString(2);
            cursor.moveToNext();

        }

        return eventid;
    }

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    public void selectDayStart() {
        DatePickerDialog dpd = new DatePickerDialog(CreateEventActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        cal1.set(year, month, day);
                        if (cal1.getTimeInMillis() > cal2.getTimeInMillis()) {
                            cal2.setTimeInMillis(cal1.getTimeInMillis() + (60 * 1000 * 60));
                        }
                        setDateTime(cal1, cal2);
                    }
                }, cal1.get(Calendar.YEAR), cal1.get(Calendar.MONTH), cal1.get(Calendar.DAY_OF_MONTH));

        dpd.show();
    }

    public void setDateTime(Calendar cal1, Calendar cal2) {
        SimpleDateFormat dfStart = null, dfEnd = null;

        // date Start
        dfStart = new SimpleDateFormat("E dd/MM/yyyy", Locale.getDefault());
        String strDateStart = dfStart.format(cal1.getTime());
        dateStart.setText(strDateStart);

        // date End
        dfEnd = new SimpleDateFormat("E dd/MM/yyyy", Locale.getDefault());
        String strDateEnd = dfEnd.format(cal2.getTime());
        dateEnd.setText(strDateEnd);

        // hour start
        SimpleDateFormat SDTTimeStart = new SimpleDateFormat("hh:mm a", Locale.getDefault());
        String strTimeStart = SDTTimeStart.format(cal1.getTime());
        timeStart.setText(strTimeStart);

        // hour end
        SimpleDateFormat SDTimeEnd = new SimpleDateFormat("hh:mm a", Locale.getDefault());
        String strTimeEnd = SDTimeEnd.format(cal2.getTime());
        timeEnd.setText(strTimeEnd);

    }


    Calendar caSave = Calendar.getInstance();

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    public void selectDayEnd() {
        long a = cal2.getTimeInMillis();
        caSave.setTimeInMillis(a);
        DatePickerDialog dpd = new DatePickerDialog(CreateEventActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {

                        caSave.set(year, month, day);

                        if (caSave.getTimeInMillis() > cal1.getTimeInMillis()) {
                            cal2.set(year, month, day);
                            setDateTime(cal1, cal2);
                        }

                    }
                }, cal2.get(Calendar.YEAR), cal2.get(Calendar.MONTH), cal2.get(Calendar.DAY_OF_MONTH));

        dpd.show();
    }

    public void selectHourStart() {

        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(CreateEventActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                cal1.set(Calendar.HOUR_OF_DAY, selectedHour);
                cal1.set(Calendar.MINUTE, selectedMinute);

                if (cal1.getTimeInMillis() > cal2.getTimeInMillis()) {
                    long a = cal1.getTimeInMillis() + (60 * 1000 * 60);
                    cal2.setTimeInMillis(a);

                }
                setDateTime(cal1, cal2);

            }
        }, cal1.get(Calendar.HOUR_OF_DAY), cal1.get(Calendar.MINUTE), false);//Yes 24 hour time
        mTimePicker.setTitle(getResources().getString(R.string.select_time));
        mTimePicker.show();
    }

    public void selecHourEnd() {

        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(CreateEventActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                cal2.set(Calendar.HOUR_OF_DAY, selectedHour);
                cal2.set(Calendar.MINUTE, selectedMinute);

                if (cal1.getTimeInMillis() > cal2.getTimeInMillis()) {
                    long a = cal2.getTimeInMillis() + (60 * 24 * 60 * 1000);
                    cal2.setTimeInMillis(a);

                }

                setDateTime(cal1, cal2);

            }
        }, cal2.get(Calendar.HOUR_OF_DAY), cal2.get(Calendar.MINUTE), false);//Yes 24 hour time
        mTimePicker.setTitle(getResources().getString(R.string.time_end));
        mTimePicker.show();
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resutlCode, Intent data) {
        super.onActivityResult(requestCode, resutlCode, data);
        if (requestCode == 1) {
            if (resutlCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                String adress = String.format("%s", place.getAddress());
                final LatLng location = place.getLatLng();
                if (place.getAddress() == null) {

                    txt_location_creat.setText(adress);
                    txt_location_creat.setVisibility(View.GONE);
                    txt_location_gone.setVisibility(View.VISIBLE);
                    txt_location_gone.setText("Not select location !");

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            context);
                    // set title
                    alertDialogBuilder.setTitle("Location inaccurate !");
                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Reselect!")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    image_position.performClick();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    // show it
                    alertDialog.show();
                    //  LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;
                    //  Toast.makeText(context, "null...." +adress, Toast.LENGTH_SHORT).show();


                } else {
                    txt_location_gone.setVisibility(View.GONE);
                    txt_location_creat.setVisibility(View.VISIBLE);
                    txt_location_creat.setText(adress);

                }
                Log.d("mmmmmmmm", "onActivityResult: " + adress);

                lg1.setText(location.latitude + "");
                lg2.setText(location.longitude + "");
            }
        } else if(requestCode == ImagePickIntentConstants.REQUEST_IMAGE_PICK) {
            if(resutlCode == RESULT_OK) {
                String imagePath = ImagePickOnActivityResult.onActivityResultAndGetOneImagePath(data);
                imv.setContentDescription(imagePath);
                Glide.with(this)
                        .load(imagePath)
                        .placeholder(R.drawable.img_loading)
                        .fitCenter()
                        .into(imv);
            }

        }

        image_position.setVisibility(View.VISIBLE);
        progressBar_location.setVisibility(View.GONE);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId == R.id.rbPuplic) {
            Regime = "true";

        } else if (checkedId == R.id.noPublic) {
            Regime = "false";

        }
    }

    ////////////////////////////SHOW DIALOG COLOR\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    private void showAlertDialog(Context context) {
        builder = new AlertDialog.Builder(context).create();
        // Prepare grid view
        GridView gridView = new GridView(this);
        feedItemss = new ArrayList<ItemDialog>();
        listAdapter = new CustomGridColor(this, feedItemss, this);
        gridView.setAdapter(listAdapter);
        String[] color_array = getResources().getStringArray(R.array.default_color_choice_values);
        String[] color_text = getResources().getStringArray(R.array.default_color_choice_text);

        if (color_array != null && color_array.length > 0) {
            for (int i = 0; i < color_array.length; i++) {
                ItemDialog item = new ItemDialog();
                item.setColor(color_array[i]);
                item.setColor_text(color_text[i]);
                feedItemss.add(item);
            }
            listAdapter.notifyDataSetChanged();
        }

        gridView.setNumColumns(4);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final ItemDialog item = feedItemss.get(position);
                square1.setBackgroundColor(Color.parseColor(item.getColor()));
                mSelectedColor = item.getColor();
                //cate = item.getColor_text().toString();
                builder.dismiss();
            }
        });
        builder.setView(gridView);
        builder.setTitle(getString(R.string.color_event));
        builder.show();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create_event, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void createEventWithName(Context ctx, String entryid, String name, String color, int allDay, String location,
                                    String startMillis, String endMillis, String idEventTigo) {

        ContentValues cv = new ContentValues();

        cv.put(CalendarContract.Events.TITLE, name);
        cv.put(Events._ID, entryid);
        cv.put(Events.DTSTART, Long.parseLong(startMillis));
        cv.put(Events.DTEND, Long.parseLong(endMillis));
        cv.put(Events.CALENDAR_ID, calendarId);
        cv.put(Events.EVENT_COLOR, Color.parseColor(color));
        cv.put(Events.ALL_DAY, allDay);
        cv.put(Events.EVENT_LOCATION, location);
        cv.put(Events.EVENT_TIMEZONE, TimeZone.getDefault().toString());
        cv.put(Events.SYNC_DATA1, idEventTigo);
        //cv.put(Events.RRULE, "FREQ=DAILY;INTERVAL=2");


        if (ActivityCompat.checkSelfPermission(CreateEventActivity.this, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
//        Uri eventsUri = Uri.parse("content://com.android.calendar/events");
//        ctx.getContentResolver().insert(eventsUri, cv);

//       ctx.getContentResolver().insert(asSyncAdapter(Events.CONTENT_URI, account.name, account.type), cv);

    }

    private static Uri asSyncAdapter(Uri uri, String account, String accountType) {
        return uri.buildUpon()
                .appendQueryParameter(android.provider.CalendarContract.CALLER_IS_SYNCADAPTER, "true")
                .appendQueryParameter(Calendars.ACCOUNT_NAME, account)
                .appendQueryParameter(Calendars.ACCOUNT_TYPE, accountType).build();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {//Write your logic here
            this.finish();
            return true;
        } else if (i == R.id.okCreate) {//       showProgress(getResources().getString(R.string.create_event));
            //        startTimer();0

            final String name_event = nameEvent.getText().toString();
            if (!isValidation(name_event)) {
                nameEvent.setError("Invalid Name");
            }

            final String local = localEvent.getText().toString();
            if (!isValidation(local)) {
                localEvent.setError("Invalid Local");
            }
            onClickOK();

            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void createSuccess(String idSc, FeedItem newEvent) {
        String[] temp = newEvent.getId_sc().split("-");
        int checkAllday = 0;
        if (newEvent.allDay.toString().equalsIgnoreCase("true"))
            checkAllday = 1;
//                createEventWithName(CreateEventActivity.this, temp[0],item.status, item.color, checkAllday,
//                        item.adlodress, item.daystart, item.dayend, item.getId_sc());

        AppAccountManager.updateCalendarSync(this, idSc, newEvent.color, Boolean.valueOf(newEvent.allDay), newEvent.daystart,
                newEvent.dayend, newEvent.status, newEvent.address);


        Intent i = new Intent(CreateEventActivity.this, EventDetailActivity.class);

        i.putExtra("mDetailsHomeActivity", idSc + "");
        // i.putExtra("phone_event", phone_event + "");
        showToast(R.string.create_event_success);

        startActivity(i);
        hideProgress();
        finish();
    }


    @Override
    public void createFail(String message, Throwable cause) {
//        if (key == AsyncCreateEvent.KEY_ERROR_ONE) {
//            // todo something
//            showToast(R.string.can_not_create_event);
//        } else {
//            showToast(R.string.network_not_connect);
//        }

        showToast(message);
    }

    @Override
    public void checkInformation(String check) {
        hideProgress();

        switch (check) {
            case CreateEventPresenter.KEY_ERROR_IDUSER:

                break;
            case CreateEventPresenter.STATUS:
                showToast(R.string.name_event_empty);
                break;
            case CreateEventPresenter.BITMAP:
                showToast(R.string.image_event_empty);
                break;
            case CreateEventPresenter.POSTION:
                showToast(R.string.position_event_empty);
                break;
            case CreateEventPresenter.LOCATION:
                showToast(R.string.location_event_empty);
                break;
            case CreateEventPresenter.CATEGORY:
                showToast(R.string.category_event_empty);
                break;
            case CreateEventPresenter.DESCRIPPTION:
                showToast(R.string.description_event_empty);
                break;
            default:

                break;
        }
    }


    // validating email id
    private boolean isValidation(String string) {
        if (string != null && string.length() > 1) {
            return true;
        }
        return false;
    }

    @Override
    public void onItemClick(int position) {
        final ItemDialog item = feedItemss.get(position);
        builder.dismiss();
        mSelectedColor = item.getColor();
        square1.setBackgroundColor(Color.parseColor(item.getColor()));
        cate = item.getColor_text();
    }


}

