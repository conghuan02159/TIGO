package com.finger.tigo.addfriend.adapter;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.finger.tigo.R;
import com.finger.tigo.items.FeedItem;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by Duong on 4/3/2017.
 */

public class FriendRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private List<FeedItem> feedItems;
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    public TextView name ;
    public TextView description ;
    public TextView dayStart;
    // The minimum amount of items to have below your current scroll position
    // before loading more.
    private int visibleThreshold;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;

    public FriendRecyclerAdapter( List<FeedItem> persons, RecyclerView mRecyclerView){
        this.feedItems = persons;

        if(mRecyclerView.getLayoutManager() instanceof LinearLayoutManager){
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                     super.onScrolled(recyclerView, dx, dy);

                        totalItemCount = linearLayoutManager.getItemCount();
                        visibleThreshold = linearLayoutManager.getChildCount();
                        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                        if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                            // End has been reached
                            // Do something
                            if (onLoadMoreListener != null) {
                                onLoadMoreListener.onLoadmore();
                            }

                            loading = true;
                        }
                    }

            });
        }


    }
    public interface OnLoadMoreListener{
        void onLoadmore();
    }

    @Override
    public int getItemCount() {
        return feedItems == null ? 0 : feedItems.size();
    }
    @Override
    public int getItemViewType(int position) {
        return feedItems.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder vh;
        if(viewType == VIEW_ITEM){
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_event_friend, parent, false);
            vh = new MyViewHolder(v);
            return  vh;
        }else if(viewType == VIEW_PROG) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_event_progess, parent, false);
            vh = new ProgressViewHolder(v);
            return vh;
        }


        return null;
    }



    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final FeedItem item = feedItems.get(position);

        if(viewHolder instanceof MyViewHolder) {
            long dayStart = Long.parseLong(item.getdaystart());
            long dayEnd = Long.parseLong(item.getDateend());
            SimpleDateFormat dsft = new SimpleDateFormat("E, dd MMM yyyy", Locale.getDefault());
            final String strStDate = dsft.format(dayStart);
            ((MyViewHolder)viewHolder).dayStart.setText(strStDate);

            ((MyViewHolder)viewHolder).name.setText(item.getName());

            // checking for null feed Description
            if (item.getUrl() != null) {
                ((MyViewHolder)viewHolder).description.setText(item.getDesciption());
                ((MyViewHolder)viewHolder).description.setVisibility(View.VISIBLE);
            } else {
                // description is null, remove from the view
                ((MyViewHolder)viewHolder).description.setVisibility(View.GONE);
            }

            Glide.with(((MyViewHolder) viewHolder).imgEvent.getContext()).load(item.getImge()).animate(android.R.anim.fade_in)
                    .error(R.drawable.background_hoa_tigo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.background_hoa_tigo)
                    .into(((MyViewHolder)viewHolder).imgEvent);
            Calendar curent = Calendar.getInstance();
            if(curent.getTimeInMillis()> dayEnd){
//                ((MyViewHolder) viewHolder).layoutItem.setBackgroundColor(activity.getResources().getColor(R.color.colorBlackTran));
                ((MyViewHolder) viewHolder).imgOldevent.setVisibility(View.VISIBLE);
            }else{
//                ((MyViewHolder) viewHolder).layoutItem.setBackgroundColor(activity.getResources().getColor(R.color.white));
                ((MyViewHolder) viewHolder).imgOldevent.setVisibility(View.GONE);
            }
        }else {
            if(feedItems.size() >= 10)
            ((ProgressViewHolder) viewHolder).progressBar.setIndeterminate(true);
            else {
                ((ProgressViewHolder) viewHolder).progressBar.setIndeterminate(false);
                ((ProgressViewHolder) viewHolder).progressBar.setVisibility(View.GONE);
            }
        }

    }

    public void setLoaded(){
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener listener){
        this.onLoadMoreListener = listener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        public TextView name ;

        public TextView description ;

         public TextView dayStart;

        ImageView imgEvent;
        LinearLayout imgOldevent;


        public MyViewHolder(View rowView) {
            super(rowView);
            name = (TextView)rowView.findViewById(R.id.name_event);
            description = (TextView)rowView.findViewById(R.id.tv_description);
            dayStart = (TextView)rowView.findViewById(R.id.tv_day_start);
            imgEvent = (ImageView)rowView.findViewById(R.id.im_event);
            imgOldevent = (LinearLayout) rowView.findViewById(R.id.img_oldevent);

        }
    }
    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progress_more);
        }
    }
}