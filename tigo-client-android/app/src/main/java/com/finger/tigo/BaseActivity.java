package com.finger.tigo;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.widget.Toast;

import com.finger.tigo.app.ApiExecuter;
import com.finger.tigo.common.dialog.LoadingProgressDialog;

import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayOutputStream;

/**
 * Created by Finger-kjh on 2017-05-24.
 */

public abstract class BaseActivity extends AppCompatActivity implements ApiExecuter.ApiProgressListener {

    private ProgressDialog mLoadingProgressDialog;
    private Toast mToast;

    public ProgressDialog createProgressDialog() {
        return new LoadingProgressDialog(this, R.style.no_bg_progress_dialog);
    }

    public void showProgress() {
        if(!isFinishing()) {
            if(mLoadingProgressDialog == null) {
                mLoadingProgressDialog = createProgressDialog();
            }

            if(!isFinishing() && !mLoadingProgressDialog.isShowing()) {
                mLoadingProgressDialog.setMessage("");
                mLoadingProgressDialog.dismiss();
                mLoadingProgressDialog.show();
            }
        }
    }

    public void showProgress(String message) {
        if(!isFinishing()) {
            if(mLoadingProgressDialog == null) {
                mLoadingProgressDialog = createProgressDialog();
            }

            if(mLoadingProgressDialog.isShowing()) {
                mLoadingProgressDialog.setMessage(message);
            } else {
                mLoadingProgressDialog.dismiss();
                mLoadingProgressDialog.setMessage(message);
                mLoadingProgressDialog.show();
            }
        }
    }

    public void hideProgress() {
        if(!isFinishing()) {
            if(mLoadingProgressDialog != null && mLoadingProgressDialog.isShowing()) {
                mLoadingProgressDialog.dismiss();
            }
        }
    }

    public void showToast(int strResId) {
        this.showToast(getResources().getString(strResId));
    }

    public void showToast(String str) {
        if(mToast == null) {
            mToast = Toast.makeText(this, str, Toast.LENGTH_SHORT);
        } else {
            mToast.setText(str);
        }
        mToast.show();
    }

    @Override
    public void onStartApi(String progressMsg) {
        if(StringUtils.isEmpty(progressMsg))
            showProgress();
        else
            showProgress(progressMsg);
    }

    @Override
    public void onFinishApi() {
        hideProgress();
    }

   public String getReducedImage(String file, int width, int height){
        BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
        bmpFactoryOptions.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
        int heightRatio = (int)Math.ceil(bmpFactoryOptions.outHeight/(float)height);
        int widthRatio = (int)Math.ceil(bmpFactoryOptions.outWidth/(float)width);
        if (heightRatio > 1 || widthRatio > 1)
        {
            if (heightRatio > widthRatio)
            {
                bmpFactoryOptions.inSampleSize = heightRatio;
            } else {
                bmpFactoryOptions.inSampleSize = widthRatio;
            }
        }
        bmpFactoryOptions.inJustDecodeBounds = false;
        bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }
}
