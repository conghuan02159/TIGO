package com.finger.tigo.detail.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.finger.tigo.R;
import com.finger.tigo.addfriend.DetailFriendActivity;
import com.finger.tigo.app.ApiExecuter;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.detail.ReplayCommentActivity;
import com.finger.tigo.detail.UpdateCommentActivity;
import com.finger.tigo.items.FeedItem;
import com.finger.tigo.session.SessionManager;
import com.finger.tigo.util.MyLog;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.finger.tigo.R.id.txt_comment_Name;

public class CommentAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private Activity activity;
    public List<FeedItem> feedItems;
    private String idUser;
    private SessionManager session;
    TextView textView;
    private TextView txtReplyComment;
    private RecyclerView recyclerViewRply;
    private TextView ViewAllComment;
    private CircleImageView imagename;
    private TextView commentName,commentFirstReply;
    private LinearLayout layoutcommentRplay;
    private TextView TimeComment;
    private TextView dialogShow;
    FeedItem item;
    String imageSS;
    private CircleImageView image;
    EditText edtEdit;
    ProgressDialog pDialog;
    String id;

    private boolean runUpdateComment = false;

    public CommentAdapter(Activity activity, List<FeedItem> feedItems) {
        this.activity = activity;
        this.feedItems = feedItems;
    }

    @Override
    public int getCount() {
        return feedItems.size();
    }
    @Override
    public Object getItem(int position) {
        return feedItems.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;

        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.item_comment, null);
        viewHolder = new ViewHolder();


        txtReplyComment=(TextView)convertView.findViewById(R.id.txtreply);
        imagename=(CircleImageView)convertView.findViewById(R.id.imagename);
        commentName=(TextView)convertView.findViewById(R.id.commentName);
        commentFirstReply=(TextView)convertView.findViewById(R.id.commentFirstReply);
        layoutcommentRplay=(LinearLayout)convertView.findViewById(R.id.layoutcommentRplay);
        TimeComment=(TextView)convertView.findViewById(R.id.txtTimecomment);
        dialogShow=(TextView)convertView.findViewById(R.id.dialogShow);
        session = new SessionManager(activity);
        idUser= session.get_id_user_session()+"";
        imageSS=session.getLpic_session();

        TextView comment_name = (TextView) convertView.findViewById(txt_comment_Name);
        textView = (TextView) convertView.findViewById(R.id.textViewchat);

        TextView comment_time = (TextView) convertView.findViewById(R.id.txt_commenttime);
        CircleImageView Image_Comment = (CircleImageView) convertView.findViewById(R.id.image_comment);
        ViewAllComment=(TextView)convertView.findViewById(R.id.ViewAllComment);
        final FeedItem item = feedItems.get(position);

        ViewAllComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(activity,ReplayCommentActivity.class);
                intent.putExtra("id_comment",feedItems.get(position).getIdRComment());
                intent.putExtra("textComment",feedItems.get(position).getStatusComment());
                intent.putExtra("avartar",feedItems.get(position).getAvatarComment());
                intent.putExtra("namecomment",feedItems.get(position).getNamecomment());
                activity.startActivity(intent);
            }
        });

        Glide.with(activity).load(item.getAvatarComment())
                .placeholder(R.drawable.avatar).error(R.drawable.avatar)
                .into(Image_Comment);

        Glide.with(activity).load(item.getProfilePicReplay())
                .placeholder(R.drawable.avatar).error(R.drawable.avatar)
                .into(imagename);
        viewHolder.ln = (LinearLayout) convertView.findViewById(R.id.ln_layout);
        viewHolder.moreMenuBtn = (RadioButton) convertView.findViewById(R.id.dialogShow);
        final ViewHolder finalViewholder = viewHolder;
        Intent iit = new Intent(activity, UpdateCommentActivity.class);
        iit.putExtra("id_user", item.getId_sc());

        comment_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(activity, DetailFriendActivity.class);
                i.putExtra(DetailFriendActivity.ID_USER, feedItems.get(position).getIdComment()+"");
                i.putExtra(DetailFriendActivity.NAME, feedItems.get(position).getNamecomment());
                i.putExtra(DetailFriendActivity.PHONE, feedItems.get(position).getPhone());
                i.putExtra(DetailFriendActivity.PROFILE_PIC, feedItems.get(position).getAvatarComment());
                activity.startActivity(i);
            }
        });

        txtReplyComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(activity, ReplayCommentActivity.class);
                // intent id comment qua replay
                intent.putExtra("id_comment",feedItems.get(position).getIdRComment());
                intent.putExtra("textComment",feedItems.get(position).getStatusComment());
                intent.putExtra("avartar",feedItems.get(position).getAvatarComment());
                intent.putExtra("namecomment",feedItems.get(position).getNamecomment());
                activity.startActivity(intent);
            }
        });

        if(idUser.equals(feedItems.get(position).getIdComment())) {

             dialogShow.setVisibility(View.VISIBLE);
        }
        else {
            dialogShow.setVisibility(View.GONE);
        }

        MyLog.e("testcomment",idUser+"---"+feedItems.get(position).getIdComment());

        dialogShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (idUser.equals(feedItems.get(position).getIdComment())) {
                    PopupMenu popup = new PopupMenu(activity, finalViewholder.moreMenuBtn, Gravity.RIGHT);
                    //Inflating the Popup using xml file
                    popup.getMenuInflater().inflate(R.menu.comment_menu, popup.getMenu());

                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem items) {
                            if (items.getItemId() == R.id.delete) {
                                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
                                builder.setMessage(R.string.delete_comment)
                                        .setCancelable(false)
                                        .setPositiveButton(R.string.notification_yes, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int idd) {
                                                deletecomment(feedItems.get(position).getIdRComment(), position);
                                            }
                                        })
                                        .setNegativeButton(R.string.notification_no, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });
                                android.app.AlertDialog alert = builder.create();
                                //Setting the title manually
                                alert.setTitle(R.string.title_comment);
                                alert.show();
//
                            } else if (items.getItemId() == R.id.update) {
                                Intent it = new Intent(activity, UpdateCommentActivity.class);
                                it.putExtra("statuscomment", feedItems.get(position).getStatusComment());
                                it.putExtra("id_comment", feedItems.get(position).getIdRComment());
                                activity.startActivity(it);
                                Log.e("kkukuku",feedItems.get(position).getStatusComment()+"----"+feedItems.get(position).getIdRComment());

                            } else {

                            }
                            return true;
                        }
                    });
                    popup.show();
                }

            }
        });

        if (!TextUtils.isEmpty(item.getStatusComment())) {
            textView.setText(item.getStatusComment());
            textView.setVisibility(View.VISIBLE);
        } else {
            // status is empty, remove from view
            textView.setVisibility(View.GONE);
        }
        CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
                Long.parseLong(item.getTimecomment()),
                System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);

        comment_time.setText(timeAgo);
        comment_name.setText(item.getNamecomment());
        countmentRply(item.getId_sc());

        commentName.setText(item.getNameReplay());
        commentFirstReply.setText(item.getContainerCommentRply());
        TimeComment.setText(item.getCurrendateCommentRply());
        ViewAllComment.setText(activity.getResources().getString(R.string.view_comment)+ "(" +item.getCountcommentplay()+ ")");

        if(feedItems.get(position).getContainerCommentRply()==null)
        {
            layoutcommentRplay.setVisibility(View.GONE);
        }
        else
        {
            layoutcommentRplay.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    public boolean isRunUpdateComment() {
        return runUpdateComment;
    }

    public void setRunUpdateComment(boolean runUpdateComment) {
        this.runUpdateComment = runUpdateComment;
    }

    static class ViewHolder
    {
        LinearLayout ln;
        RadioButton moreMenuBtn;

    }

    private void deletecomment(final String Id, final int position){
        ApiExecuter.ApiProgressListener apiProgressListener = (ApiExecuter.ApiProgressListener) activity;

        ApiExecuter.requestApi(apiProgressListener, AppConfig.URL_DELETECOMMENT, null, new ApiExecuter.ApiResultListener() {
            @Override
            public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                feedItems.remove(position);
                notifyDataSetChanged();
            }

            @Override
            public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {
                Toast.makeText(activity, errMessage, Toast.LENGTH_LONG).show();
            }
        }, "id_sc", Id);

    }

    private void countmentRply(final String id_sc)
    {
        String tag_count_comment="count_comment";
        StringRequest strReq=new StringRequest(Request.Method.POST,AppConfig.URL_COUNT_COMMENT_RPLY,
                new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

            }
        },new Response.ErrorListener()
        {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        })
        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("id_sc", id_sc);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(strReq);

    }



}
