package com.finger.tigo.addfriend;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.friend.adapter.SyncTigonAdapter;
import com.finger.tigo.items.ItemNoti;
import com.finger.tigo.session.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Duong on 5/17/2017.
 */

public class ListSyncActivity extends Activity implements SyncTigonAdapter.itemClickNotificationFriend {

    RecyclerView recyclerSyncTigon;
    private List<ItemNoti> listSync;
    SyncTigonAdapter adapter;
    SessionManager session;
    String idUser;
    
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_add_friend);
        recyclerSyncTigon = (RecyclerView) findViewById(R.id.recycler_sync_tigon);
        session = new SessionManager(this);

        idUser = AppAccountManager.getAppAccountUserId(this);
        initListSync();
        setSyncNewTigon();
        
    }

    private void initListSync() {
        listSync = new ArrayList<>();
        adapter = new SyncTigonAdapter(ListSyncActivity.this, listSync, this);

        recyclerSyncTigon.setHasFixedSize(true);
        RecyclerView.LayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(ListSyncActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerSyncTigon.setLayoutManager(horizontalLayoutManagaer);

        recyclerSyncTigon.setItemAnimator(new DefaultItemAnimator());
        recyclerSyncTigon.setAdapter(adapter);

//        recyclerSyncTigon.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerSyncTigon, new RecyclerTouchListener.ClickListener() {
//            @Override
//            public void onClick(View view, int position) {
//                ItemNoti item = feedItems.get(position);
//                updateWatchedFriend( item.getIdFriend());
//                Intent i = new Intent(getActivity(), DetailFriendActivity.class);
//                i.putExtra(LISTFRIEND, item.getIdFriend());
//                i.putExtra(DetailFriendActivity.ID_USER, item.getIdFriend());
//                i.putExtra(DetailFriendActivity.NAME,item.getName());
//                i.putExtra(DetailFriendActivity.PROFILE_PIC, item.getProfilePic());
//                i.putExtra(DetailFriendActivity.PHONE, item.getPhone());
//                startActivity(i);
//
//            }
//
//            @Override
//            public void onLongClick(View view, int position) {
//
//            }
//        }));

    }

    public  void setSyncNewTigon() {

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_SYNC_NEW, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response !=null)
                    parseJsonFeed(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("iduser", idUser);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
        // }
    }
    public void parseJsonFeed(String response){
        try {
            JSONObject jObj = new JSONObject(response);
            JSONArray feedArray = jObj.getJSONArray("feed");
            listSync.clear();
            for (int i = 0; i < feedArray.length(); i++) {
                JSONObject feedObj = (JSONObject) feedArray.get(i);
                ItemNoti item = new ItemNoti();
                item.setName(feedObj.getString("name"));
                item.setIdFriend(feedObj.getString("iduser"));
                item.setPhone(feedObj.getString("phone"));
                item.setProfilePic(feedObj.getString("profilePic"));
                listSync.add(item);

            }

            adapter.notifyDataSetChanged();



        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addFriend(int postion, String idFriend, String name, String phone, SyncTigonAdapter.MyViewHolder v) {
        addNewFriend(idUser, idFriend, name, phone);
    }
    private void addNewFriend( final String iduser, final String idfriend, final String name, final String phone) {
        // Tag used to cancel the request

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADDNEWFRIEND, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {


            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("iduser", iduser);
                params.put("idfriend", idfriend);
                params.put("name", name);
                params.put("phone", phone);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }

}
