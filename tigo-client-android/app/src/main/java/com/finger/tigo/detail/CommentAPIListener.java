package com.finger.tigo.detail;

/**
 * Created by Finger-kjh on 2017-06-08.
 */

public interface CommentAPIListener {
    void onSuccessCommentAPI(String apiUrl);
}
