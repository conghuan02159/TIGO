package com.finger.tigo.schedule.creatEvent;

import com.finger.tigo.items.FeedItem;

import java.util.List;

/**
 * Created by Conghuan on 4/11/2017.
 */

public interface OnCreatefinisheListener {
    void finishsuccess(String susscess, String id_sc, List<FeedItem> listItem);
    void finishfail(String success, int key);
    void check_information(String information);
}
