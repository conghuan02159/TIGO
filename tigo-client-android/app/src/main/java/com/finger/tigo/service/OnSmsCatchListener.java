package com.finger.tigo.service;

/**
 * Created by Duong on 3/24/2017.
 */

public interface OnSmsCatchListener<T> {
    void onSmsCatch(String message);
}
