package com.finger.tigo;


import android.app.Fragment;
import android.widget.Toast;

import com.finger.tigo.app.ApiExecuter;
import com.finger.tigo.common.dialog.LoadingProgressDialog;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by Finger-kjh on 2017-05-24.
 */

public class BaseFragment extends Fragment implements ApiExecuter.ApiProgressListener {

    private LoadingProgressDialog mLoadingProgressDialog;

    public void showProgress() {
        if(!isRemoving()) {
            if(mLoadingProgressDialog == null) {
                mLoadingProgressDialog = new LoadingProgressDialog(getActivity(), R.style.no_bg_progress_dialog);
            }

            mLoadingProgressDialog.setMessage("");

            if(!mLoadingProgressDialog.isShowing()) {
                mLoadingProgressDialog.dismiss();
                mLoadingProgressDialog.show();
            }
        }
    }

    public void showProgress(String message) {
        if(!isRemoving()) {
            if(mLoadingProgressDialog == null) {
                mLoadingProgressDialog = new LoadingProgressDialog(getActivity(), R.style.no_bg_progress_dialog);
            }

            if(mLoadingProgressDialog.isShowing()) {
                mLoadingProgressDialog.setMessage(message);
            } else {
                mLoadingProgressDialog.dismiss();
                mLoadingProgressDialog.setMessage(message);
                mLoadingProgressDialog.show();
            }
        }
    }

    public void hideProgress() {
        if(!isRemoving()) {
            if(mLoadingProgressDialog != null && mLoadingProgressDialog.isShowing()) {
                mLoadingProgressDialog.dismiss();
            }
        }
    }

    public void showToast(int strResId) {
        this.showToast(getResources().getString(strResId));
    }

    public void showToast(String str) {
        if(getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).showToast(str);
        } else {
            Toast.makeText(getActivity(), str, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onStartApi(String progressMsg) {
        if(StringUtils.isEmpty(progressMsg)) {
            showProgress();
        } else {
            showProgress(progressMsg);
        }
    }

    @Override
    public void onFinishApi() {

    }
}
