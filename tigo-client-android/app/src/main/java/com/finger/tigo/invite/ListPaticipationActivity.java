package com.finger.tigo.invite;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.finger.tigo.BaseActivity;
import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.addfriend.DetailFriendActivity;
import com.finger.tigo.app.ApiExecuter;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.invite.adapter.MyRecyclerAdapterNewList;
import com.finger.tigo.invite.adapter.RecyclerAdapterAcceptList;
import com.finger.tigo.items.ItemAddFr;
import com.finger.tigo.util.MyLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class ListPaticipationActivity extends BaseActivity {


    public  static final String TAG = "DetailsPersons";

    private List<ItemAddFr> mPersonList;
    private RecyclerAdapterAcceptList adapter;
    private MyRecyclerAdapterNewList adapterNewList;
    private RecyclerView recyclerView,recyclerViewNotFriend;
    private LinearLayout lnNotData;
    private LinearLayout lnListData;
    private TextView tvCount;

    private String idSc;
    private String idUser;
    TextView bt_addFriend;
    ItemAddFr item;
    View view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paticipation_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvCount = (TextView) findViewById(R.id.tv_count_participa);
        lnListData = (LinearLayout) findViewById(R.id.ln_list_participa);

        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setTitle(R.string.detailed_list);
        Intent intent = getIntent();
        idSc= intent.getStringExtra("particition");

        idUser = AppAccountManager.getAppAccountUserId(this);

        findViews();
        setRecyclerView();

    }
    private void findViews() {

        lnNotData = (LinearLayout) findViewById(R.id.layout_not_data);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        showProgress(getResources().getString(R.string.loading));
        bt_addFriend = (TextView) findViewById(R.id.addfriend);

        recyclerViewNotFriend=(RecyclerView)findViewById(R.id.recyclerViewNotFriend);

    }

    public void setRecyclerView() {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(ListPaticipationActivity.this);
        recyclerView.setLayoutManager(llm);

        recyclerViewNotFriend.setHasFixedSize(true);
        LinearLayoutManager linear=new LinearLayoutManager(ListPaticipationActivity.this);
        recyclerViewNotFriend.setLayoutManager(linear);


        mPersonList = new ArrayList<>();
        // Populate list
        conFigFriend(idSc);

        adapter = new RecyclerAdapterAcceptList(mPersonList, idUser, ListPaticipationActivity.this);
        recyclerView.setAdapter(adapter);

        adapterNewList=new MyRecyclerAdapterNewList(mPersonList,idUser,ListPaticipationActivity.this);
        recyclerViewNotFriend.setAdapter(adapterNewList);

        adapter.setmItemClickListener(new RecyclerAdapterAcceptList.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Intent i = new Intent(ListPaticipationActivity.this, DetailFriendActivity.class);
                ItemAddFr item = mPersonList.get(position);
                i.putExtra(DetailFriendActivity.ID_USER, item.getIdFriend());
                i.putExtra(DetailFriendActivity.NAME,item.getName());
                i.putExtra(DetailFriendActivity.PROFILE_PIC, item.getProfilePic());
                i.putExtra(DetailFriendActivity.PHONE, item.getPhone());
                startActivity(i);
            }
        });
    }
    private void conFigFriend(final String idSc) {
        // Tag used to cancel the request
        String loading =  getResources().getString(R.string.loading);
        ApiExecuter.requestApiProgressMsg(AppConfig.URL_ACCEPTED,loading, new ApiExecuter.ApiResultListener(){

            @Override
            public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    JSONArray feedArray = jObj.getJSONArray("feed");
                    MyLog.e("lalalalala",feedArray+"");
                    mPersonList.clear();
                    for (int i = 0; i < feedArray.length(); i++) {

                        JSONObject feedObj = (JSONObject) feedArray.get(i);
                        item = new ItemAddFr();

                        item.setName(feedObj.getString("name"));
                        item.setIdFriend(feedObj.getString("id_user"));
                        item.setProfilePic(feedObj.getString("profilePic"));
                        item.setPhone(feedObj.getString("phone"));
                        item.setState(feedObj.getString("state"));

                        mPersonList.add(item);
                    }
                    if(mPersonList.size() !=0){
                        lnNotData.setVisibility(View.GONE);
                        lnListData.setVisibility(View.VISIBLE);
                        String number_of_participa = getResources().getString(R.string.number_of_participa);
                        tvCount.setText(feedArray.length()+" "+number_of_participa );

                    }else{
                        lnNotData.setVisibility(View.VISIBLE);
                        lnListData.setVisibility(View.GONE);
                    }

                    adapter.notifyDataSetChanged();
                    hideProgress();
                } catch (JSONException e) {
                    e.printStackTrace();
                    hideProgress();
                    onFail(apiUrl, params, e.getMessage(), e);
                }

            }

            @Override
            public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {

            }
        } , "iduser", idUser, "id_sc", idSc);

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar actions click
        super.onOptionsItemSelected(item);
            if(item.getItemId() == android.R.id.home){
            finish();
        }
        return true;

    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();

    }
}

