package com.finger.tigo.friend;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.finger.tigo.BaseActivity;
import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.addfriend.DetailFriendActivity;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.items.ItemNoti;
import com.finger.tigo.listener.RecyclerTouchListener;
import com.finger.tigo.notification.adapter.NotificationFriendAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Duong on 5/26/2017.
 */

public class TigonRequestActivity extends BaseActivity implements NotificationFriendAdapter.itemClickNotificationFriend{
    
    RecyclerView recyclerRequesTigon;
    private NotificationFriendAdapter listAdapter;
    String idUser;
    private List<ItemNoti> feedItems;
    TextView tvCountRequest;
    Toolbar toolbar;

    
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tigon_request);
        idUser = AppAccountManager.getAppAccountUserId(this);
        tvCountRequest = (TextView) findViewById(R.id.tv_count_request_tigon);
        recyclerRequesTigon = (RecyclerView) findViewById(R.id.list_tigon_request);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitle("TiGon");

        initListNotificationfriend();
        showProgress();
        setNotificationLIstFriend();
    }

    private void initListNotificationfriend() {
        feedItems = new ArrayList<>();
        listAdapter = new NotificationFriendAdapter(TigonRequestActivity.this, feedItems, this);
        recyclerRequesTigon.setHasFixedSize(true);
        RecyclerView.LayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(TigonRequestActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerRequesTigon.setLayoutManager(horizontalLayoutManagaer);

        recyclerRequesTigon.setItemAnimator(new DefaultItemAnimator());
        recyclerRequesTigon.setAdapter(listAdapter);
        recyclerRequesTigon.addOnItemTouchListener(new RecyclerTouchListener(TigonRequestActivity.this, recyclerRequesTigon, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                ItemNoti item = feedItems.get(position);
                Intent i = new Intent(TigonRequestActivity.this, DetailFriendActivity.class);

                i.putExtra(DetailFriendActivity.ID_USER, item.getIdFriend());
                i.putExtra(DetailFriendActivity.NAME,item.getName());
                i.putExtra(DetailFriendActivity.PROFILE_PIC, item.getProfilePic());
                i.putExtra(DetailFriendActivity.PHONE, item.getPhone());
                startActivity(i);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    public  void setNotificationLIstFriend() {


        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_NOTIFICATION_FRIEND, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response !=null)
                    parseJsonFeed(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("iduser", idUser);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);

    }

    public void parseJsonFeed(String response){
        try {
            hideProgress();
            JSONObject jObj = new JSONObject(response);
            JSONArray feedArray = jObj.getJSONArray("feed");
            feedItems.clear();
            for (int i = 0; i < feedArray.length(); i++) {

                JSONObject feedObj = (JSONObject) feedArray.get(i);
                ItemNoti item = new ItemNoti();
                item.setName(feedObj.getString("name"));
                item.setIdFriend(feedObj.getString("iduser"));
                item.setPhone(feedObj.getString("phone"));
                item.setTimeStamp(feedObj.getString("created_at"));
                item.setProfilePic(feedObj.getString("profilePic"));
                item.setSta(feedObj.getString("state"));
                feedItems.add(item);

            }

            if (!feedItems.isEmpty() && feedItems != null) {

                listAdapter.notifyDataSetChanged();
                int total = feedItems.size();

                tvCountRequest.setText(getResources().getString(R.string.tigon_request)+" ("+total+")");

            }

        } catch (JSONException e) {
            hideProgress();
            e.printStackTrace();
        }
    }

    @Override
    public void updateFriend(int position, String state, NotificationFriendAdapter.MyViewHolder v) {
        final ItemNoti item = feedItems.get(position);
        String idsender = idUser;
        showProgress();
        loadAdded(idsender, item.getIdFriend(), item.getName(), item.getPhone(), state, position);
        v.layout_bt.setVisibility(View.GONE);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {//Write your logic here
            this.finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
    private void loadAdded(final String iduser, final String idfriend, final String name, final String phone,
                           final String state, final int position) {
        // Tag used to cancel the request


        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_INSERT_FRIEND, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                hideProgress();

                feedItems.remove(position);
                listAdapter.notifyDataSetChanged();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                showToast(R.string.not_connect);
                hideProgress();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("iduser", iduser);
                params.put("idfriend", idfriend);
                params.put("name", name);
                params.put("phone", phone);
                params.put("state", state);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }

}
