package com.finger.tigo.syncadapter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.login.LoginManager;
import com.finger.tigo.LoadingActivity;
import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.app.AppIntent;
import com.finger.tigo.session.SessionManager;
import com.finger.tigo.util.MyLog;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;
import java.util.Map;

public class DialogActivity extends Activity implements View.OnClickListener {
    Button ok_btn;
    SessionManager session;
    TextView tvMessage;
    String messageDelete;
    private String idUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.setFinishOnTouchOutside(false);
        setContentView(R.layout.activity_dialog);
        ok_btn = (Button) findViewById(R.id.ok_btn_id);
        tvMessage = (TextView) findViewById(R.id.message_warning);
        ok_btn.setOnClickListener(this);
        idUser = AppAccountManager.getAppAccountUserId(this);
        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        String checkWatch = b.getString("clickNotification");
        if(checkWatch !=null){
            String typeNoti = b.getString(AppIntent.ACTION_TYPE_NOTI);
            String idNoti =  b.getString(AppIntent.ACTION_ID_NOTI);
            MyLog.d("watched: ", typeNoti+" idnoty: "+idNoti);
            if(checkWatch.equalsIgnoreCase("watched_2_3_6_5"))
                postwatching(idUser,idNoti, AppConfig.URL_WATCHING_DETAIL,typeNoti);
            else{
                postwatching(idUser,  idNoti, AppConfig.URL_WATCHING_TBLIKE, typeNoti);
            }
        }
        // Note that flag changes must happen *before* the content view is set.

    }

    public void postwatching(final String idUser, final String id_noti, final String url, final String typeNoti) {
        // Hide all views

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Intent intent = new Intent();
                intent.setAction(AppIntent.ACTION_PUSH_RECEIVED);
                intent.putExtra(AppIntent.ACTION_PUSH_WATCHED, "watched");
                intent.putExtra(AppIntent.ACTION_ID_NOTI, id_noti);
                intent.putExtra(AppIntent.ACTION_TYPE_NOTI, typeNoti);
                sendBroadcast(intent);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("ttt", "onErrorResponse: "+error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_user", idUser);
                params.put("id_noti", id_noti);

                return params;
            }

        };

        // Adding request to request queue
        strReq.setTag(this.getClass().getName());
        AppController.getInstance().addToRequestQueue(strReq);
    }

    @Override
    public void onClick(View v) {
        if(messageDelete  != null) {
            this.finish();
        }else {
            logoutUser(getApplicationContext());
            this.finish();
        }

    }

    void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT)
                .show();
    }

    private void logoutUser(Context context) {
        session = new SessionManager(this);
        String iduser = AppAccountManager.getAppAccountUserId(context);
        Logout_Token(iduser + "");

        AccountManager am = (AccountManager) this.getSystemService(Context.ACCOUNT_SERVICE);
        Account[] accountsFromFirstApp = am.getAccountsByType(AppAccountManager.ACCOUNT_TYPE);
        ContentResolver.setSyncAutomatically(accountsFromFirstApp[0], "com.android.calendar", false);
        ContentResolver.removePeriodicSync(accountsFromFirstApp[0], "com.android.calendar", Bundle.EMPTY);
        Uri evuri = CalendarContract.Calendars.CONTENT_URI;
        long calendarId = Long.parseLong(am.getUserData(accountsFromFirstApp[0], AppAccountManager.USER_DATA_CALENDAR));

        Uri deleteUri = ContentUris.withAppendedId(evuri, calendarId);
        getContentResolver().delete(deleteUri, null, null);

        am.removeAccount(accountsFromFirstApp[0], null, null);

        session.setLogin(false);
        session.set_id_user_session(-1);
        session.setName_session(null);

        session.setmail_session(null);
        session.setphone_session(null);
        session.setbirhtday_session(null);
        session.setsex_session(null);
        session.setpic_session(null);
        session.setKeyCodeCountry(null);
        // Launching the login activity
        if (session.getIsLogerFb()) {
            session.setIsLogerFb(false);
            LoginManager.getInstance().logOut();
        }


        Intent intent = new Intent(this, LoadingActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        Intent a = new Intent();
        // countNotification = countNotification + 1;

        a.setAction(AppIntent.ACTION_CHANGE_NOTIFICATION_BADGE_BY_ADD_FRIEND);
        a.putExtra("finishFromSetting", true);
        this.sendBroadcast(a);
    }

    private void Logout_Token(final String id_user) {
        // Tag used to cancel the request
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_LOGOUT_TOKEN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("SettingActivity", "Login Error: " + error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                String token = FirebaseInstanceId.getInstance().getToken();
                params.put("id_user", id_user);
                params.put("id_token", token);
                Log.d("lllllll", "id_user: " + id_user);
                return params;
            }
        };
        // Adding request to request queue
        com.finger.tigo.app.AppController.getInstance().addToRequestQueue(strReq);
    }

}
