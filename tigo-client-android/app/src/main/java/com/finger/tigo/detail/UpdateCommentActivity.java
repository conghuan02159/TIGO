package com.finger.tigo.detail;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.finger.tigo.R;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.data.SQLiteHandler;
import com.finger.tigo.items.FeedItem;
import com.finger.tigo.session.SessionManager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class UpdateCommentActivity extends AppCompatActivity {

    private SQLiteHandler db;
    private SessionManager session;
    private CircleImageView profilePic;
    private ProgressDialog pDialog;
    private EditText comment;
    private String idUser;
    private String id_comment;
//
//    private String Staturcomment;

    Context context;

    private LayoutInflater inflater;

    private ListView list;


    public List<FeedItem> feedItems;
    public static final String TAG = "DetailsPersons";
    Button update,cant;
    Toolbar toolbar;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_comment);

        toolbar= (Toolbar) findViewById(R.id.toolbarComment);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setTitleTextColor(Color.WHITE);

        comment = (EditText) findViewById(R.id.comment);

        com.android.volley.toolbox.ImageLoader imageLoader = AppController.getInstance().getImageLoader();
        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();


        profilePic = (CircleImageView) findViewById(R.id.profilePic);

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        //downloadperson();
        db = new SQLiteHandler(getApplicationContext());
        session = new SessionManager(getApplicationContext());

        idUser= session.get_id_user_session()+"";

        Intent i = getIntent();
        String comments = i.getStringExtra("statuscomment");
        comment.setText(comments);
        String id = i.getStringExtra("id_comment");
        id_comment= id;

//        profilePic.setImageUrl(session.getLoginProfilePic(), imageLoader);

        Glide.with(UpdateCommentActivity.this).load(session.getLpic_session())
                .placeholder(R.drawable.avatar)
                .error(R.drawable.avatar)
                .into(profilePic);


        update = (Button) findViewById(R.id.but_OK);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customActionBar();
            }
        });
        cant = (Button) findViewById(R.id.but_Huy);
        cant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    public void customActionBar(){
        //String id_user = idUser.trim();
        String comment_id = id_comment.trim();
        String comment_status = comment.getText().toString().trim();

        //  Log.d("aaaaaaaaaaa",idSc+" status: " + status+" location: "+ location+ " daystar: "+daystart+" dayfi: "+dayfinish+ " more: " +mode+" lga: "+ lga+" lgb: " +lgb +" color: "+ color );
        if (!comment_id.isEmpty() && !comment_status.isEmpty()) {
            UpdateComment(comment_id,comment_status);

            comment.setText("");


        } else {
            Toast.makeText(getApplicationContext(),
                    R.string.success, Toast.LENGTH_LONG)
                    .show();
        }
    }

    private void UpdateComment(final String comment_id, final String comment) {
        // Tag used to cancel the request
        // String tag_string_req = "req_register";
        pDialog.setMessage(getResources().getString(R.string.upload));
        showDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPDATE_COMMENT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Register Response: " + response.toString());
                hideDialog();
                Toast.makeText(getApplicationContext(), R.string.ok_password, Toast.LENGTH_LONG).show();
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Updatestration Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
                finish();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                //String profilePic = getStringImage(bitmap);
                Log.d(TAG, "getParams: "+profilePic);
                Map<String, String> params = new HashMap<String, String>();
                params.put("ID_COMMENT", comment_id);
                params.put("ContainerComment", comment);

                //params.put("profilePic", profilePic);
                //session.setLogin(true, session.getLogin(), idUser);
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);

    }

    private  void doawloadfail(){
        Toast.makeText(UpdateCommentActivity.this,R.string.fail,Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {//Write your logic here
            this.finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //downloadperson();
   }

    @Override
    public void onOptionsMenuClosed(Menu menu) {
        super.onOptionsMenuClosed(menu);
    }
}

