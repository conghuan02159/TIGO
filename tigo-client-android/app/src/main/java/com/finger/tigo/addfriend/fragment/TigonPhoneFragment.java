package com.finger.tigo.addfriend.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.addfriend.DetailFriendActivity;
import com.finger.tigo.addfriend.async.AddfriendInteractor;
import com.finger.tigo.addfriend.model.ItemAddfriend;
import com.finger.tigo.addfriend.presenter.AddNewFriendPresenter;
import com.finger.tigo.addfriend.presenter.IAddNewFriendView;
import com.finger.tigo.libcountrypicker.fragments.CountryPicker;
import com.finger.tigo.libcountrypicker.interfaces.CountryPickerListener;
import com.finger.tigo.session.SessionManager;

import java.util.List;



/**
 * Created by Duong on 3/30/2017.
 */

public class TigonPhoneFragment extends Fragment implements IAddNewFriendView {

    public static final String CHECK_NEW_FRIEND = "checkNewFriend";
    public static final String SYNC_CONTACT_NEW_FRIEND = "sync_contact_new_friend";
    public static final int PERMISSION_REQUEST_CONTACT = 12;


    Button btCountry,btn_next_phone;
    TextView tvCountry;
    EditText inputPhone;
    TextView txtError;

    SessionManager session;
    String strCode;
    String idUser;
    CountryPicker mCountryPicker;
    String strPhone;
    ProgressDialog pDialog;
    AddNewFriendPresenter presenter;

    private Activity activity;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_tigon_phone, container, false);
        activity = getActivity();
        idUser = AppAccountManager.getAppAccountUserId(activity);
        session = new SessionManager(getActivity());
        strCode = session.getKeyCodeCountry();
        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);
        presenter = new AddNewFriendPresenter(this);
        btCountry = (Button)rootview.findViewById(R.id.bt_countrycode);
        tvCountry = (TextView)rootview.findViewById(R.id.tv_country);
        inputPhone = (EditText)rootview.findViewById(R.id.input_phone);
        txtError = (TextView)rootview.findViewById(R.id.tv_error);
        btn_next_phone = (Button)rootview.findViewById(R.id.btn_next_phone);

        tvCountry.setText(strCode);
        mCountryPicker = CountryPicker.newInstance(getResources().getString(R.string.select_country));
        btCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCountryPicker.show(getActivity().getFragmentManager(), "COUNTRY_PICKER");
            }
        });

        mCountryPicker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode,
                                        int flagDrawableResID) {
                tvCountry.setText(dialCode);
                strCode = dialCode;
                mCountryPicker.dismiss();
            }
        });
        btn_next_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSendNumber();
            }
        });

        return rootview;
    }

    public void onSendNumber() {
        strPhone = inputPhone.getText().toString();
        submitForm();

    }

    private void submitForm() {
        if (!validatePhoneNumber()) {
            return;
        }

        if (strPhone.charAt(0) == '0') {
            strPhone = strPhone.substring(1);
            strPhone = strCode + strPhone;
        } else if (strPhone.charAt(0) == '+') {

        } else {
            strPhone =  strCode + strPhone;
        }
        pDialog.setMessage(getResources().getString(R.string.search_phone));
        showDialog();

        presenter.attemptAddNewFriend( strPhone);

    }

    private boolean validatePhoneNumber() {

        if (inputPhone.getText().toString().trim().isEmpty()) {
            txtError.setText(R.string.error_add_friend);

            return false;
        }else if (!isValidPhoneNumber(inputPhone.getText().toString())) {
            txtError.setText(R.string.invalid_phone);

            return false;
        }else{
            txtError.setText("");
        }
        return true;
    }
    private static boolean isValidPhoneNumber(String mobile) {
        String regEx = "^[0-9]{10,15}$";
        return mobile.matches(regEx);
    }







    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    @Override
    public void onSuccess(String success, List<ItemAddfriend> listRegister) {
        if(success.equalsIgnoreCase(CHECK_NEW_FRIEND)){
            hideDialog();
            Intent intent = new Intent(activity, DetailFriendActivity.class);
            for (ItemAddfriend object: listRegister) {
                intent.putExtra("idUser", object.id);
                intent.putExtra("name",object.name);
                intent.putExtra("profilePic", object.profilePic);
            }
            intent.putExtra("phone", strPhone);
            startActivity(intent);
            activity.finish();

        }else if(success.equalsIgnoreCase(SYNC_CONTACT_NEW_FRIEND)){
            hideDialog();
            Toast.makeText(activity, R.string.notification_contacts, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onError(String error, int keyError) {
        if(error.equalsIgnoreCase(CHECK_NEW_FRIEND)){
            hideDialog();
            switch (keyError){
                case  AddfriendInteractor.KEY_ERROR_ONE:
                    txtError.setText( R.string.no_matches_found);
                    break;
                case AddfriendInteractor.KEY_ERROR_TWO:
                    txtError.setText( R.string.notconnect);
                    break;
            }
        } else if(error.equalsIgnoreCase(SYNC_CONTACT_NEW_FRIEND)){
            hideDialog();
            switch (keyError){
                case  AddfriendInteractor.KEY_ERROR_ONE:
                    txtError.setText( R.string.notconnect);
                    break;
            }
        }
    }
}
