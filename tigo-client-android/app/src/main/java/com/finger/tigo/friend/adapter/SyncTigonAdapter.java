package com.finger.tigo.friend.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.finger.tigo.R;
import com.finger.tigo.items.ItemNoti;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Duong on 5/17/2017.
 */

public class SyncTigonAdapter extends RecyclerView.Adapter<SyncTigonAdapter.MyViewHolder> {

    private Activity activity;
    private List<ItemNoti> listSync;

    String idUser;
    public itemClickNotificationFriend itemClick;

    public SyncTigonAdapter(Activity activity, List<ItemNoti> listSync, itemClickNotificationFriend itemClick){
        this.listSync = listSync;
        this.activity = activity;
        this.itemClick = itemClick;

    }

    public interface itemClickNotificationFriend{
        void addFriend(int postion, String idFriend, String name, String phone, MyViewHolder v);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_sync_tigon, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return listSync.size();
    }

    @Override
    public void onBindViewHolder(final  MyViewHolder viewHolder, final  int position) {
        final ItemNoti item = listSync.get(position);
        viewHolder.name.setText(item.getName());
        viewHolder.phone.setText(item.getPhone());

        Glide.with(activity).load(item.getProfilePic())
                .placeholder(R.drawable.avatar).error(R.drawable.avatar)
                .into(viewHolder.profilePic);

        viewHolder.addfriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClick.addFriend(position, item.getIdFriend(), item.getName(), item.getPhone(),  viewHolder);
                listSync.remove(position);
                notifyDataSetChanged();
            }
        });
       

    }
    public class MyViewHolder extends RecyclerView.ViewHolder {


        public TextView name;

        CircleImageView profilePic;

        public TextView phone;

        public TextView addfriend;
        
        
        public MyViewHolder(View rowView) {
            super(rowView);
            name = (TextView)rowView.findViewById(R.id.name_sync);
            profilePic = (CircleImageView)rowView.findViewById(R.id.profilePic_sync);
            phone = (TextView)rowView.findViewById(R.id.phone_sync);
            addfriend = (TextView)rowView.findViewById(R.id.addfriend_sync);

        }
    }
}
