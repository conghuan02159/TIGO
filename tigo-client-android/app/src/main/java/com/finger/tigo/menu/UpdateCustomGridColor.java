package com.finger.tigo.menu;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.finger.tigo.schedule.creatEvent.ColorSquare;
import com.finger.tigo.schedule.creatEvent.ItemDialog;

import java.util.ArrayList;
import java.util.List;

import com.finger.tigo.R;


public class UpdateCustomGridColor extends ArrayAdapter<ItemDialog> {
    ArrayList result;
    Context context;

    public LayoutInflater inflater;
    public List<ItemDialog> listItem;
    ItemColorClick itemColorClick;

    public UpdateCustomGridColor(Context context, List<ItemDialog> resource, ItemColorClick itemColorClick) {
        super(context,0, resource);
        this.context = context;
        this.listItem = resource;
        this.itemColorClick = itemColorClick;

    }
    public interface ItemColorClick {
        void onItemClick(int position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.item_grid_color,parent, false);

        final ItemDialog item = listItem.get(position);
        ColorSquare square1 = (ColorSquare) convertView.findViewById(R.id.menurow_square);
        final TextView color_text = (TextView) convertView.findViewById(R.id.grid_text);

        square1.setBackgroundColor(Color.parseColor(item.getColor()));
        color_text.setText(item.getColor_text());

        square1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                itemColorClick.onItemClick(position);



            }
        });



        return convertView;
    }

}
