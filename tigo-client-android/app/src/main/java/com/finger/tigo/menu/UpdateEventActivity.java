package com.finger.tigo.menu;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.angel.black.baframeworkimagepick.ImagePickExecuter;
import com.angel.black.baframeworkimagepick.ImagePickIntentConstants;
import com.angel.black.baframeworkimagepick.ImagePickOnActivityResult;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.finger.tigo.BaseActivity;
import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.app.ApiExecuter;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.schedule.creatEvent.ColorSquare;
import com.finger.tigo.schedule.creatEvent.CreateEventActivity;
import com.finger.tigo.schedule.creatEvent.ItemDialog;
import com.finger.tigo.session.SessionManager;
import com.finger.tigo.util.MyLog;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.kosalgeek.android.photoutil.GalleryPhoto;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public class UpdateEventActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener, View.OnClickListener, UpdateCustomGridColor.ItemColorClick {
    private static final String TAG = CreateEventActivity.class.getSimpleName();

    private ProgressDialog pDialog;

    private SessionManager session;
    private String idUser ="";

    private EditText nameEvent, localEvent, phoneEvent, description_event;
    public TextView  lg1, lg2;
    ImageView position;
    //    private ColorPickerDialog dialog;
    private String Regime;

    private RadioGroup rgRegime;

    public ColorSquare square1;
    public AlertDialog builder;
    private Context context;
    String[] strArray;

    private UpdateCustomGridColor listAdapter;
    private List<ItemDialog> feedItemss;
    private String mSelectedColor = "#fcd059";

    private ImageView imv, imv_take, image_position;

    private TextView txt_location_creat;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 0;
    public Integer number;

    private int REQUEST_CODE_PICKER = 2000;
    String selectedPhoto;
    GalleryPhoto galleryPhoto;
    private TextView dateStart, dateEnd, timeStart, timeEnd;
    private Calendar cal1, cal2;
    Spinner alarmEvent;
    Switch toggleFullDay, toggle_invite;
    Switch toggle_joinfee;
    ProgressBar progressBar_location;
    Toolbar toolbar;
    LinearLayout layout_joinfee;

    String locations;
    String positions;
    String dateStarts;
    String dateEnds;
    String timeStarts;
    String timeEnds;
    String imagess;
    String invite = "false";
    String colors ;
//    String alarms;
    String alarm = "10";
    String mode;
    double lag1;
    double lag2;
    private String id_sc;
    private String nameEvents, description_events, phoneEvents;
    private String cate = "";
    private boolean checkAllday = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);

        toolbar = (Toolbar)findViewById(R.id.toolbarCreateEvent);

        context = this;
        setSupportActionBar(toolbar );
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.ic_cancel_create_event);
        init();
        imv_take.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ImagePickExecuter.executeImagePickWithCameraToOneImage(UpdateEventActivity.this, false);

            }
        });
        imv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ImagePickExecuter.executeImagePickWithCameraToOneImage(UpdateEventActivity.this, false);
            }
        });
    }

    public void init() {

        galleryPhoto = new GalleryPhoto(getApplicationContext());
        imv_take = (ImageView) findViewById(R.id.image_takephoto);
        dateStart = (TextView) findViewById(R.id.but_chonngay1);
        dateEnd = (TextView) findViewById(R.id.but_chonngay2);
        timeStart = (TextView) findViewById(R.id.but_chongio1);
        timeEnd = (TextView) findViewById(R.id.but_chongio2);
        nameEvent = (EditText) findViewById(R.id.nameEvent);
        localEvent = (EditText) findViewById(R.id.localEvent);

        rgRegime = (RadioGroup) findViewById(R.id.rgRegime);
        phoneEvent = (EditText) findViewById(R.id.phone_event);
        alarmEvent = (Spinner) findViewById(R.id.spinner);
        description_event = (EditText) findViewById(R.id.description);
        progressBar_location = (ProgressBar) findViewById(R.id.progress_bar_location);
        image_position = (ImageView) findViewById( R.id.image_location) ;
        imv = (ImageView) findViewById(R.id.img_sk);
        txt_location_creat = (TextView) findViewById(R.id.txt_location_home);
        lg1 = (TextView) findViewById(R.id.b1);
        lg2 = (TextView) findViewById(R.id.b2);

        square1 = (ColorSquare) findViewById(R.id.menurow_square);
        toggleFullDay = (Switch) findViewById(R.id.toggle_full_day);
        toggle_invite = (Switch) findViewById(R.id.toggle_invite);
        toggle_joinfee= (Switch) findViewById(R.id.toggle_joinfee);
        toggle_joinfee.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked){
                    final Dialog dialog = new Dialog(UpdateEventActivity.this);
                    dialog.setContentView(R.layout.dialog_joinfee);
                    WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
                    lp.dimAmount = 0.7f;
                    dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                    Button dialogButton = (Button) dialog.findViewById(R.id.but_ok_joinfee);
                    // if button is clicked, close the custom dialog
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    dialog.show();
                }
                else if (!isChecked){

                }
            }
        });

        toggle_invite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton arg0, boolean isChecked){
                if(isChecked == true){
                    invite = "true";

                }
                else if (isChecked == false){
                    invite = "false";
                }
            }
        });

        toggleFullDay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {


                if(isChecked){
                    timeStart.setVisibility(View.GONE);
                    timeEnd.setVisibility(View.GONE);
                    checkAllday = true;
                } else {
                    timeEnd.setVisibility(View.VISIBLE);
                    timeStart.setVisibility(View.VISIBLE);
                    checkAllday = false;
                }

            }
        });
        //Regime = "true";
        strArray = getResources().getStringArray(R.array.alarm_array);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, strArray); //selected item will look like a spinner set from XML
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        alarmEvent.setAdapter(arrayAdapter);
        alarmEvent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                // Object item = parentView.getItemAtPosition(position);

                String item = parentView.getItemAtPosition(position).toString();
                switch (position) {
                    case 0:

                        alarm = 10+ "";
                        break;

                    case 1:
                        alarm = 20+ "";
                        break;

                    case 2:
                        alarm = 30+"";
                        break;

                    case 3:
                        alarm = 60+"";
                        break;

                    case 4:
                        alarm = 1440+ "";
                        break;

                    case 5:
                        alarm = 10080 +"";
                        break;
                    default:
                        alarm ="10";
                }
            }

            public void onNothingSelected(AdapterView<?> arg0) {// do nothing
            }

        });

        session = new SessionManager(UpdateEventActivity.this);
        idUser = AppAccountManager.getAppAccountUserId(this);

        Intent i = getIntent();
        id_sc = i.getStringExtra("id_sc");
        nameEvents = i.getStringExtra("nameEvent");
        locations = i.getStringExtra("location");
        positions = i.getStringExtra("position");
        dateStarts = i.getStringExtra("dateStart");
        dateEnds = i.getStringExtra("dateEnd");
        imagess = i.getStringExtra("image");
        colors = i.getStringExtra("color");
        alarm = i.getStringExtra("alarm");
        mode = i.getStringExtra("modes");
        cate = i.getStringExtra("category");
        invite = i.getStringExtra("allowinvite");
        description_events = i.getStringExtra("description");
        phoneEvents = i.getStringExtra("phone");
        lag1 = i.getDoubleExtra("lg1", 0.00);
        lag2 = i.getDoubleExtra("lg2", 0);
        mSelectedColor = colors;
        square1.setBackgroundColor(Color.parseColor(colors));
        if(i.getStringExtra("allday").equals("1")){
            checkAllday = true;
        }else{
            checkAllday = false;
        }

        if(checkAllday) {
            toggleFullDay.setChecked(true);
        }

        nameEvent.setText(nameEvents);
        description_event.setText(description_events);
        phoneEvent.setText(phoneEvents);
        localEvent.setText(locations);
        txt_location_creat.setText(positions);

        if(invite == null)
            invite = "false";

        if (invite.equalsIgnoreCase("true")) {

            toggle_invite.setChecked(true);

        } else {
            toggle_invite.setChecked(false);
        }
        lg1.setText(lag1+"");
        lg2.setText(lag2+"");
        Regime = mode;
        if(mode== null)
            Regime = "true";
        long daylyStart = Long.parseLong(dateStarts);
        long dailyEnd = Long.parseLong(dateEnds);
        cal1 = Calendar.getInstance();
        cal1.setTimeInMillis(daylyStart);
        cal2 = Calendar.getInstance();
        cal2.setTimeInMillis(dailyEnd);
        setDateTime(cal1, cal2);

        Glide.with(this).load(imagess)
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .override(300, 300)
                .dontTransform()
                .into(imv);

        if(alarm == null)
            alarm = "10";

        if(cate == null)
            cate = "Festival";


        image_position.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

                try {
                    Intent intent = builder.build(UpdateEventActivity.this);
                    startActivityForResult(intent, 1);

                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }

            }
        });

        square1.setOnClickListener(this);
        rgRegime.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {

                if (checkedId == R.id.rbPuplic) {
                    final Dialog dialog = new Dialog(UpdateEventActivity.this);
                    dialog.setContentView(R.layout.dialog_raido_public);
                    WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
                    lp.dimAmount = 0.7f;
                    dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                    Button dialogButton = (Button) dialog.findViewById(R.id.but_ok_joinfee);
                    // if button is clicked, close the custom dialog
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    dialog.show();

                } else if (checkedId == R.id.noPublic) {
                    final Dialog dialog2 = new Dialog(UpdateEventActivity.this);
                    dialog2.setContentView(R.layout.dialog_raido_nopublic);
                    WindowManager.LayoutParams lp2 = dialog2.getWindow().getAttributes();
                    lp2.dimAmount = 0.7f;
                    dialog2.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                    Button dialogButton2 = (Button) dialog2.findViewById(R.id.but_ok_joinfee);
                    // if button is clicked, close the custom dialog
                    dialogButton2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog2.dismiss();
                        }
                    });

                    dialog2.show();

                }

            }
        });

        dateStart.setOnClickListener(this);
        dateEnd.setOnClickListener(this);
        timeStart.setOnClickListener(this);
        timeEnd.setOnClickListener(this);
        imv.setOnClickListener(this);

    }

    public void dialogjoinfee()
    {   	// custom dialog
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_joinfee);
        dialog.setTitle("Title...");

        // set the custom dialog components - text, image and button
//        TextView text = (TextView) dialog.findViewById(R.id.text);
//        text.setText("Android custom dialog example!");
//        ImageView image = (ImageView) dialog.findViewById(R.id.image);
//        image.setImageResource(R.drawable.ic_launcher);

        Button dialogButton = (Button) dialog.findViewById(R.id.but_ok_joinfee);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }



    public  int convertToCurentJulian(Calendar calendar){

        int year = calendar.get(calendar.YEAR);

        int month = calendar.get(calendar.MONTH)+1;
        int day = calendar.get(calendar.DAY_OF_MONTH);

        int a = (14 - month) / 12;
        int y = year + 4800 - a;
        int m = month + 12 * a - 3;
        int jdn = day + (153 * m + 2)/5 + 365*y + y/4 - y/100 + y/400 - 32045;

        return jdn;
    }



    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }



    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.img_sk) {//                showFileChooser();


        } else if (i == R.id.menurow_square) {
            showAlertDialog(context);

        } else if (i == R.id.but_chonngay1) {
            selectDayStart();

        } else if (i == R.id.but_chonngay2) {
            selectDayEnd();

        } else if (i == R.id.but_chongio1) {
            selectHourStart();

        } else if (i == R.id.but_chongio2) {
            selecHourEnd();

        } else {
        }
    }

    public void onClickOK() {

        int startTime = (cal1.get(cal1.HOUR_OF_DAY) * 60) + (cal1.get(cal1.MINUTE));
        int endTime = (cal2.get(cal2.HOUR_OF_DAY) * 60) + cal2.get(cal2.MINUTE);
        int julianStart = convertToCurentJulian(cal1);
        int julianEnd = convertToCurentJulian(cal2);

        if (checkAllday) {

            cal1.set(Calendar.HOUR_OF_DAY, 0);
            cal1.set(Calendar.MINUTE, 0);
            cal1.set(Calendar.SECOND, 0);
            cal1.set(Calendar.MILLISECOND, 0);

            cal2.set(Calendar.HOUR_OF_DAY, 23);
            cal2.set(Calendar.MINUTE, 59);
            cal2.set(Calendar.SECOND, 59);
            cal2.set(Calendar.MILLISECOND, 0);

            int days = 1;
            julianStart = convertToCurentJulian(cal1);
            if ((cal2.getTimeInMillis() - cal1.getTimeInMillis()) > (1000 * 60 * 24 * 60)) {
                long timeMilisEnd = cal2.getTimeInMillis() - cal1.getTimeInMillis();

                days = (int) (timeMilisEnd / (1000 * 60 * 60 * 24));
                julianEnd = convertToCurentJulian(cal2);

            } else {
                julianEnd = julianStart;
            }

            startTime = 0;
            endTime = 1440 * days;

            // end time.this is time finish event

        }

        String title = nameEvent.getText().toString();
        String location = localEvent.getText().toString();
        String positionevent = txt_location_creat.getText().toString();
        String daystart = cal1.getTimeInMillis() + "";
        String dayfinish = cal2.getTimeInMillis() + "";
        String mode = Regime + "";
        String phoneevent = phoneEvent.getText().toString();
        String lga = lg1.getText().toString();
        String lgb = lg2.getText().toString();
        String color = mSelectedColor + "";
        String category = cate;
        String allowinvite = invite;
        String alarm = this.alarm;
        String description = description_event.getText().toString();

        MyLog.d(id_sc+" status: " + title+" location: "+ location+ " positionEvent:" + positionevent + " daystar: "+daystart+" dayfi: "
                +dayfinish+ " more: " +mode+" lga: "+ lga+" lgb: " +lgb +" color: "+ color + " category: " + category
                + " allowInvite: " + allowinvite + " alarm:" + alarm
                + " phone: " + phoneevent + " descri: " + description);


        if (!id_sc.isEmpty() && !title.isEmpty() && !location.isEmpty() && !positionevent.isEmpty()
                && !daystart.isEmpty() && !dayfinish.isEmpty() && !mode.isEmpty() && !phoneevent.isEmpty()
                && !lga.isEmpty() && !lgb.isEmpty() && !color.isEmpty() && !category.isEmpty()
                && !allowinvite.isEmpty() && !alarm.isEmpty() && !description.isEmpty()) {

            updateEvent(id_sc, idUser, title, location, positionevent, daystart, dayfinish, mode, phoneevent, lga, lgb, color,
                    category, allowinvite, alarm, description,
                    imv.getContentDescription() != null ? imv.getContentDescription().toString() : null, toggleFullDay.isChecked());
        } else {
            Toast.makeText(getApplicationContext(),
                    R.string.error_create_event, Toast.LENGTH_LONG)
                    .show();
        }
    }

    private void updateEvent(final  String idSc, final String idUser,
                             final String status, final String location, final String positions,
                             final String daystart, final String dayfinish,
                             final String mode, final String phone_event, final
                               String lg1, final String lg2, final String color,
                             final  String category, final String allowinvite, final String alarm, final String description ,
                             final String imageFilePath, final boolean allDay) {

        MyLog.d("idsc: "+ idSc+" iduser: "+ idUser+" status: "+ status+" location: "+ location+" position: "+ positions + " daystart: "+ daystart +" dayfinish: "+ dayfinish +" mode: "+ mode +" phone: "+ phone_event+" lg1: "+ lg1+" LG2: "+ lg2 +" color: "+ color +" category: "+ category +" allowin: "+ allowinvite +" alarm: "+ alarm +" desrip: "+ description
                +"allday: "+ allDay );

        String image;
        Log.d("yyyyyy", "image path: "+imageFilePath);
        if(imageFilePath == null){
            image= "";
        }else {
            image = getReducedImage(imageFilePath,512,512);
        }
        Map<String, String> params = new HashMap<String, String>();

        params.put("idsc", idSc+"");
        params.put("iduser", idUser);
        params.put("status", status);
        params.put("location", location);
        params.put("position" , positions);
        params.put("daystart", daystart);
        params.put("dayfinish", dayfinish);
        params.put("mode", mode);
        params.put("phone_event", phone_event);
        params.put("lg1", lg1);
        params.put("lg2", lg2);
        params.put("color", color);
        params.put("category",category);
        params.put("allowinvite", allowinvite);
        params.put("image", image);
        params.put("alarm", alarm);
        params.put("description",description);
        params.put("allDay", allDay ? "1" : "0");


        ApiExecuter.requestApi(this, AppConfig.URL_UPDATE_EVENT, "Updating...", params, new ApiExecuter.ApiResultListener() {
            @Override
            public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                showToast("Update finish");
                Intent returnIntent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putString("idSc", idSc);
                bundle.putString("color", color);
                bundle.putBoolean("allDay", allDay);
                bundle.putString("startMillis", daystart);
                bundle.putString("endMillis", dayfinish);
                bundle.putString("eventName", status);
                bundle.putString("location", location);
                returnIntent.putExtras(bundle);
                setResult(RESULT_OK, returnIntent);
                finish();
            }

            @Override
            public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {
                showToast(errMessage);
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    public void selectDayStart() {
        DatePickerDialog dpd = new DatePickerDialog(UpdateEventActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        MyLog.d("year=" + year + ", month=" + month + ", day=" + day);
                        cal1.set(year, month, day);

                        if(cal1.getTimeInMillis() > cal2.getTimeInMillis()){

                            cal2.setTimeInMillis(cal1.getTimeInMillis() + DateUtils.HOUR_IN_MILLIS);
                        }

                        setDateTime(cal1, cal2);

                        SimpleDateFormat sdf = new SimpleDateFormat("E dd/MM/yyyy HH:mm:ss", Locale.getDefault());
                        String startDateTime = sdf.format(cal1.getTime());
                        String endDateTime = sdf.format(cal2.getTime());

                        MyLog.d("startDateTime=" + startDateTime + ", endDateTime=" + endDateTime);

                    }
                }, cal1.get(Calendar.YEAR), cal1.get(Calendar.MONTH), cal1.get(Calendar.DAY_OF_MONTH));

        dpd.show();
    }

    public void setDateTime(Calendar cal1, Calendar cal2){
        SimpleDateFormat dfStart = null, dfEnd = null ;

        // date Start
        dfStart = new SimpleDateFormat("E dd/MM/yyyy", Locale.getDefault());
        String strDateStart = dfStart.format(cal1.getTime());
        dateStart.setText(strDateStart);

        // date End
        dfEnd = new SimpleDateFormat("E dd/MM/yyyy", Locale.getDefault());
        String strDateEnd = dfEnd.format(cal2.getTime());
        dateEnd.setText(strDateEnd);

        // hour start
        SimpleDateFormat SDTTimeStart = new SimpleDateFormat("hh:mm a", Locale.getDefault());
        String strTimeStart = SDTTimeStart.format(cal1.getTime());
        timeStart.setText(strTimeStart);

        // hour end
        SimpleDateFormat SDTimeEnd = new SimpleDateFormat("hh:mm a", Locale.getDefault());
        String strTimeEnd = SDTimeEnd.format(cal2.getTime());
        timeEnd.setText(strTimeEnd);

    }



    Calendar caSave = Calendar.getInstance();
    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    public void selectDayEnd() {
        long a = cal2.getTimeInMillis();
        caSave.setTimeInMillis(a);
        DatePickerDialog dpd = new DatePickerDialog(UpdateEventActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        MyLog.d("year=" + year + ", month=" + month + ", day=" + day);
                        caSave.set(year, month, day);

                        if(caSave.getTimeInMillis() > cal1.getTimeInMillis()){
                            cal2.set(year, month, day);
                            setDateTime(cal1, cal2);
                        }

                    }
                }, cal2.get(Calendar.YEAR), cal2.get(Calendar.MONTH), cal2.get(Calendar.DAY_OF_MONTH));

        dpd.show();
    }

    public void selectHourStart() {

        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(UpdateEventActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                cal1.set(Calendar.HOUR_OF_DAY, selectedHour);
                cal1.set(Calendar.MINUTE, selectedMinute);

                if(cal1.getTimeInMillis() > cal2.getTimeInMillis()) {
                    long a = cal1.getTimeInMillis() + (60 * 1000 * 60);
                    cal2.setTimeInMillis(a);

                }
                setDateTime(cal1, cal2);

            }
        }, cal1.get(Calendar.HOUR_OF_DAY), cal1.get(Calendar.MINUTE), false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    public void selecHourEnd() {

        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(UpdateEventActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                cal2.set(Calendar.HOUR_OF_DAY, selectedHour);
                cal2.set(Calendar.MINUTE, selectedMinute);

                if(cal1.getTimeInMillis() > cal2.getTimeInMillis()) {
                    long a = cal2.getTimeInMillis() + (60 *24* 60 * 1000);
                    cal2.setTimeInMillis(a);

                }

                setDateTime(cal1, cal2);

            }
        }, cal2.get(Calendar.HOUR_OF_DAY), cal2.get(Calendar.MINUTE), false);//Yes 24 hour time
        mTimePicker.setTitle("Time End");
        mTimePicker.show();
    }

    ///////////IMAGE///////////////


    //***************location*********************
    @Override
    protected void onActivityResult(int requestCode, int resutlCode, Intent data) {
        super.onActivityResult(requestCode, resutlCode, data);
        if (requestCode == 1) {
            if (resutlCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                //Log.d(TAG, "requestCode  :" + requestCode + "resutlCode  :" + resutlCode + "data  :" + data);
                String adress = String.format("%s", place.getAddress());
                final LatLng location = place.getLatLng();
                txt_location_creat.setText(adress);
                lg1.setText(location.latitude + "");
                lg2.setText(location.longitude + "");
            }
        } else if (requestCode == REQUEST_CODE_PICKER && resutlCode == RESULT_OK && data != null) {
//            images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);
////            StringBuilder sb = new StringBuilder();
//            for (int i = 0, l = images.size(); i < l; i++) {
//                selectedPhoto = images.get(0).getPath();
//                bitmap = encodeImageToBase64AfterResize(selectedPhoto, 512, 600000);
//                imv.setImageBitmap(bitmap);
//            }
//            textView.setText(encodedImage);

        } else if(requestCode == ImagePickIntentConstants.REQUEST_IMAGE_PICK) {
            if(resutlCode == RESULT_OK) {
                String imagePath = ImagePickOnActivityResult.onActivityResultAndGetOneImagePath(data);
                imv.setContentDescription(imagePath);
                Glide.with(this)
                        .load(imagePath)
                        .placeholder(R.drawable.img_loading)
                        .fitCenter()
                        .into(imv);
            }

        }

    }

    public void itemClicked(View v) {
        //code to check if this checkbox is checked!
        CheckBox checkBox = (CheckBox) v;
        if (checkBox.isChecked()) {
            timeStart.setVisibility(View.INVISIBLE);
            timeEnd.setVisibility(View.INVISIBLE);
        } else {
            timeStart.setVisibility(View.VISIBLE);
            timeEnd.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId == R.id.rbPuplic) {
            Regime = "true";

        } else if (checkedId == R.id.noPublic) {
            Regime = "false";

        }
    }

    ////////////////////////////SHOW DIALOG COLOR\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    private void showAlertDialog(Context context) {
        builder = new AlertDialog.Builder(context).create();
        // Prepare grid view
        GridView gridView = new GridView(this);
        feedItemss = new ArrayList<ItemDialog>();
        listAdapter = new UpdateCustomGridColor(this, feedItemss, this);
        gridView.setAdapter(listAdapter);
        String[] color_array = getResources().getStringArray(R.array.default_color_choice_values);
        String[] color_text = getResources().getStringArray(R.array.default_color_choice_text);

        if (color_array != null && color_array.length > 0) {

            for (int i = 0; i < color_array.length; i++) {
                ItemDialog item = new ItemDialog();
                item.setColor(color_array[i]);
                item.setColor_text(color_text[i]);
                feedItemss.add(item);
            }
            listAdapter.notifyDataSetChanged();
        }
        gridView.setNumColumns(4);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final ItemDialog item = feedItemss.get(position);
                square1.setBackgroundColor(Color.parseColor(item.getColor()));
                mSelectedColor = item.getColor();

                builder.dismiss();
            }
        });
        builder.setView(gridView);
        builder.setTitle(getResources().getString(R.string.color_event));
        builder.show();
        // Set grid view to alertDialog
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create_event, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {//Write your logic here
            this.finish();
            return true;
        } else if (i == R.id.okCreate) {
            onClickOK();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onItemClick(int position) {
        final ItemDialog item = feedItemss.get(position);
        builder.dismiss();
        mSelectedColor = item.getColor();
        square1.setBackgroundColor(Color.parseColor(item.getColor()));
        cate = item.getColor_text();
    }



}




