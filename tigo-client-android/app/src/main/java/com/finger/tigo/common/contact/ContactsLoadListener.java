package com.finger.tigo.common.contact;

import java.util.List;

/**
 * Created by Finger-kjh on 2017-05-24.
 */

public interface ContactsLoadListener {

    void onLoadStarted();

    void onLoading(int percent);

    void onLoadCompleted(List<Contact> contacts);
}
