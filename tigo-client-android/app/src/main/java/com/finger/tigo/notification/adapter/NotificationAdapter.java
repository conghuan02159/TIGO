package com.finger.tigo.notification.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.detail.CommentActivity;
import com.finger.tigo.detail.EventDetailActivity;
import com.finger.tigo.home.adapter.RecyclerAdapterHome;
import com.finger.tigo.notification.NotificationFragment;
import com.finger.tigo.notification.model.item_noti;
import com.finger.tigo.session.SessionManager;
import com.finger.tigo.util.MyLog;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class NotificationAdapter extends RecyclerView.Adapter {
    private final NotificationFragment.NotificationBadgeListener notificationBadgeListener;
    public List<item_noti> mlists;
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    private Context context;
    String contents;
    private int visibleThreshold;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private String iduser;
    private int mEventImgViewWidth;
    private int mEventImgViewHeight;
    SessionManager session;

    private RecyclerAdapterHome.OnLoadMoreListener onLoadMoreListener;


    public NotificationAdapter(List<item_noti> mlist, RecyclerView mRecyclerView, Context context,
                               NotificationFragment.NotificationBadgeListener notificationBadgeListener) {
        super();
        //Getting all the superheroes
        this.mlists = mlist;
        this.context = context;
        this.iduser = AppAccountManager.getAppAccountUserId(context);
        this.notificationBadgeListener = notificationBadgeListener;
        DisplayMetrics dm = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(dm);

        this.mEventImgViewWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 130, dm);
        this.mEventImgViewHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 90, dm);

        if (mRecyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();

            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    visibleThreshold = linearLayoutManager.getChildCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadmore();
                        }
                        loading = true;
                    }
                }
            });
        }


    }

    public interface IOnClickitemListener {
        void onItemClick(int position, int i, ViewHolder v);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.notification_tigo_item, parent, false);
            vh = new NotificationAdapter.ViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_event_progess, parent, false);
            vh = new NotificationAdapter.ProgressViewHolders(v);
        }
        return vh;
    }


    public void setOnLoadMoreListener(RecyclerAdapterHome.OnLoadMoreListener listener) {
        this.onLoadMoreListener = listener;

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final item_noti mitem_noti = mlists.get(position);
        session = new SessionManager(context);
        if (holder instanceof ViewHolder) {
            String url = mitem_noti.getImage();

            Glide.with(context)
                    .load(url)
                    .animate(android.R.anim.fade_in)
                    .override(mEventImgViewWidth, mEventImgViewHeight)
                    //  .thumbnail(0.5f)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.background_hoa_tigo_color)
                    .into(((ViewHolder) holder).image_even);


            Glide.with(context)
                    .load(mitem_noti.getProfilePic())
                    .animate(android.R.anim.fade_in)
                    .override(192, 192)
//                      .thumbnail(0.5f)
                    //.bitmapTransform(new CircleTransform(context))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.avatar)
                    .centerCrop()
                    .into(((ViewHolder) holder).image_user_even);

            ((ViewHolder) holder).txt_noti_name.setText(mitem_noti.getName_user_even());

            CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
                    Long.parseLong(mitem_noti.getTime_noti()),
                    System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
            ((ViewHolder) holder).txt_noti_time.setText(timeAgo);


            if ((mitem_noti.getWatching()== null )|| (mitem_noti.getWatching().equalsIgnoreCase("1"))) {

                ((ViewHolder) holder).itemlayout.setBackgroundColor(context.getResources().getColor(R.color.appwidget_item_allday_color));

            } else {

                ((ViewHolder) holder).itemlayout.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));
            }

            contents = null;

            if (mitem_noti.getMessages_data().equalsIgnoreCase("1")) {

                contents = mitem_noti.getName1() + " " + context.getResources().getString(R.string.notification_invite1) + " " + mitem_noti.getNumber_user() + " " + context.getResources().getString(R.string.notification_invite2) + " " + mitem_noti.getName_even();

            } else if (mitem_noti.getMessages_data().equalsIgnoreCase("2")) {
                String name1 = mitem_noti.getName1();
                String name2 = mitem_noti.getName2();
                //tham gia
                String textnoti = context.getResources().getString(R.string.interested);

                String name_even = mitem_noti.getName_even();
                String number_user = mitem_noti.getNumber_user();
                notiadapter(name1, name2, textnoti, name_even, number_user);

            } else if (mitem_noti.getMessages_data().equalsIgnoreCase("3")) {

                String name1 = mitem_noti.getName1();
                String name2 = mitem_noti.getName2();
                //comment
                String textnoti = context.getResources().getString(R.string.comment);
                String name_even = mitem_noti.getName_even();
                String number_user = mitem_noti.getNumber_user();
                notiadapter(name1, name2, textnoti, name_even, number_user);
            } else if (mitem_noti.getMessages_data().equalsIgnoreCase("4")) {
                String name1 = mitem_noti.getName1();
                String name2 = mitem_noti.getName2();
                //  String textnoti = " dong y tham gia su kien ";
                String textnoti = context.getResources().getString(R.string.notification_accepted_invite_event);
                String name_even = mitem_noti.getName_even();
                String number_user = mitem_noti.getNumber_user();
                Log.d("txtnane1", "name 1   : " + name1 + "  name 2  " + name2);
                notiaccept(name1, name2, textnoti, name_even, number_user);
            } else if (mitem_noti.getMessages_data().equalsIgnoreCase("5")) {
                String name1 = mitem_noti.getName_user_even();
                //update event
                String textnoti = context.getResources().getString(R.string.notification_editevent);
                String name_even = mitem_noti.getName_even();
                noti_update_even(name1, textnoti, name_even);
            } else if (mitem_noti.getMessages_data().equalsIgnoreCase("6")) {
                String name1 = mitem_noti.getName_user_even();
                String textnoti = context.getResources().getString(R.string.notification_delete_event);
                String name_even = mitem_noti.getName_even();
                noti_update_even(name1, textnoti, name_even);
            }
            Log.d("txtnane1", "onBindViewHolder: " + contents);

            ((ViewHolder) holder).itemlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    // Subtract the value when viewed in notification
                    MyLog.d("setWatching head: "+ mitem_noti.getWatching());
                    if((mitem_noti.getWatching()== null )|| (mitem_noti.getWatching().equalsIgnoreCase("1"))){

                    }else{
                        int count= session.getCountNotification();
                        if(session.getCountNotification() == 0){
                            count =0;
                        }else {
                            count = count-1;
                        }
                        session.setCountNotification(count);


                        if (notificationBadgeListener != null) {
                            notificationBadgeListener.onBadgeUpdate(session.getCountNotification());
                        }
                    }
                   //
                    /*
                    * message_data = 1, 4: update table TB_LIKE
                    * message_data = 2,3,5,6: update table DETAIL_NOTI
                    *
                    * */
                    switch (Integer.parseInt(mitem_noti.getMessages_data())) {
                        case 1:
                            //intent invite
                            Intent o = new Intent(context, EventDetailActivity.class);
                            postwatching(iduser, mitem_noti.getId_noti(), AppConfig.URL_WATCHING_TBLIKE);
                            o.putExtra("mDetailsHomeActivity", mitem_noti.getId_sc());
                            context.startActivity(o);

                            break;
                        case 4:

                            //inten  tham gia
                            Intent n = new Intent(context, EventDetailActivity.class);
                            postwatching(iduser, mitem_noti.getId_noti(), AppConfig.URL_WATCHING_TBLIKE);
                            n.putExtra("mDetailsHomeActivity", mitem_noti.getId_sc());
                            context.startActivity(n);
                            break;
                        ///////////////////

                        case 5:
                            //intent invite
                            postwatching(iduser, mitem_noti.getId_noti(), AppConfig.URL_WATCHING_DETAIL);
                            Intent i = new Intent(context, EventDetailActivity.class);
                            i.putExtra("mDetailsHomeActivity", mitem_noti.getId_sc());
                            context.startActivity(i);

                            break;

                        case 2:
                            //inten  tham gia

                            Intent p = new Intent(context, EventDetailActivity.class);
                            postwatching(iduser, mitem_noti.getId_noti(), AppConfig.URL_WATCHING_DETAIL);
                            p.putExtra("mDetailsHomeActivity", mitem_noti.getId_sc());
                            context.startActivity(p);
                            break;
                        case 3:
                            //inten comment
                            Intent c = new Intent(context, CommentActivity.class);
                            postwatching(iduser, mitem_noti.getId_noti(), AppConfig.URL_WATCHING_DETAIL);

                            c.putExtra("mDetailsHomeActivity", mitem_noti.getId_sc());
                            context.startActivity(c);
                            break;

                        case 6:
                            postwatching(iduser, mitem_noti.getId_noti(), AppConfig.URL_WATCHING_DETAIL);
                            break;
                        default:
                            break;
                    }
                    mitem_noti.setWatching("1");
                    ((ViewHolder) holder).itemlayout.setBackgroundColor(context.getResources().getColor(R.color.appwidget_item_allday_color));
                }
            });

            ((ViewHolder) holder).txt_noti_conten.setText(contents);
        } else {

            if (mlists.size() >= 10) {
                ((ProgressViewHolders) holder).progressBar.setIndeterminate(true);
                ((ProgressViewHolders) holder).progressBar.setVisibility(View.VISIBLE);
            } else {

                ((ProgressViewHolders) holder).progressBar.setIndeterminate(false);
                ((ProgressViewHolders) holder).progressBar.setVisibility(View.GONE);
            }
        }
    }

    public void postwatching(final String idUser, final String id_noti, final String url) {
        // Hide all views

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("ttt", "onErrorResponse: "+error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_user", idUser);
                params.put("id_noti", id_noti);
                Log.d("zzzzzzzzzz","id user .."+idUser + " id_noti " +id_noti + " url " + url);
                return params;
            }

        };

        // Adding request to request queue
        strReq.setTag(this.getClass().getName());
        AppController.getInstance().addToRequestQueue(strReq);
    }

    public void setLoaded() {
        loading = false;
    }

    public String noti_update_even(String name1, String textnoti, String nameeven) {

        contents = name1 + " " + textnoti + " " + nameeven;

        return contents;
    }

    public String notiadapter(String name1, String name2, String textnoti, String nameeven, String number_user) {
        switch (Integer.parseInt(number_user)) {
            case 2:
                Log.d("txtnane1", "notiadapter: " + name1);
                contents = name1 + " " + textnoti + " " + nameeven;
                break;
            case 3:

                contents = name1 + ", " + name2 + " " + textnoti + " " + nameeven;
                break;
            default:
                int n = Integer.parseInt(number_user) - 2;
                contents = name1 + ", " + name2 + " " + context.getResources().getString(R.string.and) + " " + n + " " + context.getResources().getString(R.string.other_people) + " " + textnoti + " " + nameeven;

                break;
        }
        return contents;
    }

    public String notiaccept(String name1, String name2, String textnoti, String nameeven, String number_user) {
        switch (Integer.parseInt(number_user)) {
            case 1:


                contents = name1 + " " + textnoti + " " + nameeven;
                break;
            case 2:

                contents = name1 + ", " + name2 + " " + textnoti + " " + nameeven;
                break;
            default:
                int n = Integer.parseInt(number_user) - 2;
                contents = name1 + ", " + name2 + " " + context.getResources().getString(R.string.and) + " " + n + " " + context.getResources().getString(R.string.other_people) + " " + textnoti + " " + nameeven;

                break;
        }
        return contents;
    }

    public void add_noti(item_noti itemnew) {
        mlists.add(0, itemnew);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {

        return mlists.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mlists.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout itemlayout;
        public ImageView image_even;
        public CircleImageView image_user_even;
        public TextView txt_noti_name;
        public TextView txt_noti_time;
        public TextView txt_noti_conten;


        public ViewHolder(View itemView) {
            super(itemView);
            itemlayout = (LinearLayout) itemView.findViewById(R.id.layoutcolor);
            image_even = (ImageView) itemView.findViewById(R.id.image_even);
            image_user_even = (CircleImageView) itemView.findViewById(R.id.image_user_even);
            txt_noti_name = (TextView) itemView.findViewById(R.id.txt_noti_name);
            txt_noti_time = (TextView) itemView.findViewById(R.id.txt_noti_time);
            txt_noti_conten = (TextView) itemView.findViewById(R.id.txt_noti_content);
        }
    }

    public static class ProgressViewHolders extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolders(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progress_more);
        }
    }

}