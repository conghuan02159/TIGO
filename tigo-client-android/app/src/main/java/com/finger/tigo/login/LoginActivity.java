package com.finger.tigo.login;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.addfriend.AddFriendActivity;
import com.finger.tigo.app.AppIntent;
import com.finger.tigo.libcountrypicker.fragments.CountryPicker;
import com.finger.tigo.libcountrypicker.interfaces.CountryPickerListener;
import com.finger.tigo.libcountrypicker.models.Country;
import com.finger.tigo.login.async.AsynchLoginInteractor;
import com.finger.tigo.login.model.itemLogin;
import com.finger.tigo.session.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Duong on 2/27/2017.
 */

public class LoginActivity extends AppCompatActivity implements ILoginView {

    public static final String LOGIN_TIGON = "loginTigon";
    public static final String LOGIN_FACEBOOK = "loginFacebook";
    public static final int REQUEST_READ_CALENDAR = 123;

    private static final int PERMISSIONS_REQUEST_WRITE_CALENDAR = 0;


    EditText inputEmail;
    EditText inputPassword;
    LoginButton loginFaceBook;
    SessionManager session;
    TextView counTry,txt_forgot;
    ImageView imBanner;
    ImageView imLoginFb;
    Button butLogin;

    LoginPresenter presenter;
    private CallbackManager callbackManager;
    private CountryPicker mCountryPicker;
    private String strCode;
    ProgressDialog pDialog;
    private AccountManager mAccountManager;
    private static final String TAG = "LoginActivity";



    public static final String ACCOUNT_NAME_SPLITTER = "@";

    /**
     * The default email to populate the email field with.
     */
    public static final String EXTRA_EMAIL = "com.example.android.authenticatordemo.extra.EMAIL";
    private String password;
    Account account;
    boolean checkLoginFace = false;
//    private SQLiteHandler sqlite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(this);
        setContentView(R.layout.activity_login);
        inputEmail = (EditText)findViewById(R.id.email);
        inputPassword = (EditText)findViewById(R.id.password);
        loginFaceBook = (LoginButton)findViewById(R.id.login_button);
        counTry = (TextView)findViewById(R.id.tv_country);
        butLogin = (Button)findViewById(R.id.butLogin);
        txt_forgot = (TextView)findViewById(R.id.txt_forgot);
        mAccountManager = AccountManager.get(this);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

//        sqlite = new SQLiteHandler(this);
        session = new SessionManager(this);
        mCountryPicker = CountryPicker.newInstance(getResources().getString(R.string.select_country));
        Country country = mCountryPicker.getUserCountryInfo(this);
        session.setKeyCodeCountry(country.getDialCode());
        strCode = session.getKeyCodeCountry();
        counTry.setText(country.getDialCode());
        counTry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCountryPicker.show(getFragmentManager(), "COUNTRY_PICKER");
            }
        });
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        mCountryPicker.setListener(new CountryPickerListener() {
            @Override public void onSelectCountry(String name, String code, String dialCode,
                                                  int flagDrawableResID) {
                counTry.setText(dialCode);
                strCode = dialCode;
                mCountryPicker.dismiss();
            }
        });


        presenter = new LoginPresenter(this);

        loginFaceBook.setReadPermissions(Arrays.asList("public_profile, user_birthday"));
        callbackManager = CallbackManager.Factory.create();

        loginFaceBook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {


                                setProfileToView(object);
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException exception) {

            }
        });
        butLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginTigon(v);
            }
        });
        txt_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgotPass(v);
            }
        });

    }

    public void loginTigon(View view) {

        String phone = inputEmail.getText().toString();
        password = inputPassword.getText().toString();
        if(phone.equalsIgnoreCase("") || password.equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), R.string.error_login, Toast.LENGTH_SHORT).show();
        }else{
            pDialog.setMessage(getResources().getString(R.string.login));
            showDialog();
            if (phone.charAt(0) == '0') {
                phone = phone.substring(1);
                phone = strCode + phone;
            } else if (phone.charAt(0) == '+') {

            } else {
                phone = strCode + phone;
            }
            presenter.attemptLogin(phone, password);
        }
    }
    public void forgotPass(View view) {
        Intent intent = new Intent(LoginActivity.this, ForgotPassword.class);
        startActivity(intent);
    }

    @Override
    public void onSuccess(String success, List<itemLogin> list) {



        if(success.equalsIgnoreCase(LOGIN_TIGON)){
            hideDialog();
            checkLoginFace = false;

            for(itemLogin object: list){

//                sqlite.addUser(object.id,  object.name,object.phone, Integer.parseInt(object.sex), object.profilePic, object.birthday, object.createAt);


                session.set_id_user_session(object.id);
                session.setName_session(object.name);
                session.setmail_session(object.email);
                session.setphone_session(object.phone);
                session.setbirhtday_session(object.birthday);
                session.setpic_session(object.profilePic);

                if (session.getKeySoundEvent() == null) {
                    session.setSound("tigobabysmall");
                }
                session.setLogin(true);

                account = new Account(object.name, AppAccountManager.ACCOUNT_TYPE);
                if (mAccountManager.addAccountExplicitly(account, password, null)) {

                    if (Build.VERSION.SDK_INT >= 23) {
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED
                                && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {

                            checkAppPermissions();

                        }else if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED
                                && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED){

                            AppAccountManager.saveAccountUserDataAndSyncCalendar(this, account, String.valueOf(object.id), object.name);

                            Intent intent = new Intent(LoginActivity.this, AddFriendActivity.class);
                            startActivity(intent);

                            Intent a = new Intent();
                            a.setAction(AppIntent.ACTION_FINISH_ACTIVITY);
                            a.putExtra("finishFromLogin", true);
                            this.sendBroadcast(a);
                            finish();
                        }


                    }else{
                        AppAccountManager.saveAccountUserDataAndSyncCalendar(this, account, String.valueOf(object.id), object.name);

                        Intent intent = new Intent(LoginActivity.this, AddFriendActivity.class);

                        startActivity(intent);

                        Intent a = new Intent();
                        a.setAction(AppIntent.ACTION_FINISH_ACTIVITY);
                        a.putExtra("finishFromLogin", true);
                        this.sendBroadcast(a);
                        finish();
                    }






                } else {
                    Log.v(TAG,"no new account created");
                }




            }
        }else if(success.equalsIgnoreCase(LOGIN_FACEBOOK)) {

            hideDialog();
            checkLoginFace = true;
            for(itemLogin object: list){

                session.set_id_user_session(object.id);
                session.setName_session(object.name);
                session.setphone_session(object.phone);
                session.setpic_session(object.profilePic);

                if(object.state == 0) {

                    Intent intent = new Intent(LoginActivity.this, FacebookVerificode.class);
                    startActivity(intent);
                    finish();
                }else{

                    Account account = new Account(object.name, AppAccountManager.ACCOUNT_TYPE);
                    if (mAccountManager.addAccountExplicitly(account, password, null)) {

                        if (Build.VERSION.SDK_INT >= 23 ) {

                            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED
                                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {

                                checkAppPermissions();

                            }else if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED
                                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED){

                                AppAccountManager.saveAccountUserDataAndSyncCalendar(this, account, String.valueOf(object.id), object.name);

                                session.setLogin(true);
                                session.setIsLogerFb(true);
                                Intent intent = new Intent(LoginActivity.this, AddFriendActivity.class);
                                startActivity(intent);
                            }

                        }else{

                            AppAccountManager.saveAccountUserDataAndSyncCalendar(this, account, String.valueOf(object.id), object.name);

                            session.setLogin(true);
                            session.setIsLogerFb(true);
                            Intent intent = new Intent(LoginActivity.this, AddFriendActivity.class);
                            startActivity(intent);
                        }

                    } else {
                        Log.v(TAG,"no new account created");
                    }

//                    sqlite.addUser(object.id,  object.name,object.phone, Integer.parseInt(object.sex), object.profilePic, object.birthday, object.createAt);

                    Intent a = new Intent();
                    a.setAction(AppIntent.ACTION_FINISH_ACTIVITY);
                    a.putExtra("finishFromLogin", true);
                    this.sendBroadcast(a);
                    finish();
                }
            }
        }

    }

    @Override
    public void onError(String onError, int keyError) {
        if(onError.equalsIgnoreCase(LOGIN_TIGON)){
            hideDialog();
            switch (keyError){
                case AsynchLoginInteractor.KEY_ERROR_ONE:
                    Toast.makeText(getApplicationContext(), getString(R.string.error_password), Toast.LENGTH_SHORT).show();
                    break;
                case AsynchLoginInteractor.KEY_ERROR_TWO:
                    Toast.makeText(getApplicationContext(), R.string.information_not_correct, Toast.LENGTH_SHORT).show();
                    break;
                case AsynchLoginInteractor.KEY_ERROR_THREE:
                    Toast.makeText(getApplicationContext(), R.string.not_connect, Toast.LENGTH_SHORT).show();
                    break;
                case AsynchLoginInteractor.KEY_ERROR_FOUR:
                    Toast.makeText(getApplicationContext(), R.string.not_connect, Toast.LENGTH_SHORT).show();
                    break;
                default:
                    break;
            }
        }else if(onError.equalsIgnoreCase(LOGIN_FACEBOOK)){
            hideDialog();
            switch (keyError){
                case AsynchLoginInteractor.KEY_ERROR_ONE:
                    Toast.makeText(getApplicationContext(), R.string.not_connect, Toast.LENGTH_SHORT).show();
                    break;

                default:
                    break;
            }
        }

    }

    private void createAccountAndNext() {

    }

    private void setProfileToView(JSONObject jsonObject) {
        try {
            int sex;

            String genderf = jsonObject.getString("gender");
            if (genderf.equalsIgnoreCase("male")) {
                sex = 1;
            } else {
                sex = 0;
            }
            String nameface = jsonObject.getString("name");
            String idFace = jsonObject.getString("id");

            presenter.attemptLoginFacebook(idFace, nameface, sex);

        } catch (JSONException e) {

        }
    }
    private void checkAppPermissions() {

        // No explanation needed, we can request the permission.
        ActivityCompat.requestPermissions(LoginActivity.this,
                new String[]{Manifest.permission.WRITE_CALENDAR},
                PERMISSIONS_REQUEST_WRITE_CALENDAR);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_WRITE_CALENDAR: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay!
                    if(!checkLoginFace) {
                        session.setIsLogerFb(true);
                    }
                    AppAccountManager.saveAccountUserDataAndSyncCalendar(this, account, String.valueOf(session.get_id_user_session()), session.getLName_session());

                    Intent intent = new Intent(LoginActivity.this, AddFriendActivity.class);
                    startActivity(intent);
                    session.setLogin(true);
                    Intent a = new Intent();
                    a.setAction(AppIntent.ACTION_FINISH_ACTIVITY);
                    a.putExtra("finishFromLogin", true);
                    this.sendBroadcast(a);
                    finish();


                } else {
                    AccountManager am =  (AccountManager)this.getSystemService(Context.ACCOUNT_SERVICE);
                    Account[] accountsFromFirstApp = am.getAccountsByType(AppAccountManager.ACCOUNT_TYPE);
                    am.removeAccount(accountsFromFirstApp[0], null, null);
                    finish();
                    Toast.makeText(getApplicationContext(), R.string.user_rejected_calendar_write_permission, Toast.LENGTH_LONG).show();

                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


}
