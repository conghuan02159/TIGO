package com.finger.tigo.schedule.model;

/**
 * Created by Duong on 4/17/2017.
 */

public class ItemSchedule {

    public int year;
    public int month;
    public int day;
    public String title;
    public String id;
    public String color;
    public String place;
    public String time;


    public ItemSchedule(int year, int month, int day, String title, String id, String color,
                          String place, String time) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.title = title;
        this.id = id;
        this.color = color;
        this.place = place;
        this.time = time;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }


    public String getPlace() {
        return place;
    }

    public String getTime() {
        return time;
    }
}
