package com.finger.tigo.util;

import android.util.Log;

import com.finger.tigo.BuildConfig;


public class MyLog {
    public static final String LOG_TAG = MyLog.class.getSimpleName();

    /** Log Level Error **/
    public static final void e(String message) {
        if (BuildConfig.DEBUG)
            Log.e(LOG_TAG, buildLogMsg(message));
    }

    /** Log Level Error **/
    public static final void e(String tag, String message) {
        if (BuildConfig.DEBUG)
            Log.e(tag, buildLogMsg(message));
    }

    public static void e() {
        if (BuildConfig.DEBUG)
            Log.e(LOG_TAG, buildLogMsg(""));
    }

    /** Log Level Warning **/
    public static final void w(String message) {
        if (BuildConfig.DEBUG)
            Log.w(LOG_TAG, buildLogMsg(message));
    }

    /** Log Level Warning **/
    public static final void w(String tag, String message) {
        if (BuildConfig.DEBUG)
            Log.w(tag, buildLogMsg(message));
    }

    public static void i() {
        if (BuildConfig.DEBUG)
            Log.i(LOG_TAG, buildLogMsg(""));
    }

    /** Log Level Information **/
    public static final void i(String message) {
        if (BuildConfig.DEBUG)
            Log.i(LOG_TAG, buildLogMsg(message));
    }

    /** Log Level Information **/
    public static final void i(String tag, String message) {
        if (BuildConfig.DEBUG)
            Log.i(tag, buildLogMsg(message));
    }

    public static void d() {
        if (BuildConfig.DEBUG)
            Log.d(LOG_TAG, buildLogMsg(""));
    }

    /** Log Level Debug **/
    public static final void d(String message) {
        if (BuildConfig.DEBUG)
            Log.d(LOG_TAG, buildLogMsg(message));
    }

    /** Log Level Debug **/
    public static final void d(String tag, String message) {
        if (BuildConfig.DEBUG)
            Log.d(tag, buildLogMsg(message));
    }

    /** Log Level Verbose **/
    public static final void v(String message) {
        if (BuildConfig.DEBUG)
            Log.v(LOG_TAG, buildLogMsg(message));
    }

    /** Log Level Verbose **/
    public static final void v(String tag, String message) {
        if (BuildConfig.DEBUG)
            Log.v(tag, buildLogMsg(message));
    }

    public static String buildLogMsg(String message) {
        StackTraceElement ste = Thread.currentThread().getStackTrace()[4];

        StringBuilder sb = new StringBuilder();

        sb.append("[");
        try {
            sb.append(ste.getFileName().replace(".java", ""));
        } catch (NullPointerException e) {
            // 릴리즈 일때는 ste.getFileName() 이 null
        }
        sb.append("::");
        sb.append(ste.getMethodName());
        sb.append("] ");
        sb.append(message);

        return sb.toString();
    }
}
