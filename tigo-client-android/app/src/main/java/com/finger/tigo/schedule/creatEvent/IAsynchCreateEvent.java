package com.finger.tigo.schedule.creatEvent;

import com.finger.tigo.app.ApiExecuter;

/**
 * Created by Conghuan on 4/11/2017.
 */

public interface IAsynchCreateEvent {
    void validateCreateEvent(ApiExecuter.ApiProgressListener apiProgressListener, String progressMsg, ApiExecuter.ApiResultListener listener, final String idSc, final String idUser, final String status, final String location,
                             final String positions, final String daystart, final String dayfinish, final String mode, final String phone_event,
                             final String lg1, final String lg2, final String color, final String category, final String allowinvite,
                             final String alarm, final String description, final String imageFilePath, final int allday);
}
