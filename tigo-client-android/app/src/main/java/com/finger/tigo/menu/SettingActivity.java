package com.finger.tigo.menu;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.finger.tigo.LoadingActivity;
import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppIntent;
import com.finger.tigo.session.SessionManager;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;
import java.util.Map;

import me.leolin.shortcutbadger.ShortcutBadger;

import static com.finger.tigo.R.id.spChoose;

public class SettingActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    private SessionManager session;
    int id;

    TextView name, phone, email, birth;
    ImageView update;
    public static TextView tvSound;
    LinearLayout layout_about;


    Switch settingAlarm, setting_partici, setting_all, setting_invite, setting_comment;

    String iduser;


    public static Intent createIntent(Activity activity, Uri uri) {
        Intent intent = new Intent(activity, SettingActivity.class);
        intent.setData(uri);
        return intent;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thiet_lap);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        settingAlarm = (Switch) findViewById(R.id.toggleAlarm);
        setting_partici = (Switch) findViewById(R.id.togglepartici);
//        setting_all = (ToggleButton) findViewById(R.id.toggleAll);
        setting_invite = (Switch) findViewById(R.id.toggle_invitess);
        setting_comment = (Switch) findViewById(R.id.togglecomment);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.setting));
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        toolbar.setTitleTextColor(Color.WHITE);
        actionBar.setDisplayHomeAsUpEnabled(true);

        layout_about = (LinearLayout) findViewById(R.id.layout_about);
        layout_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),AboutActivity.class);
                startActivity(intent);
            }
        });

        session = new SessionManager(SettingActivity.this);

//        Intent i = getIntent();
//        iduser = i.getStringExtra("iduser");

        name = (TextView) findViewById(R.id.name);
//        phone = (TextView) findViewById(R.id.phone);
        email = (TextView) findViewById(R.id.email);
        birth = (TextView) findViewById(R.id.birthday);
        update = (ImageView) findViewById(R.id.update);
        tvSound = (TextView) findViewById(spChoose);
        final SessionManager session = new SessionManager(this);

        SettingNotification();

        setting_partici.setOnCheckedChangeListener(this);
        setting_invite.setOnCheckedChangeListener(this);
        setting_comment.setOnCheckedChangeListener(this);
        setting_partici.setOnCheckedChangeListener(this);
        settingAlarm.setOnCheckedChangeListener(this);

//        profilePic = (CircleImageView) findViewById(R.id.img_avatar);
//        imgview_backgroud = (ImageView) findViewById(R.id.imgbackgroud);
        //logout = (Button) v.findViewById(R.id.logout);

        final Uri uri = getIntent().getData();
//        mExecutor.submit(new LoadScaledImageTask(this, uri, profilePic, calcImageSize()));

        final String[] strArray = getResources().getStringArray(R.array.sound_array);

        final ArrayAdapter<String> spArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, strArray) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }

            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }

        }; //selected item will look like a spinner set from XML

        tvSound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomDialogClass cdd = new CustomDialogClass(SettingActivity.this);
                cdd.show();
            }

        });




        LinearLayout but_logout = (LinearLayout) findViewById(R.id.but_logout);
        but_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(SettingActivity.this);
                //Uncomment the below code to Set the message and title from the strings.xml file
                //builder.setMessage(R.string.dialog_message) .setTitle(R.string.dialog_title);
                //Setting message manually and performing action on button click
                builder.setMessage(R.string.want_logout)
                        .setCancelable(false)
                        .setPositiveButton(R.string.notification_yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int idd) {
                                logoutUser();
                            }
                        })
                        .setNegativeButton(R.string.notification_no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //  Action for 'NO' Button
                                dialog.cancel();
                            }
                        });

                //Creating dialog box
                android.app.AlertDialog alert = builder.create();
                //Setting the title manually
                alert.setTitle(R.string.logout);
                alert.show();

            }
        });

//        getUser();
    }

    private void SettingNotification() {

        if (session.getIsAlarm()==true) {
            settingAlarm.setChecked(true);
        } else {
            settingAlarm.setChecked(false);
        }

        if (session.getPref_setting_parti()==true) {
            setting_partici.setChecked(true);
        } else {
            setting_partici.setChecked(false);
        }

        if (session.getPref_setting_invite()==true) {
            setting_invite.setChecked(true);
        } else {
            setting_invite.setChecked(false);

        }
        if (session.getPref_setting_comment()==true) {
            setting_comment.setChecked(true);
        } else {
            setting_comment.setChecked(false);
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int i = buttonView.getId();
        if (i == R.id.togglepartici) {
            if (isChecked) {
                isChecked = true;
                session.setPref_setting_parti(true);

            } else {
                isChecked = false;
                session.setPref_setting_parti(false);
            }

        } else if (i == R.id.toggle_invitess) {
            if (isChecked) {
                isChecked = true;
                session.setPref_setting_invite(true);
            } else {
                isChecked = false;
                session.setPref_setting_invite(false);
            }

        } else if (i == R.id.togglecomment) {
            if (isChecked) {
                isChecked = true;
                session.setPref_setting_comment(true);
            } else {
                isChecked = false;
                session.setPref_setting_comment(false);
            }

        } else if (i == R.id.toggleAlarm) {
            if (isChecked) {
                session.setIsAlarm(false);
            } else {
                session.setIsAlarm(true);
            }


        } else {
        }
    }

    private void logoutUser() {
        iduser = AppAccountManager.getAppAccountUserId(getApplication());
        Logout_Token(iduser + "");

        AccountManager am = (AccountManager) this.getSystemService(Context.ACCOUNT_SERVICE);
        Account[] accountsFromFirstApp = am.getAccountsByType(AppAccountManager.ACCOUNT_TYPE);
        ContentResolver.setSyncAutomatically(accountsFromFirstApp[0], "com.android.calendar", false);
        ContentResolver.removePeriodicSync(accountsFromFirstApp[0], "com.android.calendar", Bundle.EMPTY);
        Uri evuri = CalendarContract.Calendars.CONTENT_URI;
        long calendarId = Long.parseLong(am.getUserData(accountsFromFirstApp[0], AppAccountManager.USER_DATA_CALENDAR));

        Uri deleteUri = ContentUris.withAppendedId(evuri, calendarId);
        getContentResolver().delete(deleteUri, null, null);

        am.removeAccount(accountsFromFirstApp[0], null, null);

        session.setLogin(false);
        session.set_id_user_session(-1);
        session.setName_session(null);

        session.setmail_session(null);
        session.setphone_session(null);
        session.setbirhtday_session(null);
        session.setsex_session(null);
        session.setpic_session(null);
        session.setKeyCodeCountry(null);
        session.setCountFriendNotification(0);
        session.setCountNotification(0);
        session.setCountNotificationScreen(0);
        ShortcutBadger.removeCount(getApplicationContext());
        // Launching the login activity
        if (session.getIsLogerFb()) {
            session.setIsLogerFb(false);
            LoginManager.getInstance().logOut();
        }


        Intent intent = new Intent(this, LoadingActivity.class);
        startActivity(intent);
        Intent a = new Intent();
        // countNotification = countNotification + 1;

        a.setAction(AppIntent.ACTION_CHANGE_NOTIFICATION_BADGE_BY_ADD_FRIEND);
        a.putExtra("finishFromSetting", true);
        this.sendBroadcast(a);

        finish();


    }

    private void Logout_Token(final String id_user) {
        // Tag used to cancel the request
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_LOGOUT_TOKEN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("SettingActivity", "Login Error: " + error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                String token = FirebaseInstanceId.getInstance().getToken();

                params.put("id_user", id_user);
                params.put("id_token", token);
                Log.d("lllllll", "id_user: " + id_user);
                return params;
            }
        };
        // Adding request to request queue
        com.finger.tigo.app.AppController.getInstance().addToRequestQueue(strReq);
    }

    @Override
    public void onResume() {
        super.onResume();


    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {//Write your logic here
            this.finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
//        getUser();
    }


}
