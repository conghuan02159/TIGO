package com.finger.tigo.detail.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.finger.tigo.R;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.detail.EventDetailActivity;
import com.finger.tigo.detail.adapter.RecyclerAdapterComment;
import com.finger.tigo.items.FeedItem;
import com.finger.tigo.listener.DividerItemDecoration;
import com.finger.tigo.session.SessionManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TabCommentFragment extends Fragment {

    RecyclerView recyclerView;
    TextView count_comment;
    TextView details_comment;
    private List<FeedItem> feedItems;
    private RecyclerAdapterComment listAdapter;
    SessionManager session;

    String id;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.activity_tab_comment_fragment, container, false);
        recyclerView = (RecyclerView)v.findViewById(R.id.listcomment);
        count_comment = (TextView)v.findViewById(R.id.count_comment);
        details_comment = (TextView)v.findViewById(R.id.details_comment);

        feedItems = new ArrayList<>();

        session = new SessionManager(getActivity());

        id = EventDetailActivity.idSc;
        initRecyclerView();
        downloadComment(id);


//        count_comment.setText(EventDetailActivity.countComment);
//        details_comment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(getActivity(), CommentActivity.class);
//                i.putExtra("mDetailsHomeActivity", id + "");
//                i.putExtra("countcomment", EventDetailActivity.countComment);
//                startActivity(i);
//            }
//        });
        return v;
    }


    private void initRecyclerView() {
        feedItems = new ArrayList<>();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        listAdapter = new RecyclerAdapterComment( feedItems, getActivity(), null);
        recyclerView.setAdapter(listAdapter);

//        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerTouchListener.ClickListener() {
//            @Override
//            public void onClick(View view, int position) {
//                FeedItem item = feedItems.get(position);
//                Intent i = new Intent(getActivity(), EventDetailActivity.class);
//
//                i.putExtra(M_DETAILS_HOME_ACTIVITY, item.getId_sc() + "");
//                startActivity(i);
//            }
//
//            @Override
//            public void onLongClick(View view, int position) {
//
//            }
//        }));

    }


    private void downloadComment(final String id_sc) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_TOP_COMMENT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    JSONArray feedArray = jObj.getJSONArray("comments");
                    feedItems.clear();
                    for (int i = 0; i < feedArray.length(); i++) {
                        final JSONObject feedComment = (JSONObject) feedArray.get(i);
                        FeedItem item = new FeedItem();

                        item.setIdComment(feedComment.getString("ID_USER"));
                        item.setNamecomment(feedComment.getString("name"));
                        item.setAvatarComment(feedComment.getString("profilePic"));
                        item.setStatusComment(feedComment.getString("ContainerComment"));
                        item.setTimecomment(feedComment.getString("Currendate"));
                        item.setPhone(feedComment.getString("phone"));
                        // get repaly comment
                        item.setCountcommentplay(feedComment.getString("CountComment"));
                        item.setIdRComment(feedComment.getString("ID_COMMENT"));


                        String idUserReplayComment = feedComment.isNull("iduserReplay") ? null : feedComment.getString("iduserReplay");
                        String nameReplay = feedComment.isNull("nameReplay") ? null : feedComment.getString("nameReplay");
                        String profilePic = feedComment.isNull("profilePicReplay") ? null : feedComment.getString("profilePicReplay");
                        String ContainerCommentRply = feedComment.isNull("ContainerCommentRply") ? null : feedComment.getString("ContainerCommentRply");
                        String currentdateRply = feedComment.isNull("currentdateRply") ? null : feedComment.getString("currentdateRply");



                        item.setIdUserReplayComment(idUserReplayComment);
                        item.setNameReplay(nameReplay);
                        item.setProfilePicReplay(profilePic);
                        item.setContainerCommentRply(ContainerCommentRply);
                        item.setCurrendateCommentRply(currentdateRply);

                        //Log.d("ccccc", "onLongClick:" + item.getId_sc());
                        feedItems.add(item);
                    }
                    doawloadsuccess();

                } catch (Exception ex) {
                    // JSON parsing error

                    //Log.d("tttttt", "error: "+ex.getMessage());

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.d("tttt", "onErrorResponse: "+error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("id_sc", id_sc);

                return params;
            }
        };
        strReq.setTag(this.getClass().getName());
        AppController.getInstance().addToRequestQueue(strReq);
    }

    private void doawloadsuccess() {
        recyclerView.setVisibility(View.VISIBLE);
//        progressBar.setVisibility(View.INVISIBLE);
        listAdapter.notifyDataSetChanged();
    }



    @Override
    public void onResume() {
        super.onResume();
        downloadComment(id);
    }
}
