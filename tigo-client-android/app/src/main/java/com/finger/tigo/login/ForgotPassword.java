package com.finger.tigo.login;

/**
 * Created by Duong on 3/29/2017.
 */

import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.finger.tigo.R;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.items.ItemAddFr;
import com.finger.tigo.libcountrypicker.fragments.CountryPicker;
import com.finger.tigo.libcountrypicker.interfaces.CountryPickerListener;
import com.finger.tigo.login.async.AsynchLoginInteractor;
import com.finger.tigo.login.model.itemLogin;
import com.finger.tigo.register.stateprogress.StateProgressBar;
import com.finger.tigo.service.OnSmsCatchListener;
import com.finger.tigo.service.SmsVerifyCatcher;
import com.finger.tigo.session.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ForgotPassword extends AppCompatActivity implements ILoginView{

    public static final String SEARCH_PHONE = "searchPhone";
    public static final String FORGOT_OTP = "forgotOtp";
    public static final String RESET_PASSWORD = "resetPassword";
    public static String TAG = "ForgetPassword";

    String descriptionData[] = {"Phone", "Verify", "Password"};

    EditText inputPhone;
    EditText et_verificode;
    TextView count_time;
    Button bResend,bt_countrycode,btn_next,btn_edit_mobile,btn_finish,btVerificode;
    TextView tvMobile;
    TextView tvCountry;
    EditText password;
    EditText repeatPass;
    LinearLayout lnPhoneNumber;
    LinearLayout layoutVerifiCode;
    LinearLayout layoutResetPass;
    TextInputLayout til;

    String strPhone = "1";
    String StrCode;
    String strUser;

    ArrayList<ItemAddFr> arraySearch = new ArrayList<>();

    private int currentTime;
    private int waitingTime = 30;
    private CountryPicker mCountryPicker;

    LoginPresenter presenter;
    ProgressDialog progressDialog;
    SessionManager session;
    SmsVerifyCatcher smsVerifyCatcher;
    StateProgressBar stateProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        inputPhone = (EditText)findViewById(R.id.et_phonenumber);
        et_verificode = (EditText)findViewById(R.id.et_verificode);
        count_time = (TextView)findViewById(R.id.count_time);
        bResend = (Button)findViewById(R.id.btnResend);
        tvMobile = (TextView)findViewById(R.id.txt_edit_mobile);
        tvCountry = (TextView)findViewById(R.id.tv_country);
        password = (EditText)findViewById(R.id.et_password);
        repeatPass = (EditText)findViewById(R.id.et_repeat_password);
        lnPhoneNumber = (LinearLayout)findViewById(R.id.lnPhoneNumber);
        layoutVerifiCode = (LinearLayout)findViewById(R.id.ln_verificode);
        layoutResetPass = (LinearLayout)findViewById(R.id.ln_reset_pass);
        til = (TextInputLayout)findViewById(R.id.txt_phonenumber_layout);
        bt_countrycode = (Button)findViewById(R.id.bt_countrycode);
        btn_next = (Button)findViewById(R.id.btn_next);
        btn_edit_mobile = (Button)findViewById(R.id.btn_edit_mobile);
        btn_finish = (Button)findViewById(R.id.btn_finish);
        btVerificode = (Button)findViewById(R.id.btVerificode);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        presenter = new LoginPresenter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        stateProgressBar = (StateProgressBar) findViewById(R.id.usage_stateprogressbar);
        stateProgressBar.setStateDescriptionData(descriptionData);
        stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.ONE);
         session = new SessionManager(getApplicationContext());

        tvCountry.setText(session.getKeyCodeCountry());
        StrCode = session.getKeyCodeCountry();
        currentTime = waitingTime;
        count_time.setText("" + currentTime);

        mCountryPicker = CountryPicker.newInstance(getResources().getString(R.string.select_country));

        mCountryPicker.setListener(new CountryPickerListener() {
            @Override public void onSelectCountry(String name, String code, String dialCode,
                                                  int flagDrawableResID) {
                tvCountry.setText(dialCode);
                StrCode = dialCode;
                mCountryPicker.dismiss();
            }
        });

        currentTime = waitingTime;
        count_time.setText("" + currentTime);

        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                String code = parseCode(message);//Parse verification code
                et_verificode.setText(code);//set code in edit text


                //then you can send verification code to server
            }
        });
        bt_countrycode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CountryCode();
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhoneNumber();
            }
        });
        bResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResendOtp();
            }
        });
        btn_edit_mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditMobile();
            }
        });
        btn_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Finish();
            }
        });
        btVerificode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Verificode();
            }
        });
    }
    private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{6}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }


    public void CountryCode(){
        mCountryPicker.show(getFragmentManager(), "COUNTRY_PICKER");
    }


    public void PhoneNumber(){
        strPhone = inputPhone.getText().toString().trim();
        submitForm();
    }


    public void ResendOtp(){
        currentTime = waitingTime;
        count_time.setText(currentTime+"");
        handler.removeCallbacks(runnable);
        runnable.run();
        bResend.setVisibility(View.GONE);
        putVerifiCode();
    }



    public void EditMobile(){
        handler.removeCallbacks(runnable);
        layoutVerifiCode.setVisibility(View.GONE);
        lnPhoneNumber.setVisibility(View.VISIBLE);
        bResend.setVisibility(View.GONE);

    }

    // reset pass

    public void Finish(){
        String strPassword = password.getText().toString();
        String strRepeatPass =repeatPass.getText().toString();
        if (strPassword.equals("")&& strRepeatPass.equals("")) {
            Toast.makeText(getApplicationContext(),R.string.error_the_password, Toast.LENGTH_SHORT).show();
        } else if(!strPassword.equals(strRepeatPass)){
            Toast.makeText(getApplicationContext(),R.string.error_the_password, Toast.LENGTH_SHORT).show();
        }else{
            progressDialog.setMessage(getResources().getString(R.string.reset_password));
            showDialog();
            presenter.attemptResetPass(strUser, strPassword);
        }
    }


    public void Verificode(){
        String otp = et_verificode.getText().toString().trim();
        if (!otp.isEmpty()) {
            progressDialog.setMessage(getResources().getString(R.string.create_verificode));
            showDialog();
            presenter.attemptForgotOtp(otp);
        } else {
            Toast.makeText(getApplicationContext(), R.string.error_otp, Toast.LENGTH_SHORT).show();
        }

    }

    private void submitForm() {
        if (!validatePhoneNumber()) {
            return;
        }

        if (strPhone.charAt(0) == '0') {
            strPhone = strPhone.substring(1);
            strPhone = StrCode + strPhone;
        } else if (strPhone.charAt(0) == '+') {

        } else {
            strPhone =  StrCode + strPhone;
        }
        progressDialog.setMessage(getResources().getString(R.string.create_phone));
        showDialog();

        presenter.attemptCheckPhone(strPhone);
    }
    private boolean validatePhoneNumber() {

        if (inputPhone.getText().toString().trim().isEmpty()) {

            til.setErrorEnabled(true);
            til.setError(getString(R.string.empty_phone));

            requestFocus(til);
            return false;
        }else if (!isValidPhoneNumber(inputPhone.getText().toString())) {

            til.setErrorEnabled(true);
            til.setError(getString(R.string.invalid_phone));
            requestFocus(til);
            return false;
        }else{
            til.setErrorEnabled(false);

        }
        return true;
    }
    private static boolean isValidPhoneNumber(String mobile) {
        String regEx = "^[0-9]{10,15}$";
        return mobile.matches(regEx);
    }
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void putVerifiCode() {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_RESEND_VERIFICODE, new Response.Listener<String>(){

            @Override
            public void onResponse(String response) {

                try{
                    JSONObject jObj = new JSONObject(response);
                    JSONArray feedArray = jObj.getJSONArray("feed");

                }catch (JSONException e){
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Registration Error: " + error.getMessage());

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                String idUser = session.get_id_user_session()+"";
                params.put("id_user", idUser);
                params.put("phone", strPhone);

                return params;

            }

        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq);

    }

    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        public void run() {

            currentTime--;
//
            if (currentTime == 0) {
                handler.removeCallbacks(runnable);
                bResend.setVisibility(View.VISIBLE);
                count_time.setText("" + currentTime);
            } else if (currentTime >= 0) {
                count_time.setText("" + currentTime);

                handler.postDelayed(this, 1000);

            }
        }
    };

    @Override
    public void onSuccess(String success, List<itemLogin> list) {
        if(success.equalsIgnoreCase(SEARCH_PHONE)) {
            hideDialog();
            layoutVerifiCode.setVisibility(View.VISIBLE);
            lnPhoneNumber.setVisibility(View.GONE);
            currentTime = waitingTime;
            count_time.setText(currentTime + "");
            runnable.run();
            tvMobile.setText(strPhone);
            for(itemLogin object: list)
                strUser = object.id+"";
            stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);

        }else if(success.equalsIgnoreCase(FORGOT_OTP)){
            hideDialog();
            layoutResetPass.setVisibility(View.VISIBLE);
            layoutVerifiCode.setVisibility(View.GONE);
            stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);

        }else if(success.equalsIgnoreCase(RESET_PASSWORD)){
            hideDialog();
            Toast.makeText(getApplicationContext(), R.string.changed_password, Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public void onError(String error, int key) {
        if(error.equalsIgnoreCase(SEARCH_PHONE)) {
            hideDialog();
            switch (key) {
                case AsynchLoginInteractor.KEY_ERROR_ONE:
                    Toast.makeText(getApplicationContext(), R.string.phonenumber_not_registered, Toast.LENGTH_SHORT).show();
                    break;
                case AsynchLoginInteractor.KEY_ERROR_TWO:
                    Toast.makeText(getApplicationContext(), R.string.notconnect, Toast.LENGTH_SHORT).show();
                    break;
            }
        }else if(error.equalsIgnoreCase(FORGOT_OTP)){
            hideDialog();
            switch (key) {
                case AsynchLoginInteractor.KEY_ERROR_ONE:

                    Toast.makeText(getApplicationContext(), R.string.error_verificode, Toast.LENGTH_SHORT).show();
                    break;
                case AsynchLoginInteractor.KEY_ERROR_TWO:
                    Toast.makeText(getApplicationContext(), R.string.notconnect, Toast.LENGTH_SHORT).show();
                    break;
            }
        }else if(error.equalsIgnoreCase(RESET_PASSWORD)){
            hideDialog();
            switch (key) {
                case AsynchLoginInteractor.KEY_ERROR_ONE:
                    Toast.makeText(getApplicationContext(), R.string.notconnect, Toast.LENGTH_SHORT).show();
                    break;

            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsVerifyCatcher.onStop();
    }

    /**
     * need for Android 6 real time permissions
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
    private void showDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    private void hideDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }
}


