package com.finger.tigo.register.model;

/**
 * Created by Duong on 3/24/2017.
 */

public class ItemRegister {

    public String id;
    public String name;
    public String sex;
    public String profilePic;
    public String phone;
    public String birthday;
    public String countEvent;
    public String countFriend;
    public int confirm;

    public ItemRegister(String id, String name, String birthday, String sex, String phone){
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.birthday = birthday;
        this.phone = phone;
    }

    public ItemRegister(String name, String phone){
        this.name = name;
        this.phone = phone;
    }

    public ItemRegister (String id, String name, String profilePic){
        this.id= id;
        this.name = name;
        this.profilePic = profilePic;
    }

    public ItemRegister(int confirm, String countFriend, String countEvent){
        this.confirm = confirm;
        this.countFriend = countFriend;
        this.countEvent = countEvent;

    }
    public ItemRegister(int confirm){
        this.confirm = confirm;
    }
}
