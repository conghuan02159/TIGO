package com.finger.tigo.detail.async;

import com.finger.tigo.app.ApiExecuter;

/**
 * Created by Duong on 4/5/2017.
 */

public interface IEventDetailAsyncInteractor {

    void InteractorGetEventDetail(ApiExecuter.ApiProgressListener apiProgressListener, ApiExecuter.ApiResultListener apiResultListener, String idUser, String idSc);
    void interactorJoinOrNotJoin(ApiExecuter.ApiProgressListener apiProgressListener, String progressMsg, ApiExecuter.ApiResultListener apiResultListener, String idUser,  String idSc, String state);
    void IterractorInvite(ApiExecuter.ApiProgressListener apiProgressListener, ApiExecuter.ApiResultListener apiResultListener, String idUser, String id_sc,String state);

}
