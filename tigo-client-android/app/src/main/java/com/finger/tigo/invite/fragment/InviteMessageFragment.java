package com.finger.tigo.invite.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.finger.tigo.BaseFragment;
import com.finger.tigo.R;
import com.finger.tigo.common.contact.Contact;
import com.finger.tigo.common.contact.ContactsLoadListener;
import com.finger.tigo.common.contact.ContactsLoader;
import com.finger.tigo.detail.EventDetailActivity;
import com.finger.tigo.invite.InviteActivity;
import com.finger.tigo.invite.adapter.MessageRecylerAdapter;
import com.finger.tigo.invite.adapter.MyRecyclerAdapterMessageDialog;
import com.finger.tigo.libautolabel.AutoLabelUI;
import com.finger.tigo.libautolabel.Label;
import com.finger.tigo.schedule.creatEvent.Person;

import java.util.ArrayList;
import java.util.List;


public class InviteMessageFragment extends BaseFragment {
    private AutoLabelUI mAutoLabel;
    private final String KEY_INSTANCE_STATE_PEOPLE = "statePeople";
    private static final int PERMISSION_REQUEST_CONTACT= 123;
    String swap;
    public static List<Person> mPersonList;
    public static List<Person> filteredList;
    public static List<Person> filteredListMessageDialog;
    private MessageRecylerAdapter adapter;
    private MyRecyclerAdapterMessageDialog adapterMessageDialog;
    private RecyclerView recyclerView,recyclerViewMessageDialog;
    private EditText searchView;
    private Button btSent;
    int counter=0;
    private TextView txtCounter;
    private LinearLayout layoutlabel;
    AlertDialog.Builder dialog;
    View dialogView;
    private Person per;
    boolean isSelected;
    ProgressBar progressinvite;
    int i=0;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.activity_sent_message, container, false);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        init(inflatedView);
        setRecyclerView();
        return inflatedView;
    }
    private void init(View v) {
        progressinvite=(ProgressBar)v.findViewById(R.id.progressinvite);
        recyclerView = (RecyclerView)v.findViewById(R.id.recyclerView);
        btSent = (Button) v.findViewById(R.id.sent);
        txtCounter=(TextView)v.findViewById(R.id.txtCount);
        layoutlabel=(LinearLayout)v.findViewById(R.id.layoutlabel);

        mAutoLabel=(AutoLabelUI)v.findViewById(R.id.label_view_mg);
        mAutoLabel.setBackgroundResource(R.drawable.round_corner_background);
        searchView = (EditText) v.findViewById(R.id.searchView);
        progressinvite.setVisibility(View.INVISIBLE);
        searchView.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                InviteMessageFragment.this.adapter.getFilter().filter(s.toString());

            }

        });
        layoutlabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowDialog();
            }
        });
        btSent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.dialog_send_message)
                        .setCancelable(false)
                        .setPositiveButton(R.string.notification_yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int idd) {

                                if (filteredList != null && !filteredList.isEmpty())

                                    for (int i = 0; i < filteredList.size(); i++) {
                                        if (filteredList.get(i).isSelected() == true) {
                                            swap = swap + ";" + filteredList.get(i).getAge();
                                        }
                                    }
                                Intent intent = getActivity().getIntent();
                                String strEvent = intent.getStringExtra(EventDetailActivity.PUT_EVENT);
                                Intent smsIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + swap));
                                smsIntent.putExtra("sms_body", "You are invited to participate: " + strEvent + ". download the app to join the event");
                                startActivity(smsIntent);
                            }
                        })
                        .setNegativeButton(R.string.notification_no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //  Action for 'NO' Button
                                dialog.cancel();
                            }
                        });

                //Creating dialog box
                android.app.AlertDialog alert = builder.create();
                //Setting the title manually
                alert.setTitle(R.string.dialog_title);
                alert.show();
            }
        });


//        btSent.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(filteredList!=null && !filteredList.isEmpty())
//
//                    for(int i =0; i<filteredList.size();i++){
//                        if(filteredList.get(i).isSelected()== true){
//                            swap = swap+";"+filteredList.get(i).getAge();
//                        }
//                    }
//                Intent intent = getActivity().getIntent();
//                String strEvent = intent.getStringExtra(EventDetailActivity.PUT_EVENT);
//                Intent smsIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:"+swap));
//                smsIntent.putExtra("sms_body", R.string.invite_message +strEvent+ R.string.invite_message1);
//                startActivity(smsIntent);
//            }
//        });
    }

    private void ShowDialog() {
        dialog=new AlertDialog.Builder(getActivity());
        dialog.setTitle(R.string.list_selected);
        dialog.setPositiveButton(R.string.cancelnot, null);
        dialog.setNegativeButton(R.string.ok_password, null);

        LayoutInflater inflater=getActivity().getLayoutInflater();
        dialogView=inflater.inflate(R.layout.fragment_invite_message_dialog,null);
        dialog.setView(dialogView);

        recyclerViewMessageDialog = (RecyclerView)dialogView.findViewById(R.id.recyclerViewmessagedialog);
        recyclerViewMessageDialog.setLayoutManager(new LinearLayoutManager(dialogView.getContext()));
        filteredListMessageDialog = new ArrayList<Person>();
        adapterMessageDialog=new MyRecyclerAdapterMessageDialog(filteredListMessageDialog);
        recyclerViewMessageDialog.setAdapter(adapterMessageDialog);
        final AlertDialog alertDialog=dialog.create();
        alertDialog.show();
        Button positiveButton=alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        positiveButton.setTextColor(getResources().getColor(android.R.color.black));
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPositiveButtonClicked(alertDialog);
            }
        });
        Button negativeButton=alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        negativeButton.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNegativeButtonClicked(alertDialog);
            }
        });

        showlog();
        //adapterMessageDialog.notifyDataSetChanged();
    }
    private void showlog()
    {
        if(filteredList!=null&&!filteredList.isEmpty())
        {

            for(int i=0;i<filteredList.size();i++)
            {
                if(filteredList.get(i).isSelected())
                {
                    filteredListMessageDialog.add(new Person(filteredList.get(i).name, filteredList.get(i).id, true,i));
                }
            }
            //Log.e("DDDD",filteredListMessageDialog.get(1).getName());
            adapterMessageDialog.notifyDataSetChanged();

            adapterMessageDialog.setmItemClickListener(new MyRecyclerAdapterMessageDialog.OnItemClickListener() {
                @Override
                public void onItemClick(View v, int position) {

                    itemListClickedDialog(position);
                }
            });


        }
    }
    private void onPositiveButtonClicked(AlertDialog alertDialog) {

        alertDialog.cancel();
    }
    private void onNegativeButtonClicked(AlertDialog alertDialog) {
        if(filteredListMessageDialog!=null && !filteredListMessageDialog.isEmpty()){
            for(int i =0; i< filteredListMessageDialog.size();i++){
                if(!filteredListMessageDialog.get(i).isSelected()){
                    itemListClicked(filteredListMessageDialog.get(i).position);
                }
            }
        }

        alertDialog.dismiss();
    }

    public void loadContacts(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.READ_CONTACTS)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(R.string.contacts_access);
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setMessage(R.string.please_contacts_access);//TODO put real question
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            requestPermissions(
                                    new String[]
                                            {Manifest.permission.READ_CONTACTS}
                                    , PERMISSION_REQUEST_CONTACT);
                        }
                    });
                    builder.show();
                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.READ_CONTACTS},
                            PERMISSION_REQUEST_CONTACT);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            }else{
                getAllContacts();
            }
        }
        else{
            getAllContacts();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CONTACT: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getAllContacts();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    // ToastMaster.showMessage(getActivity(),"No permission for contacts");
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    private void getAllContacts() {

        ContactsLoader contactLoader = new ContactsLoader(getActivity(), new ContactsLoadListener() {
            @Override
            public void onLoadStarted() {

            }

            @Override
            public void onLoading(int percent) {
                if(getActivity() != null && ((InviteActivity) getActivity()).getCurrentTab() == 1) {
                    showProgress("loading contacts... " + percent + "%");
                } else {
                    hideProgress();
                }
            }

            @Override
            public void onLoadCompleted(List<Contact> contacts) {
                hideProgress();
                mPersonList = new ArrayList<>();

                for (Contact contact : contacts) {
                    mPersonList.add(new Person(contact.name, contact.phone, false));
                }

                filteredList = new ArrayList<Person>();
                if(mPersonList !=null) {
                    filteredList.addAll(mPersonList);
                    adapter = new MessageRecylerAdapter(filteredList);
                    recyclerView.setAdapter(adapter);
                    adapter.setmItemClickListener(new MessageRecylerAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(View v, int position) {
                            itemListClicked(position);
                        }
                    });
                }
            }
        });

        contactLoader.execute();
    }

    public void setRecyclerView() {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm);
        loadContacts();
    }
    private void itemListClicked(int position){
        per = filteredList.get(position);
        isSelected = per.isSelected();
        boolean success;

        if(isSelected){
            adapter.setItemSelected(position, false);
            success = mAutoLabel.removeName(position);
            i--;
            if(i>1)
            {
                txtCounter.setText("+"+(i-1)+ getResources().getString(R.string.invite_send));
                txtCounter.setVisibility(View.VISIBLE);
            }
            else
            {
                txtCounter.setVisibility(View.GONE);
            }
        }else {
            adapter.setItemSelected(position, true);
            success = mAutoLabel.addLabel( position, per.getName());
            i++;
            if(i>1)
            {
                txtCounter.setText("+"+(i-1)+ getResources().getString(R.string.invite_send));
                txtCounter.setVisibility(View.VISIBLE);
            }
            else
            {
                txtCounter.setVisibility(View.GONE);
            }
        }
        if (success) {
            adapter.setItemSelected(position, !isSelected);
        }
    }

    private void itemListClickedDialog(int position){

        per = filteredListMessageDialog.get(position);
        isSelected = per.isSelected();
        boolean success;

        if(isSelected){
            adapterMessageDialog.setItemSelected(position, false);
//            adapter.setItemSelected(per.position, false);
//            success = mAutoLabel.removeName(per.position);
//
//            i--;
//            if(i>2)
//            {
//                txtCounter.setText(""+i);
//                txtCounter.setVisibility(View.VISIBLE);
//            }
//            else
//            {
//                txtCounter.setVisibility(View.GONE);
//            }
        }else {
            adapterMessageDialog.setItemSelected(position, true);
//            adapter.setItemSelected(per.position, true);
//
//            success = mAutoLabel.addLabel( per.position, per.getName());
//            i++;
//            if(i>2)
//            {
//                txtCounter.setText(""+i);
//                txtCounter.setVisibility(View.VISIBLE);
//            }
//            else
//            {
//                txtCounter.setVisibility(View.GONE);
//            }
        }
//        if (success) {
//            adapterMessageDialog.setItemSelected(position, !isSelected);
//        }
    }
    private void setListeners() {
        mAutoLabel.setOnLabelsCompletedListener(new AutoLabelUI.OnLabelsCompletedListener() {
            @Override
            public void onLabelsCompleted() {
                Snackbar.make(recyclerView, R.string.complete, Snackbar.LENGTH_SHORT).show();
            }
        });
        mAutoLabel.setOnRemoveLabelListener(new AutoLabelUI.OnRemoveLabelListener() {
            @Override
            public void onRemoveLabel(Label removedLabel, int position) {
                adapter.setItemSelected(position, false);

                i--;
                if(i>2)
                {
                    txtCounter.setText(""+i);
                    txtCounter.setVisibility(View.VISIBLE);
                }
                else
                {
                    txtCounter.setVisibility(View.GONE);
                }
            }
        });
        mAutoLabel.setOnLabelsEmptyListener(new AutoLabelUI.OnLabelsEmptyListener() {
            @Override
            public void onLabelsEmpty() {
                Snackbar.make(recyclerView,R.string.empty, Snackbar.LENGTH_SHORT).show();
            }
        });

        mAutoLabel.setOnLabelClickListener(new AutoLabelUI.OnLabelClickListener() {
            @Override
            public void onClickLabel(Label labelClicked) {
                Snackbar.make(recyclerView, labelClicked.getText(), Snackbar.LENGTH_SHORT).show();
            }
        });

    }


}

