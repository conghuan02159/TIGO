package com.finger.tigo.friend;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.finger.tigo.BaseActivity;
import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.addfriend.AddNewFriendActivity;
import com.finger.tigo.addfriend.DetailFriendActivity;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.app.AppIntent;
import com.finger.tigo.friend.adapter.HorizontalAdapter;
import com.finger.tigo.friend.adapter.ListFriendAdapter;
import com.finger.tigo.items.ItemNoti;
import com.finger.tigo.listener.RecyclerTouchListener;
import com.finger.tigo.notification.adapter.NotificationFriendAdapter;
import com.finger.tigo.session.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Duong on 3/14/2017.
 */

public class ListFriendActivity extends BaseActivity implements NotificationFriendAdapter.itemClickNotificationFriend {


    public static final String LISTFRIEND = "listfriend";

    RecyclerView listNotification;
    RecyclerView listFriend;
    RecyclerView recylerNewFriend;

    LinearLayout lnNotFriend;
    LinearLayout layoutNotification;
    LinearLayout layoutNewFriend;
    LinearLayout layoutFriend;

    TextView tvCountTigonNoti;
    TextView tvCountNewTigon;
    TextView tvTigon;
    TextView tvContinue;
    TextView tvPlus;
    TextView tvNewTigonCount;
    

    private NotificationFriendAdapter listAdapter;
    private HorizontalAdapter horizontalAdapter;
    private ListFriendAdapter friendAdapter;
    private List<ItemNoti> feedItems;
    private List<ItemNoti> horizontalItem;
    private List<ItemNoti> friendItem;

    private String idUser;
    private String s;

    SessionManager session;
    Toolbar toolbar;

    FloatingActionButton fab;
    int countNewTigon = 0;



    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_tigon);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        fab = (FloatingActionButton)findViewById(R.id.fab);
        listNotification = (RecyclerView) findViewById(R.id.listNotifi);
        listFriend = ( RecyclerView) findViewById(R.id.listFriend);
        recylerNewFriend = (RecyclerView) findViewById(R.id.recylernewfriend);
        lnNotFriend = (LinearLayout) findViewById(R.id.activity_not_friend);
        layoutNotification = (LinearLayout)findViewById(R.id.layoutNotification);
        layoutNewFriend = (LinearLayout) findViewById(R.id.layoutNewFriend);
        layoutFriend = (LinearLayout) findViewById(R.id.layoutFriend);
        tvCountTigonNoti = (TextView) findViewById(R.id.tv_count_tigon_notifi);
        tvCountNewTigon = (TextView) findViewById(R.id.tv_new_tigon);
        tvTigon = (TextView) findViewById(R.id.tv_tigon);
        tvContinue = (TextView) findViewById(R.id.tv_continue);
        tvPlus = (TextView) findViewById(R.id.tv_plus);
        tvNewTigonCount = (TextView) findViewById(R.id.txt_new_tigon_count);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitle("TiGon");
        session = new SessionManager(this);
        idUser = AppAccountManager.getAppAccountUserId(this);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fab();
            }
        });

        tvContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ListFriendActivity.this, TigonRequestActivity.class);
                startActivity(i);
            }
        });
        tvNewTigonCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ListFriendActivity.this, TigonNewFriendActivity.class);
                startActivity(i);
            }
        });

        initListNotificationfriend();
        initListNewFriend();
        initListFriend();

        showProgress(getResources().getString(R.string.loading));
        setNotificationLIstFriend();

    }

    public void fab(){
        Intent intent = new Intent(ListFriendActivity.this,AddNewFriendActivity.class);
        startActivity(intent);
    }

    private void initListNotificationfriend() {
        feedItems = new ArrayList<>();
        listAdapter = new NotificationFriendAdapter(ListFriendActivity.this, feedItems, this);
        listNotification.setHasFixedSize(true);
        RecyclerView.LayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(ListFriendActivity.this, LinearLayoutManager.VERTICAL, false);
        listNotification.setLayoutManager(horizontalLayoutManagaer);

        listNotification.setItemAnimator(new DefaultItemAnimator());
        listNotification.setAdapter(listAdapter);
    }

    private void initListNewFriend(){
        // listNewFriend.setVisibility(View.INVISIBLE);
        horizontalItem = new ArrayList<>();
        horizontalAdapter = new HorizontalAdapter(ListFriendActivity.this,horizontalItem);
        recylerNewFriend.setHasFixedSize(true);
        RecyclerView.LayoutManager horizontalLayoutManager
                = new LinearLayoutManager(ListFriendActivity.this, LinearLayoutManager.HORIZONTAL, false);
        recylerNewFriend.setLayoutManager(horizontalLayoutManager);
        recylerNewFriend.setItemAnimator(new DefaultItemAnimator());
        recylerNewFriend.setAdapter(horizontalAdapter);

        recylerNewFriend.addOnItemTouchListener(new RecyclerTouchListener(ListFriendActivity.this, recylerNewFriend, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                ItemNoti item = horizontalItem.get(position);
                updateWatchedFriend( item.getIdFriend());
                Intent i = new Intent(ListFriendActivity.this, DetailFriendActivity.class);

                i.putExtra(DetailFriendActivity.ID_USER, item.getIdFriend());
                i.putExtra(DetailFriendActivity.NAME,item.getName());
                i.putExtra(DetailFriendActivity.PROFILE_PIC, item.getProfilePic());
                i.putExtra(DetailFriendActivity.PHONE, item.getPhone());

                startActivity(i);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }

    private void initListFriend(){
        friendItem = new ArrayList<>();
        friendAdapter = new ListFriendAdapter(ListFriendActivity.this, friendItem);

        listFriend.setHasFixedSize(true);
        RecyclerView.LayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(ListFriendActivity.this, LinearLayoutManager.VERTICAL, false);
        listFriend.setLayoutManager(horizontalLayoutManagaer);
        listFriend.setItemAnimator(new DefaultItemAnimator());
        listFriend.setAdapter(friendAdapter);

        listFriend.addOnItemTouchListener(new RecyclerTouchListener(ListFriendActivity.this, listFriend, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                ItemNoti item = friendItem.get(position);

                Intent i = new Intent(ListFriendActivity.this, DetailFriendActivity.class);
                i.putExtra(LISTFRIEND, item.getIdFriend());
                i.putExtra(DetailFriendActivity.ID_USER, item.getIdFriend());
                i.putExtra(DetailFriendActivity.NAME,item.getName());
                i.putExtra(DetailFriendActivity.PROFILE_PIC, item.getProfilePic());
                i.putExtra(DetailFriendActivity.PHONE, item.getPhone());
                startActivity(i);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }


    // set notification list friend

    public  void setNotificationLIstFriend() {

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_NOTIFICATION_FRIEND, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response !=null)
                    parseJsonFeed(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("iduser", idUser);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
        // }
    }

    public void parseJsonFeed(String response){
        try {
            JSONObject jObj = new JSONObject(response);
            JSONArray feedArray = jObj.getJSONArray("feed");
            feedItems.clear();
            for (int i = 0; i < feedArray.length(); i++) {

                JSONObject feedObj = (JSONObject) feedArray.get(i);
                ItemNoti item = new ItemNoti();
                item.setName(feedObj.getString("name"));
                item.setIdFriend(feedObj.getString("iduser"));
                item.setPhone(feedObj.getString("phone"));
                item.setTimeStamp(feedObj.getString("created_at"));
                item.setProfilePic(feedObj.getString("profilePic"));
                item.setSta(feedObj.getString("state"));
                feedItems.add(item);
                if(i+1 == 2){
                    break;
                }

            }

            if (!feedItems.isEmpty() && feedItems != null) {

                layoutNotification.setVisibility(View.VISIBLE);
                listAdapter.notifyDataSetChanged();
                lnNotFriend.setVisibility(View.GONE);
                int total = feedArray.length();

                tvCountTigonNoti.setText(getResources().getString(R.string.tigon_request)+" ("+total+")");
                if(total >2){
                    tvContinue.setVisibility(View.VISIBLE);
                }else
                    tvContinue.setVisibility(View.GONE);
            } else {
                layoutNotification.setVisibility(View.GONE);

            }
            setListNewFriend();


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    // set horizontal list friend

    public  void setListNewFriend() {
        // Tag used to cancel the request

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_LIST_NEW_FRIEND, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response !=null)
                    parseJsonNewFriend(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                hideProgress();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("iduser", idUser);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);

    }

    public void parseJsonNewFriend(String response) {
        if (response != null) {
        try {

            JSONObject jObj = new JSONObject(response);
            JSONArray feedArray = jObj.getJSONArray("feed");
            horizontalItem.clear();
            for (int i = 0; i < feedArray.length(); i++) {

                JSONObject feedObj = (JSONObject) feedArray.get(i);
                String s = feedObj.getString("name");
                String result[] = s.split(" ");
                for (String r : result) {
                    s = r;
                }

                ItemNoti item = new ItemNoti(feedObj.getString("name"), s, feedObj.getString("phone"),
                        feedObj.getString("profilePic"), feedObj.getString("iduser"));

                horizontalItem.add(item);
                if(i+1 == 2){
                    break;
                }
            }

            if (horizontalItem != null && !horizontalItem.isEmpty()) {

                layoutNewFriend.setVisibility(View.VISIBLE);
                horizontalAdapter.notifyDataSetChanged();
                lnNotFriend.setVisibility(View.GONE);
                int toltal = horizontalItem.size();
                tvCountNewTigon.setText(getResources().getString(R.string.new_tigon) + " (" + toltal + ")");

            } else {
                layoutNewFriend.setVisibility(View.GONE);
            }
            if (feedArray.length() > 2) {
                tvPlus.setVisibility(View.VISIBLE);
                tvNewTigonCount.setVisibility(View.VISIBLE);
                countNewTigon = feedArray.length() - 2;
                tvNewTigonCount.setText(countNewTigon + " " + getResources().getString(R.string.person));
            } else {
                tvPlus.setVisibility(View.GONE);
                tvNewTigonCount.setVisibility(View.GONE);
            }

            setListFriend();
        } catch (JSONException e) {
            e.printStackTrace();

        }
    }else {

        }
    }
    // set list friend from server
    public  void setListFriend() {
        // Tag used to cancel the request

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_NOTIFICATION_LIST_OLD_FRIEND, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response !=null)
                    parseListFriend(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                hideProgress();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("iduser", idUser);
                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }

    public void parseListFriend(String response){
        try {
            JSONObject jObj = new JSONObject(response);
            JSONArray feedArray = jObj.getJSONArray("feed");
            friendItem.clear();
            for (int i = 0; i < feedArray.length(); i++) {
                JSONObject feedObj = (JSONObject) feedArray.get(i);

                ItemNoti item = new ItemNoti(feedObj.getString("name"), feedObj.getString("profilePic"),
                        feedObj.getString("phone"), feedObj.getString("iduser"));

                friendItem.add(item);
            }

            if (friendItem != null && !friendItem.isEmpty()) {
                int total = friendItem.size();
                tvTigon.setText(getResources().getString(R.string.list_tigon)+" ("+total+")");
                layoutFriend.setVisibility(View.VISIBLE);
                friendAdapter.notifyDataSetChanged();
                lnNotFriend.setVisibility(View.GONE);

            } else {
                if(feedItems.size() == 0 && horizontalItem.size()  == 0 ) {
                    lnNotFriend.setVisibility(View.VISIBLE);
                    layoutFriend.setVisibility(View.GONE);
                    layoutNewFriend.setVisibility(View.GONE);
                    layoutNotification.setVisibility(View.GONE);

                }
                layoutFriend.setVisibility(View.GONE);

            }
            hideProgress();

        } catch (JSONException e) {
            e.printStackTrace();
            hideProgress();
        }
    }

    // update watched
    private void updateWatchedFriend( final String repicient ) {
        // Tag used to cancel the request

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPDATE_WATCHED_FRIEND, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                hideDialog();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("onerror", "Registration Error: " + error.getMessage());

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("sender", idUser+"");
                params.put("repicient", repicient);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }

    BroadcastReceiver appendChatScreenMsgReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();
            if (b != null) {

                if (b.getBoolean("reloadAddFriend")) {
                    setNotificationLIstFriend();
                    setListNewFriend();
                }
            }
        }
    };



    @Override
    public void onResume(){
        registerReceiver(this.appendChatScreenMsgReceiver, new IntentFilter(AppIntent.ACTION_CHANGE_NOTIFICATION_BADGE_BY_ADD_FRIEND));
        super.onResume();
        setNotificationLIstFriend();
        setListNewFriend();
        setListFriend();
    }
    @Override
    public void onDestroy() {
        unregisterReceiver(appendChatScreenMsgReceiver);
        super.onDestroy();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {//Write your logic here
            this.finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void updateFriend(int position, String state, NotificationFriendAdapter.MyViewHolder v) {
        final ItemNoti item = feedItems.get(position);
        String idsender = idUser;

        loadAdded(idsender, item.getIdFriend(), item.getName(), item.getPhone(), state, item.getProfilePic(), position);

        v.layout_bt.setVisibility(View.GONE);
    }
    private void loadAdded(final String iduser, final String idfriend, final String name, final String phone,
                           final String state, final String profilePic, final int position) {
        // Tag used to cancel the request
        showProgress(getResources().getString(R.string.loading));

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_INSERT_FRIEND, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {


                hideProgress();
                s = name;

                String result[] = s.split(" ");
                for(String r : result){
                    s = r;
                }
                ItemNoti itemnewFriend = new ItemNoti(name, s,phone, profilePic, idfriend);

                horizontalItem.add(0, itemnewFriend);
                horizontalAdapter.notifyDataSetChanged();
                layoutNewFriend.setVisibility(View.VISIBLE);
                recylerNewFriend.setVisibility(View.VISIBLE);
                feedItems.remove(position);
                listAdapter.notifyDataSetChanged();
                if(horizontalItem.size() >=2){
                    tvPlus.setVisibility(View.VISIBLE);
                    tvNewTigonCount.setVisibility(View.VISIBLE);
                    countNewTigon++;
                    tvNewTigonCount.setText(countNewTigon + " " + getResources().getString(R.string.person));
                } else {
                    tvPlus.setVisibility(View.GONE);
                    tvNewTigonCount.setVisibility(View.GONE);
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                showToast(R.string.not_connect);
                hideProgress();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("iduser", iduser);
                params.put("idfriend", idfriend);
                params.put("name", name);
                params.put("phone", phone);
                params.put("state", state);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }

}
