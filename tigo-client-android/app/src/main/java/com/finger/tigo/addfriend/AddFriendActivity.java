package com.finger.tigo.addfriend;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.finger.tigo.BaseActivity;
import com.finger.tigo.Introslider.PrefManager;
import com.finger.tigo.R;
import com.finger.tigo.SlideMenuActivity;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.app.ApiExecuter;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppIntent;
import com.finger.tigo.common.contact.Contact;
import com.finger.tigo.common.contact.ContactsLoadListener;
import com.finger.tigo.common.contact.ContactsLoader;
import com.finger.tigo.session.SessionManager;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AddFriendActivity extends BaseActivity {

    private static final int PERMISSION_REQUEST_CONTACT= 123;
    PrefManager prefManager;

    private String idUser;

    String create, codeFace;

    ImageView imSync;
    Button sync_contact,next_sync;
    SessionManager session;


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefManager = new PrefManager(this);
        if(!prefManager.isAddFriendLaunch()){
            launchHomeFace();
        }

        setContentView(R.layout.activity_add_friends_laucher);
        imSync = (ImageView)findViewById(R.id.im_icon_sync_pink);
        sync_contact = (Button)findViewById(R.id.sync_contact);
        next_sync = (Button)findViewById(R.id.next_sync);
        Glide.with(this).load(R.drawable.icon_sync_pink).into(imSync);
        session = new SessionManager(this);
        idUser = AppAccountManager.getAppAccountUserId(this);

        prefManager = new PrefManager(this);


        Intent i = getIntent();
        codeFace = i.getStringExtra("codeFace");

       if(codeFace!=null){
           Intent a = new Intent();
           a.setAction(AppIntent.ACTION_ADD_FRIEND_FINISHED);
           a.putExtra("finishFromAddfriend", true);
           this.sendBroadcast(a);

           if (!prefManager.isAddFriendFace()) {
                launchHomeFace();
            }
       }
        sync_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                synchContact(v);
            }
        });
        next_sync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next(v);
            }
        });
    }


    public void synchContact(View view){
        showProgress(getResources().getString(R.string.contact_sync));
        loadContacts();
    }

    public void next(View view){
        if(create!=null) {
            launchHomeScreen();
        }else if(codeFace != null){
            launchHomeFace();
        }else{
            launchHomeScreen();
        }
    }

    private void launchHomeScreen() {
        prefManager.setIsAddFriednsLaunch(false);
        startActivity(new Intent(AddFriendActivity.this, SlideMenuActivity.class));
        finish();
    }
    private void launchHomeFace(){
        prefManager.setIsAddFriednsFace(false);
        startActivity(new Intent(AddFriendActivity.this, SlideMenuActivity.class));

        finish();
    }

    public void loadContacts(){
//
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(AddFriendActivity.this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(AddFriendActivity.this,
                        Manifest.permission.READ_CONTACTS)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(AddFriendActivity.this);
                    builder.setTitle(R.string.contacts_access);
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setMessage(R.string.please_contacts_access);//TODO put real question
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            requestPermissions(
                                    new String[]
                                            {Manifest.permission.READ_CONTACTS}
                                    , PERMISSION_REQUEST_CONTACT);
                        }
                    });
                    builder.show();

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(AddFriendActivity.this,
                            new String[]{Manifest.permission.READ_CONTACTS},
                            PERMISSION_REQUEST_CONTACT);

                }
            }else{
                getAllContacts();
            }
        }
        else{
            getAllContacts();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CONTACT: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getAllContacts();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    hideProgress();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void getAllContacts() {

        ContactsLoader contactLoader = new ContactsLoader(this, new ContactsLoadListener() {
            @Override
            public void onLoadStarted() {
                showProgress("loading contacts...");
            }

            @Override
            public void onLoading(int percent) {
                showProgress("loading contacts... " + percent + "%");
            }

            @Override
            public void onLoadCompleted(List<Contact> contacts) {
                hideProgress();

                if(contacts !=null && !contacts.isEmpty()) {
                    AddFriend(contacts, idUser);

                }else{

                    Intent i = new Intent(AddFriendActivity.this, SlideMenuActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        });

        contactLoader.execute(session.getKeyCodeCountry());

    }

    private void AddFriend( final  List<Contact> listContact, final String idUser) {
        // Tag used to cancel the request
        JSONArray arrPhone = new JSONArray();
        JSONArray arrName = new JSONArray();
        Map<String, String> params = new HashMap<String, String>();
        params.put("iduser", idUser);

        for(Contact object: listContact){

            arrPhone.put(object.phone);
            arrName.put(object.name);

        }

        params.put("friendPhone", arrPhone.toString());
        params.put("friendName", arrName.toString());

        ApiExecuter.requestApi(this, AppConfig.URL_ADDFRIEND, "Syncronizing friend...", params, new ApiExecuter.ApiResultListener() {
                    @Override
                    public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                        try {
                            Intent i = new Intent(AddFriendActivity.this, SlideMenuActivity.class);
                            startActivity(i);
                            finish();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            onFail(apiUrl, params, ex.getMessage(), ex);
                        }
                    }

                    @Override
                    public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {
                        showToast(errMessage);
                    }
                });
    }

}
