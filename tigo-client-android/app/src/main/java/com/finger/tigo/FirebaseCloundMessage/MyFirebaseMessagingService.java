package com.finger.tigo.FirebaseCloundMessage;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.finger.tigo.R;
import com.finger.tigo.addfriend.DetailFriendActivity;
import com.finger.tigo.app.AppIntent;
import com.finger.tigo.detail.CommentActivity;
import com.finger.tigo.detail.EventDetailActivity;
import com.finger.tigo.session.SessionManager;
import com.finger.tigo.syncadapter.DialogActivity;
import com.finger.tigo.util.MyLog;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class MyFirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService{
    SessionManager session;
    String name2, name1, string_noti, id_noti, id_sc, messages_data, type_noti, name_even,
            number_user,time_noti,name,image_use_event , image_event;
    String nameFriend, profilePicFriend, phoneFriend, idFriend;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
//imv.getContentDescription() != null ? imv.getContentDescription().toString() : null
        showNotification(remoteMessage.getData().get("message")
                , remoteMessage.getData().get("name_noti") != null ? remoteMessage.getData().get("name_noti") : null,
                remoteMessage.getData().get("status_noti") != null ? remoteMessage.getData().get("status_noti") : null);
    }

    private void showNotification(String message, String name_noti, String status_noti) {
        session = new SessionManager(this);
        Log.d("lllllllll", "onMessageReceived: "+message+" name: "+name_noti+ " alkdjf: "+status_noti);
        if(message.equalsIgnoreCase("notificationlogin")){
            MyLog.d("logout...." );
            Intent intet = new Intent(getApplicationContext(), DialogActivity.class);
            intet.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(intet);
        }

        if(message.equalsIgnoreCase("notificationAddFriend") || message.equalsIgnoreCase("notificationAddFriendConfirm")) {
            try {
                JSONObject obj = new JSONObject(status_noti);

                for (int i = 0; i < obj.length(); i++) {
                    nameFriend = obj.getString("name");
                    profilePicFriend = obj.getString("profilePic");
                    phoneFriend = obj.getString("phone");
                    idFriend = obj.getString("id_user");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (message.equalsIgnoreCase("notificationAddFriend")) {


                Intent i = new Intent(this, DetailFriendActivity.class);
                i.putExtra(DetailFriendActivity.ID_USER, idFriend);
                i.putExtra(DetailFriendActivity.PROFILE_PIC, profilePicFriend);
                i.putExtra(DetailFriendActivity.PHONE, phoneFriend);
                i.putExtra(DetailFriendActivity.NAME, nameFriend);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);



                    String strSound = session.getKeySoundEvent();
                    Resources res = getResources();
                    final int soundId = res.getIdentifier(strSound, "raw", getPackageName());
                    String mystring = getResources().getString(R.string.notification_add_friend);
                    mystring = nameFriend + " " + mystring;
                    PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, i, PendingIntent.FLAG_UPDATE_CURRENT);
                    NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                            .setAutoCancel(true)
                            .setContentTitle("TIGO")
                            .setContentText(mystring)
                            .setSmallIcon(R.drawable.ic_notification_tigo)
                            .setLargeIcon(getBitmapFromURL(profilePicFriend))
                            .setContentIntent(pendingIntent);
                    builder.setSound(Uri.parse("android.resource://"
                            + this.getPackageName() + "/" + soundId));
                    NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    manager.notify(1, builder.build());

                Intent a = new Intent();
                // countNotification = countNotification + 1;

                a.setAction(AppIntent.ACTION_CHANGE_NOTIFICATION_BADGE_BY_ADD_FRIEND);
                a.putExtra("reloadAddFriend", true);
                a.putExtra("friend", 1);
                this.sendBroadcast(a);

            } else if (message.equalsIgnoreCase("notificationAddFriendConfirm")) {


                    String strSound = session.getKeySoundEvent();
                    Resources res = getResources();
                    final int soundId = res.getIdentifier(strSound, "raw", getPackageName());
                    Intent i = new Intent(this, DetailFriendActivity.class);

                    i.putExtra(DetailFriendActivity.ID_USER, idFriend);
                    i.putExtra(DetailFriendActivity.PROFILE_PIC, profilePicFriend);
                    i.putExtra(DetailFriendActivity.PHONE, phoneFriend);
                    i.putExtra(DetailFriendActivity.NAME, nameFriend);

                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    String mystring = getResources().getString(R.string.notification_accepted_friend);
                    mystring = nameFriend + " " + mystring;
                    PendingIntent pendingIntent = PendingIntent.getActivity(this, 2, i, PendingIntent.FLAG_UPDATE_CURRENT);

                    NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                            .setAutoCancel(true)
                            .setContentTitle("TIGO")
                            .setContentText(mystring)
                            .setSmallIcon(R.drawable.ic_notification_tigo)
                            .setLargeIcon(getBitmapFromURL(profilePicFriend))
                            .setContentIntent(pendingIntent);
                    builder.setSound(Uri.parse("android.resource://"
                            + this.getPackageName() + "/" + soundId));
                    NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    manager.notify(2, builder.build());

                Intent a = new Intent();
                a.setAction(AppIntent.ACTION_CHANGE_NOTIFICATION_BADGE_BY_ADD_FRIEND);
                a.putExtra("reloadAddFriend", true);
                a.putExtra("friend", 1);
                this.sendBroadcast(a);
            }
        }else if(name_noti != null && status_noti !=null){

            try {
                JSONObject obj = new JSONObject(name_noti);
                for (int i = 0; i < obj.length(); i++) {
                    name1 =  obj.getString("top1");
                    name2 = obj.isNull("top2") ? null : obj.getString("top2");
                }

            }catch(JSONException e){
                e.printStackTrace();
            }
            try {
                JSONObject objstatus = null;
                objstatus = new JSONObject(status_noti);
                for (int i = 0; i < objstatus.length(); i++) {

                    id_noti = objstatus.getString("id_noti");
                    id_sc = objstatus.getString("id_sc");
                    messages_data = objstatus.getString("message_data");
                    type_noti = objstatus.getString("type_noti");
                    name_even = objstatus.getString("name_even");
                    number_user  = objstatus.getString("number_user");
                    time_noti = objstatus.getString("time_noti");
                    name = objstatus.getString("name");

                    image_use_event = objstatus.getString("profilePic");
                    image_event = objstatus.getString("image");
                    //              number = objstatus.isNull("number_user") ? null : objstatus.getString("number_user");

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (messages_data.equalsIgnoreCase("1")) {
                final String invite = getResources().getString(R.string.notification_invite_friend);
                noti(invite);
                // string_noti = name1 + R.string.notification_invite1 + number_user + R.string.notification_invite2 + name_even;

            } else if (messages_data.equalsIgnoreCase("2")) {
                final String txtpa = getResources().getString(R.string.join);
                noti(txtpa);
            } else if (messages_data.equalsIgnoreCase("3")) {

                String txtco = getResources().getString(R.string.notification_comment_);
                noti(txtco);
            } else if(messages_data.equalsIgnoreCase("4")){
                final String accept = getResources().getString(R.string.invite_accepted);
                notiaccept(accept);
            }else if(messages_data.equalsIgnoreCase("5")) {
                final String update = getResources().getString(R.string.notification_editevent);
                noti_update_event(update);
            }else  if(messages_data.equalsIgnoreCase("6")) {
                final String delete = getResources().getString(R.string.notification_delete_event);
                noti_update_event(delete);
            }

            if(message.equalsIgnoreCase("notificationComment")){

                if (session.getPref_setting_comment()==true ) {

                    String strSound = session.getKeySoundEvent();
                    Resources res = getResources();
                    final  int soundId = res.getIdentifier(strSound, "raw", getPackageName());
                    Intent i = new Intent(this, CommentActivity.class);
                    i.putExtra("mDetailsHomeActivity", id_sc);
                    i.putExtra("clickNotification", "watched_2_3_6_5");
                    i.putExtra(AppIntent.ACTION_ID_NOTI,id_noti);
                    i.putExtra(AppIntent.ACTION_TYPE_NOTI, "comment");
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    PendingIntent pendingIntent = PendingIntent.getActivity(this,3,i,PendingIntent.FLAG_UPDATE_CURRENT);
                    android.support.v7.app.NotificationCompat.Builder builder = (android.support.v7.app.NotificationCompat.Builder) new android.support.v7.app.NotificationCompat.Builder(this)
                            .setAutoCancel(true)
                            .setContentTitle(getResources().getString(R.string.app_name))
                            .setContentText(string_noti)
                            .setLargeIcon(resizeBitmap(image_use_event,24,24))
                            .setSmallIcon(R.drawable.ic_notification_tigo)
                            .setLargeIcon(getBitmapFromURL(image_use_event))
                            .setStyle(new NotificationCompat.BigPictureStyle()
                                    .bigPicture(getBitmapFromURL(image_event))
                                    .setSummaryText(string_noti)
                            )
                            .setContentIntent(pendingIntent);
                    builder.setSound( Uri.parse("android.resource://"
                            + this.getPackageName() + "/" + soundId));
                    NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    manager.notify(3,builder.build());
                }

                Intent a = new Intent();
                a.setAction(AppIntent.ACTION_PUSH_RECEIVED);
                a.putExtra("reload", true);
                a.putExtra("id_noti", id_noti);
                a.putExtra("name1", name1);
                a.putExtra("name2", name2);
                a.putExtra("id_sc", id_sc);
                a.putExtra("messages_data", messages_data);
                a.putExtra("type_noti", type_noti);
                a.putExtra("name_even", name_even);
                a.putExtra("number_user", number_user);
                a.putExtra("time_noti", time_noti);
                a.putExtra("name",name);
                a.putExtra("image_use_event", image_use_event);
                a.putExtra("image_event", image_event);
                this.sendBroadcast(a);

            }
            else if(message.equalsIgnoreCase("notification_participation")){

                if (session.getPref_setting_parti()==true ) {
                    String strSound = session.getKeySoundEvent();
                    Resources res = getResources();
                    final  int soundId = res.getIdentifier(strSound, "raw", getPackageName());
                    Intent i = new Intent(this, EventDetailActivity.class);
                    i.putExtra("clickNotification", "watched_2_3_6_5");
                    i.putExtra(AppIntent.ACTION_ID_NOTI,id_noti);
                    i.putExtra(AppIntent.ACTION_TYPE_NOTI, "paticipation");

                    i.putExtra(EventDetailActivity.M_DETAILS_HOME_ACTIVITY, id_sc);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    PendingIntent pendingIntent = PendingIntent.getActivity(this,4,i,PendingIntent.FLAG_UPDATE_CURRENT);
                    android.support.v7.app.NotificationCompat.Builder builder = (android.support.v7.app.NotificationCompat.Builder) new android.support.v7.app.NotificationCompat.Builder(this)
                            .setAutoCancel(true)
                            .setContentTitle(getResources().getString(R.string.app_name))
                            .setContentText(string_noti)
                            .setLargeIcon(getBitmapFromURL(image_use_event))
                            .setSmallIcon(R.drawable.ic_notification_tigo)
                            .setContentIntent(pendingIntent);
                    builder.setSound( Uri.parse("android.resource://"
                            + this.getPackageName() + "/" + soundId));
                    NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    manager.notify(4,builder.build());

                }

                Intent a = new Intent();
                a.setAction(AppIntent.ACTION_PUSH_RECEIVED);
                a.putExtra("reload", true);
                a.putExtra("id_noti", id_noti);
                a.putExtra("name1", name1);
                a.putExtra("name2", name2);
                a.putExtra("id_sc", id_sc);
                a.putExtra("messages_data", messages_data);
                a.putExtra("type_noti", type_noti);
                a.putExtra("name_even", name_even);
                a.putExtra("number_user", number_user);
                a.putExtra("time_noti", time_noti);
                a.putExtra("name",name);
                a.putExtra("image_use_event", image_use_event);
                a.putExtra("image_event", image_event);
                this.sendBroadcast(a);

            }else if(message.equalsIgnoreCase("notification_invite")){

                if (session.getPref_setting_invite()==true ) {

                    String strSound = session.getKeySoundEvent();
                    Resources res = getResources();
                    final  int soundId = res.getIdentifier(strSound, "raw", getPackageName());
                    Intent i = new Intent(this, EventDetailActivity.class);
                    i.putExtra("clickNotification", "watched_1_4");
                    i.putExtra(AppIntent.ACTION_ID_NOTI,id_noti);
                    i.putExtra(AppIntent.ACTION_TYPE_NOTI, "invite");
                    i.putExtra(EventDetailActivity.M_DETAILS_HOME_ACTIVITY, id_sc);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    Log.d("ooooooooooo", "showNotification: "+ string_noti);
                    PendingIntent pendingIntent = PendingIntent.getActivity(this,0,i,PendingIntent.FLAG_UPDATE_CURRENT);
                    android.support.v7.app.NotificationCompat.Builder builder = (android.support.v7.app.NotificationCompat.Builder) new android.support.v7.app.NotificationCompat.Builder(this)
                            .setAutoCancel(true)
                            .setContentTitle("TIGO")
                            .setContentText(string_noti)
                            .setLargeIcon((getBitmapFromURL(image_use_event)))
                            .setSmallIcon(R.drawable.ic_notification_tigo)
                            .setContentIntent(pendingIntent);
                    builder.setSound( Uri.parse("android.resource://"
                            + this.getPackageName() + "/" + soundId));
                    NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    manager.notify(0,builder.build());
                }
                Intent a = new Intent();
                a.setAction(AppIntent.ACTION_PUSH_RECEIVED);
                a.putExtra("reload", true);

                a.putExtra("id_noti", id_noti);
                a.putExtra("name1", name1);
                a.putExtra("name2", name2);
                a.putExtra("id_sc", id_sc);
                a.putExtra("messages_data", messages_data);
                a.putExtra("type_noti", type_noti);
                a.putExtra("name_even", name_even);
                a.putExtra("number_user", number_user);
                a.putExtra("time_noti", time_noti);
                a.putExtra("name",name);
                a.putExtra("image_use_event", image_use_event);
                a.putExtra("image_event", image_event);
                this.sendBroadcast(a);
            }else if(message.equalsIgnoreCase("notification_accept_invite")){

                    String strSound = session.getKeySoundEvent();
                    Resources res = getResources();
                    final  int soundId = res.getIdentifier(strSound, "raw", getPackageName());
                    Intent i = new Intent(this, EventDetailActivity.class);
                    i.putExtra(EventDetailActivity.M_DETAILS_HOME_ACTIVITY, id_sc);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.putExtra("clickNotification", "watched_1_4");
                    i.putExtra(AppIntent.ACTION_ID_NOTI,id_noti);
                i.putExtra(AppIntent.ACTION_TYPE_NOTI, "accept");
                    PendingIntent pendingIntent = PendingIntent.getActivity(this,6,i,PendingIntent.FLAG_UPDATE_CURRENT);
                    android.support.v7.app.NotificationCompat.Builder builder = (android.support.v7.app.NotificationCompat.Builder) new android.support.v7.app.NotificationCompat.Builder(this)
                            .setAutoCancel(true)
                            .setContentTitle("TIGO")
                            .setContentText(string_noti)
                            .setLargeIcon((getBitmapFromURL(image_use_event)))
                            .setSmallIcon(R.drawable.ic_notification_tigo)
                            .setContentIntent(pendingIntent);

                    builder.setSound( Uri.parse("android.resource://"
                            + this.getPackageName() + "/" + soundId));
                    NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    manager.notify(6,builder.build());



                Intent a = new Intent();
                a.setAction(AppIntent.ACTION_PUSH_RECEIVED);
                a.putExtra("reload", true);

                a.putExtra("id_noti", id_noti);
                a.putExtra("name1", name1);
                a.putExtra("name2", name2);
                a.putExtra("id_sc", id_sc);
                a.putExtra("messages_data", messages_data);
                a.putExtra("type_noti", type_noti);
                a.putExtra("name_even", name_even);
                a.putExtra("number_user", number_user);
                a.putExtra("time_noti", time_noti);
                a.putExtra("name",name);
                a.putExtra("image_use_event", image_use_event);
                a.putExtra("image_event", image_event);
                this.sendBroadcast(a);
            }else if(message.equalsIgnoreCase("notificationUpdateschedule")){
                    String strSound = session.getKeySoundEvent();
                    Resources res = getResources();
                    final int soundId = res.getIdentifier(strSound, "raw", getPackageName());
                    Intent i = new Intent(this, EventDetailActivity.class);
                    i.putExtra(EventDetailActivity.M_DETAILS_HOME_ACTIVITY, id_sc);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("clickNotification", "watched_2_3_6_5");
                i.putExtra(AppIntent.ACTION_ID_NOTI,id_noti);
                i.putExtra(AppIntent.ACTION_TYPE_NOTI, "editeevent");
                    PendingIntent pendingIntent = PendingIntent.getActivity(this, 7, i, PendingIntent.FLAG_UPDATE_CURRENT);
                    android.support.v7.app.NotificationCompat.Builder builder = (android.support.v7.app.NotificationCompat.Builder) new android.support.v7.app.NotificationCompat.Builder(this)
                            .setAutoCancel(true)
                            .setContentTitle("TIGO")
                            .setContentText(string_noti)
                            .setLargeIcon(getBitmapFromURL(image_use_event))
                            .setSmallIcon(R.drawable.ic_notification_tigo)
                            .setStyle(new NotificationCompat.BigPictureStyle()
                                    .bigPicture(getBitmapFromURL(image_event))
                                    .setSummaryText(string_noti)
                            )
                            .setContentIntent(pendingIntent);
                    builder.setSound(Uri.parse("android.resource://"
                            + this.getPackageName() + "/" + soundId));
                    NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    manager.notify(7, builder.build());

                Intent a = new Intent();
                a.setAction(AppIntent.ACTION_PUSH_RECEIVED);
                a.putExtra("reload", true);

                a.putExtra("id_noti", id_noti);
                a.putExtra("name1", name1);
                a.putExtra("name2", name2);
                a.putExtra("id_sc", id_sc);
                a.putExtra("messages_data", messages_data);
                a.putExtra("type_noti", type_noti);
                a.putExtra("name_even", name_even);
                a.putExtra("number_user", number_user);
                a.putExtra("time_noti", time_noti);
                a.putExtra("name",name);
                a.putExtra("image_use_event", image_use_event);
                a.putExtra("image_event", image_event);
                this.sendBroadcast(a);
            }else if(message.equalsIgnoreCase("notificationdeleteschedule")){

                    String strSound = session.getKeySoundEvent();
                    Resources res = getResources();
                    final int soundId = res.getIdentifier(strSound, "raw", getPackageName());
                    Intent i = new Intent(this, DialogActivity.class);
                    i.putExtra(EventDetailActivity.M_DETAILS_HOME_ACTIVITY, id_sc);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("clickNotification", "watched_2_3_6_5");
                i.putExtra(AppIntent.ACTION_ID_NOTI,id_noti);
                i.putExtra(AppIntent.ACTION_TYPE_NOTI,"delete event");

                    PendingIntent pendingIntent = PendingIntent.getActivity(this, 8, i, PendingIntent.FLAG_UPDATE_CURRENT);
                    android.support.v7.app.NotificationCompat.Builder builder = (android.support.v7.app.NotificationCompat.Builder) new android.support.v7.app.NotificationCompat.Builder(this)
                            .setAutoCancel(true)
                            .setContentTitle("TIGO")
                            .setContentText(string_noti)
                            .setLargeIcon(getBitmapFromURL(image_use_event))
                            .setSmallIcon(R.drawable.ic_notification_tigo)
                            .setStyle(new NotificationCompat.BigPictureStyle()
                                    .bigPicture(getBitmapFromURL(image_event))
                                    .setSummaryText(string_noti)
                            )
                            .setContentIntent(pendingIntent);
                    builder.setSound(Uri.parse("android.resource://"
                            + this.getPackageName() + "/" + soundId));
                    NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    manager.notify(8, builder.build());

                Intent a = new Intent();
                a.setAction(AppIntent.ACTION_PUSH_RECEIVED);
                a.putExtra("reload", true);
                a.putExtra("id_noti", id_noti);
                a.putExtra("name1", name1);
                a.putExtra("name2", name2);
                a.putExtra("id_sc", id_sc);
                a.putExtra("messages_data", messages_data);
                a.putExtra("type_noti", type_noti);
                a.putExtra("name_even", name_even);
                a.putExtra("number_user", number_user);
                a.putExtra("time_noti", time_noti);
                a.putExtra("name",name);
                a.putExtra("image_use_event", image_use_event);
                a.putExtra("image_event", image_event);
                this.sendBroadcast(a);
            }
        }
    }

    // notification event
    public String noti(final String textnoti) {
        string_noti = null;
        switch (Integer.parseInt(number_user)) {

            case 1:
            case  2:

                string_noti = name1 + " " + textnoti + " " + name_even;
                break;
            case 3:

                string_noti = name1 + ", " + name2 + " " + textnoti + " " + name_even;
                break;
            default:
                //   String commenta = getResources().getString(R.string.notification_comment);
                int n = Integer.parseInt(number_user) - 2;
                if (messages_data.equalsIgnoreCase("1")) {
                    // bao dai duong da moi ban va 4 nguoi khac tham gia su kien abc
                    string_noti = name1 +" "+ textnoti +" " + getResources().getString(R.string.and)+" "+ n+" "+getResources().getString(R.string.other_people)+ " "+getResources().getString(R.string.notification_invited) +" "+ name_even;
                } else {
                    string_noti = name1 + ", " + name2 +" "+ getResources().getString(R.string.and)+" " + n +" "+ getResources().getString(R.string.other_people) +" "+ textnoti + " " + name_even;
                }

                break;
        }
        return string_noti;
    }

    public String notiaccept(final String accept){
        string_noti = null;
        switch (Integer.parseInt(number_user)) {
            case 1:
                //  String comment = getResources().getString(R.string.notification_comment);
                string_noti = name1 + " " + accept + " " + name_even;
                break;
            case 2:
                //   String comments = getResources().getString(R.string.notification_comment);
                string_noti = name1 + ", " + name2 + " " + accept + " " + name_even;
                break;
            default:
                //  String commenta = getResources().getString(R.string.notification_comment);
                int n = Integer.parseInt(number_user) - 2;
                string_noti = name1 + ", " + name2 +" "+ R.string.and + " "+ n +" "+ R.string.other_people +" "+ accept + " " + name_even;
                break;
        }
        return string_noti;
    }

    public String noti_update_event(final String upadte_even){
        string_noti = null;
        //  String comment = getResources().getString(R.string.notification_comment);
        string_noti = name1 + " " + upadte_even + " " + name_even;
        return string_noti;
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }
    public Bitmap resizeBitmap(String photoPath, int targetW, int targetH) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(photoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        int scaleFactor = 1;
        if ((targetW > 0) || (targetH > 0)) {
            scaleFactor = Math.min(photoW/targetW, photoH/targetH);
        }

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true; //Deprecated API 21

        return BitmapFactory.decodeFile(photoPath, bmOptions);
    }



}

