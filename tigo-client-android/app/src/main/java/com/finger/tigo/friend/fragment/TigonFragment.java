package com.finger.tigo.friend.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.addfriend.DetailFriendActivity;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.app.AppIntent;
import com.finger.tigo.friend.adapter.HorizontalAdapter;
import com.finger.tigo.friend.adapter.ListFriendAdapter;
import com.finger.tigo.items.ItemNoti;
import com.finger.tigo.listener.RecyclerTouchListener;
import com.finger.tigo.notification.adapter.NotificationFriendAdapter;
import com.finger.tigo.session.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;




/**
 * Created by Duong on 5/17/2017.
 */

public class TigonFragment extends Fragment implements NotificationFriendAdapter.itemClickNotificationFriend {


    public static final String LISTFRIEND = "listfriend";

    RecyclerView listNotification;
    RecyclerView listFriend;
    RecyclerView recylerNewFriend;

    LinearLayout lnNotFriend;
    LinearLayout layoutNotification;
    LinearLayout layoutNewFriend;
    LinearLayout layoutFriend;

    private NotificationFriendAdapter listAdapter;
    private HorizontalAdapter horizontalAdapter;
    private ListFriendAdapter friendAdapter;
    private List<ItemNoti> feedItems;
    private List<ItemNoti> horizontalItem;
    private List<ItemNoti> friendItem;
    private ProgressDialog pDialog;

    SessionManager session;
    private String idUser;
    private String s;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_list_tigon, container, false);

        init(rootview);

        session = new SessionManager(getActivity());
        idUser = AppAccountManager.getAppAccountUserId(getActivity());

        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(true);
        pDialog.setMessage(getResources().getString(R.string.loading));
        showDialog();

        initListNotificationfriend();
        initListNewFriend();
        initListFriend();

        setNotificationLIstFriend();
        setListNewFriend();
        setListFriend();

        return rootview;
    }

    public void init(View v){
        listNotification = (RecyclerView) v.findViewById(R.id.listNotifi);
        listFriend = ( RecyclerView) v.findViewById(R.id.listFriend);
        recylerNewFriend = (RecyclerView) v.findViewById(R.id.recylernewfriend);
        lnNotFriend = (LinearLayout) v.findViewById(R.id.activity_not_friend);
        layoutNotification = (LinearLayout) v.findViewById(R.id.layoutNotification);
        layoutNewFriend = (LinearLayout) v.findViewById(R.id.layoutNewFriend);
        layoutFriend = (LinearLayout) v.findViewById(R.id.layoutFriend);
    }

    private void initListNotificationfriend() {
        feedItems = new ArrayList<>();
        listAdapter = new NotificationFriendAdapter(getActivity(), feedItems, this);
        listNotification.setHasFixedSize(true);
        RecyclerView.LayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        listNotification.setLayoutManager(horizontalLayoutManagaer);

        listNotification.setItemAnimator(new DefaultItemAnimator());
        listNotification.setAdapter(listAdapter);
        listNotification.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), listNotification, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                ItemNoti item = feedItems.get(position);
                updateWatchedFriend( item.getIdFriend());
                Intent i = new Intent(getActivity(), DetailFriendActivity.class);
                i.putExtra(LISTFRIEND, item.getIdFriend());
                i.putExtra(DetailFriendActivity.ID_USER, item.getIdFriend());
                i.putExtra(DetailFriendActivity.NAME,item.getName());
                i.putExtra(DetailFriendActivity.PROFILE_PIC, item.getProfilePic());
                i.putExtra(DetailFriendActivity.PHONE, item.getPhone());
                startActivity(i);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }

    private void initListNewFriend(){
        // listNewFriend.setVisibility(View.INVISIBLE);
        horizontalItem = new ArrayList<>();
        horizontalAdapter = new HorizontalAdapter(getActivity(),horizontalItem);
        recylerNewFriend.setHasFixedSize(true);
        RecyclerView.LayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recylerNewFriend.setLayoutManager(horizontalLayoutManagaer);
//        recylerNewFriend.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.HORIZONTAL));
        recylerNewFriend.setItemAnimator(new DefaultItemAnimator());
        recylerNewFriend.setAdapter(horizontalAdapter);

        recylerNewFriend.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recylerNewFriend, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                ItemNoti item = horizontalItem.get(position);
                updateWatchedFriend( item.getIdFriend());
                Intent i = new Intent(getActivity(), DetailFriendActivity.class);

                i.putExtra(DetailFriendActivity.ID_USER, item.getIdFriend());
                i.putExtra(DetailFriendActivity.NAME,item.getName());
                i.putExtra(DetailFriendActivity.PROFILE_PIC, item.getProfilePic());
                i.putExtra(DetailFriendActivity.PHONE, item.getPhone());
                startActivity(i);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }


    private void initListFriend(){
        friendItem = new ArrayList<ItemNoti>();
        friendAdapter = new ListFriendAdapter(getActivity(), friendItem);

        listFriend.setHasFixedSize(true);
        RecyclerView.LayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        listFriend.setLayoutManager(horizontalLayoutManagaer);
        listFriend.setItemAnimator(new DefaultItemAnimator());
        listFriend.setAdapter(friendAdapter);

        listFriend.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), listFriend, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                ItemNoti item = friendItem.get(position);

                Intent i = new Intent(getActivity(), DetailFriendActivity.class);
                i.putExtra(LISTFRIEND, item.getIdFriend());
                i.putExtra(DetailFriendActivity.ID_USER, item.getIdFriend());
                i.putExtra(DetailFriendActivity.NAME,item.getName());
                i.putExtra(DetailFriendActivity.PROFILE_PIC, item.getProfilePic());
                i.putExtra(DetailFriendActivity.PHONE, item.getPhone());
                startActivity(i);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }


    // set notification list friend

    public  void setNotificationLIstFriend() {

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_NOTIFICATION_FRIEND, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response !=null)
                    parseJsonFeed(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("iduser", idUser);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
        // }
    }

    public void parseJsonFeed(String response){
        try {
            JSONObject jObj = new JSONObject(response);
            JSONArray feedArray = jObj.getJSONArray("feed");
            feedItems.clear();
            for (int i = 0; i < feedArray.length(); i++) {

                JSONObject feedObj = (JSONObject) feedArray.get(i);
                ItemNoti item = new ItemNoti();
                item.setName(feedObj.getString("name"));
                item.setIdFriend(feedObj.getString("iduser"));
                item.setPhone(feedObj.getString("phone"));
                item.setTimeStamp(feedObj.getString("created_at"));
                item.setProfilePic(feedObj.getString("profilePic"));
                item.setSta(feedObj.getString("state"));
                feedItems.add(item);

            }

            if (!feedItems.isEmpty() && feedItems != null) {
                Log.d("ffffff", "notification tigon ");
                layoutNotification.setVisibility(View.VISIBLE);
                listAdapter.notifyDataSetChanged();
                lnNotFriend.setVisibility(View.GONE);
            } else {
                layoutNotification.setVisibility(View.GONE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    // set horizontal list friend

    public  void setListNewFriend() {
        // Tag used to cancel the request

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_LIST_NEW_FRIEND, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response !=null)
                    parseJsonNewFriend(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("iduser", idUser);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);

    }

    public void parseJsonNewFriend(String response){
        try {
            JSONObject jObj = new JSONObject(response);
            JSONArray feedArray = jObj.getJSONArray("feed");
            horizontalItem.clear();
            for (int i = 0; i < feedArray.length(); i++) {
                JSONObject feedObj = (JSONObject) feedArray.get(i);
                String s = feedObj.getString("name");

                String result[] = s.split(" ");
                for (String r : result) {
                    s = r;
                }

                ItemNoti item = new ItemNoti(feedObj.getString("name"), s, feedObj.getString("phone"), feedObj.getString("profilePic"), feedObj.getString("iduser"));

                horizontalItem.add(item);
            }

            if (horizontalItem != null && !horizontalItem.isEmpty()) {

                layoutNewFriend.setVisibility(View.VISIBLE);
                horizontalAdapter.notifyDataSetChanged();
                lnNotFriend.setVisibility(View.GONE);
            } else {
                layoutNewFriend.setVisibility(View.GONE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    // set list friend from server
    public  void setListFriend() {
        // Tag used to cancel the request

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_NOTIFICATION_LIST_OLD_FRIEND, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response !=null)
                    parseListFriend(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("iduser", idUser);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);

    }

    public void parseListFriend(String response){
        try {
            JSONObject jObj = new JSONObject(response);
            JSONArray feedArray = jObj.getJSONArray("feed");
            friendItem.clear();
            for (int i = 0; i < feedArray.length(); i++) {
                JSONObject feedObj = (JSONObject) feedArray.get(i);

                ItemNoti item = new ItemNoti(feedObj.getString("name"), feedObj.getString("profilePic"),
                        feedObj.getString("phone"), feedObj.getString("iduser"));

                friendItem.add(item);
            }

            if (friendItem != null && !friendItem.isEmpty()) {
                hideDialog();
                layoutFriend.setVisibility(View.VISIBLE);
                layoutNotification.setVisibility(View.GONE);
                friendAdapter.notifyDataSetChanged();
                lnNotFriend.setVisibility(View.GONE);


            } else {
                if(feedItems.size() == 0 && horizontalItem.size()  == 0 ) {
                    lnNotFriend.setVisibility(View.VISIBLE);
                    layoutFriend.setVisibility(View.GONE);
                    layoutNewFriend.setVisibility(View.GONE);
                    layoutNotification.setVisibility(View.GONE);
                }
                layoutFriend.setVisibility(View.GONE);
                hideDialog();


            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // update watched
    private void updateWatchedFriend( final String repicient ) {
        // Tag used to cancel the request

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPDATE_WATCHED_FRIEND, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                hideDialog();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("onerror", "Registration Error: " + error.getMessage());

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("sender", idUser+"");
                params.put("repicient", repicient);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }

    BroadcastReceiver appendChatScreenMsgReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();
            if (b != null) {

                if (b.getBoolean("reloadAddFriend")) {

                    setNotificationLIstFriend();
                    int count = 0;
                    count = b.getInt("friend");

                    Intent a = new Intent();
                    // countNotification = countNotification + 1;

                    a.setAction(AppIntent.ACTION_CHANGE_NOTIFICATION_BADGE_BY_ADD_FRIEND);
                    a.putExtra("reloadAddFriend", true);
                    a.putExtra("friend", count);
                    getActivity().sendBroadcast(a);
                    getActivity().invalidateOptionsMenu();
                }
            }
        }
    };

    @Override
    public void onResume(){
        getActivity().registerReceiver(this.appendChatScreenMsgReceiver, new IntentFilter(AppIntent.ACTION_CHANGE_NOTIFICATION_BADGE_BY_ADD_FRIEND));
        super.onResume();
        setNotificationLIstFriend();
        setListNewFriend();
        setListFriend();
    }
    @Override
    public void onDestroy() {
        getActivity().unregisterReceiver(appendChatScreenMsgReceiver);
        super.onDestroy();


    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onPause() {
        super.onPause();

    }



    @Override
    public void updateFriend(int position, String state, NotificationFriendAdapter.MyViewHolder v) {

        final ItemNoti item = feedItems.get(position);
        String idsender = idUser;

        loadAdded(idsender, item.getIdFriend(), item.getName(), item.getPhone(), state, item.getProfilePic(), position);

        v.layout_bt.setVisibility(View.GONE);


    }

    private void loadAdded(final String iduser, final String idfriend, final String name, final String phone,
                           final String state, final String profilePic, final int position) {
        // Tag used to cancel the request
        pDialog.setMessage(getResources().getString(R.string.sending));
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_INSERT_FRIEND, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                hideDialog();
                s = name;

                String result[] = s.split(" ");
                for(String r : result){
                    s = r;
                }
                ItemNoti itemnewFriend = new ItemNoti(name, s,phone, profilePic, idfriend);
                horizontalItem.add(0, itemnewFriend);
                horizontalAdapter.notifyDataSetChanged();
                layoutNewFriend.setVisibility(View.VISIBLE);
                recylerNewFriend.setVisibility(View.VISIBLE);
                feedItems.remove(position);
                listAdapter.notifyDataSetChanged();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), R.string.notconnect, Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("iduser", iduser);
                params.put("idfriend", idfriend);
                params.put("name", name);
                params.put("phone", phone);
                params.put("state", state);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }



    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
