/**
 * Copyright (c) 2012-2013, Gerald Garcia, David Wiesner, Timo Berger
 *
 * This file is part of Andoid Caldav Sync Adapter Free.
 *
 * Andoid Caldav Sync Adapter Free is free software: you can redistribute 
 * it and/or modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3 of the 
 * License, or at your option any later version.
 *
 * Andoid Caldav Sync Adapter Free is distributed in the hope that 
 * it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Andoid Caldav Sync Adapter Free.  
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.finger.tigo.syncadapter;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SyncResult;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Calendars;
import android.provider.CalendarContract.Events;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.login.LoginActivity;
import com.finger.tigo.schedule.alarm.AlarmReceiver;
import com.finger.tigo.schedule.libCalendar.Event;
import com.finger.tigo.session.SessionManager;
import com.finger.tigo.util.DateUtil;
import com.finger.tigo.util.MyLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import static android.content.Context.ALARM_SERVICE;


//import java.net.MalformedURLException;
//import java.security.GeneralSecurityException;

public class SyncAdapter extends AbstractThreadedSyncAdapter {

	public static final String IDSC_ALARM = "idsc_alarm";
	String[] decryptedid = new String[2];
	private static final String TAG = "SyncAdapter";
	private AccountManager mAccountManager;
	private String mVersion = "";
	private int mCountPerformSync = 0;
	private int mCountSyncCanceled = 0;
	private int mCountProviderFailed = 0;
	ContentResolver contentResolver;
	private int mCountProviderFailedMax = 3;
	private Context mContext;
	private SessionManager session;
	//	int calendarid;
	List<Uri> notifyevent = new ArrayList<>();

	List<Event> listAlarm;
	AlarmManager[] alarmManager;
	ArrayList<PendingIntent> intentArray;
	private Intent myIntent;
	private PendingIntent pending_intent;


/*	private static final String[] CALENDAR_PROJECTION = new String[] {
	    Calendars._ID,                           // 0
	    Calendars.ACCOUNT_NAME,                  // 1
	    Calendars.CALENDAR_DISPLAY_NAME,         // 2
	    Calendars.OWNER_ACCOUNT,                 // 3
	    Calendar.CTAG                            // 4
	};*/
	  
/*	// The indices for the projection array above.
	private static final int PROJECTION_ID_INDEX = 0;
	private static final int PROJECTION_ACCOUNT_NAME_INDEX = 1;
	private static final int PROJECTION_DISPLAY_NAME_INDEX = 2;
	private static final int PROJECTION_OWNER_ACCOUNT_INDEX = 3;
*/
	
/*
	private static final String[] EVENT_PROJECTION = new String[] {
		Events._ID,
		Events._SYNC_ID,
		Events.SYNC_DATA1,
		Events.CALENDAR_ID
	};
*/

	// ignore same CTag
	//private static final boolean FORCE_SYNCHRONIZE = false;
	// drop all calendar before synchro
	//private static final boolean DROP_CALENDAR_EVENTS = false;

	public SyncAdapter(Context context, boolean autoInitialize) {
		super(context, autoInitialize);
		//android.os.Debug.waitForDebugger();
		session = new SessionManager(context);
		mAccountManager = AccountManager.get(context);
		listAlarm = new ArrayList<>();
		try {
			mVersion = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		mContext = context;
		contentResolver = context.getContentResolver();
//		mContext = context;
	}

	private int updateCalendarEntry(ContentProviderClient provider, Account account, int entryID,
									String name, int color, int allDay, String location,
									String startMillis, String endMillis, String idSc) {
		int iNumRowsUpdated = 0;
		int calendarid;

		try {
			calendarid = Integer.parseInt(mAccountManager.getUserData(account, AppAccountManager.USER_DATA_CALENDAR));
		} catch (NumberFormatException e) {
			MyLog.w("calendarId is null or not valid int");
			return 0;
		}

		MyLog.d("startTime=" + DateUtil.getDateTimeString(Long.parseLong(startMillis)) + ", endTime=" + DateUtil.getDateTimeString(Long.parseLong(endMillis))
				+ "timeZone=" + TimeZone.getDefault().toString());

		ContentValues cv = new ContentValues();
		System.out.println(calendarid);
		cv.put(Events.TITLE, name);
		cv.put(Events.DTSTART, Long.parseLong(startMillis));
		cv.put(Events.DTEND, Long.parseLong(endMillis));
		cv.put(Events.CALENDAR_ID, calendarid);
		cv.put(Events.EVENT_COLOR, color);
		cv.put(Events.ALL_DAY, allDay);
		cv.put(Events.EVENT_LOCATION, location);
		cv.put(Events.EVENT_TIMEZONE, TimeZone.getDefault().toString());
		cv.put(Events.SYNC_DATA1,idSc);
		cv.put(Events._ID, entryID);

		Uri eventUri = ContentUris.withAppendedId(Events.CONTENT_URI, entryID);

		try {
			iNumRowsUpdated = provider.update(asSyncAdapter(eventUri,account.name,account.type),cv,null,null);
		} catch (RemoteException e) {
			e.printStackTrace();
		}


		if(iNumRowsUpdated==0)
		{

			createEventWithName(mContext, account, entryID+"", name, color, allDay, location, startMillis+"", endMillis+"", idSc, calendarid);
		}

//		session.setSyncCalendarId(iNumRowsUpdated);
		return iNumRowsUpdated;
	}


	public  void createEventWithName(Context ctx, Account account, String entryid, String name, int color,
									 int allDay, String location, String startMillis, String endMillis, String idSc, int calendarid) {

		MyLog.i("entryid=" + entryid + ", name=" + name + ", location=" + location
				+ ", color=" + color + ", allDay=" + allDay + ", startMillis=" + DateUtil.getDateTimeString(Long.parseLong(startMillis))
				+ ", endMillis=" + DateUtil.getDateTimeString(Long.parseLong(endMillis)));

		ContentValues cv = new ContentValues();
		cv.put(CalendarContract.Events.TITLE, name);
		cv.put(Events._ID,entryid);
		cv.put(Events.DTSTART, Long.parseLong(startMillis));
		cv.put(Events.DTEND, Long.parseLong(endMillis));
		cv.put(Events.CALENDAR_ID, calendarid);
		cv.put(Events.EVENT_COLOR, color);

		cv.put(Events.ALL_DAY, allDay);
		cv.put(Events.EVENT_LOCATION, location);
		cv.put(Events.EVENT_TIMEZONE, TimeZone.getDefault().toString());
		cv.put(Events.SYNC_DATA1,idSc);

		//cv.put(Events.RRULE, "FREQ=DAILY;INTERVAL=2");

		if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
			return ;
		}

		Uri newEvent = ctx.getContentResolver().insert(asSyncAdapter(Events.CONTENT_URI, account.name, account.type), cv);
		notifyevent.add(newEvent);

	}


	@Override
	public void onPerformSync(Account account, Bundle extras, String authority,
							  ContentProviderClient provider, SyncResult syncResult) {

		MyLog.i("autority=" + authority + ", syncResult=" + syncResult.toString());
		String idUser = mAccountManager.getUserData(account, AppAccountManager.USER_DATA_ID);
		String idSc = extras.getString("idSc");

		if (idSc == null) {
			// sync all
			this.mCountPerformSync += 1;

			agendar(idUser, provider, account);

			for (Uri uri : notifyevent) {
				try {
					getContext().getContentResolver().notifyChange(uri, null, true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		} else {
			// sync specific event

			String color = extras.getString("color");
			int allDay = extras.getBoolean("allDay") ? 1 : 0;
			long startMillis = Long.parseLong(extras.getString("startMillis"));
			long endMillis = Long.parseLong(extras.getString("endMillis"));
			String eventName = extras.getString("eventName");
			String location = extras.getString("location");

			int eventId = Integer.parseInt(idSc.split("-")[0]);

			syncCalendar(provider, account, idUser, idSc, eventId, eventName, location, color, allDay, startMillis, endMillis);

			getContext().getContentResolver().notifyChange(ContentUris.withAppendedId(Events.CONTENT_URI, eventId), null, false);
		}
	}

	private void syncCalendar(final ContentProviderClient provider, final Account account, String idUser, String idSc,
							  final int eventId, final String eventName, final String location, final String color, final int checkAllday,
							  final long startMillis, final long endMillis) {

		MyLog.i("idUser=" + idUser + ", eventId=" + eventId + ", eventName=" + eventName + ", location=" + location
				+ ", color=" + color + ", checkAllday=" + checkAllday + ", startMillis=" + DateUtil.getDateTimeString(startMillis)
				+ ", endMillis=" + DateUtil.getDateTimeString(endMillis));

		updateCalendarEntry(provider, account, eventId, eventName, Color.parseColor(color),
				checkAllday, location, startMillis+"", endMillis+"", idSc);
	}


	public  void agendar(final String iduser, final ContentProviderClient provider, final  Account account) {
		AppController.getInstance().getRequestQueue().getCache().clear();

		StringRequest strReq = new StringRequest(Request.Method.POST,
				AppConfig.URL_AGENDA, new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {

				if(response !=null){
					parseJsonFeed(response, provider, account);
				}

			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				error.printStackTrace();

			}
		}) {
			@Override
			protected Map<String, String> getParams() {
				// Posting params to register url
				Map<String, String> params = new HashMap<String, String>();
				params.put("iduser", iduser);
				return params;

			}
		};

		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(strReq);

	}

	public void parseJsonFeed(String response, ContentProviderClient provider, Account account){
		try {

			JSONObject jObj = new JSONObject(response);
			JSONArray feedArray = jObj.getJSONArray("feed");

			listAlarm.clear();
			alarmManager = new AlarmManager[feedArray.length()];
			intentArray = new ArrayList<PendingIntent>();
			myIntent = new Intent(mContext, AlarmReceiver.class);
			int j = 0;

			int gmtOffset = TimeZone.getDefault().getRawOffset();
			for (int l = 0; l < feedArray.length(); l++) {

				JSONObject feedObj = (JSONObject) feedArray.get(l);
				Event item = new Event();
				String idEventTigo = feedObj.getString("id_sc");
				decryptedid = feedObj.getString("id_sc").split("-");

				item.id= Long.parseLong(decryptedid[0]);

				item.title = (feedObj.getString("status"));
				item.color= Color.parseColor(feedObj.getString("color"));
				item.location = feedObj.getString("location");
				item.alarm =Long.parseLong(feedObj.getString("alarm"));
				item.idSc =  feedObj.getString("id_sc");
				item.image = feedObj.getString("image");

				int checkAllday = 0;
				if(feedObj.getString("allday").equalsIgnoreCase("1")){
					checkAllday = 1;
					long longStart = Long.parseLong(feedObj.getString("daystart"))+ gmtOffset;
					item.startMillis = longStart ;
					long longEnd = Long.parseLong(feedObj.getString("dayfinish"))+ gmtOffset;
					item.endMillis= longEnd;

				}else{
					checkAllday = 0;
					item.startMillis = Long.parseLong(feedObj.getString("daystart"));
					item.endMillis= Long.parseLong(feedObj.getString("dayfinish"));
				}

				long alarm = item.alarm * 1000 * 60;
				long timeAlarm = item.startMillis - alarm;
				long time = System.currentTimeMillis();
				if (time <= timeAlarm) {
					listAlarm.add(new Event(timeAlarm, item.idSc,item.title.toString(), item.startMillis, item.image));
				}


				item.hasAlarm = false;
				item.isRepeating = false;
				item.selfAttendeeStatus = 0;

				updateCalendarEntry(provider, account, (int) item.id,item.title.toString(),item.color,
						checkAllday,item.location.toString(),item.startMillis+"",item.endMillis+"", idEventTigo);

			}

			if(listAlarm!=null && !listAlarm.isEmpty()) {

				for (int i = 0; i < listAlarm.size(); i++) {

					myIntent.putExtra(IDSC_ALARM, listAlarm.get(i).idSc);
					myIntent.putExtra("indexAlarm", i);
					myIntent.putExtra("title", listAlarm.get(i).status);
					myIntent.putExtra("startDay", listAlarm.get(i).startMillisNoti);
					myIntent.putExtra("image", listAlarm.get(i).image);
					pending_intent = PendingIntent.getBroadcast(mContext, i, myIntent, i);
					alarmManager[i] = (AlarmManager) mContext.getSystemService(ALARM_SERVICE);
					alarmManager[i].set(AlarmManager.RTC_WAKEUP, listAlarm.get(i).timeAlarm, pending_intent);
					intentArray.add(pending_intent);

				}


			}



			if (session.getIsAlarm()) {
				if (intentArray != null && !intentArray.isEmpty())
					for (int i = 0; i < intentArray.size(); i++) {
						alarmManager[i].cancel(intentArray.get(i));
					}
			}

		} catch (JSONException e) {
		}
	}


	public void onSyncCanceled () {
		//TODO: implement SyncCanceled
		this.mCountSyncCanceled += 1;
		Log.v(TAG, "onSyncCanceled() count:" + String.valueOf(this.mCountSyncCanceled));
	}


	private Account UpgradeAccount(Account OldAccount) {
		String Username = OldAccount.name;
		String Type = OldAccount.type;
		String Password = this.mAccountManager.getPassword(OldAccount);
		String idUser = this.mAccountManager.getUserData(OldAccount, AppAccountManager.USER_DATA_ID);

		Account NewAccount = new Account(Username + LoginActivity.ACCOUNT_NAME_SPLITTER + idUser, Type);
		if (this.mAccountManager.addAccountExplicitly(NewAccount, Password, null)) {
			this.mAccountManager.setUserData(NewAccount, AppAccountManager.USER_DATA_ID, idUser);
			this.mAccountManager.setUserData(NewAccount, AppAccountManager.USER_DATA_USERNAME, Username);
		}
		this.mAccountManager.removeAccount(OldAccount, null, null);

		return NewAccount;
	}

	private void dropAllEvents(Account account, ContentProviderClient provider,	Uri calendarUri) throws RemoteException {

		Log.i(TAG, "Deleting all calendar events for "+calendarUri);

		String selection = "(" + Events.CALENDAR_ID + " = ?)";
		String[] selectionArgs = new String[] {Long.toString(ContentUris.parseId(calendarUri))};

		provider.delete(asSyncAdapter(Events.CONTENT_URI, account.name, account.type),
				selection, selectionArgs);

	}

	private static Uri asSyncAdapter(Uri uri, String account, String accountType) {
		return uri.buildUpon()
				.appendQueryParameter(android.provider.CalendarContract.CALLER_IS_SYNCADAPTER,"true")
				.appendQueryParameter(Calendars.ACCOUNT_NAME, account)
				.appendQueryParameter(Calendars.ACCOUNT_TYPE, accountType).build();
	}

}

