package com.finger.tigo.items;

public class FeedItem {
	public int id;
	public String name;
	public String namecomment;
	public String avatarComment;
	public String status;
	public String image;
	public String profilePic;
	public String timeStamp;
	public String url;
	public String timestart;
	public String daystart;
	public String timeend;
	public String dayend;
	public String address;
	public String position;
	public String color;
	public Double lg1;
	public Double lg2;
	public String mode;
	public String alarm;
	public String email;
	public String description;
	public String idSc;
	public String phone;
	public String idRComment;
	public String profilePicReplay;
	public String nameReplay;
	public String containerCommentRply;
	public String currendateCommentRply;
	public String countcommentplay;
	public String idUserReplayComment;
	public String idComment;
	public String phoneEvent;
	public String category;
	public String allowInvite;
	public String allDay;
	public String password;




	public FeedItem(int id, String name, String namecomment, String avatarComment, String status, String image, String profilePic, String timeStamp, String url, String timestart, String timeend, String dayend, String daystart, String address, String position, String color, Double lg1, Double lg2, String mode, String alarm, String email, String id_sc_like, String count_participation, String count_care, String countComment, Double lng, Double lat, String id_sc, String statusComment, String timecomment, String codeComment) {
		this.id = id;
		this.name = name;
		this.namecomment = namecomment;
		this.avatarComment = avatarComment;
		this.status = status;
		this.image = image;
		this.profilePic = profilePic;
		this.timeStamp = timeStamp;
		this.url = url;
		this.timestart = timestart;
		this.timeend = timeend;
		this.dayend = dayend;
		this.daystart = daystart;
		this.address = address;
		this.position = position;
		this.color = color;
		this.lg1 = lg1;
		this.lg2 = lg2;
		this.mode = mode;
		this.alarm = alarm;
		this.email = email;
		this.id_sc_like = id_sc_like;
		this.count_participation = count_participation;
		this.count_care = count_care;
		this.countComment = countComment;
		this.lng = lng;
		this.lat = lat;
		idSc = id_sc;
		StatusComment = statusComment;
		Timecomment = timecomment;
		CodeComment = codeComment;
	}

	public FeedItem(String idSc, String name, String desciption, String image, String daystart, String dayend){
		this.idSc = idSc;
		this.name = name;
		this.description = desciption;
		this.image = image;
		this.daystart = daystart;
		this.dayend = dayend;
	}
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAllDay() {
		return allDay;
	}

	public void setAllDay(String allDay) {
		this.allDay = allDay;
	}

	public String getAllowInvite() {
		return allowInvite;
	}

	public void setAllowInvite(String allowInvite) {
		this.allowInvite = allowInvite;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getPhoneEvent() {
		return phoneEvent;
	}

	public void setPhoneEvent(String phoneEvent) {
		this.phoneEvent = phoneEvent;
	}

	public String getIdComment() {
		return idComment;
	}

	public void setIdComment(String idComment) {
		this.idComment = idComment;
	}

	public String getCountcommentplay() {
		return countcommentplay;
	}

	public void setCountcommentplay(String countcommentplay) {
		this.countcommentplay = countcommentplay;
	}

	public String getIdUserReplayComment() {
		return idUserReplayComment;
	}

	public void setIdUserReplayComment(String idUserReplayComment) {
		this.idUserReplayComment = idUserReplayComment;
	}

	public String getContainerCommentRply() {
		return containerCommentRply;
	}

	public void setContainerCommentRply(String containerCommentRply) {
		this.containerCommentRply = containerCommentRply;
	}

	public String getNameReplay() {
		return nameReplay;
	}

	public void setNameReplay(String nameReplay) {
		this.nameReplay = nameReplay;
	}

	public String getCurrendateCommentRply() {
		return currendateCommentRply;
	}

	public void setCurrendateCommentRply(String currendateCommentRply) {
		this.currendateCommentRply = currendateCommentRply;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getDesciption() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAvatarComment() {
		return avatarComment;
	}
	public void setAvatarComment(String avatarComment) {
		this.avatarComment = avatarComment;
	}
	public String getNamecomment() {
		return namecomment;
	}
	public void setNamecomment(String namecomment) {
		this.namecomment = namecomment;
	}

	public String getViewnow() {
		return viewnow;
	}

	public void setViewnow(String viewnow) {
		this.viewnow = viewnow;
	}

	public  String viewnow;

	public String getCount_view() {
		return count_view;
	}

	public void setCount_view(String count_view) {
		this.count_view = count_view;
	}

	public String count_view;
	public String getStatus_pass() {
		return status_pass;
	}

	public void setStatus_pass(String status_pass) {
		this.status_pass = status_pass;
	}

	public String status_pass;

	public String getProfilePicReplay() {
		return profilePicReplay;
	}

	public void setProfilePicReplay(String profilePicReplay) {
		this.profilePicReplay = profilePicReplay;
	}

	public String getIdRComment() {
		return idRComment;
	}

	public void setIdRComment(String idRComment) {
		this.idRComment = idRComment;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getId_sc_like() {
		return id_sc_like;
	}

	public void setId_sc_like(String id_sc_like) {
		this.id_sc_like = id_sc_like;
	}

	public String id_sc_like;

	public String getCount_participation() {
		return count_participation;
	}

	public void setCount_participation(String count_participation) {
		this.count_participation = count_participation;
	}

	public String getCount_care() {
		return count_care;
	}

	public void setCount_care(String count_care) {
		this.count_care = count_care;
	}

	public String count_participation;
	public String count_care;

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Double getLg1() {
		return lg1;
	}

	public void setLg1(Double lg1) {
		this.lg1 = lg1;
	}

	public Double getLg2() {
		return lg2;
	}

	public void setLg2(Double lg2) {
		this.lg2 = lg2;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getAlarm() {
		return alarm;
	}

	public void setAlarm(String alarm) {
		this.alarm = alarm;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getCountComment() {
		return countComment;
	}

	public void setCountComment(String countComment) {
		this.countComment = countComment;
	}

	public String countComment;
	public FeedItem(){
	}
	public FeedItem(Double lng, Double lat) {
		this.lng = lng;
		this.lat = lat;
	}
	public Double lng;
	public Double lat;
	public String getId_sc() {
		return idSc;
	}
	public void setId_sc(String id_sc) {
		idSc = id_sc;
	}

	public String getStatusComment() {
		return StatusComment;
	}
	public void setStatusComment(String statusComment) {
		StatusComment = statusComment;
	}
	public String StatusComment;
	public String getCodeComment() {
		return CodeComment;
	}
	public void setCodeComment(String codeComment) {
		CodeComment = codeComment;
	}
	public String getTimecomment() {
		return Timecomment;
	}
	public void setTimecomment(String timecomment) {
		Timecomment = timecomment;
	}
	public String Timecomment;
	public String CodeComment;
	public Double getLng() {
		return lng;
	}
	public void setLng(Double lng) {
		this.lng = lng;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Double getLat() {
		return lat;
	}
	public void setLat(Double lat) {
		this.lat = lat;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImge() {
		return image;
	}
	public void setImge(String image) {
		this.image = image;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getProfilePic() {
		return profilePic;
	}
	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTimestart() {
		return timestart;
	}
	public void setTimestart(String timestart) {
		this.timestart = timestart;
	}
	public String getdaystart() {
		return daystart;
	}
	public void setdaystart(String datestart) {
		this.daystart = datestart;
	}
	public String getTimeend() {
		return timeend;
	}
	public void setTimeend(String timeend) {
		this.timeend = timeend;
	}
	public String getDateend() {
		return dayend;
	}
	public void setDayend(String dayend) {
		this.dayend = dayend;
	}
	public String id_user;
	public String getId_user() {return id_user;}
	public void setId_user(String id_user) {
		this.id_user = id_user;
	}
}