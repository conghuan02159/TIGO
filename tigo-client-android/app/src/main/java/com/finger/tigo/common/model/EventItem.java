package com.finger.tigo.common.model;

/**
 * Created by Duong on 4/5/2017.
 */

public class EventItem {

    public String status;
    public String color;
    public String position;
    public String location;
    public double lg1;
    public double lg2;
    public String dayStart;
    public String dayFinish;
    public boolean allDay;
    public String image;
    public String phone;
    public String desription;
    public String state;
    public String countJoin;
    public String userName;
    public String countComment;
    public String CheckInvite;
    public String idUser;

    public EventItem(String iduser, String status, String color, String position, String location,
                     double lg1, double lg2, String dayStart, String dayFinish, boolean allDay,
                     String image, String phone, String desription, String state, String countJoin, String userName, String countComment, String CheckInvite){
        this.idUser = iduser;
        this.status = status;
        this.color = color;
        this.position = position;
        this.location = location;
        this.lg1 = lg1;
        this.lg2= lg2;
        this.dayStart = dayStart;
        this.dayFinish = dayFinish;
        this.allDay = allDay;
        this.image = image;
        this.phone = phone;
        this.desription = desription;
        this.state = state;
        this.countJoin = countJoin;
        this.userName = userName;
        this.countComment = countComment;
        this.CheckInvite = CheckInvite;
    }
    public EventItem(String state){
        this.state = state;
    }
}
