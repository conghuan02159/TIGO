package com.finger.tigo.register;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.finger.tigo.BaseActivity;
import com.finger.tigo.Introslider.PrefManager;
import com.finger.tigo.R;
import com.finger.tigo.SlideMenuActivity;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.addfriend.ListSyncActivity;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.app.AppIntent;
import com.finger.tigo.common.contact.Contact;
import com.finger.tigo.common.contact.ContactsLoadListener;
import com.finger.tigo.common.contact.ContactsLoader;
import com.finger.tigo.libcountrypicker.fragments.CountryPicker;
import com.finger.tigo.libcountrypicker.interfaces.CountryPickerListener;
import com.finger.tigo.register.async.AsyncRegisterInteractor;
import com.finger.tigo.register.model.ItemRegister;
import com.finger.tigo.register.stateprogress.StateProgressBar;
import com.finger.tigo.service.OnSmsCatchListener;
import com.finger.tigo.service.SmsVerifyCatcher;
import com.finger.tigo.session.SessionManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class RegisterActivity extends BaseActivity implements IRegisterView {

    public static final String CREATE_INFORMATION = "createInformation";
    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String VERIFICODE = "verificode";
    public static final String SYNC_CONTACT = "syncContact";

    private static final int PERMISSION_REQUEST_CONTACT = 123;
    private static final int PERMISSIONS_REQUEST_WRITE_CALENDAR = 0;
    private int KEY_BACK_PRESS = 0;
    Account account;

    protected String[] descriptionData = {"Create", "Phone", "Verify", "Sync"};
    // date and time
    private int mYear;
    private int mMonth;
    private int mDay;
//    private ProgressDialog pDialog;
    private SessionManager session;

    RegisterPresenter presenter;


    TextInputLayout textNameLayout;
    TextInputLayout textPasswordLayout;
    TextInputLayout textEnterPassLayout;
    TextInputLayout textPhoneLayout;
    EditText inputFullName;
    EditText inputEnterpasseword;
    EditText inputPassword;
    EditText etVerificode;
    EditText inputPhone;
    RadioButton male;
    RadioButton female;
    TextView birthday;
    TextView tvMobile;
    TextView count_time;
    TextView tvCountry;
    Button change_date_but;
    Button btMobile;
    Button bResend;
    Button btSendNumber;
    Button btCountry,btnNext,btVerificode,sync_contact,next_sync;
    LinearLayout layoutCreate;
    LinearLayout layoutPhone;
    // Layout input verificode
    public LinearLayout layoutInputVerificode;

    public LinearLayout layoutSync;

    public StateProgressBar stateProgressBar;
    private SmsVerifyCatcher smsVerifyCatcher;

    private String phone;
    private int currentTime;
    private int waitingTime = 60;
    String name;
    String password;
    String enterpassword;
    long birthdays;
    int sexs;

    String idUser;
    private String strCode;
    private Calendar calendar;
    private CountryPicker mCountryPicker;
    PrefManager prefManager;
    AccountManager mAccountManager;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creat_acount);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        textNameLayout = (TextInputLayout)findViewById(R.id.text_name_layout);
        textPasswordLayout = (TextInputLayout)findViewById(R.id.text_password_layout);
        textEnterPassLayout = (TextInputLayout)findViewById(R.id.text_enter_pass);
        textPhoneLayout = (TextInputLayout)findViewById(R.id.txt_phonenumber_layout);
        inputFullName = (EditText)findViewById(R.id.name);
        inputEnterpasseword = (EditText)findViewById(R.id.enterpassword);
        inputPassword = (EditText)findViewById(R.id.password);
        etVerificode = (EditText)findViewById(R.id.et_verificode);
        inputPhone = (EditText)findViewById(R.id.et_phonenumber);
        male = (RadioButton)findViewById(R.id.male);
        female = (RadioButton)findViewById(R.id.female);
        birthday = (TextView)findViewById(R.id.birthday);
        tvMobile = (TextView)findViewById(R.id.txt_edit_mobile);
        count_time = (TextView)findViewById(R.id.count_time);
        tvCountry = (TextView)findViewById(R.id.tv_country);
        change_date_but = (Button)findViewById(R.id.but_Setdate);
        btMobile = (Button)findViewById(R.id.btn_edit_mobile);
        bResend = (Button)findViewById(R.id.btnResend);
        btSendNumber = (Button)findViewById(R.id.btn_next_phone);
        btCountry = (Button)findViewById(R.id.bt_countrycode);
        layoutCreate = (LinearLayout)findViewById(R.id.layoutCreate);
        layoutPhone = (LinearLayout)findViewById(R.id.lnPhoneNumber);
        btnNext = (Button)findViewById(R.id.btnNext);
        btVerificode = (Button)findViewById(R.id.btVerificode);
        sync_contact = (Button)findViewById(R.id.sync_contact);
        next_sync = (Button)findViewById(R.id.next_sync);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        presenter = new RegisterPresenter(this);
        mAccountManager = AccountManager.get(this);
        prefManager = new PrefManager(this);
        stateProgressBar = (StateProgressBar) findViewById(R.id.usage_stateprogressbar);
        stateProgressBar.setStateDescriptionData(descriptionData);
        stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.ONE);

        layoutInputVerificode = (LinearLayout) findViewById(R.id.lnVerificode);
        layoutSync = (LinearLayout) findViewById(R.id.layoutSync);

        calendar = Calendar.getInstance();
        mYear = calendar.get(Calendar.YEAR);
        mMonth = calendar.get(Calendar.MONTH);
        mDay = calendar.get(Calendar.DAY_OF_MONTH);

        SimpleDateFormat
                dft = new SimpleDateFormat("E dd/MM/yyyy", Locale.getDefault());
        String strDate = dft.format(calendar.getTime());
        birthday.setText(strDate);

        currentTime = waitingTime;
        count_time.setText("" + currentTime);

        // Session manager
        session = new SessionManager(this);
        strCode = session.getKeyCodeCountry();

        tvCountry.setText(strCode);
        mCountryPicker = CountryPicker.newInstance(getResources().getString(R.string.select_country));
        btCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCountryPicker.show(getFragmentManager(), "COUNTRY_PICKER");
            }
        });

        mCountryPicker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode,
                                        int flagDrawableResID) {
                tvCountry.setText(dialCode);
                strCode = dialCode;
                mCountryPicker.dismiss();
            }
        });


        male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (male.isChecked())
                    sexs = 1;
            }
        });
        female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (female.isChecked())
                    sexs = 0;
            }
        });

        change_date_but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                selectBirthday();
            }
        });

        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                String code = parseCode(message);//Parse verification code
                etVerificode.setText(code);//set code in edit text
//                putOtp();
                //then you can send verification code to server
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCreateInformation(v);
            }
        });
        btSendNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSendNumber(v);
            }
        });
        btVerificode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendVerificode();
            }
        });
        bResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onResendOTP();
            }
        });
        btMobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditMobile();
            }
        });
        sync_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SyncContact();
            }
        });
        next_sync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NextHome();
            }
        });

    }

    @Override
    public ProgressDialog createProgressDialog() {
        return new ProgressDialog(this);
    }

    private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{6}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }

    // create infomation

    public void onCreateInformation(View v) {

        name = inputFullName.getText().toString();
        password = inputPassword.getText().toString();
        enterpassword = inputEnterpasseword.getText().toString();
        presenter.attemRegisterCreate(name, password, enterpassword);

    }

    // create phone

    public void onSendNumber(View view) {
        phone = inputPhone.getText().toString();
        birthdays = calendar.getTimeInMillis();

        currentTime = waitingTime;
        count_time.setText("" + currentTime);

//        pDialog.setMessage(getText(R.string.register));
//        showDialog();
        showProgress(getString(R.string.register));

        presenter.attemRegisterPhone(strCode, phone, name, password, enterpassword, sexs, birthdays);


    }

    // send verificode

    public void sendVerificode() {

        putOtp();

    }

    private void putOtp() {
        String otp = etVerificode.getText().toString().trim();
        InputMethodManager inputManager =
                (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(
                this.getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
        if (!otp.isEmpty()) {
            showProgress(getResources().getString(R.string.send_OTP));
            presenter.attemRegisterVerifiCode(otp);
        } else {
            Toast.makeText(getApplicationContext(), R.string.error_otp, Toast.LENGTH_SHORT).show();
        }
    }

    public void onResendOTP() {

        currentTime = waitingTime;
        count_time.setText(currentTime + "");
        handler.removeCallbacks(runnable);
        runnable.run();
        bResend.setVisibility(View.GONE);
        etVerificode.setText("");
        idUser = session.get_id_user_session() + "";
        putVerifiCode(idUser, session.getphone_session());

    }


    public void EditMobile() {
        handler.removeCallbacks(runnable);
        layoutInputVerificode.setVisibility(View.GONE);
        layoutPhone.setVisibility(View.VISIBLE);
        stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
        etVerificode.setText("");
    }

    // sync contact


    public void SyncContact() {
        showProgress(getResources().getString(R.string.contact_sync));
        idUser = session.get_id_user_session() + "";
        askForContactPermission();
    }

    // next of sync contact

    public void NextHome() {
//        launchHomeScreen();
        account = new Account(session.getLName_session(), AppAccountManager.ACCOUNT_TYPE);

        if (mAccountManager.addAccountExplicitly(account, password, null)) {
            if (Build.VERSION.SDK_INT >= 23) {

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {

                    checkAppPermissions();

                } else if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED) {

                    AppAccountManager.saveAccountUserDataAndSyncCalendar(this, account, String.valueOf(session.get_id_user_session()), session.getLName_session());

                    Intent i = new Intent(RegisterActivity.this, SlideMenuActivity.class);
                    startActivity(i);
                    finish();

                }

            } else {
                AppAccountManager.saveAccountUserDataAndSyncCalendar(this, account, String.valueOf(session.get_id_user_session()), session.getLName_session());

                session.setSyncDate(Calendar.getInstance().getTimeInMillis());

                Intent i = new Intent(RegisterActivity.this, SlideMenuActivity.class);
                startActivity(i);
                finish();

            }

        }

    }


    private void launchHomeScreen() {
        prefManager.setIsAddFriednsLaunch(false);

        startActivity(new Intent(RegisterActivity.this, SlideMenuActivity.class));
        Intent a = new Intent();
        a.setAction(AppIntent.ACTION_FINISH_ACTIVITY);
        a.putExtra("finishFromLogin", true);
        this.sendBroadcast(a);
        finish();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void selectBirthday() {
        DatePickerDialog dpd = new DatePickerDialog(RegisterActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        calendar.set(year, month, day);
                        mMonth = month;
                        mDay = day;
                        mYear = year;
                        SimpleDateFormat dft = null;
                        dft = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
                        String strDate = dft.format(calendar.getTime());
                        birthday.setText(strDate);


                    }
                }, mYear, mMonth, mDay);

        dpd.show();
    }

    public void putVerifiCode(final String strIdUser, final String strPhone) {

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_RESEND_VERIFICODE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {


            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("id_user", strIdUser);
                params.put("phone", strPhone);

                return params;

            }

        };
        AppController.getInstance().addToRequestQueue(strReq);

    }

    private void checkAppPermissions() {
        // Here, thisActivity is the current activity


        // No explanation needed, we can request the permission.
        ActivityCompat.requestPermissions(RegisterActivity.this,
                new String[]{Manifest.permission.WRITE_CALENDAR},
                PERMISSIONS_REQUEST_WRITE_CALENDAR);


    }


    public void askForContactPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(RegisterActivity.this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(RegisterActivity.this,
                        Manifest.permission.READ_CONTACTS)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                    builder.setTitle(R.string.contacts_access);
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setMessage(R.string.please_contacts_access);//TODO put real question
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            requestPermissions(
                                    new String[]
                                            {Manifest.permission.READ_CONTACTS}
                                    , PERMISSION_REQUEST_CONTACT);
                        }
                    });
                    builder.show();
                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(RegisterActivity.this,
                            new String[]{Manifest.permission.READ_CONTACTS},
                            PERMISSION_REQUEST_CONTACT);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                getAllContacts();
            }
        } else {
            getAllContacts();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERMISSION_REQUEST_CONTACT: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getAllContacts();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    hideProgress();
                }
                return;
            }
            case PERMISSIONS_REQUEST_WRITE_CALENDAR: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    AppAccountManager.saveAccountUserDataAndSyncCalendar(this, account, String.valueOf(session.get_id_user_session()), session.getLName_session());

                    session.setSyncDate(Calendar.getInstance().getTimeInMillis());

                    Intent i = new Intent(RegisterActivity.this, SlideMenuActivity.class);
                    startActivity(i);
                    finish();


                } else {
                    AccountManager am = (AccountManager) this.getSystemService(Context.ACCOUNT_SERVICE);
                    Account[] accountsFromFirstApp = am.getAccountsByType(AppAccountManager.ACCOUNT_TYPE);
                    am.removeAccount(accountsFromFirstApp[0], null, null);
                    finish();
                    Toast.makeText(getApplicationContext(), R.string.user_rejected_calendar_write_permission, Toast.LENGTH_LONG).show();

                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void getAllContacts() {

        ContactsLoader contactsLoader = new ContactsLoader(this, new ContactsLoadListener() {
            @Override
            public void onLoadStarted() {
                showProgress(getResources().getString(R.string.contact_sync));
            }

            @Override
            public void onLoading(int percent) {

            }

            @Override
            public void onLoadCompleted(List<Contact> contacts) {
                hideProgress();
                if (contacts != null && !contacts.isEmpty())
                    presenter.attemRegisterSyncContact(contacts, idUser);
            }
        });

        contactsLoader.execute(strCode);
    }


    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        public void run() {

            currentTime--;
            if (currentTime == 0) {
                handler.removeCallbacks(runnable);
                bResend.setVisibility(View.VISIBLE);
                count_time.setText("" + currentTime);
            } else if (currentTime >= 0) {
                count_time.setText("" + currentTime);

                handler.postDelayed(this, 1000);

            }
        }
    };

    @Override
    public void onSuccess(String success, List<ItemRegister> listRegister) {

        if (success.equalsIgnoreCase(CREATE_INFORMATION)) {
            KEY_BACK_PRESS++;
            stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
            layoutCreate.setVisibility(View.GONE);
            layoutPhone.setVisibility(View.VISIBLE);

        } else if (success.equalsIgnoreCase(PHONE_NUMBER)) {
            hideProgress();
            layoutInputVerificode.setVisibility(View.VISIBLE);
            layoutPhone.setVisibility(View.GONE);
            stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);

            for (ItemRegister object : listRegister) {

                int id = Integer.parseInt(object.id);
                session.set_id_user_session(id);
                session.setName_session(object.name);
                session.setbirhtday_session(object.birthday);
                session.setsex_session(object.sex);
                session.setphone_session(object.phone);
                tvMobile.setText(object.phone);
            }

            if (session.getKeySoundEvent() == null) {
                session.setSound("tigobabysmall");
            }

            currentTime = waitingTime;
            count_time.setText("" + currentTime);
            runnable.run();


        } else if (success.equalsIgnoreCase(VERIFICODE)) {
            hideProgress();
            stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.FOUR);
            layoutInputVerificode.setVisibility(View.GONE);
            layoutSync.setVisibility(View.VISIBLE);
            session.setLogin(true);





        } else if (success.equalsIgnoreCase(SYNC_CONTACT)) {
            hideProgress();
//            Toast.makeText(getApplicationContext(), R.string.signup, Toast.LENGTH_SHORT).show();
            account = new Account(session.getLName_session(), AppAccountManager.ACCOUNT_TYPE);

            if (mAccountManager.addAccountExplicitly(account, password, null)) {
                if (Build.VERSION.SDK_INT >= 23) {

                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {

                        checkAppPermissions();

                    } else if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED) {

                        AppAccountManager.saveAccountUserDataAndSyncCalendar(this, account,
                                String.valueOf(session.get_id_user_session()), session.getLName_session());

                        session.setSyncDate(Calendar.getInstance().getTimeInMillis());

                        for (ItemRegister item : listRegister) {
                            if (item.confirm > 0) {

                                Intent i = new Intent(RegisterActivity.this, ListSyncActivity.class);
                                startActivity(i);
                                finish();
                            } else {

                                Intent i = new Intent(RegisterActivity.this, SlideMenuActivity.class);
                                startActivity(i);
                                finish();
                            }
                        }

                    }

                } else {
                    AppAccountManager.saveAccountUserDataAndSyncCalendar(this, account, String.valueOf(session.get_id_user_session()), session.getLName_session());

                    session.setSyncDate(Calendar.getInstance().getTimeInMillis());
                    for (ItemRegister item : listRegister) {
                        if (item.confirm > 0) {

                            Intent i = new Intent(RegisterActivity.this, ListSyncActivity.class);
                            startActivity(i);
                            finish();
                        } else {

                            Intent i = new Intent(RegisterActivity.this, SlideMenuActivity.class);
                            startActivity(i);
                            finish();
                        }
                    }

                }
                //  Log.v(TAG, "new account created");


            } else {
                // Log.v(TAG,"no new account created");
            }


        }

    }

    @Override
    public void onError(String error, int keyError) {
        if (error.equalsIgnoreCase(CREATE_INFORMATION)) {

            switch (keyError) {
                case AsyncRegisterInteractor.KEY_ERROR_ONE:
                    textNameLayout.setErrorEnabled(true);
                    textNameLayout.setError(getString(R.string.empty_name));
                    requestFocus(textNameLayout);
                    break;
                case AsyncRegisterInteractor.KEY_ERROR_TWO:
                    textPasswordLayout.setErrorEnabled(true);
                    textPasswordLayout.setError(getString(R.string.error_password));
                    requestFocus(textPasswordLayout);
                    break;
                case AsyncRegisterInteractor.KEY_ERROR_THREE:
                    textEnterPassLayout.setErrorEnabled(true);
                    textEnterPassLayout.setError(getString(R.string.error_enter_password));
                    requestFocus(textEnterPassLayout);
                    break;
                default:
                    break;
            }

        } else if (error.equalsIgnoreCase(PHONE_NUMBER)) {
            hideProgress();
            switch (keyError) {
                case AsyncRegisterInteractor.KEY_ERROR_ONE:

                    textPhoneLayout.setErrorEnabled(true);
                    textPhoneLayout.setError(getString(R.string.empty_phone));
                    requestFocus(textPhoneLayout);
                    break;
                case AsyncRegisterInteractor.KEY_ERROR_TWO:

                    textPhoneLayout.setErrorEnabled(true);
                    textPhoneLayout.setError(getString(R.string.invalid_phone));
                    requestFocus(textPhoneLayout);
                    break;
                case AsyncRegisterInteractor.KEY_ERROR_THREE:

                    Toast.makeText(getApplicationContext(), R.string.phone_used, Toast.LENGTH_SHORT).show();
                    break;
                case AsyncRegisterInteractor.KEY_ERROR_FOUR:

                    Toast.makeText(getApplicationContext(), R.string.notconnect, Toast.LENGTH_SHORT).show();
                    break;
                default:
                    break;
            }
        } else if (error.equalsIgnoreCase(VERIFICODE)) {
            hideProgress();
            switch (keyError) {
                case AsyncRegisterInteractor.KEY_ERROR_ONE:
                    Toast.makeText(getApplicationContext(), R.string.error_verificode, Toast.LENGTH_SHORT).show();
                    break;
                case AsyncRegisterInteractor.KEY_ERROR_TWO:
                    Toast.makeText(getApplicationContext(), R.string.notconnect, Toast.LENGTH_SHORT).show();
                    break;

                default:
                    break;
            }
        } else if (error.equalsIgnoreCase(SYNC_CONTACT)) {
            hideProgress();
            switch (keyError) {
                case AsyncRegisterInteractor.KEY_ERROR_FOUR:

                    Toast.makeText(getApplicationContext(), R.string.notconnect, Toast.LENGTH_SHORT).show();
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();
         smsVerifyCatcher.onStop();
    }


    /**
     * need for Android 6 real time permissions
     */

    @Override
    public void onBackPressed() {
        if (KEY_BACK_PRESS != 0) {
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(RegisterActivity.this);
            builder.setMessage(R.string.register_cancel_msg)
                    .setCancelable(false)
                    .setPositiveButton(R.string.notification_yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int idd) {

                            finish();

                        }
                    })
                    .setNegativeButton(R.string.notification_no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //  Action for 'NO' Button
                            dialog.cancel();
                        }
                    });

            //Creating dialog box
            android.app.AlertDialog alert = builder.create();
            //Setting the title manually
            alert.show();
        } else {
            super.onBackPressed();
            finish();
        }

    }


}