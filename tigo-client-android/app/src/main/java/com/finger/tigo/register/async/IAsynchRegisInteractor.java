package com.finger.tigo.register.async;

import com.finger.tigo.common.contact.Contact;

import java.util.List;

/**
 * Created by Duong on 3/23/2017.
 */

public interface IAsynchRegisInteractor {

    void validateCredentailsAsync(OnRegisFinishedListener listener, String name, String password, String rePassword);

    void validatePhoneAsync(String  countryCode, OnRegisFinishedListener listener, String phone, String name, String password, String rePassword, int sex, long birthday);

    void validateVerificodeAsync(OnRegisFinishedListener listener, String verificode);

    void validateSyncContactAsync(OnRegisFinishedListener listener, List<Contact> listContact, String idUser);
}
