package com.finger.tigo.addfriend.model;

/**
 * Created by Duong on 4/3/2017.
 */



/**
 * Created by Duong on 3/24/2017.
 */

public class ItemAddfriend {

    public String id;
    public String name;
    public String sex;
    public String profilePic;
    public String phone;
    public String birthday;
    public String countEvent;
    public String countFriend;
    public int confirm;
    // data event

    public String image;

    public String description;
    public String daystart;
    public String state;

    public ItemAddfriend(String state){
        this.state = state;
    }


    public ItemAddfriend (String id, String name, String profilePic){
        this.id= id;
        this.name = name;
        this.profilePic = profilePic;
    }

    public ItemAddfriend(int confirm, String countEvent, String countFriend){
        this.confirm = confirm;
        this.countFriend = countFriend;
        this.countEvent = countEvent;

    }
    
    public ItemAddfriend(String id, String name, String desciption, String image, String dayStart){
        this.id = id;
        this.name = name;
        this.description = desciption;
        this.image = image;
        this.daystart = dayStart;
    }
}
