package com.finger.tigo.register.stateprogress.utils;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by MyPC on 21/03/2017.
 */

public class FontManager {
    private static final Map<String, Typeface> FONTS = new HashMap<String, Typeface>();

    public static final String ROOT = "fonts/",
            FONTAWESOME = ROOT + "fontawesome-webfont.ttf";

    public static Typeface getTypeface(Context context, String fontName) {

//        Typeface typeface = FONTS.get(fontName);
//
//        if (typeface == null) {
//            typeface = Typeface.createFromAsset(context.getAssets(), fontName);
//            FONTS.put(fontName, typeface);
//        }
//
//        return typeface;

        synchronized (FONTS) {
            if (!FONTS.containsKey(fontName)) {
                try {
                    Typeface t = Typeface.createFromAsset(context.getAssets(),
                            fontName);
                    FONTS.put(fontName, t);
                } catch (Exception e) {

                    return null;
                }
            }
            return FONTS.get(fontName);
        }
    }


}
