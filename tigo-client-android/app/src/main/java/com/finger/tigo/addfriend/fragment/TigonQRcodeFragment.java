package com.finger.tigo.addfriend.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.addfriend.QR_code.DecoderActivity;
import com.finger.tigo.session.SessionManager;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.util.HashMap;
import java.util.Map;

import static android.graphics.Color.WHITE;


public class TigonQRcodeFragment extends Fragment {
    Button btn_scanqr;
    ImageView qr_image;
    SessionManager session;
    String idUser;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_tigon_qrcode, container, false);
        //   feedItems = new ArrayList<model_qr>();
        session = new SessionManager(getActivity());

        idUser = AppAccountManager.getAppAccountUserId(getActivity());

        qr_image = (ImageView) rootview.findViewById(R.id.qrcode_image);
        btn_scanqr = (Button) rootview.findViewById(R.id.btn_scanqr);
        int width = 500;
        int height = 500;
        int smallestDimension = width < height ? width : height;
        //setting parameters for qr code
        String charset = "UTF-8";
        Map<EncodeHintType, ErrorCorrectionLevel> hintMap =new HashMap<EncodeHintType, ErrorCorrectionLevel>();
        hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
        CreateQRCode(idUser, charset, hintMap, smallestDimension, smallestDimension);

        btn_scanqr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity() , DecoderActivity.class);
                startActivity(in);
                getActivity().finish();
            }
        });
        return rootview;
    }

    public  void CreateQRCode(String qrCodeData, String charset, Map hintMap, int qrCodeheight, int qrCodewidth){

        try {
            //generating qr code in bitmatrix type
            BitMatrix matrix = new MultiFormatWriter().encode(new String(qrCodeData.getBytes(charset), charset),
                    BarcodeFormat.QR_CODE, qrCodewidth, qrCodeheight, hintMap);
            //converting bitmatrix to bitmap

            int width = matrix.getWidth();
            int height = matrix.getHeight();
            int[] pixels = new int[width * height];
            // All are 0, or black, by default
            for (int y = 0; y < height; y++) {
                int offset = y * width;
                for (int x = 0; x < width; x++) {
                    //pixels[offset + x] = matrix.get(x, y) ? BLACK : WHITE;
                    pixels[offset + x] = matrix.get(x, y) ?
                            ResourcesCompat.getColor(getResources(), R.color.color_toolbar,null) :WHITE;
                }
            }

            Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
            //setting bitmap to image view
//            Bitmap overlay = BitmapFactory.decodeResource(getResources(), R.drawable.ic_phone);
            qr_image.setImageBitmap(mergeBitmaps( bitmap));
            Log.d("aaaaaaaaaaa", "CreateQRCode: "+bitmap);
        }catch (Exception er){
            Log.e("QrGenerate",er.getMessage());
        }
    }


    public Bitmap mergeBitmaps( Bitmap bitmap) {

        int height = bitmap.getHeight();
        int width = bitmap.getWidth();

        Bitmap combined = Bitmap.createBitmap(width, height, bitmap.getConfig());
        Canvas canvas = new Canvas(combined);


        canvas.drawBitmap(bitmap, new Matrix(), null);

//        int centreX = (canvasWidth  - overlay.getWidth()) /2;
//        int centreY = (canvasHeight - overlay.getHeight()) /2 ;
//        canvas.drawBitmap(overlay, centreX, centreY, null);

        return combined;
    }

}