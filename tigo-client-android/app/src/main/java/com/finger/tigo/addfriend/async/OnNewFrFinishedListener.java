package com.finger.tigo.addfriend.async;

import com.finger.tigo.addfriend.model.ItemAddfriend;

import java.util.List;

/**
 * Created by Duong on 4/3/2017.
 */

public interface OnNewFrFinishedListener {
    void listenerSuccess(String success, List<ItemAddfriend> listRegister);
    void listenerError(String error, int keyError);
}
