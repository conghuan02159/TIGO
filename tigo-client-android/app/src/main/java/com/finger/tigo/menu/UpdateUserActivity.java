package com.finger.tigo.menu;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.finger.tigo.BaseActivity;
import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.app.ApiExecuter;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.items.FeedItem;
import com.finger.tigo.session.SessionManager;
import com.finger.tigo.util.MyLog;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.finger.tigo.R.id.birthday;


public class UpdateUserActivity extends BaseActivity {


    private SessionManager session;
    private int mYear;
    private int mMonth;
    private int mDay;
    private Calendar calendar;
    private TextView birthday_user, name_user;
    private String idUser;
    private LinearLayout change_birthday, layout_name;
    Context context;
    private LayoutInflater inflater;
    private ListView list;



    public List<FeedItem> feedItems;
    public static final String TAG = "DetailsPersons";
    EditText e_name,edtoldpass,edtnewpass,edtphoneNumber;
    Button update, cant,btn_Huy,btn_Ok;
    LinearLayout layoutUpdate;
    AlertDialog.Builder dialog;
    View dialogView;
    public static String Password="password";
    String id_user,phone,pass,passold;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_user);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.detail_user));
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        toolbar.setTitleTextColor(Color.WHITE);
        actionBar.setDisplayHomeAsUpEnabled(true);
        session=new SessionManager(this);

        name_user = (TextView) findViewById(R.id.name);
        birthday_user = (TextView) findViewById(birthday);
        change_birthday = (LinearLayout) findViewById(R.id.but_birthday);
        layoutUpdate=(LinearLayout)findViewById(R.id.layoutUpdatepass);
        layoutUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updatePassword();
            }
        });


//        pDialog = new ProgressDialog(this);
//        pDialog.setCancelable(false);
        //downloadperson();
        session = new SessionManager(getApplicationContext());

        getUser();

        layout_name = (LinearLayout) findViewById(R.id.name_user);
        layout_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog=new Dialog(UpdateUserActivity.this);
//                dialog.setTitle("Change PassWord");
                //setting custom layout to dialog
                dialog.setContentView(R.layout.edit_name);

                e_name = (EditText) dialog.findViewById(R.id.e_name);
                e_name.setText(name_user.getText());
//                txt.setText("Nhập mật khẩu mới.");
                //adding button click event
                Button dismissButton = (Button) dialog.findViewById(R.id.but_Huy);
                dismissButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                Button okButton = (Button) dialog.findViewById(R.id.but_OK);
                okButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        name_user.setText(e_name.getText());
                        dialog.dismiss();
                    }
                });
                dialog.show();

            }
        });
        update = (Button) findViewById(R.id.but_OK);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customActionBar();

            }
        });
        cant = (Button) findViewById(R.id.but_Huy);
        cant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public ProgressDialog createProgressDialog() {
        return new ProgressDialog(this);
    }

    private void updatePassword() {

         final Dialog dialog=new Dialog(UpdateUserActivity.this);
//        dialog.setPositiveButton(R.string.cancelnot, null);
//        dialog.setNegativeButton(R.string.ok_password, null);
        LayoutInflater inflater=getLayoutInflater();
//        dialogView=inflater.inflate(R.layout.item_dialog_change_password,null);
        dialog.setContentView(R.layout.item_dialog_change_password);
        edtoldpass=(EditText)dialog.findViewById(R.id.edtoldPass) ;
        edtnewpass=(EditText)dialog.findViewById(R.id.edtNewPass);

        Button btnCancel=(Button)dialog.findViewById(R.id.btnCancelChange);
        Button btnOk=(Button)dialog.findViewById(R.id.btnOkChange);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               dialog.cancel();
            }
        });
        dialog.show();
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                informationPassWord();
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    public void customActionBar(){
        String id_user = idUser.trim();
        String name = name_user.getText().toString().trim();
        String birthday = calendar.getTimeInMillis()+"".trim();
        //  Log.d("aaaaaaaaaaa",idSc+" status: " + status+" location: "+ location+ " daystar: "+daystart+" dayfi: "+dayfinish+ " more: " +mode+" lga: "+ lga+" lgb: " +lgb +" color: "+ color );
        if (!id_user.isEmpty() && !name.isEmpty() && !birthday.isEmpty()) {
            UpdateUser(id_user,name, birthday);

        } else {
            Toast.makeText(getApplicationContext(),
                    R.string.error_enter_detail, Toast.LENGTH_LONG)
                    .show();
        }
    }
    public void selectBirthday() {
        DatePickerDialog dpd = new DatePickerDialog(UpdateUserActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        calendar.set(year, month, day);

                        mMonth = month;
                        mDay = day;
                        mYear = year;
                        SimpleDateFormat dft = null;
                        dft = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
                        String strDate = dft.format(calendar.getTime());
                        birthday_user.setText(strDate);


                    }
                }, mYear, mMonth, mDay);
        //dpd.getDatePicker().setMinDate(System.currentTimeMillis());
        dpd.show();
    }

    private void getUser(){
        idUser = AppAccountManager.getAppAccountUserId(this);
        name_user.setText(session.getLName_session());
        birthday_user.setText(session.getbirhtday_session());


        change_birthday.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                selectBirthday();
            }
        });
        calendar = Calendar.getInstance();
        if(session.getbirhtday_session() !=null) {
            long birtMili = Long.parseLong(session.getbirhtday_session());
            calendar.setTimeInMillis(birtMili);
        }
        mYear = calendar.get(Calendar.YEAR);
        mMonth = calendar.get(Calendar.MONTH);
        mDay = calendar.get(Calendar.DAY_OF_MONTH);
        SimpleDateFormat dft = new SimpleDateFormat("E dd/MM/yyyy", Locale.getDefault());
        String strDate = dft.format(calendar.getTime());
        birthday_user.setText(strDate);

    }
    public void informationPassWord(){
         id_user = idUser.trim();
         pass = edtnewpass.getText().toString().trim();
         passold=edtoldpass.getText().toString().trim();

        Log.e("thongtin",id_user+"---"+pass);
        if (!id_user.isEmpty() && !pass.isEmpty()&& !passold.isEmpty()) {
            if(pass.length()>=8) {
                CheckoldPassWord(id_user, passold);
            }
            else
            {
                Toast.makeText(this,"Ban Nhập chưa đủ 8 kí tự",Toast.LENGTH_SHORT).show();
            }

        }
        else
        {
            Toast.makeText(this,getResources().getString(R.string.information),Toast.LENGTH_LONG).show();
        }

    }
    private void UpdateUser(final String id_user, final String name_user, final String birthday_user) {

        ApiExecuter.requestApi(this, AppConfig.URL_UPDATE_USER, getString(R.string.upload), new ApiExecuter.ApiResultListener() {
            @Override
            public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                session.setName_session(name_user);
                session.setbirhtday_session(birthday_user);

                setResult(RESULT_OK);
                finish();
            }

            @Override
            public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {
                showToast(errMessage);
            }
        }, "id_user", id_user, "name", name_user, "birthday", birthday_user);
    }

    public void ResetPassword(final String iduser, final String password) {
        ApiExecuter.requestApi(this, AppConfig.URL_RESET_PASS, getString(R.string.changing_password), new ApiExecuter.ApiResultListener() {
            @Override
            public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                showToast(R.string.success_change_pw);
            }

            @Override
            public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {
                showToast(errMessage);
            }
        }, "id_user", iduser, "password", password);
    }

    public void CheckoldPassWord(final String id_user, final String password) {
        ApiExecuter.requestApi(this, AppConfig.URL_CHECK_PASSWORD, getString(R.string.changing_password), new ApiExecuter.ApiResultListener() {
            @Override
            public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                try {
                    if(response!=null) {
                        JSONObject jObj = new JSONObject(response);
                        String error=jObj.getString("error");
                        String error_msg=jObj.getString("error_msg");

                        if(error_msg.equalsIgnoreCase("1"))
                        {
                            ResetPassword(id_user,pass);
                            Toast.makeText(UpdateUserActivity.this,getResources().getString(R.string.change_pass),Toast.LENGTH_LONG).show();
                        }
                        else if(error_msg.equalsIgnoreCase("0"))
                        {
                            Toast.makeText(UpdateUserActivity.this,getResources().getString(R.string.change_not_pass),Toast.LENGTH_LONG).show();
                        }
                        MyLog.e("error_msg=" + error_msg);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    onFail(apiUrl, params, e.getMessage(), e);
                }
            }

            @Override
            public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {
                showToast(errMessage);
            }
        }, "id_user", id_user, "password", password);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {//Write your logic here
            this.finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getUser();


    }

}
