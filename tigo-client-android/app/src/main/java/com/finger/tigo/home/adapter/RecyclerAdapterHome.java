package com.finger.tigo.home.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.addfriend.DetailFriendActivity;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.common.model.EventItem;
import com.finger.tigo.detail.CommentActivity;
import com.finger.tigo.detail.EventDetailActivity;
import com.finger.tigo.home.ViewPagerImageActivity;
import com.finger.tigo.items.FeedItem;
import com.finger.tigo.session.SessionManager;
import com.finger.tigo.util.MyLog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.finger.tigo.detail.EventDetailActivity.M_DETAILS_HOME_ACTIVITY;
import static com.finger.tigo.detail.EventDetailActivity.idSc;

/**
 * Created by Duong on 4/3/2017.
 */

public class RecyclerAdapterHome extends RecyclerView.Adapter {


    private List<FeedItem> feedItems;
    private List <EventItem> eventItem;
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    public static String PUT_ID_SC = "putIdSc";
    public static String PUT_IMAGE = "image";
    String TAG = "FeedListAdapter";
    public String id_sc;
    public String click1 = "0";
    public String click0 = "1";
    // The minimum amount of items to have below your current scroll position
    // before loading more.
    private int visibleThreshold;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private RecyclerAdapterHome.OnLoadMoreListener onLoadMoreListener;
    private itemParticipationClick itemParticipaClick;
    private SessionManager session;

    Activity activity;

    public RecyclerAdapterHome(Activity activity, List<FeedItem> persons, RecyclerView mRecyclerView, itemParticipationClick itemParticipaClick){
        this.feedItems = persons;
        this.itemParticipaClick = itemParticipaClick;
        this.activity = activity;

        if(mRecyclerView.getLayoutManager() instanceof LinearLayoutManager){

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();

            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    visibleThreshold = linearLayoutManager.getChildCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    MyLog.e("aaa",loading+"");
                    MyLog.e("aaaaa",totalItemCount+"");
                    MyLog.e("aaaaaa",lastVisibleItem+"");
                    MyLog.e("aaaaaaaa",visibleThreshold+"");
                    if(!loading && totalItemCount <= (lastVisibleItem+ visibleThreshold)){
                        // End has been reached
                        // Do something
                        if(onLoadMoreListener != null){
                            onLoadMoreListener.onLoadmore();
                        }
                        loading = true;
                    }
                }
            });
        }

    }
    public interface OnLoadMoreListener{
        void onLoadmore();
    }
    public interface itemParticipationClick {
        void onItemClick(int position, int i, RecyclerView.ViewHolder v);
    }

    @Override
    public int getItemCount() {
        return feedItems.size();
    }
    @Override
    public int getItemViewType(int position) {
        return feedItems.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder vh;
            if(viewType == VIEW_ITEM){
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.listitemfeed, parent, false);
                vh = new RecyclerAdapterHome.MyViewHolder(v);
            }else {
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_event_progess, parent, false);
                vh = new RecyclerAdapterHome.ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {
        final FeedItem item = feedItems.get(position);
        session=new SessionManager(activity);
        final String idUser = AppAccountManager.getAppAccountUserId(activity);

        if(viewHolder instanceof RecyclerAdapterHome.MyViewHolder) {

            id_sc = item.getId_sc();
            Log.e("lalalaa",idUser+"---"+item.getId_user());

            if(idUser.equals(item.getId_user()))
            {
                feedItems.get(position).setStatus_pass(click0);
                ((MyViewHolder) viewHolder).btJoin.setEnabled(false);
                ((MyViewHolder) viewHolder).btJoin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                      Toast.makeText(activity,activity.getResources().getString(R.string.myinvite),Toast.LENGTH_LONG).show();
                    }
                });
            }
           else {
                ((MyViewHolder) viewHolder).btJoin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (item.getStatus_pass().equalsIgnoreCase("1")) {
                            itemParticipaClick.onItemClick(position, 1,  viewHolder);
                            int co = Integer.parseInt(feedItems.get(position).getCount_participation()) - 1;
                            feedItems.get(position).setCount_participation("" + co);
                            String d = feedItems.get(position).getCount_participation();
                            ((MyViewHolder) viewHolder).number_participation.setText(d);
                            feedItems.get(position).setStatus_pass(click1);

                        }
                        else
                        {
                            itemParticipaClick.onItemClick(position, 0,  viewHolder);
                            int co = Integer.parseInt(feedItems.get(position).getCount_participation()) + 1;
                            feedItems.get(position).setCount_participation("" + co);
                            String s = feedItems.get(position).getCount_participation();
                            ((MyViewHolder) viewHolder).number_participation.setText(s);
                            feedItems.get(position).setStatus_pass(click0);
                            MyLog.e("soluongthamgia",feedItems.get(position).getCount_participation());
                        }
                    }
                });

            }
                ((MyViewHolder)viewHolder).lnJoin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((MyViewHolder)viewHolder).btJoin.performClick();
                    }
                });


            ((MyViewHolder)viewHolder).lnDetailHome.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(activity, EventDetailActivity.class);

                    intent.putExtra(M_DETAILS_HOME_ACTIVITY, item.getId_sc());
                    intent.putExtra(EventDetailActivity.POST_ITEM_HOME, position+ "");

                    activity.startActivity(intent);
                    int co = 0;

                    co = Integer.parseInt(feedItems.get(position).getCount_view()) + 1;
                    feedItems.get(position).setCount_view("" + co);
                    String s = feedItems.get(position).getCount_view();
                    ((MyViewHolder)viewHolder).numberView.setText(s);
                    count_view(String.valueOf(idSc), String.valueOf(co));

                }
            });

            ((MyViewHolder)viewHolder).numberView.setText(item.getCount_view());
            ((MyViewHolder)viewHolder).lnComment.setOnClickListener(new View.OnClickListener() {
                                                      @Override
                                                      public void onClick(View v) {
                                                          Intent i = new Intent(activity, CommentActivity.class);
                                                          i.putExtra("mDetailsHomeActivity", item.getId_sc());
                                                          i.putExtra("countcomment", item.getCountComment());
                                                          i.putExtra("positem",position+"" );
                                                          activity.startActivity(i);
                                                      }
                                                  }
            );

            if (item.getStatus_pass().equalsIgnoreCase("1")) {
                //Inflating the Popup using xml file
                ((MyViewHolder)viewHolder).number_participation.setText(item.getCount_participation());
                ((MyViewHolder)viewHolder).btJoin.setBackgroundResource(R.drawable.ic_tigo_home_select);
                ((MyViewHolder)viewHolder).txtJoin.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
            } else {
                ((MyViewHolder)viewHolder).number_participation.setText(item.getCount_participation());
                ((MyViewHolder)viewHolder).btJoin.setBackgroundResource(R.drawable.ic_tigo_home);
                ((MyViewHolder)viewHolder).txtJoin.setTextColor(activity.getResources().getColor(R.color.dot_light_screen1));
            }

            ((MyViewHolder)viewHolder).name.setText(item.getName());

            ((MyViewHolder) viewHolder).numberComment.setText(item.getCountComment());
            MyLog.e("SoComment",item.getCountComment());
            CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
                    Long.parseLong(item.getTimeStamp()),
                    System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);

            ((MyViewHolder)viewHolder).timeStamp.setText(timeAgo);
            long dayStart = Long.parseLong(item.getdaystart());

            SimpleDateFormat dsft = new SimpleDateFormat("E, dd MMM yyyy", Locale.getDefault());
            final String strStDate = dsft.format(dayStart);
            SimpleDateFormat month = new SimpleDateFormat("MMM", Locale.getDefault());
            String strMonth = month.format(dayStart);
            SimpleDateFormat day = new SimpleDateFormat("dd", Locale.getDefault());
            String stDay = day.format(dayStart);
//		download_count_tg(id_sc,Count_participation);
            ((MyViewHolder)viewHolder).month.setText(strMonth);
            ((MyViewHolder)viewHolder).day.setText(stDay);
            long dayEnd = Long.parseLong(item.getDateend());
            SimpleDateFormat deft = new SimpleDateFormat("E, dd MMM yyyy", Locale.getDefault());
            String strEDate = deft.format(dayStart);
            final String dayEvent = strStDate + " - " + strEDate;
            ((MyViewHolder)viewHolder).dayEvent.setText(dayEvent);
            SimpleDateFormat timeStartSF = new SimpleDateFormat("HH:mm a", Locale.getDefault());
            final String startTime = timeStartSF.format(dayStart);
            SimpleDateFormat timeEndSF = new SimpleDateFormat("HH:mm a", Locale.getDefault());
            String endTime = timeEndSF.format(dayEnd);

            Calendar curent = Calendar.getInstance();
            if(curent.getTimeInMillis()> dayEnd)
            {
                ((MyViewHolder) viewHolder).btJoin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(activity,activity.getResources().getString(R.string.home_list_invite),Toast.LENGTH_SHORT).show();
                    }
                });
            }

            final String timeEvent = startTime + " - " + endTime;
            ((MyViewHolder)viewHolder).timeEvent.setText(timeEvent);
            ((MyViewHolder)viewHolder).location.setText(item.getAddress());
            // Check for empty status message
            if (!TextUtils.isEmpty(item.getStatus())) {
                ((MyViewHolder)viewHolder).status.setText(item.getStatus());
                ((MyViewHolder)viewHolder).status.setVisibility(View.VISIBLE);
            } else {
                // status is empty, remove from view
                ((MyViewHolder)viewHolder).status.setVisibility(View.GONE);
            }

            if (item.getMode().equalsIgnoreCase("true")) {
                ((MyViewHolder)viewHolder).mode_home.setImageResource(R.drawable.ic_public);
            } else {
                ((MyViewHolder)viewHolder).mode_home.setImageResource(R.drawable.ic_private);
            }

            if (!item.getImge().isEmpty()) {
                (((MyViewHolder) viewHolder).feedImageView).setVisibility(View.VISIBLE);
                (((MyViewHolder) viewHolder).progressBar_image).setVisibility(View.VISIBLE);
                Glide.with(activity)
                        .load(item.getImge())
                        .animate(android.R.anim.fade_in)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.background_hoa_tigo)
                        .centerCrop()
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                ((MyViewHolder) viewHolder).progressBar_image.setVisibility(View.GONE);

                                return false;
                            }
                        })
                        .into(((MyViewHolder) viewHolder).feedImageView);
            } else {
                (((MyViewHolder) viewHolder).feedImageView).setVisibility(View.GONE);
                (((MyViewHolder) viewHolder).progressBar_image).setVisibility(View.GONE);

            }

            ((MyViewHolder)viewHolder).feedImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(activity, ViewPagerImageActivity.class);
                    i.putExtra(PUT_IMAGE, item.getImge());
                    activity.startActivity(i);
                }
            });


            Glide.with(activity).load(item.getProfilePic())
                    .placeholder(R.drawable.avatar)
                    .error(R.drawable.avatar)
                    .dontAnimate()
                    .into( ((MyViewHolder)viewHolder).profilePic);

            ((MyViewHolder) viewHolder).profilePic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(activity, DetailFriendActivity.class);
                    i.putExtra(DetailFriendActivity.ID_USER, feedItems.get(position).getId_user()+"");
                    i.putExtra(DetailFriendActivity.NAME, feedItems.get(position).getName());
                    i.putExtra(DetailFriendActivity.PHONE, feedItems.get(position).getPhone());
                    i.putExtra(DetailFriendActivity.PROFILE_PIC, feedItems.get(position).getProfilePic());
                    activity.startActivity(i);
                }
            });

            ((MyViewHolder)viewHolder).lnShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String a = activity.getResources().getString(R.string.person_share);
                    String b = activity.getResources().getString(R.string.name_share);
                    String c = activity.getResources().getString(R.string.date_share);
                    String d = activity.getResources().getString(R.string.time_share);
                    String e = activity.getResources().getString(R.string.where_share);
                    String f = activity.getResources().getString(R.string.person_participation);

                    String message = "\n" + a + feedItems.get(position).getName()
                            + "\n" + b + feedItems.get(position).getStatus()
                            + "\n" + c + strStDate
                            + "\n" + d + startTime
                            + "\n" + e + feedItems.get(position).getAddress()
                            + "\n" + f + feedItems.get(position).getCount_participation()
                            + "\n " + feedItems.get(position).getImge();
                    shareAction(message);
                }
            });
        }else {
            if(feedItems.size() >=10) {
                ((RecyclerAdapterHome.ProgressViewHolder) viewHolder).progressBar.setVisibility(View.VISIBLE);
                ((RecyclerAdapterHome.ProgressViewHolder) viewHolder).progressBar.setIndeterminate(true);
            }
            else{
                ((RecyclerAdapterHome.ProgressViewHolder) viewHolder).progressBar.setIndeterminate(false);
                ((RecyclerAdapterHome.ProgressViewHolder) viewHolder).progressBar.setVisibility(View.GONE);
            }
        }
    }
    public void setLoaded(){
        loading = false;
    }

    public void setOnLoadMoreListener(RecyclerAdapterHome.OnLoadMoreListener listener){
        this.onLoadMoreListener = listener;
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView name ;
        CircleImageView profilePic;
        TextView timeStamp;
        ImageView feedImageView;
        TextView month;
        TextView day;
        TextView status;
        TextView timeEvent;
        TextView location;
        TextView dayEvent;
        TextView number_participation;
        TextView numberView;
        TextView numberComment;
        LinearLayout lnComment;
        LinearLayout lnJoin;
        public ImageView btJoin;
        public TextView txtJoin;
        public AnimationDrawable animation;
        LinearLayout lnShare;
        LinearLayout lnDetailHome;
        ProgressBar progressBar_image;
        private ImageView mode_home;


        public MyViewHolder(View rowView) {
            super(rowView);
            name = (TextView) rowView.findViewById(R.id.name);
            profilePic = (CircleImageView) rowView.findViewById(R.id.profilePic);
            timeStamp = (TextView) rowView.findViewById(R.id.timestamp);
            feedImageView = (ImageView) rowView.findViewById(R.id.even_image_home);
            month = (TextView) rowView.findViewById(R.id.thang_sk);
            day = (TextView) rowView.findViewById(R.id.ngay_sk);
            status = (TextView) rowView.findViewById(R.id.txtStatusMsg);
            timeEvent = (TextView) rowView.findViewById(R.id.time_event_home);
            location = (TextView) rowView.findViewById(R.id.local_Event);
            dayEvent = (TextView) rowView.findViewById(R.id.day_event_home);;
            number_participation = (TextView) rowView.findViewById(R.id.number_participation);
            numberView = (TextView) rowView.findViewById(R.id.numbertview);
            numberComment = (TextView) rowView.findViewById(R.id.number_Comment);
            lnComment = (LinearLayout) rowView.findViewById(R.id.btn_Comment);
            lnJoin = (LinearLayout) rowView.findViewById(R.id.btn_inter);
            btJoin = (ImageView) rowView.findViewById(R.id.paticipa);
            txtJoin = (TextView) rowView.findViewById(R.id.txtinter);
            lnShare = (LinearLayout) rowView.findViewById(R.id.btn_share);
            lnDetailHome = (LinearLayout) rowView.findViewById(R.id.details_home);
            progressBar_image = (ProgressBar) rowView.findViewById(R.id.progress_bar_home);
            mode_home = (ImageView) rowView.findViewById(R.id.image_mode_home);

        }
    }


    public  class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progress_more) ;

        }
    }

    public void shareAction(String message) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_TEXT, message);
        activity.startActivity(Intent.createChooser(share, activity.getResources().getString(R.string.title_share)));
    }

    private void count_view(final String id_sc, final String numberview) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_COUNT_VIEW, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

//			download_count_tg(id_sc);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_sc", id_sc);
                params.put("number_view", numberview);
                return params;
            }
        };
        // Adding request to request queue

        AppController.getInstance().addToRequestQueue(strReq);
    }
}