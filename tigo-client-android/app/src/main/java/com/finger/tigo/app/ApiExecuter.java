package com.finger.tigo.app;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.finger.tigo.R;
import com.finger.tigo.util.MyLog;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Finger-kjh on 2017-06-02.
 */

public class ApiExecuter {
    private static final int DEFAULT_TIMEOUT = 30000;

    private static String mNetworkErrMsg;
    private static String mServerErrMsg;

    public static void initialize(Context context) {
        mNetworkErrMsg = context.getString(R.string.error_network_msg);
        mServerErrMsg = context.getString(R.string.error_server_msg);
    }

    private static Map<String, String> buildParams(String[] keyValueParams) {
        if(keyValueParams != null && keyValueParams.length % 2 != 0) {
            throw new IllegalArgumentException("keyValueParams must 2 multiple!! length : " + keyValueParams.length);
        }

        Map<String, String> params = new HashMap<>();

        if(keyValueParams != null) {
            for (int i = 0; i < keyValueParams.length; i+=2) {
                params.put(keyValueParams[i], keyValueParams[i+1]);
            }
        }

        return params;
    }

    public static void requestApiProgressMsg(String apiUrl, String progressMsg, ApiResultListener apiResultListener, String... keyValueParams) {
        Map<String, String> params = buildParams(keyValueParams);
        requestApi(null, apiUrl, false, progressMsg, DEFAULT_TIMEOUT, params, apiResultListener);
    }

    public static void requestApiNoProgress(String apiUrl, ApiResultListener apiResultListener, String... keyValueParams) {
        Map<String, String> params = buildParams(keyValueParams);
        requestApi(null, apiUrl, false, null, DEFAULT_TIMEOUT, params, apiResultListener);
    }

    public static void requestApi(ApiProgressListener apiProgressListener, String apiUrl, String progressMsg, ApiResultListener apiResultListener, String... keyValueParams) {
        Map<String, String> params = buildParams(keyValueParams);
        requestApi(apiProgressListener, apiUrl, true, progressMsg, DEFAULT_TIMEOUT, params, apiResultListener);
    }

    public static void requestApi(ApiProgressListener apiProgressListener, String apiUrl, boolean showProgress, String progressMsg, ApiResultListener apiResultListener, String... keyValueParams) {
        Map<String, String> params = buildParams(keyValueParams);
        requestApi(apiProgressListener, apiUrl, showProgress, progressMsg, DEFAULT_TIMEOUT, params, apiResultListener);
    }

    public static void requestApi(ApiProgressListener apiProgressListener, String apiUrl, Map<String, String> params, ApiResultListener apiResultListener) {
        requestApi(apiProgressListener, apiUrl, true, null, DEFAULT_TIMEOUT, params, apiResultListener);
    }

    public static void requestApi(ApiProgressListener apiProgressListener, String apiUrl, String progressMsg, Map<String, String> params, ApiResultListener apiResultListener) {
        requestApi(apiProgressListener, apiUrl, true, progressMsg, DEFAULT_TIMEOUT, params, apiResultListener);
    }

    public static void requestApi(final ApiProgressListener apiProgressListener, final String apiUrl, final boolean showProgress,
                                  String progressMsg, int timeout, final Map<String, String> params, final ApiResultListener apiResultListener) {
        if(showProgress) {
            apiProgressListener.onStartApi(progressMsg);
        }

        StringRequest strReq = new StringRequest(Request.Method.POST,

                apiUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                MyLog.d("apiUrl : " + apiUrl + ", response=" + response);

                if(showProgress) {
                    apiProgressListener.onFinishApi();
                }

                if(apiResultListener != null) {
                    apiResultListener.onSuccessApi(apiUrl, params, response);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                if(apiProgressListener != null) {
                    apiProgressListener.onFinishApi();
                }

                if(apiResultListener != null) {

                    if(error instanceof NetworkError) {
                        apiResultListener.onFail(apiUrl, params, mNetworkErrMsg, error.getCause());
                    } else if(error instanceof ServerError) {
                        apiResultListener.onFail(apiUrl, params, mServerErrMsg, error.getCause());
                    } else {
                        apiResultListener.onFail(apiUrl, params, error.getMessage(), error.getCause());
                    }
                }

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }
        };
        // Adding request to request queue
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                timeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq);
    }

    public interface ApiResultListener {
        void onSuccessApi(String apiUrl, Map<String, String> params, String response);
        void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause);
    }

    public interface ApiProgressListener {
        void onStartApi(String progressMsg);
        void onFinishApi();
    }
}
