package com.finger.tigo.menu;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.angel.black.baframeworkimagepick.ImagePickExecuter;
import com.angel.black.baframeworkimagepick.ImagePickIntentConstants;
import com.angel.black.baframeworkimagepick.ImagePickOnActivityResult;
import com.angel.black.baframeworkimagepick.imagepick.util.BitmapUtil;
import com.bumptech.glide.Glide;
import com.finger.tigo.BaseActivity;
import com.finger.tigo.R;
import com.finger.tigo.app.ApiExecuter;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.session.SessionManager;
import com.finger.tigo.util.FileUtil;
import com.finger.tigo.util.MyLog;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageActivity;
import com.theartofdev.edmodo.cropper.CropImageOptions;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Map;

/**
 * Created by Finger-kjh on 2017-05-29.
 */

public class ProfileAvatarActivity extends BaseActivity {

    private SessionManager session;

    private Toolbar mToolbar;
    private ImageView mProfileImgView;

    private String idUser;
    private boolean mChangedUpdateImage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_avatar);

        session = new SessionManager(this);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        mToolbar.setTitleTextColor(Color.WHITE);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(session.getLName_session());

        mProfileImgView = (ImageView) findViewById(R.id.profile_img);

        idUser = getIntent().getStringExtra("idUser");

        String profilePicUrl = getIntent().getStringExtra("profilePicUrl");

        displayProfileImage(profilePicUrl);
    }

    private void displayProfileImage(String profilePicUrl) {
        MyLog.d("profilePicUrl=" + profilePicUrl);
        Glide.with(this)
                .load(profilePicUrl)
                .placeholder(R.drawable.background_hoa_tigo)
                .dontAnimate()      // if you dont write this, CircleImageView is not displayed.
                .into(mProfileImgView);
    }


    private void displayProfileImage(Uri fileUri) {
        String realPath = FileUtil.getRealFilePathFromUri(this, fileUri);

        final File file = new File(realPath);
        MyLog.d("file.exist()=" + file.exists() + ", path=" + file.getAbsolutePath());

        if(file.exists()) {
            Glide.with(this)
                    .load(file)
                    .placeholder(R.drawable.background_hoa_tigo)
                    .dontAnimate()      // if you dont write this, CircleImageView is not displayed.
                    .into(mProfileImgView);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_profile_avatar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else if(item.getItemId() == R.id.menu_edit) {
            ImagePickExecuter.executeImagePickWithCameraToOneImage(ProfileAvatarActivity.this, false, CropImageView.CropShape.OVAL);
            return true;
        } else if(item.getItemId() == R.id.menu_complete) {
            updateimage(idUser, Uri.parse(mProfileImgView.getContentDescription().toString()));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        MyLog.d("requestCode=" + requestCode + ", resultCode=" + resultCode);

        if(requestCode == ImagePickIntentConstants.REQUEST_IMAGE_PICK) {
            if(resultCode == RESULT_OK) {
                String imagePath = ImagePickOnActivityResult.onActivityResultAndGetOneImagePath(data);

                //test file size check
                MyLog.d("imagePath=" + imagePath + FileUtil.getFileSize(new File(imagePath)));

                Intent intent = new Intent(ProfileAvatarActivity.this, CropImageActivity.class);
                CropImageOptions cropImageOptions = new CropImageOptions();
                cropImageOptions.cropShape = CropImageView.CropShape.OVAL;
                cropImageOptions.outputRequestSizeOptions = CropImageView.RequestSizeOptions.SAMPLING;
                cropImageOptions.outputRequestWidth = 1080;
                cropImageOptions.outputRequestHeight = 1080;
                cropImageOptions.activityTitle = getResources().getString(R.string.change_avatar);
                cropImageOptions.aspectRatioX = cropImageOptions.aspectRatioY = 1;
                cropImageOptions.fixAspectRatio = true;
                cropImageOptions.outputCompressQuality = 90;
                cropImageOptions.outputCompressFormat = Bitmap.CompressFormat.JPEG;
                intent.putExtra(CropImage.CROP_IMAGE_EXTRA_OPTIONS, cropImageOptions);
                intent.putExtra(CropImage.CROP_IMAGE_EXTRA_SOURCE, Uri.fromFile(new File(imagePath)));

                startActivityForResult(intent, ImagePickIntentConstants.REQUEST_EDIT_IMAGE);

            }
        } else if(requestCode == ImagePickIntentConstants.REQUEST_EDIT_IMAGE) {
            if (resultCode == RESULT_OK) {
                CropImageView.CropResult result = data.getParcelableExtra(CropImage.CROP_IMAGE_EXTRA_RESULT);
                MyLog.d("crop result.getUri()=" + result.getUri() + ", size=" + FileUtil.getFileSize(new File(FileUtil.getRealFilePathFromUri(this, result.getUri()))));
                displayProfileImage(result.getUri());
                mProfileImgView.setContentDescription(result.getUri().toString());

                mChangedUpdateImage = true;

                // set visible complete menu
                mToolbar.getMenu().getItem(1).setVisible(true);
            }
        }
    }

    private void updateimage(final String id_user, final Uri updateImageUri) {
        ApiExecuter.requestApi(this, AppConfig.URL_UPDATE_IMAGE, getString(R.string.change_avatar), new ApiExecuter.ApiResultListener() {
            @Override
            public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                MyLog.i("update image success! response=" + response);

                try {
                    JSONObject jObj = new JSONObject(response);
                    JSONObject user = jObj.getJSONObject("user");

                    String id_user = user.getString("id_user");
                    String pic =  user.getString("profilePic");

                    session.setpic_session(pic);

                    setResult(RESULT_OK);
                    finish();

                } catch (Exception ex) {
                    // JSON parsing error
                    ex.printStackTrace();
                    onFail(apiUrl, params, ex.getMessage(), ex);
                }
            }

            @Override
            public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {
                showToast(R.string.change_avatar_fail_msg);
            }
        }, "id_user", id_user, "profilePic", encodeImageToBase64(updateImageUri));
    }

    public String encodeImageToBase64(Uri fileUri) {
        Bitmap bmp = BitmapFactory.decodeFile(FileUtil.getRealFilePathFromUri(this, fileUri));
        MyLog.d("bmp size=" + FileUtil.getDataSizeString(bmp.getByteCount()));

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, BitmapUtil.getOutputQualityByResoulution(bmp), baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);

        MyLog.d("encodedBase64 str size=" + encodedImage.length());
        return encodedImage;
    }
}
