package com.finger.tigo.schedule.async;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.schedule.model.ItemSchedule;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Duong on 4/17/2017.
 */

public class AsynchScheduleInteractor implements  IAsynchScheduleInteractor {


    public static final int KEY_ERROR_ONE = 1;
    List<ItemSchedule> listSchedule;
    Calendar calendar;
    long dayConstan = 86400000;

    @Override
    public void MonthInteractorAsync(ConfirmDataListener listener, String idUser) {
        listSchedule = new ArrayList<>();
        calendar = Calendar.getInstance();
        setData(listener, idUser);

    }

    private void setData(final ConfirmDataListener listener, final String iduser) {
        AppController.getInstance().getRequestQueue().getCache().clear();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_AGENDA, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                if(response !=null){
                    parseJsonFeed(listener, response);
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                listener.onError(MonthFragment.MONTH_SCHEDULE, KEY_ERROR_ONE);

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("iduser", iduser);
                return params;

            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);

    }

    public void parseJsonFeed(ConfirmDataListener listener, String response){
        
        try {

            JSONObject jObj = new JSONObject(response);
            JSONArray feedArray = jObj.getJSONArray("feed");

            for (int l = 0; l < feedArray.length(); l++) {
             
                JSONObject feedObj = (JSONObject) feedArray.get(l);

                long timeMiliStar = Long.parseLong(feedObj.getString("daystart"));
                long timeMiliEnd = Long.parseLong(feedObj.getString("dayfinish"));
                calendar.setTimeInMillis(timeMiliStar);
                int yearStar = calendar.get(Calendar.YEAR);
                int monthStar = calendar.get(Calendar.MONTH);
                int dayStar = calendar.get( calendar.DAY_OF_MONTH);
                SimpleDateFormat simpleStar = new SimpleDateFormat("hh:mm a", Locale.getDefault());
                String strTimeStar = simpleStar.format(calendar.getTime());
                
                calendar.setTimeInMillis(timeMiliEnd);
                
                SimpleDateFormat simpleEnd = new SimpleDateFormat("hh:mm a", Locale.getDefault());
                String strTimeEnd = simpleEnd.format(calendar.getTime());
                
                listSchedule.add(new ItemSchedule( yearStar,  monthStar,dayStar, feedObj.getString("status"),
                        feedObj.getInt("id_sc")+"",   feedObj.getString("color"),feedObj.getString("location"),strTimeStar + " - "+strTimeEnd ));
                
            }
//            listener.onSuccess(MonthFragment.MONTH_SCHEDULE, listSchedule);


        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("tttt", "parseJsonFeed: "+e.getMessage());
        }
    }

}
