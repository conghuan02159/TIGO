package com.finger.tigo.schedule.creatEvent;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by duong on 04/08/2016.
 */
public class Person implements Parcelable {


    public String name;
    public String id;
    public String photoId;
    private boolean selected;
    private String email;
    private  String state;
    private int check;
    private String count;
    private String participation;
    public String description_event;
    public String day_event;
    public String dayend;
    public String avatar;
    public int position;


    public Person(){}
    public Person(String name, String age , boolean selected) {
        this.name = name;
        this.id = age;
        this.selected = selected;

    }
    public Person(String name, String age, String profilePic , boolean selected) {
        this.name = name;
        this.id = age;
        this.photoId = profilePic;
        this.selected = selected;

    }
    public Person(String name, String id, boolean selected, int position)
    {
        this.name=name;
        this.id=id;
        this.selected=selected;
        this.position=position;

    }

    public Person(String name, String age, String profilePic , boolean selected, String state) {
        this.name = name;
        this.id = age;
        this.photoId = profilePic;
        this.selected = selected;
        this.state = state;
    }
    public Person(String id, String name, String email, String profilePic , boolean selected, String state) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.photoId = profilePic;
        this.selected = selected;
        this.state = state;
    }

    public Person(String id, String name, String profilePic, String count, String participation) {
        this.id = id;
        this.name = name;
        this.photoId = profilePic;
        this.count = count;
        this.participation = participation;
    }
    public Person(String id, String name, String email, String profilePic , boolean selected, String state, int positon) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.photoId = profilePic;
        this.selected = selected;
        this.state = state;
        this.position=positon;
    }

    public Person(String id, String name, String profilePic, String avatar, String day_event, String dayend,  String description_event, String count, String participation) {
        this.id = id;
        this.name = name;
        this.photoId = profilePic;
        this.avatar = avatar;
        this.day_event = day_event;
        this.dayend = dayend;
        this.description_event = description_event;
        this.count = count;
        this.participation = participation;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCheck() {
        return check;
    }

    public void setCheck(int check) {
        this.check = check;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAge() {
        return id;
    }

    public void setAge(String age) {
        this.id = age;
    }

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParticipation() {
        return participation;
    }

    public void setParticipation(String participation) {
        this.participation = participation;
    }

    public String getDescription_event(){ return description_event;}

    public void setDescription_event(String description_event) {this.description_event = description_event;}

    public String getDay_event(){ return  day_event;}

    public void setDay_event(String day_event) {this.day_event = day_event;}
    public String getDateend() {
        return dayend;
    }
    public void setDayend(String dayend) {
        this.dayend = dayend;
    }


    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(this.name);
        dest.writeString(this.id);
        dest.writeString(this.photoId);

        dest.writeByte(selected ? (byte) 1 : (byte) 0);
    }

    private Person(Parcel in){
        this.name = in.readString();
        this.id = in.readString();
        this.photoId = in.readString();

        this.selected = in.readByte() !=0;
    }

    public static final Parcelable.Creator<Person> CREATOR = new Parcelable.Creator<Person>(){

        @Override
        public Person createFromParcel(Parcel source) {
            return new Person(source);
        }

        @Override
        public Person[] newArray(int size) {
            return new Person[size];
        }
    };
}