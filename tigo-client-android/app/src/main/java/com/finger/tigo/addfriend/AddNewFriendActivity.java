package com.finger.tigo.addfriend;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.finger.tigo.R;
import com.finger.tigo.addfriend.adapter.PagperAdapterTigon;


public class AddNewFriendActivity extends AppCompatActivity {

    Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_friend);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (ViewPager)findViewById(R.id.pager);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.tabAdd));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setTitleTextColor(Color.WHITE);
        tabLayout.addTab(tabLayout.newTab().setText(R.string.add_friend).setIcon(R.drawable.tab_addfr_3));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.number_phone).setIcon(R.drawable.tab_addfr_1));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.code_qr).setIcon(R.drawable.tab_addfr_2));

        tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);
        viewPager.setOffscreenPageLimit(3);
        final PagerAdapter adapter = new PagperAdapterTigon(getFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {//Write your logic here
            this.finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

}