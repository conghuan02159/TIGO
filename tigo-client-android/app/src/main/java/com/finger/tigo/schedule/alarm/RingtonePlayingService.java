package com.finger.tigo.schedule.alarm;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;

import com.finger.tigo.R;
import com.finger.tigo.detail.EventDetailActivity;
import com.finger.tigo.session.SessionManager;
import com.finger.tigo.syncadapter.SyncAdapter;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import static com.finger.tigo.detail.EventDetailActivity.M_DETAILS_HOME_ACTIVITY;

/**
 * Created by duong on 31/08/2016.
 */
public class RingtonePlayingService extends Service {


    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {


        final NotificationManager mNM = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);

        String rechard_id = intent.getStringExtra(SyncAdapter.IDSC_ALARM);
        int index = intent.getIntExtra("indexAlarm", 0);
        long timeMillsStart = intent.getLongExtra("startday", 0);
        String urlImage = intent.getStringExtra("image");
        String title = intent.getStringExtra("title");

        new generatePictureStyleNotification(this,rechard_id, index, timeMillsStart, urlImage, title).execute();


        return START_NOT_STICKY;
    }

    public class generatePictureStyleNotification extends AsyncTask<String, Void, Bitmap> {

        private Context mContext;
        private String rechard_id;
        private int index;
        private long startMilllis;
        private String imageUrl;
        private String title;

        private MediaPlayer mMediaPlayer;


        public generatePictureStyleNotification(Context context, String rechard_id, int index, long startMilllis,
                                                String imageUrl, String title) {
            super();
            this.mContext = context;
            this.rechard_id = rechard_id;
            this.index = index;
            this.startMilllis = startMilllis;
            this.imageUrl = imageUrl;
            this.title = title;
        }

        @Override
        protected Bitmap doInBackground(String... params) {

            InputStream in;
            try {
                URL url = new URL(imageUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                in = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(in);
                return myBitmap;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        private String getDate(long timeMillsi) {
            DateFormat dfDate = new SimpleDateFormat("yyyy/MM/dd");
            String date = dfDate.format(timeMillsi);
            DateFormat dfTime = new SimpleDateFormat("HH:mm");
            String time = dfTime.format(timeMillsi);
            return date + " " + time;
        }

        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);

            Intent intent = new Intent(mContext, EventDetailActivity.class);
            intent.putExtra(M_DETAILS_HOME_ACTIVITY, rechard_id);

            String message =mContext.getResources().getString(R.string.event_alarm) +" " + title + " "+
                    mContext.getResources().getString(R.string.event_take_place)+" " + getDate(startMilllis);

            PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 100, intent, PendingIntent.FLAG_ONE_SHOT);

            NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
            Notification notif = new Notification.Builder(mContext)
                    .setContentIntent(pendingIntent)
                    .setContentTitle("Tigo")
                    .setContentText(message)
                    .setSmallIcon(R.drawable.ic_notification_tigo)
                    .setLargeIcon(getCircleBitmap(result))
                    .build();
            notif.flags |= Notification.FLAG_AUTO_CANCEL;

            SessionManager session = new SessionManager(mContext);
            if(!session.getIsAlarm()) {
                String actualSound = session.getKeySoundEvent();
                Resources res = getResources();
                int soundId = res.getIdentifier(actualSound, "raw", getPackageName());

                mMediaPlayer = MediaPlayer.create(mContext, soundId);
                mMediaPlayer.start();
            }

            notificationManager.notify(0, notif);


        }

        private Bitmap getCircleBitmap(Bitmap bitmap) {
            final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                    bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            final Canvas canvas = new Canvas(output);

            final int color = Color.RED;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            final RectF rectF = new RectF(rect);

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawOval(rectF, paint);

            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);

            bitmap.recycle();

            return output;
        }


    }




}