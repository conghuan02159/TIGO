package com.finger.tigo.items;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by duong on 27/07/2016.
 */
public class ItemAddFr  implements Parcelable, Serializable {


    public String name;
    public String id;
    public String photoId;
    private boolean selected;
    public String email;
    private String age;
    private String state;
    private  String IdFriend;
    public String phone;

    public String getIdFriend() {
        return IdFriend;
    }

    public void setIdFriend(String idFriend) {
        IdFriend = idFriend;
    }

    public String getProfilePic() {
        return ProfilePic;
    }
    public String getPhone()
    {
        return phone;
    }
    public void setPhone(String phone)
    {
       this.phone = phone;
    }

    public void setProfilePic(String profilePic) {
        ProfilePic = profilePic;
    }


    private String ProfilePic;
    public ItemAddFr(String name, String id, String email, String profilePic , boolean selected) {
        this.name = name;
        this.id = id;
        this.photoId = profilePic;
        this.selected = selected;
        this.email = email;

    }


    public ItemAddFr(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(this.name);
        dest.writeString(this.id);
        dest.writeString(this.photoId);
        dest.writeString(this.email);
        dest.writeByte(selected ? (byte) 1 : (byte) 0);
    }

    private ItemAddFr (Parcel in){
        this.name = in.readString();
        this.id = in.readString();
        this.photoId = in.readString();
        this.email = in.readString();
        this.selected = in.readByte() !=0;
    }

    public static final Parcelable.Creator<ItemAddFr> CREATOR = new Parcelable.Creator<ItemAddFr>(){

        @Override
        public ItemAddFr createFromParcel(Parcel source) {
            return new ItemAddFr(source);
        }

        @Override
        public ItemAddFr[] newArray(int size) {
            return new ItemAddFr[size];
        }
    };
}
