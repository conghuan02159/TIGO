package com.finger.tigo.addfriend.adapter;


import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentStatePagerAdapter;

import com.finger.tigo.addfriend.fragment.TigonPhoneFragment;
import com.finger.tigo.addfriend.fragment.TigonQRcodeFragment;
import com.finger.tigo.friend.fragment.TigonSyncFragment;

/**
 * Created by Duong on 3/30/2017.
 */

public class PagperAdapterTigon extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagperAdapterTigon(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                TigonSyncFragment tab0 = new TigonSyncFragment();
                return tab0;
            case 1:
                TigonPhoneFragment tab1 = new TigonPhoneFragment();
                return tab1;
            case 2:
                TigonQRcodeFragment tab2 = new TigonQRcodeFragment();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}