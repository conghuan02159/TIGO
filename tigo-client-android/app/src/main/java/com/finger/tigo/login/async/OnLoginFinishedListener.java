package com.finger.tigo.login.async;


import com.finger.tigo.login.model.itemLogin;

import java.util.List;

/**
 * Created by Duong on 2/27/2017.
 */

public interface OnLoginFinishedListener {

    void onError(String error, int keyError);
    void onNetWorkSuccess(String success,List<itemLogin> list);
}
