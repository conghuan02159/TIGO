package com.finger.tigo.invite.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.finger.tigo.R;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.items.ItemAddFr;
import com.finger.tigo.session.SessionManager;
import com.finger.tigo.util.MyLog;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by DELL on 6/9/2017.
 */

public class MyRecyclerAdapterNewList extends RecyclerView.Adapter<MyRecyclerAdapterNewList.PersonViewHolder> {

    OnItemClickListener mItemClickListener;
    Activity context;
    List<ItemAddFr> people;
    String userId;

    public OnItemClickListener getmItemClickListener() {
        return mItemClickListener;
    }
    public void setmItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }
    public MyRecyclerAdapterNewList(List<ItemAddFr> persons, String userId, Activity context){
        this.userId = userId;
        this.people = persons;
        this.context=context;
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.customlist_addfriend, parent, false);
        return new PersonViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final PersonViewHolder holder, final int position) {
        holder.personName.setText(people.get(position).name);
        String strPhone = people.get(position).getPhone();
        String strCutPhone = cutPhone(strPhone);
        holder.phone.setText(strCutPhone+"xxx");
        SessionManager sessionManager=new SessionManager(context);
        userId=sessionManager.get_id_user_session()+"";

        Glide.with(context).load(people.get(position).getProfilePic()).into(holder.personPhoto);

//        if(people.get(position).getState().equalsIgnoreCase("1") || people.get(position).getIdFriend().equalsIgnoreCase(userId)){
//            holder.bt_addFriend.setVisibility(View.GONE);
//            holder.tvSent.setVisibility(View.GONE);
//        }else {
//            holder.bt_addFriend.setVisibility(View.VISIBLE);
//            holder.tvSent.setVisibility(View.GONE);
//            holder.phone.setText(context.getResources().getString(R.string.newtigo));
//            holder.phone.setTextColor(context.getResources().getColor(R.color.colorPrimary));
//        }
//        if(people.get(position).getState().equalsIgnoreCase("1"))
//        {
//            holder.personName.setVisibility(View.GONE);
//            holder.personPhoto.setVisibility(View.GONE);
//            holder.phone.setVisibility(View.GONE);
//        }

        if (people.get(position).getState().equalsIgnoreCase("0"))
        {
            holder.personName.setVisibility(View.VISIBLE);
            holder.personPhoto.setVisibility(View.VISIBLE);
            holder.phone.setText(context.getResources().getString(R.string.newtigo));
            holder.phone.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.bt_addFriend.setVisibility(View.VISIBLE);

            if(people.get(position).getIdFriend().equalsIgnoreCase(userId))
            {
                holder.bt_addFriend.setVisibility(View.GONE);
                holder.personName.setVisibility(View.GONE);
                holder.personPhoto.setVisibility(View.GONE);
                holder.phone.setVisibility(View.GONE);
            }
        }

        else if (people.get(position).getState().equalsIgnoreCase("1")|| people.get(position).getIdFriend().equalsIgnoreCase(userId))
        {
            holder.personName.setVisibility(View.GONE);
            holder.personPhoto.setVisibility(View.GONE);
            holder.phone.setVisibility(View.GONE
            );

        }
        if(people.get(position).getState().equalsIgnoreCase("2")){
            holder.tvSent.setText(R.string.sent_your_invitation);
            holder.tvSent.setVisibility(View.VISIBLE);
            holder.bt_addFriend.setVisibility(View.GONE);
        }
        MyLog.e("people",people.get(position).getIdFriend());

        holder.bt_addFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddFriend(people.get(position).getIdFriend(), people.get(position).getName(), people.get(position).getPhone());
                holder.bt_addFriend.setVisibility(View.GONE);
                holder.tvSent.setVisibility(View.VISIBLE);

            }
        });

    }
    public String cutPhone(String str) {
        if (str != null && str.length() > 0) {
            str = str.substring(0, str.length()-3);
        }
        return str;
    }
    public interface OnItemClickListener{
        void onItemClick(View v, int position);
    }

    @Override
    public int getItemCount() {
        return people.size();
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
    public class PersonViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        TextView personName;
        TextView phone;
        CircleImageView personPhoto;
        public TextView bt_addFriend;
        private TextView tvSent;


        public PersonViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            personName = (TextView) itemView.findViewById(R.id.person_name);
            phone = (TextView) itemView.findViewById(R.id.person_phone);
            personPhoto = (CircleImageView) itemView.findViewById(R.id.profilePic);
            bt_addFriend = (TextView) itemView.findViewById(R.id.addfriend);
            tvSent = (TextView) itemView.findViewById(R.id.sent_your_invitation);
        }

        @Override
        public void onClick(View view) {
            if(mItemClickListener != null){
                mItemClickListener.onItemClick(view, getPosition());
            }
        }
    }
    private void AddFriend( final String idfriend,final String name, final String phone) {
        // Tag used to cancel the request

        /*pDialog.setMessage("Sent...");
        showDialog();*/
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADDNEWFRIEND, new Response.Listener<String>(){

            @Override
            public void onResponse(String response) {
                //hideDialog();
                //context.finish();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                //hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<>();
                // userId = session.get_id_user_session()+"";
                params.put("iduser", userId);
                params.put("idfriend", idfriend);
                params.put("name", name);
                params.put("phone", phone);
                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(strReq);
    }

}
