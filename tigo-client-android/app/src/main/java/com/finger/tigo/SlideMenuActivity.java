package com.finger.tigo;


import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ImageView;
import android.widget.Toast;

import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.app.ApiExecuter;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppIntent;
import com.finger.tigo.friend.ListFriendActivity;
import com.finger.tigo.home.HomeFragment;
import com.finger.tigo.menu.fragment.MenuFragment;
import com.finger.tigo.notification.NotificationFragment;
import com.finger.tigo.notification.badgettextview.MenuItemBadge;
import com.finger.tigo.schedule.AllInOneFragment;
import com.finger.tigo.schedule.creatEvent.CreateEventActivity;
import com.finger.tigo.search.SearchEventActivity;
import com.finger.tigo.session.SessionManager;
import com.finger.tigo.util.MyLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import me.leolin.shortcutbadger.ShortcutBadger;


/**
 * Created by Bao Dai Duong on 5/17/2016.
 */
public class SlideMenuActivity extends BaseActivity implements NotificationFragment.NotificationBadgeListener {


    public static TabLayout tabLayout;
    private static BadgeView mBadgeView;
    private ViewPager viewPager;
    public  MenuItem menuItemNotification;
    public Toolbar toolbar;
//    public static List<Integer> mBadgeCountList = new ArrayList<>();
    public static SessionManager session;
    public static Context mContext;
    public static  Handler handler;
    String idUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slide_menu);
        mContext = this;

        handler = new Handler();
        AccountManager am =  (AccountManager)this.getSystemService(Context.ACCOUNT_SERVICE);
        Account[] accountsFromFirstApp = am.getAccountsByType(AppAccountManager.ACCOUNT_TYPE);
        Bundle settingsBundle = new Bundle();
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        ContentResolver.requestSync(accountsFromFirstApp[0],"com.android.calendar",new Bundle());
        idUser = AppAccountManager.getAppAccountUserId(this);
         toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);

        if(BuildConfig.FLAVOR.equals("AllOneBank")) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }

        // countNotification = countNotification + 1;
         session = new SessionManager(this);

        initViewPagerAndTabs();

    }



    private void initViewPagerAndTabs() {

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(4);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout_main);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        tabLayout.setOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {

                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        super.onTabSelected(tab);
                        int position = tab.getPosition();

                        if (position == 0) {

                            if ((getSupportActionBar() != null)) {
                                getSupportActionBar().setTitle(R.string.home);
                               toolbar.setSubtitle("");
                            }

                        } else if (position == 1) {

                            if ((getSupportActionBar() != null)) {
                                getSupportActionBar().setTitle(R.string.schedule);
                                toolbar.setSubtitle("");
                            }

                        } else if (position == 2) {
                            mBadgeView.setText(formatBadgeNumber(0));
                            ShortcutBadger.removeCount(SlideMenuActivity.this);
                            if(session.getCountNotification() != 0) {
                                session.setCountNotification(0);

                                createCountNotification(idUser, session.getCountNotification() + "", session.getCountFriendNotification() + "");
                            }
                            if ((getSupportActionBar() != null)) {
                                getSupportActionBar().setTitle(R.string.notification);
                                toolbar.setSubtitle("");
                            }


                        } else if (position == 3) {

                            if ((getSupportActionBar() != null)) {
                                getSupportActionBar().setTitle(R.string.menu);
                                toolbar.setSubtitle("");
                            }

                        }

                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {


                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                }
        );

    }

    private void setupTabIcons() {
        int[] tabIcons = {
                R.drawable.tab_main_home,
                R.drawable.tab_main_schedule,
                R.drawable.tab_main_notifi,
                R.drawable.tab_main_personal
        };

        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        tabLayout.getTabAt(3).setIcon(tabIcons[3]);

        // init Badge view

        initBadgeViews();
        setUpTabBadge();

    }
    public View getTabItemView() {
         View view = LayoutInflater.from(mContext).inflate(R.layout.tab_layout_item, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.img_icon_tab);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                imageView.setBackground(mContext.getResources().getDrawable(R.drawable.tab_main_notifi));
            }else{
                imageView.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.tab_main_notifi));

        }

        View target = view.findViewById(R.id.badgeview_target);

        mBadgeView = new BadgeView(mContext);
        mBadgeView.setTargetView(target);
        mBadgeView.setBadgeGravity(Gravity.RIGHT);
        mBadgeView.setTextSize(10);
        mBadgeView.setText(formatBadgeNumber(session.getCountNotification()));

        return view;
    }

    @Nullable
    public static String formatBadgeNumber(int value) {
        if (value <= 0) {
            return null;
        }

        if (value < 100) {
            // equivalent to String#valueOf(int);
            return Integer.toString(value);
        }

        // my own policy
        return "99+";
    }
    private void initBadgeViews() {

            BadgeView tmp = new BadgeView(this);
            tmp.setBadgeMargin(0, 6, 10, 0);
            tmp.setTextSize(10);

    }


    public void setUpTabBadge() {

        handler.post(new Runnable() {
            @Override
            public void run() {
                TabLayout.Tab tab = tabLayout.getTabAt(2);
                View customView = tab.getCustomView();
                if (customView != null) {
                    ViewParent parent = customView.getParent();

                    if (parent != null) {
                        ((ViewGroup) parent).removeView(customView);
                    }
                }
                // CustomView
                tab.setCustomView(getTabItemView());

//        tabLayout.getTabAt(tabLayout.getSelectedTabPosition()).getCustomView().setSelected(true);
                if(session.getCountNotification() !=0) {
                    tabLayout.getTabAt(2).setIcon(R.drawable.tab_main_notifi);
                }
            }
        });

    }

    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());
        adapter.addFrag(new HomeFragment(), "HOME");
        adapter.addFrag(new AllInOneFragment(), "SCHEDULE");
        adapter.addFrag(new NotificationFragment(), "NOTIFICATION");
        adapter.addFrag(new MenuFragment(), "MEUNU");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onBadgeUpdate(int value) {
        MyLog.d("ddddddd", value+"");
        if(tabLayout.getSelectedTabPosition()!=2){

            mBadgeView.setText(formatBadgeNumber(value));
            // setUpTabBadge();
            if(value ==0){
                ShortcutBadger.removeCount(SlideMenuActivity.this);
            }
        }else{

            session.setCountNotification(0);
            MyLog.d("ddddddddd_else: "+session.getCountNotification());
            mBadgeView.setText(formatBadgeNumber(0));
            ShortcutBadger.removeCount(SlideMenuActivity.this);
            createCountNotification(idUser, session.getCountNotification()+"", session.getCountFriendNotification()+"");

        }


    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);

        }

        @Override
        public CharSequence getPageTitle(int position) {

            // return null to display only the icon
            return null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_send, menu);

        menuItemNotification = menu.findItem(R.id.notifica);

        MenuItemBadge.update(this, menuItemNotification, new MenuItemBadge.Builder()
                .iconDrawable(ContextCompat.getDrawable(this, R.drawable.ic_tigo_white))
                .iconTintColor(Color.WHITE)
                .textColor(Color.WHITE)
                .textBackgroundColor(Color.parseColor("#EF4738")));
        if(session.getCountFriendNotification() != 0) {
            MenuItemBadge.getBadgeTextView(menuItemNotification).setBadgeCount(session.getCountFriendNotification());

        }else{

            MenuItemBadge.getBadgeTextView(menuItemNotification).clearHighLightMode();
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent i;
        int itemId = item.getItemId();
        if (itemId == R.id.search) {
            i = new Intent(SlideMenuActivity.this, SearchEventActivity.class);
            startActivity(i);

        } else if (itemId == R.id.add_event) {
            i = new Intent(SlideMenuActivity.this, CreateEventActivity.class);
            startActivity(i);

        } else if (itemId == R.id.notifica) {
            toggleRedIconInNewFeatureMenu(item);
            session.setCountNotification(0);
            createCountNotification(idUser, session.getCountNotification() + "", "0");
            if (session.getCountFriendNotification() > 0) {
                MenuItemBadge.getBadgeTextView(menuItemNotification).setBadgeCount(session.getCountFriendNotification() + "");
                invalidateOptionsMenu();
            } else {
                toggleRedIconInNewFeatureMenu(menuItemNotification);
            }

            i = new Intent(SlideMenuActivity.this, ListFriendActivity.class);
            startActivity(i);

        } else {
        }
        return super.onOptionsItemSelected(item);

    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        menuItemNotification = menu.findItem(R.id.notifica);
        ///toggleRedIconInNewFeatureMenu(menuItemNotification);
        return super.onPrepareOptionsMenu(menu);

    }

    private void toggleRedIconInNewFeatureMenu(MenuItem menuItemNewFeature) {

        MenuItemBadge.getBadgeTextView(menuItemNewFeature).clearHighLightMode();

    }

    // count notification friend
    private void countNotification() {
        ApiExecuter.requestApiNoProgress(AppConfig.URL_COUNT_HISTORY_NOTIFICATION, new ApiExecuter.ApiResultListener() {
            @Override
            public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                try {
                    session.setCountNotification(0);
                    JSONObject jObj = new JSONObject(response);
                    JSONObject confirm = jObj.getJSONObject("countNotification");
                    String countNotifi = confirm.isNull("countNotifi") ? null: confirm.getString("countNotifi");
                    String countFriendNotif = confirm.isNull("countFriendNotifi") ? null: confirm.getString("countFriendNotifi");
                    if(countFriendNotif !=null)
                        session.setCountFriendNotification(Integer.parseInt(countFriendNotif));



                } catch (JSONException e) {
                    e.printStackTrace();
                    onFail(apiUrl, params, e.getMessage(), e);
                }
            }

            @Override
            public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {

            }
        }, "iduser", idUser);
    }


    // set count notification
    public void createCountNotification(final String idUser, final String countNotifi, final String countNotififriend){

        ApiExecuter.requestApiNoProgress(AppConfig.URL_CREATE_COUNT_NOTIFICATION, new ApiExecuter.ApiResultListener() {
            @Override
            public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {


                        MyLog.d("create Noti: "+ session.getCountNotification());


                    }
                } catch (JSONException ex) {
                    ex.printStackTrace();
                    MyLog.d(ex.getMessage());
                }

            }

            @Override
            public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {

            }
        }, "iduser", idUser, "countNoti", countNotifi, "countFriendNoti", countNotififriend);


    }

    long prevBackPressTime;
    @Override
    public void onBackPressed() {
        if(System.currentTimeMillis() - prevBackPressTime > 3000) {
            Toast.makeText(this, R.string.app_exit_toast_msg, Toast.LENGTH_LONG).show();
            prevBackPressTime = System.currentTimeMillis();
            return;
        }

        super.onBackPressed();
    }

    BroadcastReceiver appendChatScreenMsgReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();
            if (b != null) {

                if (b.getBoolean("reloadAddFriend")) {

                    int  numberFriendNotifi =0;

                    numberFriendNotifi = session.getCountFriendNotification();
                    numberFriendNotifi = numberFriendNotifi +1;
                    session.setCountFriendNotification(numberFriendNotifi);
                    createCountNotification(idUser, session.getCountNotification()+"", numberFriendNotifi+"" );

                    MenuItemBadge.getBadgeTextView(menuItemNotification).setBadgeCount(numberFriendNotifi+"");
                    invalidateOptionsMenu();
                }else if(b.getBoolean("finishFromSetting")){

                    finish();
                }
            }
        }
    };


    @Override
    public void onResume(){
        registerReceiver(this.appendChatScreenMsgReceiver, new IntentFilter(AppIntent.ACTION_CHANGE_NOTIFICATION_BADGE_BY_ADD_FRIEND));

        AccountManager am =  (AccountManager)this.getSystemService(Context.ACCOUNT_SERVICE);
        Account[] accountsFromFirstApp = am.getAccountsByType(AppAccountManager.ACCOUNT_TYPE);
        if(accountsFromFirstApp.length ==0)
        {
            Intent i = new Intent(SlideMenuActivity.this, LoadingActivity.class);
            startActivity(i);
//            // User is already logged in. Take him to main activity
            Toast.makeText(getApplicationContext(), "tai khoan cua ban da bi xoa", Toast.LENGTH_SHORT).show();
            finish();
        }

        countNotification();

        super.onResume();

    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(appendChatScreenMsgReceiver);
        super.onDestroy();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        MyLog.d("requestCode=" + requestCode + ", resultCode=" + resultCode);

        if (requestCode == AppIntent.REQUEST_UPDATE_EVENT) {
            if (resultCode == RESULT_OK) {
                AppAccountManager.updateCalendarSync(this, data.getExtras());

            }
        }
    }
}
