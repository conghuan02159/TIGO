package com.finger.tigo.schedule.creatEvent;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.finger.tigo.app.ApiExecuter;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.items.FeedItem;
import com.finger.tigo.util.MyLog;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Conghuan on 4/11/2017.
 */

public class AsyncCreateEvent implements IAsynchCreateEvent {
    public static final int KEY_ERROR_ONE = 1;
    public static final int KEY = 2;

    //   public String encodeiamge;
    List<FeedItem> listDataEvent;
    private ByteArrayOutputStream bytearrayoutputstream;
    byte[] BYTE;
    String image = "";


    public String getReducedBitmap(Bitmap imgPath, int MaxWidth, long MaxFileSize) {
        try {


            imgPath.compress(Bitmap.CompressFormat.JPEG,30,bytearrayoutputstream);

            BYTE = bytearrayoutputstream.toByteArray();
            int temp = BYTE.length;
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            // read image file
            BitmapFactory.decodeByteArray(BYTE,0,BYTE.length,options);
            int srcWidth = options.outWidth;
            int scale = 1;
            while ((srcWidth > MaxWidth) || (temp > MaxFileSize)) {
                srcWidth /= 2;
                scale *= 2;
                temp = temp / 2;
            }
            options.inJustDecodeBounds = false;
            options.inSampleSize = scale;
            options.inScaled = false;
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            // encodeiamge = Base64.encodeToString(BYTE, Base64.DEFAULT);


            return ( Base64.encodeToString(BYTE, Base64.DEFAULT));
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void validateCreateEvent(ApiExecuter.ApiProgressListener apiProgressListener, String progressMsg, ApiExecuter.ApiResultListener listener, String idSc, String idUser,
                                    String status, String location, String positions, String daystart, String dayfinish,
                                    String mode, String phone_event, String lg1, String lg2, String color, String category,
                                    String allowinvite, String alarm, String description, String imageFilePath, int allDay) {

        bytearrayoutputstream = new ByteArrayOutputStream();
        listDataEvent = new ArrayList<>();

        registerEvent(apiProgressListener, progressMsg, listener, idSc, idUser, status, location, positions, daystart, dayfinish,
                mode, phone_event, lg1, lg2, color, category, allowinvite, alarm, description,
                 imageFilePath , allDay);
    }



    private void registerEvent(ApiExecuter.ApiProgressListener apiProgressListener, String progressMsg, final ApiExecuter.ApiResultListener listener, final  String idSc, final String idUser,
                               final String status, final String location, final String positions,
                               final String daystart, final String dayfinish,
                               final String mode, final String phone_event, final
                               String lg1, final String lg2, final String color,
                               final  String category, final String allowinvite,
                               final String alarm, final String description , final String imageevent,
                               final int allDay ) {
        if (imageevent == null) {
            MyLog.d("image .. : "+image);
        } else {
            image = getReducedImage(imageevent,512,512);
        }
        ApiExecuter.requestApi(apiProgressListener, AppConfig.URL_CREATE_EVENT, progressMsg, listener,
                "idsc", idSc, "iduser", idUser, "status", status, "location", location, "position", positions,
                "daystart", daystart, "dayfinish", dayfinish, "mode", mode, "phone_event", phone_event,
                "lg1", lg1, "lg2", lg2, "color", color, "category", category, "allowinvite", allowinvite,
                "image", image, "alarm", alarm, "description", description, "allDay", allDay+"");

    }



    String getReducedImage(String file, int width, int height){
        BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
        bmpFactoryOptions.inJustDecodeBounds = true;
       Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
        int heightRatio = (int)Math.ceil(bmpFactoryOptions.outHeight/(float)height);
        int widthRatio = (int)Math.ceil(bmpFactoryOptions.outWidth/(float)width);
        if (heightRatio > 1 || widthRatio > 1)
        {
            if (heightRatio > widthRatio)
            {
                bmpFactoryOptions.inSampleSize = heightRatio;
            } else {
                bmpFactoryOptions.inSampleSize = widthRatio;
            }
        }
        bmpFactoryOptions.inJustDecodeBounds = false;
        bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

}