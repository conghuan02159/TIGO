package com.finger.tigo.invite.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.Glide;
import com.finger.tigo.R;
import com.finger.tigo.app.AppController;
import com.finger.tigo.invite.fragment.InviteFriendFragment;
import com.finger.tigo.schedule.creatEvent.Person;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class MyRecyclerAdapterDialog extends RecyclerView.Adapter<MyRecyclerAdapterDialog.PersonViewHolder> implements Filterable{

    OnItemClickListener mItemClickListener;
    List<Person> people;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    Context mContext;

    private CustomFilter userFilter;

    public OnItemClickListener getmItemClickListener() {
        return mItemClickListener;
    }

    public void setmItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public MyRecyclerAdapterDialog(List<Person> persons,Context mContext){
        this.people = persons;
        userFilter = new CustomFilter(MyRecyclerAdapterDialog.this);
        this.mContext = mContext;
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_item_dialog, parent, false);
        return new PersonViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PersonViewHolder holder, int position) {
        holder.personName.setText(people.get(position).name);
        holder.personAge.setText(people.get(position).getEmail());
        //holder.personPhoto.setImageUrl(people.get(position).getPhotoId(), imageLoader);
        Glide.with(mContext).load(people.get(position).getPhotoId()).into(holder.personPhoto);

        if(people.get(position).getState().equalsIgnoreCase("1")){

            holder.tvInvited.setVisibility(View.VISIBLE);
            holder.cbSelected.setVisibility(View.GONE);

        }
        else
        {
            holder.cbSelected.setChecked(people.get(position).isSelected());
            holder.tvInvited.setVisibility(View.GONE);
            holder.cbSelected.setVisibility(View.VISIBLE);
        }

    }

    public void setItemSelected(int position, boolean isSelected){
        if (position != -1) {
            people.get(position).setSelected(isSelected);
            notifyDataSetChanged();
        }
    }

    List<Person> getPeople(){
        return people;
    }

    public interface OnItemClickListener{
        void onItemClick(View v, int position);
    }

    @Override
    public int getItemCount() {
        return people.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setPeople(List<Person> people) {
        this.people = people;
    }

//    public interface OnItemClickListener {
//
//        void onItemClick(View view, int position);
//    }


    @Override
    public Filter getFilter() {

        return userFilter;
    }

    public class PersonViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener{

        public final View mView;
        public TextView personName;
        public TextView personAge;
        CircleImageView personPhoto;
        public CheckBox cbSelected;
        public TextView tvInvited;

        public PersonViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            if (imageLoader == null)
                imageLoader = AppController.getInstance().getImageLoader();
            itemView.setOnClickListener(this);
            personName = (TextView) itemView.findViewById(R.id.person_name_dialog);
            personAge = (TextView) itemView.findViewById(R.id.person_age_dialog);
            personPhoto = (CircleImageView) itemView.findViewById(R.id.profilePicDialog);
            cbSelected = (CheckBox) itemView.findViewById(R.id.cbSelectedDialog);
            tvInvited = (TextView) itemView.findViewById(R.id.tvInvited);
        }
        @Override
        public String toString(){
            return super.toString()+" '"+personName +"'";
        }

        @Override
        public void onClick(View v) {
            if(mItemClickListener != null){
                mItemClickListener.onItemClick(v, getPosition());
            }
        }
    }
    public class CustomFilter extends Filter {
        private MyRecyclerAdapterDialog mAdapter;
        private CustomFilter(MyRecyclerAdapterDialog mAdapter) {
            super();
            this.mAdapter = mAdapter;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            InviteFriendFragment.filteredList.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                InviteFriendFragment.filteredList.addAll(InviteFriendFragment.mPersonList);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final Person mWords : InviteFriendFragment.mPersonList) {
                    if (mWords.getName().toLowerCase().startsWith(filterPattern)) {
                        InviteFriendFragment.filteredList.add(mWords);
                    }
                }
            }
            System.out.println("Count Number " + InviteFriendFragment.filteredList.size());
            results.values = InviteFriendFragment.filteredList;
            results.count = InviteFriendFragment.filteredList.size();
            return results;
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            System.out.println("Count Number 2 " + ((List<Person>) results.values).size());
            this.mAdapter.notifyDataSetChanged();
        }
    }

}
