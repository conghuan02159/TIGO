package com.finger.tigo.items;

/**
 * Created by duong on 01/08/2016.
 */
public class ItemNoti {

    private String idUser;
    private String idFriend;
    public String Name;
    public String profilePic;
    private String timeStamp;
    private String status;
    private String check;
    private String phone;
    private String location;
    private String position;
    private double lg1;
    private double lg2;
    private String timeStart;
    private String timeEnd;
    private String dayStart;
    private String dayEnd;
    private String idsc;
    private String state;
    private String image;
    private String sta;
    private String sendName;
    public String nameCut;


    public ItemNoti(String name, String nameCut, String phone, String profilePic, String idFriend){
        this.Name= name;
        this.nameCut = nameCut;
        this.phone = phone;
        this.profilePic = profilePic;
        this.idFriend = idFriend;

    }
    public ItemNoti(String name, String profilePic, String phone, String idFriend){
        this.Name= name;
        this.profilePic = profilePic;
        this.phone = phone;
        this.idFriend = idFriend;

    }


    public ItemNoti(){
    }

    public double getLg2() {
        return lg2;
    }

    public void setLg2(double lg2) {
        this.lg2 = lg2;
    }

    public ItemNoti(double lg2, double lg1) {
        this.lg2 = lg2;
        this.lg1 = lg1;
    }

    public String getSendName() {
        return sendName;
    }

    public void setSendName(String sendName) {
        this.sendName = sendName;
    }

    public double getLg1() {
        return lg1;
    }

    public void setLg1(double lg1) {
        this.lg1 = lg1;
    }

    public String getSta() {
        return sta;
    }

    public void setSta(String sta) {
        this.sta = sta;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getIdsc() {
        return idsc;
    }

    public void setIdsc(String idsc) {
        this.idsc = idsc;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public String getDayStart() {
        return dayStart;
    }

    public void setDayStart(String dayStart) {
        this.dayStart = dayStart;
    }

    public String getDayEnd() {
        return dayEnd;
    }

    public void setDayEnd(String dayEnd) {
        this.dayEnd = dayEnd;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCheck() {
        return check;
    }

    public void setCheck(String check) {
        this.check = check;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdFriend() {
        return idFriend;
    }

    public void setIdFriend(String idFriend) {
        this.idFriend = idFriend;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}
