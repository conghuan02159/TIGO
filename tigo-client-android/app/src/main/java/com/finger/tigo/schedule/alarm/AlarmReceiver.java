package com.finger.tigo.schedule.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.finger.tigo.syncadapter.SyncAdapter;

/**
 * Created by duong on 31/08/2016.
 */
public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {


//
        String richard_id = intent.getStringExtra(SyncAdapter.IDSC_ALARM);
        int index = intent.getIntExtra("indexAlarm", 0);
        String title = intent.getStringExtra("title");
        long startMillisNoti = intent.getLongExtra("startDay", 0);
        String image = intent.getStringExtra("image");

        Intent serviceIntent = new Intent(context,RingtonePlayingService.class);
        serviceIntent.putExtra(SyncAdapter.IDSC_ALARM, richard_id);
        serviceIntent.putExtra("indexAlarm", index);
        serviceIntent.putExtra("title", title);
        serviceIntent.putExtra("startday", startMillisNoti);
        serviceIntent.putExtra("image", image);
        context.startService(serviceIntent);
    }

}