package com.finger.tigo.schedule.tabson;

        import android.content.Context;
        import android.support.v4.view.ViewPager;
        import android.util.AttributeSet;
        import android.view.MotionEvent;

/**
 * Created by duong on 07/07/2016.
 */
public class NonSliding extends ViewPager {

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return false;
    }

    public NonSliding(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NonSliding(Context context) {
        super(context);
    }
}
