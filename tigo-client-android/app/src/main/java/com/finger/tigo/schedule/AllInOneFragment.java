package com.finger.tigo.schedule;


import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.AsyncQueryHandler;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Calendars;
import android.provider.CalendarContract.Events;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.detail.EventDetailActivity;
import com.finger.tigo.schedule.libCalendar.AllInOneMenuExtensionsInterface;
import com.finger.tigo.schedule.libCalendar.CalendarController;
import com.finger.tigo.schedule.libCalendar.CalendarController.EventHandler;
import com.finger.tigo.schedule.libCalendar.CalendarController.EventInfo;
import com.finger.tigo.schedule.libCalendar.CalendarController.EventType;
import com.finger.tigo.schedule.libCalendar.CalendarController.ViewType;
import com.finger.tigo.schedule.libCalendar.CalendarToolbarHandler;
import com.finger.tigo.schedule.libCalendar.DayFragment;
import com.finger.tigo.schedule.libCalendar.DayOfMonthDrawable;
import com.finger.tigo.schedule.libCalendar.ExtensionsFactory;
import com.finger.tigo.schedule.libCalendar.GeneralPreferences;
import com.finger.tigo.schedule.libCalendar.Utils;
import com.finger.tigo.schedule.libCalendar.agenda.AgendaFragment;
import com.finger.tigo.schedule.libCalendar.month.MonthByWeekFragment;
import com.finger.tigo.util.MyLog;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static android.provider.CalendarContract.Attendees.ATTENDEE_STATUS;
import static android.provider.CalendarContract.EXTRA_EVENT_ALL_DAY;
import static android.provider.CalendarContract.EXTRA_EVENT_BEGIN_TIME;
import static android.provider.CalendarContract.EXTRA_EVENT_END_TIME;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Bao Dai Duong on 5/17/2016.
 */
public class AllInOneFragment extends Fragment implements EventHandler,
        SharedPreferences.OnSharedPreferenceChangeListener, SearchView.OnQueryTextListener, SearchView.OnSuggestionListener {

    private static final String TAG = "AllInOneActivity";
    private static final boolean DEBUG = false;
    private static final String EVENT_INFO_FRAGMENT_TAG = "EventInfoFragment";
    private static final String BUNDLE_KEY_RESTORE_TIME = "key_restore_time";
    private static final String BUNDLE_KEY_EVENT_ID = "key_event_id";
    private static final String BUNDLE_KEY_RESTORE_VIEW = "key_restore_view";
    private static final String BUNDLE_KEY_CHECK_ACCOUNTS = "key_check_for_accounts";
    private static final int HANDLER_KEY = 0;
    private static final int PERMISSIONS_REQUEST_WRITE_CALENDAR = 0;

    // Indices of buttons for the drop down menu (tabs replacement)
    // Must match the strings in the array buttons_list in arrays.xml and the
    // OnNavigationListener
    private static final int BUTTON_DAY_INDEX = 0;
    private static final int BUTTON_WEEK_INDEX = 1;
    private static final int BUTTON_MONTH_INDEX = 2;
    private static final int BUTTON_AGENDA_INDEX = 3;
    private static boolean mIsMultipane;
    private static boolean mIsTabletConfig;
    private static boolean mShowAgendaWithMonth;
    private static boolean mShowEventDetailsWithAgenda;
    DayOfMonthDrawable mDayOfMonthIcon;
    int mOrientation;
    BroadcastReceiver mCalIntentReceiver;
    private CalendarController mController;

    RadioGroup radioGroup;
    FloatingActionButton fabToDay;
    Dialog dialogIntent;
    Intent intent;

    // Create an observer so that we can update the views whenever a
    // Calendar event changes.
    private final ContentObserver mObserver = new ContentObserver(new Handler()) {
        @Override
        public boolean deliverSelfNotifications() {
            return true;
        }

        @Override
        public void onChange(boolean selfChange) {
            MyLog.i("selfChange=" + selfChange);
            eventsChanged();
        }
    };
    private boolean mOnSaveInstanceStateCalled = false;
    private boolean mBackToPreviousView = false;
    private ContentResolver mContentResolver;
    private int mPreviousView;
    private int mCurrentView;
    private boolean mPaused = true;
    private boolean mUpdateOnResume = false;
    private boolean mHideControls = false;
    private boolean mShowSideViews = true;
    private boolean mShowWeekNum = false;
    public  TextView mHomeTime;
//    private TextView mDateRange;
//    private TextView mWeekTextView;
    private View mMiniMonth;
    private View mCalendarsList;
    private View mMiniMonthContainer;
    private final Animator.AnimatorListener mSlideAnimationDoneListener = new Animator.AnimatorListener() {

        @Override
        public void onAnimationCancel(Animator animation) {
        }

        @Override
        public void onAnimationEnd(Animator animation) {
            int visibility = mShowSideViews ? View.VISIBLE : View.GONE;
            mMiniMonth.setVisibility(visibility);
            mCalendarsList.setVisibility(visibility);
            mMiniMonthContainer.setVisibility(visibility);
        }

        @Override
        public void onAnimationRepeat(Animator animation) {
        }

        @Override
        public void onAnimationStart(Animator animation) {
        }
    };
    // private FloatingActionButton mFab;
    private View mSecondaryPane;
    private String mTimeZone;
    private boolean mShowCalendarControls;
    private boolean mShowEventInfoFullScreenAgenda;
    private boolean mShowEventInfoFullScreen;
    private int mWeekNum;
    private int mCalendarControlsAnimationTime;
    private int mControlsAnimateWidth;
    private int mControlsAnimateHeight;
    private long mViewEventId = -1;
    private long mIntentEventStartMillis = -1;
    private long mIntentEventEndMillis = -1;
    private int mIntentAttendeeResponse = CalendarContract.Attendees.ATTENDEE_STATUS_NONE;
    private boolean mIntentAllDay = false;
    // private DrawerLayout mDrawerLayout;
    private Toolbar mToolbar;
    //  private NavigationView mNavigationView;
    private int mCurrentMenuItem;
    private CalendarToolbarHandler mCalendarToolbarHandler;
    // Action bar
    private ActionBar mActionBar;
    private SearchView mSearchView;
    private MenuItem mSearchMenu;
    private MenuItem mControlsMenu;
    private Menu mOptionsMenu;
    private QueryHandler mHandler;
    private final Runnable mHomeTimeUpdater = new Runnable() {
        @Override
        public void run() {
            mTimeZone = Utils.getTimeZone(getActivity(), mHomeTimeUpdater);
            updateSecondaryTitleFields(-1);
            getActivity().invalidateOptionsMenu();
            Utils.setMidnightUpdater(mHandler, mTimeChangesUpdater, mTimeZone);
        }
    };
    // runs every midnight/time changes and refreshes the today icon
    private final Runnable mTimeChangesUpdater = new Runnable() {
        @Override
        public void run() {
            mTimeZone = Utils.getTimeZone(getActivity(), mHomeTimeUpdater);
            getActivity().invalidateOptionsMenu();
            Utils.setMidnightUpdater(mHandler, mTimeChangesUpdater, mTimeZone);
        }
    };
    private boolean mCheckForAccounts = true;
    private String mHideString;
    private String mShowString;
    // Params for animating the controls on the right
    private RelativeLayout.LayoutParams mControlsParams;
    private LinearLayout.LayoutParams mVerticalControlsParams;
    private AllInOneMenuExtensionsInterface mExtensions = ExtensionsFactory
            .getAllInOneMenuExtensions();

//    @Override
//    public void onNewIntent(Intent intent) {
//
//        String action = intent.getAction();
//        if (DEBUG)
//            Log.d(TAG, "New intent received " + intent.toString());
//        // Don't change the date if we're just returning to the app's home
//        if (Intent.ACTION_VIEW.equals(action)
//                && !intent.getBooleanExtra(Utils.INTENT_KEY_HOME, false)) {
//            long millis = parseViewAction(intent);
//            if (millis == -1) {
//                millis = Utils.timeFromIntentInMillis(intent);
//            }
//            if (millis != -1 && mViewEventId == -1 && mController != null) {
//                Time time = new Time(mTimeZone);
//                time.set(millis);
//                time.normalize(true);
//                mController.sendEvent(this, EventType.GO_TO, time, time, -1, ViewType.CURRENT);
//            }
//        }
//    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle icicle) {
        super.onCreateView(inflater, container, icicle);
        if (icicle != null && icicle.containsKey(BUNDLE_KEY_CHECK_ACCOUNTS)) {

            mCheckForAccounts = icicle.getBoolean(BUNDLE_KEY_CHECK_ACCOUNTS);
        }
        // Launch add google account if this is first time and there are no
        // accounts yet
        if (mCheckForAccounts
                && !Utils.getSharedPreference(getActivity(), GeneralPreferences.KEY_SKIP_SETUP, false)) {

            mHandler = new QueryHandler(getActivity().getContentResolver());
            mHandler.startQuery(0, null, Calendars.CONTENT_URI, new String[]{
                    Calendars._ID
            }, null, null /* selection args */, null /* sort order */);
        }

        // This needs to be created before setContentView
        mController = CalendarController.getInstance(getActivity());

        // Check and ask for most needed permissions
        checkAppPermissions();

        // Get time from intent or icicle
        long timeMillis = -1;
        int viewType = -1;
        final Intent intent = getActivity().getIntent();
        if (icicle != null) {
            timeMillis = icicle.getLong(BUNDLE_KEY_RESTORE_TIME);
            viewType = icicle.getInt(BUNDLE_KEY_RESTORE_VIEW, -1);
        } else {
            String action = intent.getAction();
            if (Intent.ACTION_VIEW.equals(action)) {
                // Open EventInfo later
                timeMillis = parseViewAction(intent);
            }

            if (timeMillis == -1) {
                timeMillis = Utils.timeFromIntentInMillis(intent);
            }
        }

        if (viewType == -1 || viewType > ViewType.MAX_VALUE) {
            viewType = Utils.getViewTypeFromIntentAndSharedPref(getActivity());
        }
        mTimeZone = Utils.getTimeZone(getActivity(), mHomeTimeUpdater);
        Time t = new Time(mTimeZone);
        t.set(timeMillis);

        if (DEBUG) {
            if (icicle != null && intent != null) {
                Log.d(TAG, "both, icicle:" + icicle.toString() + "  intent:" + intent.toString());
            } else {
                Log.d(TAG, "not both, icicle:" + icicle + " intent:" + intent);
            }
        }

        Resources res = getResources();
        mHideString = res.getString(R.string.hide_controls);
        mShowString = res.getString(R.string.show_controls);
        mOrientation = res.getConfiguration().orientation;
        if (mOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            mControlsAnimateWidth = (int) res.getDimension(R.dimen.calendar_controls_width);
            if (mControlsParams == null) {
                mControlsParams = new RelativeLayout.LayoutParams(mControlsAnimateWidth, 0);
            }
            mControlsParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        } else {
            // Make sure width is in between allowed min and max width values
            mControlsAnimateWidth = Math.max(res.getDisplayMetrics().widthPixels * 45 / 100,
                    (int) res.getDimension(R.dimen.min_portrait_calendar_controls_width));
            mControlsAnimateWidth = Math.min(mControlsAnimateWidth,
                    (int) res.getDimension(R.dimen.max_portrait_calendar_controls_width));
        }

        mControlsAnimateHeight = (int) res.getDimension(R.dimen.calendar_controls_height);

        mHideControls = !Utils.getSharedPreference(
                getActivity(), GeneralPreferences.KEY_SHOW_CONTROLS, true);
        mIsMultipane = Utils.getConfigBool(getActivity(), R.bool.multiple_pane_config);
        mIsTabletConfig = Utils.getConfigBool(getActivity(), R.bool.tablet_config);
        mShowAgendaWithMonth = Utils.getConfigBool(getActivity(), R.bool.show_agenda_with_month);
        mShowCalendarControls =
                Utils.getConfigBool(getActivity(), R.bool.show_calendar_controls);
        mShowEventDetailsWithAgenda =
                Utils.getConfigBool(getActivity(), R.bool.show_event_details_with_agenda);
        mShowEventInfoFullScreenAgenda =
                Utils.getConfigBool(getActivity(), R.bool.agenda_show_event_info_full_screen);
        mShowEventInfoFullScreen =
                Utils.getConfigBool(getActivity(), R.bool.show_event_info_full_screen);
        mCalendarControlsAnimationTime = res.getInteger(R.integer.calendar_controls_animation_time);
        Utils.setAllowWeekForDetailView(mIsMultipane);
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.all_in_one, container, false);
        if (Build.VERSION.SDK_INT >= 21) {
            getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        fabToDay = (FloatingActionButton) v.findViewById(R.id.fab_to_day);

        fabToDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Time time = null;
                int viewTypee = ViewType.CURRENT;
                long extras = CalendarController.EXTRA_GOTO_TIME;
                time = new Time(mTimeZone);
                time.setToNow();
                extras |= CalendarController.EXTRA_GOTO_TODAY;
                mController.sendEvent(this, EventType.GO_TO, time, null, time, -1, viewTypee, extras, null, null);
            }
        });

        radioGroup = (RadioGroup) v.findViewById(R.id.rdogrp);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                if (checkedId == R.id.btn1) {
                    if (mCurrentView != ViewType.DAY) {
                        mController.sendEvent(this, EventType.GO_TO, null, null, -1, ViewType.DAY);
                    }

                } else if (checkedId == R.id.btn2) {
                    if (mCurrentView != ViewType.WEEK) {
                        mController.sendEvent(this, EventType.GO_TO, null, null, -1, ViewType.WEEK);
                    }

                } else if (checkedId == R.id.btn3) {
                    if (mCurrentView != ViewType.MONTH) {
                        mController.sendEvent(this, EventType.GO_TO, null, null, -1, ViewType.MONTH);
                    }

                } else if (checkedId == R.id.btn4) {
                    if (mCurrentView != ViewType.AGENDA) {
                        mController.sendEvent(this, EventType.GO_TO, null, null, -1, ViewType.AGENDA);
                    }

                }
            }
        });



//        if (mIsTabletConfig) {
//            mDateRange = (TextView) v.findViewById(R.id.date_bar);
//            mWeekTextView = (TextView) v.findViewById(R.id.week_num);
//        } else {
//            mDateRange = (TextView) getActivity().getLayoutInflater().inflate(R.layout.date_range_title, null);
//        }

        setupToolbar(viewType);
        // setupNavDrawer();


        mHomeTime = (TextView) v.findViewById(R.id.home_time);
        mMiniMonth = v.findViewById(R.id.mini_month);
        if (mIsTabletConfig && mOrientation == Configuration.ORIENTATION_PORTRAIT) {
            mMiniMonth.setLayoutParams(new RelativeLayout.LayoutParams(mControlsAnimateWidth,
                    mControlsAnimateHeight));
        }
        mCalendarsList = v.findViewById(R.id.calendar_list);
        mMiniMonthContainer = v.findViewById(R.id.mini_month_container);
        mSecondaryPane = v.findViewById(R.id.secondary_pane);

        // Must register as the first activity because this activity can modify
        // the list of event handlers in it's handle method. This affects who
        // the rest of the handlers the controller dispatches to are.
        mController.registerFirstEventHandler(HANDLER_KEY, this);

        initFragments(timeMillis, viewType, icicle);

        // Listen for changes that would require this to be refreshed
        SharedPreferences prefs = GeneralPreferences.getSharedPreferences(getActivity());
        prefs.registerOnSharedPreferenceChangeListener(this);

        mContentResolver = getActivity().getContentResolver();

        return v;
    }

    private void checkAppPermissions() {
        // Here, thisActivity is the current activity
        if (Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_CALENDAR)
                != PackageManager.PERMISSION_GRANTED) {

            // No explanation needed, we can request the permission.

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_CALENDAR},
                    PERMISSIONS_REQUEST_WRITE_CALENDAR);


        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_WRITE_CALENDAR: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay!

                } else {
                    Toast.makeText(getApplicationContext(), R.string.user_rejected_calendar_write_permission, Toast.LENGTH_LONG).show();
                    getActivity().finish();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void setupToolbar(int viewType) {

        mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        if (mToolbar == null) {
            if (DEBUG) {
                Log.d(TAG, "Didn't find a toolbar");
            }
            return;
        }

        if (!mIsTabletConfig) {
            mCalendarToolbarHandler = new CalendarToolbarHandler((AppCompatActivity) getActivity(), mToolbar, viewType);
        } else {
            int titleResource;
            switch (viewType) {
                case ViewType.AGENDA:
                    titleResource = R.string.agenda_view;
                    break;
                case ViewType.DAY:
                    titleResource = R.string.day_view;
                    break;
                case ViewType.MONTH:
                    titleResource = R.string.month_view;
                    break;
                case ViewType.WEEK:
                default:
                    titleResource = R.string.week_view;
                    break;
            }
            mToolbar.setTitle(titleResource);
        }
        // mToolbar.setTitle(getTitle());


//        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                AllInOneActivity.this.openDrawer();
//            }
//        });

        if (mActionBar == null) return;
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setHomeButtonEnabled(true);
    }

//    public void openDrawer() {        mActionBar.setDisplayHomeAsUpEnabled(true);

//        mDrawerLayout.openDrawer(Gravity.LEFT);
//    }
//
//    public void setupNavDrawer() {
//        if (mDrawerLayout == null) {
//            if (DEBUG) {
//                Log.d(TAG, "mDrawerLayout is null - Can not setup the NavDrawer! Have you set the android.support.v7.widget.DrawerLayout?");
//            }
//            return;
//        }
//        mNavigationView.setNavigationItemSelectedListener(this);
//        showActionBar();
//    }



    private void hideActionBar() {
        if (mActionBar == null) return;
        mActionBar.hide();
    }

    private void showActionBar() {
        if (mActionBar == null) return;
        mActionBar.show();
    }

    private long parseViewAction(final Intent intent) {
        long timeMillis = -1;
        Uri data = intent.getData();
        if (data != null && data.isHierarchical()) {
            List<String> path = data.getPathSegments();
            if (path.size() == 2 && path.get(0).equals("events")) {
                try {
                    mViewEventId = Long.valueOf(data.getLastPathSegment());
                    if (mViewEventId != -1) {
                        mIntentEventStartMillis = intent.getLongExtra(EXTRA_EVENT_BEGIN_TIME, 0);
                        mIntentEventEndMillis = intent.getLongExtra(EXTRA_EVENT_END_TIME, 0);
                        mIntentAttendeeResponse = intent.getIntExtra(
                                ATTENDEE_STATUS, CalendarContract.Attendees.ATTENDEE_STATUS_NONE);
                        mIntentAllDay = intent.getBooleanExtra(EXTRA_EVENT_ALL_DAY, false);
                        timeMillis = mIntentEventStartMillis;
                    }
                } catch (NumberFormatException e) {
                    // Ignore if mViewEventId can't be parsed
                }
            }
        }
        return timeMillis;
    }

    // Clear buttons used in the agenda view
    private void clearOptionsMenu() {
        if (mOptionsMenu == null) {
            return;
        }

    }
    @Override
    public void onStop(){
        super.onStop();

    }

    @Override
    public void onResume() {
        super.onResume();


        // Must register as the first activity because this activity can modify
        // the list of event handlers in it's handle method. This affects who
        // the rest of the handlers the controller dispatches to are.
        mController.registerFirstEventHandler(HANDLER_KEY, this);

        mOnSaveInstanceStateCalled = false;
        mContentResolver.registerContentObserver(CalendarContract.Events.CONTENT_URI,
                true, mObserver);
        if (mUpdateOnResume) {
            initFragments(mController.getTime(), mController.getViewType(), null);
            mUpdateOnResume = false;
        }
        Time t = new Time(mTimeZone);
        t.set(mController.getTime());
        mController.sendEvent(this, EventType.UPDATE_TITLE, t, t, -1, ViewType.CURRENT,
                mController.getDateFlags(), null, null);

        if (mControlsMenu != null) {
            mControlsMenu.setTitle(mHideControls ? mShowString : mHideString);
        }
        mPaused = false;

        if (mViewEventId != -1 && mIntentEventStartMillis != -1 && mIntentEventEndMillis != -1) {
            long currentMillis = System.currentTimeMillis();
            long selectedTime = -1;
            if (currentMillis > mIntentEventStartMillis && currentMillis < mIntentEventEndMillis) {
                selectedTime = currentMillis;
            }
            mController.sendEventRelatedEventWithExtra(this, EventType.VIEW_EVENT, mViewEventId,
                    mIntentEventStartMillis, mIntentEventEndMillis, -1, -1,
                    EventInfo.buildViewExtraLong(mIntentAttendeeResponse, mIntentAllDay),
                    selectedTime);
            mViewEventId = -1;
            mIntentEventStartMillis = -1;
            mIntentEventEndMillis = -1;
            mIntentAllDay = false;
        }
        Utils.setMidnightUpdater(mHandler, mTimeChangesUpdater, mTimeZone);
        // Make sure the today icon is up to date
//        getActivity().invalidateOptionsMenu();

        mCalIntentReceiver = Utils.setTimeChangesReceiver(getActivity(), mTimeChangesUpdater);
    }


    @Override
    public void onPause() {
        super.onPause();

        mController.deregisterEventHandler(HANDLER_KEY);
        mPaused = true;
        mHomeTime.removeCallbacks(mHomeTimeUpdater);

        mContentResolver.unregisterContentObserver(mObserver);
        if (getActivity().isFinishing()) {
            // Stop listening for changes that would require this to be refreshed
            SharedPreferences prefs = GeneralPreferences.getSharedPreferences(getActivity());
            prefs.unregisterOnSharedPreferenceChangeListener(this);
        }
        // FRAG_TODO save highlighted days of the week;
        if (mController.getViewType() != ViewType.EDIT) {
            Utils.setDefaultView(getActivity(), mController.getViewType());

        }
        Utils.resetMidnightUpdater(mHandler, mTimeChangesUpdater);
        Utils.clearTimeChangesReceiver(getActivity(), mCalIntentReceiver);
    }

//    @Override
//    public void onUserLeaveHint() {
//        mController.sendEvent(this, EventType.USER_HOME, null, null, -1, ViewType.CURRENT);
//        super.onUserLeaveHint();
//    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        mOnSaveInstanceStateCalled = true;
        super.onSaveInstanceState(outState);
        outState.putLong(BUNDLE_KEY_RESTORE_TIME, mController.getTime());
        outState.putInt(BUNDLE_KEY_RESTORE_VIEW, mCurrentView);
        if (mCurrentView == ViewType.EDIT) {
            outState.putLong(BUNDLE_KEY_EVENT_ID, mController.getEventId());
        } else if (mCurrentView == ViewType.AGENDA) {
            FragmentManager fm = getFragmentManager();
            Fragment f = fm.findFragmentById(R.id.main_pane);
            if (f instanceof AgendaFragment) {
                outState.putLong(BUNDLE_KEY_EVENT_ID, ((AgendaFragment) f).getLastShowEventId());
            }
        }
        outState.putBoolean(BUNDLE_KEY_CHECK_ACCOUNTS, mCheckForAccounts);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        SharedPreferences prefs = GeneralPreferences.getSharedPreferences(getActivity());
        prefs.unregisterOnSharedPreferenceChangeListener(this);

        mController.deregisterAllEventHandlers();

        CalendarController.removeInstance(getActivity());
    }

    private void initFragments(long timeMillis, int viewType, Bundle icicle) {
        if (DEBUG) {
            Log.d(TAG, "Initializing to " + timeMillis + " for view " + viewType);
        }
        FragmentTransaction ft = getFragmentManager().beginTransaction();


        if (!mShowCalendarControls || viewType == ViewType.EDIT) {
            mMiniMonth.setVisibility(View.GONE);
            mCalendarsList.setVisibility(View.GONE);
        }

        EventInfo info = null;
        if (viewType == ViewType.EDIT) {
            mPreviousView = GeneralPreferences.getSharedPreferences(getActivity()).getInt(
                    GeneralPreferences.KEY_START_VIEW, GeneralPreferences.DEFAULT_START_VIEW);

            long eventId = -1;
            Intent intent = getActivity().getIntent();
            Uri data = intent.getData();
            if (data != null) {
                try {
                    eventId = Long.parseLong(data.getLastPathSegment());
                } catch (NumberFormatException e) {
                    if (DEBUG) {
                        Log.d(TAG, "Create new event");
                    }
                }
            } else if (icicle != null && icicle.containsKey(BUNDLE_KEY_EVENT_ID)) {
                eventId = icicle.getLong(BUNDLE_KEY_EVENT_ID);
            }

            long begin = intent.getLongExtra(EXTRA_EVENT_BEGIN_TIME, -1);
            long end = intent.getLongExtra(EXTRA_EVENT_END_TIME, -1);
            info = new EventInfo();
            if (end != -1) {
                info.endTime = new Time();
                info.endTime.set(end);
            }
            if (begin != -1) {
                info.startTime = new Time();
                info.startTime.set(begin);
            }
            info.id = eventId;
            // We set the viewtype so if the user presses back when they are
            // done editing the controller knows we were in the Edit Event
            // screen. Likewise for eventId
            mController.setViewType(viewType);
            mController.setEventId(eventId);
        } else {
            mPreviousView = viewType;
        }

        setMainPane(ft, R.id.main_pane, viewType, timeMillis, true);
        ft.commit(); // this needs to be after setMainPane()

        Time t = new Time(mTimeZone);
        t.set(timeMillis);
        if (viewType == ViewType.AGENDA && icicle != null) {
            mController.sendEvent(this, EventType.GO_TO, t, null,
                    icicle.getLong(BUNDLE_KEY_EVENT_ID, -1), viewType);
        } else if (viewType != ViewType.EDIT) {
            mController.sendEvent(this, EventType.GO_TO, t, null, -1, viewType);
        }
    }

//    @Override
//    public void onBackPressed() {
//        if (mCurrentView == ViewType.EDIT || mBackToPreviousView) {
//            mController.sendEvent(this, EventType.GO_TO, null, null, -1, mPreviousView);
//        } else {
//            super.onBackPressed();
//        }
//    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        super.onCreateOptionsMenu(menu);
//        mOptionsMenu = menu;
//        getMenuInflater().inflate(R.menu.all_in_one_title_bar, menu);
//
//        // Add additional options (if any).
//        Integer extensionMenuRes = mExtensions.getExtensionMenuResource(menu);
//        if (extensionMenuRes != null) {
//            getMenuInflater().inflate(extensionMenuRes, menu);
//        }
//
//        mSearchMenu = menu.findItem(R.id.action_search);
//        mSearchView = (SearchView) MenuItemCompat.getActionView(mSearchMenu);
//        if (mSearchView != null) {
//            Utils.setUpSearchView(mSearchView, this);
//            mSearchView.setOnQueryTextListener(this);
//            mSearchView.setOnSuggestionListener(this);
//        }
//
//        // Hide the "show/hide controls" button if this is a phone
//        // or the view type is "Month" or "Agenda".
//
//        mControlsMenu = menu.findItem(R.id.action_hide_controls);
//        if (!mShowCalendarControls) {
//            if (mControlsMenu != null) {
//                mControlsMenu.setVisible(false);
//                mControlsMenu.setEnabled(false);
//            }
//        } else if (mControlsMenu != null && mController != null
//                && (mController.getViewType() == ViewType.MONTH ||
//                mController.getViewType() == ViewType.AGENDA)) {
//            mControlsMenu.setVisible(false);
//            mControlsMenu.setEnabled(false);
//        } else if (mControlsMenu != null) {
//            mControlsMenu.setTitle(mHideControls ? mShowString : mHideString);
//        }
//
//        MenuItem menuItem = menu.findItem(R.id.action_today);
//        if (Utils.isJellybeanOrLater()) {
//            // replace the default top layer drawable of the today icon with a
//            // custom drawable that shows the day of the month of today
//            LayerDrawable icon = (LayerDrawable) menuItem.getIcon();
//            Utils.setTodayIcon(icon, this, mTimeZone);
//        } else {
//            menuItem.setIcon(R.drawable.ic_menu_today_no_date_holo_light);
//        }
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        Time t = null;
//        int viewType = ViewType.CURRENT;
//        long extras = CalendarController.EXTRA_GOTO_TIME;
//        final int itemId = item.getItemId();
//        if (itemId == R.id.action_refresh) {
//            mController.refreshCalendars();
//            return true;
//        } else if (itemId == R.id.action_today) {
//            viewType = ViewType.CURRENT;
//            t = new Time(mTimeZone);
//            t.setToNow();
//            extras |= CalendarController.EXTRA_GOTO_TODAY;
//        } else if (itemId == R.id.action_goto) {
//            Time todayTime;
//            t = new Time(mTimeZone);
//            t.set(mController.getTime());
//            todayTime = new Time(mTimeZone);
//            todayTime.setToNow();
//            if (todayTime.month == t.month) {
//                t = todayTime;
//            }
//
//
//
//        } else if (itemId == R.id.action_hide_controls) {
//            mHideControls = !mHideControls;
//            Utils.setSharedPreference(
//                    this, GeneralPreferences.KEY_SHOW_CONTROLS, !mHideControls);
//            item.setTitle(mHideControls ? mShowString : mHideString);
//            if (!mHideControls) {
//                mMiniMonth.setVisibility(View.VISIBLE);
//                mCalendarsList.setVisibility(View.VISIBLE);
//                mMiniMonthContainer.setVisibility(View.VISIBLE);
//            }
//            final ObjectAnimator slideAnimation = ObjectAnimator.ofInt(this, "controlsOffset",
//                    mHideControls ? 0 : mControlsAnimateWidth,
//                    mHideControls ? mControlsAnimateWidth : 0);
//            slideAnimation.setDuration(mCalendarControlsAnimationTime);
//            ObjectAnimator.setFrameDelay(0);
//            slideAnimation.start();
//            return true;
//        } else if (itemId == R.id.action_search) {
//            return false;
//        } else {
//            return mExtensions.handleItemSelected(item, this);
//        }
//        mController.sendEvent(this, EventType.GO_TO, t, null, t, -1, viewType, extras, null, null);
//        return true;
//    }


    /**
     * Sets the offset of the controls on the right for animating them off/on
     * screen. ProGuard strips this if it's not in proguard.flags
     *
     * @param controlsOffset The current offset in pixels
     */
    public void setControlsOffset(int controlsOffset) {
        if (mOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            mMiniMonth.setTranslationX(controlsOffset);
            mCalendarsList.setTranslationX(controlsOffset);
            mControlsParams.width = Math.max(0, mControlsAnimateWidth - controlsOffset);
            mMiniMonthContainer.setLayoutParams(mControlsParams);
        } else {
            mMiniMonth.setTranslationY(controlsOffset);
            mCalendarsList.setTranslationY(controlsOffset);
            if (mVerticalControlsParams == null) {
                mVerticalControlsParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, mControlsAnimateHeight);
            }
            mVerticalControlsParams.height = Math.max(0, mControlsAnimateHeight - controlsOffset);
            mMiniMonthContainer.setLayoutParams(mVerticalControlsParams);
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
        if (key.equals(GeneralPreferences.KEY_WEEK_START_DAY)) {
            if (mPaused) {
                mUpdateOnResume = true;
            } else {
                initFragments(mController.getTime(), mController.getViewType(), null);
            }
        }
    }

    private void setMainPane(
            FragmentTransaction ft, int viewId, int viewType, long timeMillis, boolean force) {
        if (mOnSaveInstanceStateCalled) {
            return;
        }
        if (!force && mCurrentView == viewType) {
            return;
        }

        // Remove this when transition to and from month view looks fine.
        boolean doTransition = viewType != ViewType.MONTH && mCurrentView != ViewType.MONTH;
        FragmentManager fragmentManager = getFragmentManager();
        // Check if our previous view was an Agenda view
        // TODO remove this if framework ever supports nested fragments
        if (mCurrentView == ViewType.AGENDA) {
            // If it was, we need to do some cleanup on it to prevent the
            // edit/delete buttons from coming back on a rotation.
            Fragment oldFrag = fragmentManager.findFragmentById(viewId);
            if (oldFrag instanceof AgendaFragment) {
                ((AgendaFragment) oldFrag).removeFragments(fragmentManager);
            }
        }

        if (viewType != mCurrentView) {
            // The rules for this previous view are different than the
            // controller's and are used for intercepting the back button.
            if (mCurrentView != ViewType.EDIT && mCurrentView > 0) {
                mPreviousView = mCurrentView;
            }
            mCurrentView = viewType;
        }
        // Create new fragment
        Fragment frag = null;
        Fragment secFrag = null;
        switch (viewType) {
            case ViewType.AGENDA:
                radioGroup.check(R.id.btn4);
                // mNavigationView.getMenu().findItem(R.id.agenda_menu_item).setChecked(true);
                frag = new AgendaFragment(timeMillis, false);
                if (mIsTabletConfig) {
                    mToolbar.setTitle(R.string.agenda_view);
                }
                break;
            case ViewType.DAY:
                radioGroup.check(R.id.btn1);
                // mNavigationView.getMenu().findItem(R.id.day_menu_item).setChecked(true);
                frag = DayFragment.newInstance(timeMillis, 1);
                if (mIsTabletConfig) {
                    mToolbar.setTitle(R.string.day_view);
                }
                break;
            case ViewType.MONTH:
                radioGroup.check(R.id.btn3);
                //  mNavigationView.getMenu().findItem(R.id.month_menu_item).setChecked(true);
                frag = new MonthByWeekFragment(timeMillis, false);
                if (mShowAgendaWithMonth) {
                    secFrag = new AgendaFragment(timeMillis, false);
                }
                if (mIsTabletConfig) {
                    mToolbar.setTitle(R.string.month_view);
                }
                break;
            case ViewType.WEEK:
            default:
                radioGroup.check(R.id.btn2);
                //  mNavigationView.getMenu().findItem(R.id.week_menu_item).setChecked(true);
                frag = DayFragment.newInstance(timeMillis, 7);
                if (mIsTabletConfig) {
                    mToolbar.setTitle(R.string.week_view);
                }
                break;
        }
        // Update the current view so that the menu can update its look according to the
        // current view.
        if (mCalendarToolbarHandler != null) {
            mCalendarToolbarHandler.setCurrentMainView(viewType);
        }

        if (!mIsTabletConfig) {
            refreshActionbarTitle(timeMillis);
        }



        // Show date only on tablet configurations in views different than Agenda
//        if (!mIsTabletConfig) {
//            mDateRange.setVisibility(View.GONE);
//        } else if (viewType != ViewType.AGENDA) {
//            mDateRange.setVisibility(View.VISIBLE);
//        } else {
//            mDateRange.setVisibility(View.GONE);
//        }

        // Clear unnecessary buttons from the option menu when switching from the agenda view
        if (viewType != ViewType.AGENDA) {
            clearOptionsMenu();
        }

        boolean doCommit = false;
        if (ft == null) {
            doCommit = true;
            ft = fragmentManager.beginTransaction();
        }

        if (doTransition) {
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        }

        ft.replace(viewId, frag);
        if (mShowAgendaWithMonth) {

            // Show/hide secondary fragment

            if (secFrag != null) {
                ft.replace(R.id.secondary_pane, secFrag);
                mSecondaryPane.setVisibility(View.VISIBLE);
            } else {
                mSecondaryPane.setVisibility(View.GONE);
                Fragment f = fragmentManager.findFragmentById(R.id.secondary_pane);
                if (f != null) {
                    ft.remove(f);
                }
                mController.deregisterEventHandler(R.id.secondary_pane);
            }
        }
        if (DEBUG) {
            Log.d(TAG, "Adding handler with viewId " + viewId + " and type " + viewType);
        }
        // If the key is already registered this will replace it
        mController.registerEventHandler(viewId, (EventHandler) frag);
        if (secFrag != null) {
            mController.registerEventHandler(viewId, (EventHandler) secFrag);
        }

        if (doCommit) {

            ft.commit();
        }
    }

    private void refreshActionbarTitle(long timeMillis) {
        if (mCalendarToolbarHandler != null) {
            mCalendarToolbarHandler.setTime(timeMillis);
        }
    }

    private void setTitleInActionBar(EventInfo event) {
        if (event.eventType != EventType.UPDATE_TITLE) {
            return;
        }

        final long start = event.startTime.toMillis(false /* use isDst */);
        final long end;
        if (event.endTime != null) {
            end = event.endTime.toMillis(false /* use isDst */);
        } else {
            end = start;
        }

        final String msg = Utils.formatDateRange(getActivity(), start, end, (int) event.extraLong);
//        CharSequence oldDate = mDateRange.getText();
//        mDateRange.setText(msg);
        updateSecondaryTitleFields(event.selectedTime != null ? event.selectedTime.toMillis(true)
                : start);
//        if (!TextUtils.equals(oldDate, msg)) {
//            mDateRange.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED);
//            if (mShowWeekNum ) {
////                mWeekTextView.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED);
//            }
//        }
    }

    private void updateSecondaryTitleFields(long visibleMillisSinceEpoch) {
        mShowWeekNum = Utils.getShowWeekNumber(getActivity());
        mTimeZone = Utils.getTimeZone(getActivity(), mHomeTimeUpdater);
        if (visibleMillisSinceEpoch != -1) {
            int weekNum = Utils.getWeekNumberFromTime(visibleMillisSinceEpoch, getActivity());
            mWeekNum = weekNum;
        }

//        if (mShowWeekNum && (mCurrentView == ViewType.WEEK) && mIsTabletConfig
//                && mWeekTextView != null) {
//            String weekString = getResources().getQuantityString(R.plurals.weekN, mWeekNum,
//                    mWeekNum);
//            mWeekTextView.setText(weekString);
//            mWeekTextView.setVisibility(View.VISIBLE);
//        } else
            if (visibleMillisSinceEpoch != -1
                && mCurrentView == ViewType.DAY && mIsTabletConfig) {
            Time time = new Time(mTimeZone);
            time.set(visibleMillisSinceEpoch);
            int julianDay = Time.getJulianDay(visibleMillisSinceEpoch, time.gmtoff);
            time.setToNow();
            int todayJulianDay = Time.getJulianDay(time.toMillis(false), time.gmtoff);
            String dayString = Utils.getDayOfWeekString(julianDay, todayJulianDay,
                    visibleMillisSinceEpoch, getActivity());
//            mWeekTextView.setText(dayString);
//            mWeekTextView.setVisibility(View.VISIBLE);
       }
//       else if (mWeekTextView != null && (!mIsTabletConfig || mCurrentView != ViewType.DAY)) {
////            mWeekTextView.setVisibility(View.GONE);
//        }

        if (mHomeTime != null
                && (mCurrentView == ViewType.DAY || mCurrentView == ViewType.WEEK
                || mCurrentView == ViewType.AGENDA)
                && !TextUtils.equals(mTimeZone, Time.getCurrentTimezone())) {
            Time time = new Time(mTimeZone);
            time.setToNow();
            long millis = time.toMillis(true);
            boolean isDST = time.isDst != 0;
            int flags = DateUtils.FORMAT_SHOW_TIME;
            if (DateFormat.is24HourFormat(getActivity())) {
                flags |= DateUtils.FORMAT_24HOUR;
            }
            // Formats the time as
            String timeString = (new StringBuilder(
                    Utils.formatDateRange(getActivity(), millis, millis, flags))).append(" ").append(
                    TimeZone.getTimeZone(mTimeZone).getDisplayName(
                            isDST, TimeZone.SHORT, Locale.getDefault())).toString();
            mHomeTime.setText(timeString);
            mHomeTime.setVisibility(View.VISIBLE);
            // Update when the minute changes
            mHomeTime.removeCallbacks(mHomeTimeUpdater);
            mHomeTime.postDelayed(
                    mHomeTimeUpdater,
                    DateUtils.MINUTE_IN_MILLIS - (millis % DateUtils.MINUTE_IN_MILLIS));
        } else if (mHomeTime != null) {
            mHomeTime.setVisibility(View.GONE);
        }
    }

    @Override
    public long getSupportedEventTypes() {
        return EventType.GO_TO | EventType.VIEW_EVENT | EventType.UPDATE_TITLE;
    }

    @Override
    public void handleEvent(final EventInfo event) {
        long displayTime = -1;
        if (event.eventType == EventType.GO_TO) {
            if ((event.extraLong & CalendarController.EXTRA_GOTO_BACK_TO_PREVIOUS) != 0) {
                mBackToPreviousView = true;
            } else if (event.viewType != mController.getPreviousViewType()
                    && event.viewType != ViewType.EDIT) {
                // Clear the flag is change to a different view type
                mBackToPreviousView = false;
            }

            setMainPane(
                    null, R.id.main_pane, event.viewType, event.startTime.toMillis(false), false);
            if (mSearchView != null) {
                mSearchView.clearFocus();
            }
            if (mShowCalendarControls) {
                int animationSize = (mOrientation == Configuration.ORIENTATION_LANDSCAPE) ?
                        mControlsAnimateWidth : mControlsAnimateHeight;
                boolean noControlsView = event.viewType == ViewType.MONTH || event.viewType == ViewType.AGENDA;
                if (mControlsMenu != null) {
                    mControlsMenu.setVisible(!noControlsView);
                    mControlsMenu.setEnabled(!noControlsView);
                }
                if (noControlsView || mHideControls) {
                    // hide minimonth and calendar frag
                    mShowSideViews = false;
                    if (!mHideControls) {
                        final ObjectAnimator slideAnimation = ObjectAnimator.ofInt(this,
                                "controlsOffset", 0, animationSize);
                        slideAnimation.addListener(mSlideAnimationDoneListener);
                        slideAnimation.setDuration(mCalendarControlsAnimationTime);
                        ObjectAnimator.setFrameDelay(0);
                        slideAnimation.start();
                    } else {
                        mMiniMonth.setVisibility(View.GONE);
                        mCalendarsList.setVisibility(View.GONE);
                        mMiniMonthContainer.setVisibility(View.GONE);
                    }
                } else {
                    // show minimonth and calendar frag
                    mShowSideViews = true;
                    mMiniMonth.setVisibility(View.VISIBLE);
                    mCalendarsList.setVisibility(View.VISIBLE);
                    mMiniMonthContainer.setVisibility(View.VISIBLE);
                    if (!mHideControls &&
                            (mController.getPreviousViewType() == ViewType.MONTH ||
                                    mController.getPreviousViewType() == ViewType.AGENDA)) {
                        final ObjectAnimator slideAnimation = ObjectAnimator.ofInt(this,
                                "controlsOffset", animationSize, 0);
                        slideAnimation.setDuration(mCalendarControlsAnimationTime);
                        ObjectAnimator.setFrameDelay(0);
                        slideAnimation.start();
                    }
                }
            }
            displayTime = event.selectedTime != null ? event.selectedTime.toMillis(true)
                    : event.startTime.toMillis(true);
            if (!mIsTabletConfig) {
                refreshActionbarTitle(displayTime);
            }
        } else if (event.eventType == EventType.VIEW_EVENT) {

            // If in Agenda view and "show_event_details_with_agenda" is "true",
            // do not create the event info fragment here, it will be created by the Agenda
            // fragment

            if (mCurrentView == ViewType.AGENDA && mShowEventDetailsWithAgenda) {
                if (event.startTime != null && event.endTime != null) {
                    // Event is all day , adjust the goto time to local time
                    if (event.isAllDay()) {
                        Utils.convertAlldayUtcToLocal(
                                event.startTime, event.startTime.toMillis(false), mTimeZone);
                        Utils.convertAlldayUtcToLocal(
                                event.endTime, event.endTime.toMillis(false), mTimeZone);
                    }
                    mController.sendEvent(this, EventType.GO_TO, event.startTime, event.endTime,
                            event.selectedTime, event.id, ViewType.AGENDA,
                            CalendarController.EXTRA_GOTO_TIME, null, null);
                } else if (event.selectedTime != null) {
                    mController.sendEvent(this, EventType.GO_TO, event.selectedTime,
                            event.selectedTime, event.id, ViewType.AGENDA);
                }
            } else {
                // TODO Fix the temp hack below: && mCurrentView !=
                // ViewType.AGENDA
                if (event.selectedTime != null && mCurrentView != ViewType.AGENDA) {
                    mController.sendEvent(this, EventType.GO_TO, event.selectedTime,
                            event.selectedTime, -1, ViewType.CURRENT);
                }
                final int response = event.getResponse();
                if ((mCurrentView == ViewType.AGENDA && mShowEventInfoFullScreenAgenda) ||
                        ((mCurrentView == ViewType.DAY || (mCurrentView == ViewType.WEEK) ||
                                mCurrentView == ViewType.MONTH) && mShowEventInfoFullScreen)){
                    // start event info as activity
                     intent = new Intent(Intent.ACTION_VIEW);
                    final Uri eventUri = ContentUris.withAppendedId(Events.CONTENT_URI, event.id);

                    final Cursor cursor = getActivity().getContentResolver()
                            .query(
                                    Uri.parse("content://com.android.calendar/events/" + event.id),
                                    new String[] { "calendar_id","sync_data1", "title","dtstart", "dtend", "eventColor",
                                            "eventLocation", "allDay", "calendar_color" },
                                    null,
                                    null, null);
                    assert cursor != null;
                    cursor.moveToFirst();
                    AccountManager am = (AccountManager) getActivity().getSystemService(Context.ACCOUNT_SERVICE);
                    Account[] accountsFromFirstApp = am.getAccountsByType(AppAccountManager.ACCOUNT_TYPE);
                    String calendarId = am.getUserData(accountsFromFirstApp[0], AppAccountManager.USER_DATA_CALENDAR);


//                    Log.d("aaaaa", "color: "+cursor.getString(8)+ " hexColor: "+hexColor);
                    if(calendarId.equalsIgnoreCase(cursor.getString(0))){
//                        Log.d("aaaaaa", "idSC: "+cursor.getString(1));
                        intent = new Intent(getActivity(), EventDetailActivity.class);
                        intent.putExtra(EventDetailActivity.M_DETAILS_HOME_ACTIVITY, cursor.getString(1));
                        startActivity(intent);
                    }else {
//                        intent=new Intent(getActivity(), CreateEventActivity.class);
//                        startActivity(intent);

                        dialogIntent = new Dialog(getActivity());
                        dialogIntent.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                        dialogIntent.setContentView(R.layout.custom_dialog_event);
                        dialogIntent.setContentView(R.layout.custom_dialog_intent_event);
                        dialogIntent.show();
                        TextView tvDayStart = (TextView) dialogIntent.findViewById(R.id.tv_day_start);
                        TextView tvTitle = (TextView) dialogIntent.findViewById(R.id.tv_title);
                        LinearLayout lnDay = (LinearLayout) dialogIntent.findViewById(R.id.ln_day);
                        TextView tvTimeStart = (TextView) dialogIntent.findViewById(R.id.tv_time_start);
                        TextView tvTimeEnd = (TextView) dialogIntent.findViewById(R.id.tv_time_end);
                        TextView tvAllday = (TextView) dialogIntent.findViewById(R.id.tv_all_day);
                        ImageView imEvent = (ImageView) dialogIntent.findViewById(R.id.im_color_event);

                        SimpleDateFormat dfStart = null, dfEnd = null ;
                        long dateStart = Long.parseLong(cursor.getString(3));
                        long dateEnd = 0;

                        try {
                            dateEnd = Long.parseLong(cursor.getString(4));
                        } catch (NumberFormatException e) {
                            MyLog.w(e.getMessage());
                        }

                        // date Start
                        dfStart = new SimpleDateFormat("E dd/MM/yyyy", Locale.getDefault());
                        String strDateStart = dfStart.format(dateStart);
                        tvDayStart.setText(strDateStart);
                        tvTitle.setText(cursor.getString(2));

                        String hexColor = String.format("#%06X", (0xFFFFFF & Integer.parseInt(cursor.getString(8))));
                        imEvent.setBackgroundColor(Color.parseColor(hexColor));

                        if(cursor.getString(7).equalsIgnoreCase("1")){
                            lnDay.setVisibility(View.GONE);
                            tvAllday.setVisibility(View.VISIBLE);
                        }else{
                            lnDay.setVisibility(View.VISIBLE);
                            tvAllday.setVisibility(View.GONE);

                            SimpleDateFormat SDTTimeStart = new SimpleDateFormat("hh:mm a", Locale.getDefault());
                            String strTimeStart = SDTTimeStart.format(dateStart);
                            tvTimeStart.setText(strTimeStart);

                            if(dateEnd > 0) {
                                // hour end
                                SimpleDateFormat SDTimeEnd = new SimpleDateFormat("hh:mm a", Locale.getDefault());
                                String strTimeEnd = SDTimeEnd.format(dateEnd);
                                tvTimeEnd.setText(strTimeEnd);
                            }
                        }
//*************************************************************************************************************************************
//                        dialogIntent = new Dialog(getActivity());
//                        dialogIntent.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                        dialogIntent.setContentView(R.layout.custom_dialog_intent_event);
//                        dialogIntent.show();
//
//                        TextView tv_message = (TextView) dialogIntent .findViewById(R.id.txt_dia);
//                        tv_message.setText(getActivity().getResources().getString(R.string.event_not_tigo));
//
//                        Button bt_yes = (Button)dialogIntent.findViewById(R.id.btn_yes);
//                        Button bt_no = (Button)dialogIntent.findViewById(R.id.btn_no);
//
//                        bt_yes.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                intent.setData(eventUri);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT |
//                                        Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                                intent.putExtra(EXTRA_EVENT_BEGIN_TIME, event.startTime.toMillis(false));
//                                intent.putExtra(EXTRA_EVENT_END_TIME, event.endTime.toMillis(false));
//                                intent.putExtra(ATTENDEE_STATUS, response);
//                                Log.d("aaaaaa", "startDate: "+ event.startTime.toMillis(false)+" DateEnd: "+event.endTime.toMillis(false)
//                                    + " title: "+ cursor.getString(2));
//
////                                startActivity(intent);
//                            }
//                        });
//                        bt_no.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                dialogIntent.dismiss();
//                            }
//                        });
                        

                    }
                } else {
                    // start event info as a dialog

                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    // if we have an old popup replace it
                    Fragment fOld = fm.findFragmentByTag(EVENT_INFO_FRAGMENT_TAG);
                    if (fOld != null && fOld.isAdded()) {
                        ft.remove(fOld);
                    }

                    ft.commit();
                }
            }
            displayTime = event.startTime.toMillis(true);
        } else if (event.eventType == EventType.UPDATE_TITLE) {
            setTitleInActionBar(event);
            if (!mIsTabletConfig) {
                refreshActionbarTitle(mController.getTime());
            }
        }
        updateSecondaryTitleFields(displayTime);
    }


    // Needs to be in proguard whitelist
    // Specified as listener via android:onClick in a layout xml
    public void handleSelectSyncedCalendarsClicked(View v) {
        mController.sendEvent(this, EventType.LAUNCH_SETTINGS, null, null, null, 0, 0,
                CalendarController.EXTRA_GOTO_TIME, null,
                null);
    }

    @Override
    public void eventsChanged() {
        mController.sendEvent(this, EventType.EVENTS_CHANGED, null, null, -1, ViewType.CURRENT);
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        mSearchMenu.collapseActionView();
        mController.sendEvent(this, EventType.SEARCH, null, null, -1, ViewType.CURRENT, 0, query,
                getActivity().getComponentName());
        return true;
    }

    @Override
    public boolean onSuggestionSelect(int position) {
        return false;
    }
    @Override
    public boolean onSuggestionClick(int position) {
        mSearchMenu.collapseActionView();
        return false;
    }



    private class QueryHandler extends AsyncQueryHandler {
        public QueryHandler(ContentResolver cr) {
            super(cr);
        }
        @Override
        protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
            mCheckForAccounts = false;
            try {
                // If the query didn't return a cursor for some reason return
                if (cursor == null || cursor.getCount() > 0 || getActivity().isFinishing()) {
                    return;
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
            Bundle options = new Bundle();
            options.putCharSequence("introMessage",
                    getResources().getString(R.string.create_an_account_desc));
            options.putBoolean("allowSkip", true);
            AccountManager am = AccountManager.get(getActivity());
            am.addAccount("com.google", CalendarContract.AUTHORITY, null, options,
                    getActivity(),
                    new AccountManagerCallback<Bundle>() {
                        @Override
                        public void run(AccountManagerFuture<Bundle> future) {
                            if (future.isCancelled()) {
                                return;
                            }
                            try {
                                Bundle result = future.getResult();
                                boolean setupSkipped = result.getBoolean("setupSkipped");

                                if (setupSkipped) {
                                    Utils.setSharedPreference(getActivity(),
                                            GeneralPreferences.KEY_SKIP_SETUP, true);
                                }

                            } catch (OperationCanceledException ignore) {
                                // The account creation process was canceled
                            } catch (IOException ignore) {
                            } catch (AuthenticatorException ignore) {
                            }
                        }
                    }, null);
        }
    }
}

