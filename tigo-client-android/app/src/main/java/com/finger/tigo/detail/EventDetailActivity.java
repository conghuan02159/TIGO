package com.finger.tigo.detail;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.finger.tigo.BaseActivity;
import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.app.ApiExecuter;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.app.AppIntent;
import com.finger.tigo.common.model.EventItem;
import com.finger.tigo.detail.adapter.RecyclerAdapterComment;
import com.finger.tigo.detail.fragment.MySupportMapFragment;
import com.finger.tigo.home.ViewPagerImageActivity;
import com.finger.tigo.home.adapter.RecyclerAdapterHome;
import com.finger.tigo.invite.InviteActivity;
import com.finger.tigo.invite.ListPaticipationActivity;
import com.finger.tigo.items.FeedItem;
import com.finger.tigo.menu.UpdateEventActivity;
import com.finger.tigo.remittance.RemittanceBroadcastReceiver;
import com.finger.tigo.session.SessionManager;
import com.finger.tigo.util.DateUtil;
import com.finger.tigo.util.MyLog;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class EventDetailActivity extends BaseActivity implements EventDetailView, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, CommentAPIListener  {


    public static final String M_DETAILS_HOME_ACTIVITY = "mDetailsHomeActivity";
    public static final String POST_ITEM_HOME = "mPostItemHome";
    public static final String RES_LOAD_DETAILS_HOME = "resLoadDetailsHome";
    public static final String STATE_JOIN = "stateJoin";
    public static final String INFO_EVENT = "infoEvent";
    public static final String POST_JOIN_OR_NOT_JOIN = "postJoinOrNotJoin";
    public static final String POST_ACCEPT = "postaccept";
    public static final String ACCEPT = "accept";
    public static final String CANCEL = "delete";

    public static int MY_PERMISSIONS_REQUEST_CALL_PHONE = 1234;

    public   String LOCATION_NAME = "mLocationName";
    public   String POSITION = "mPosition";
    public   String IMAGE_EVENT = "mImageEvent";

    public static final String PUT_EVENT = "putEvent";

    public String countComment;
    public static String idSc;


    public  double lg1;
    public  double lg2;
    private String mEventColor;
    private boolean mAllDay;

    ViewGroup contentContainer;
    NestedScrollView nestedScrollView;
    TextView nameEvents;
    ImageView btJoin;
    TextView mday;
    TextView details_inter_number;
    TextView mMonth;
    TextView location;
    TextView position;
    TextView daystart;
    TextView timeInterval;
    TextView userName;
    Toolbar toolbar;
    CollapsingToolbarLayout collapsingToolbar;
    ImageView imEvent;
    ImageView btn_accep_invite;
    ImageView btn_cancel_invite;
    RecyclerView recyclerView;
    TextView count_comment;
    TextView txtSeeAllComment;
    TextView textJoin;
    Button ReptryConnect;
    LinearLayout linearLayout_map;

    LinearLayout layout_particition, layout_invite, layout_call,layout_remittance,layoutHor;

    RelativeLayout layoutJoin;

    private List<FeedItem> feedItems;
    private RecyclerAdapterComment listAdapter;

    private String phone;

    private String strEvent;
    String idUser;
    String state;
    String postItem;
    SessionManager session;

    SessionManager sessionManager;
    EventDetailPresenter mEventDetailPresenter;
    ProgressDialog pDialog;

    private GoogleApiClient client;

    LocationManager locationManager;

    MySupportMapFragment mapFragment;
    private GoogleMap mMap;
    LatLng location_out;

    LocationRequest mLocationRequest;
    private RemittanceBroadcastReceiver mRemittanceBroadcastReceiver;

    private AppBarLayout appbarLayout;
    private boolean mTouchedScreen;
    private long mDayStart;
    private long mDayEnd;
    private EventItem mEventData;
    LinearLayout layoutNotConnect;
    LinearLayout layoutDetails;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_home);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        collapsingToolbar = (CollapsingToolbarLayout)findViewById(R.id.toolbar_layout);
        appbarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        appbarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset)-appBarLayout.getTotalScrollRange() == 0) {
                    toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                } else
                {
                    toolbar.setBackgroundColor(getResources().getColor(R.color.event_detail_header_transparent));
                }
            }
        });

        contentContainer = (ViewGroup) findViewById(R.id.content_container);
        contentContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // solution "stopped scroll finger release" - Mr.Kim
                // do nothing
            }
        });
        nestedScrollView = (NestedScrollView)findViewById(R.id.nested_scroll_view);
        nameEvents = (TextView)findViewById(R.id.eventname);
        btJoin = (ImageView)findViewById(R.id.btJoin);
        mday = (TextView)findViewById(R.id.day);
        details_inter_number = (TextView)findViewById(R.id.embark);
        mMonth = (TextView)findViewById(R.id.month);
        location = (TextView)findViewById(R.id.location);
        position = (TextView)findViewById(R.id.position);
        daystart = (TextView)findViewById(R.id.daystart);
        timeInterval = (TextView)findViewById(R.id.time_interval);
        userName = (TextView)findViewById(R.id.nameUser);
        layoutNotConnect = (LinearLayout) findViewById(R.id.layout_not_connect);
        layoutDetails = (LinearLayout) findViewById(R.id.layout_details_event);
        ReptryConnect = (Button) findViewById(R.id.bt_reptry_connect);
        count_comment = (TextView)findViewById(R.id.count_comment_detail);
        linearLayout_map = (LinearLayout) findViewById(R.id.LinearLayout_map);

        imEvent = (ImageView)findViewById(R.id.image_event);
        btn_accep_invite= (ImageView)findViewById(R.id.btn_accep_invite);
        btn_cancel_invite = (ImageView)findViewById(R.id.btn_cancel_invite);
        recyclerView = (RecyclerView)findViewById(R.id.listcomment);
//        textEvented=(TextView)findViewById(R.id.textEvented);
        nestedScrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mTouchedScreen = !(event.getAction() == MotionEvent.ACTION_UP);
                return false;
            }
        });
        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//                MyLog.i(scrollX + ", " + scrollY + " || " + oldScrollX + ", " + oldScrollY + ", mTouchedScreen=" + mTouchedScreen);

                    if (mapFragment != null) {
                        mapFragment.setFocused(false);
                    }


                // Set the appbarLayout to expand at scroll top.
                if(oldScrollY > 0 && scrollY == 0 && !mTouchedScreen) {
                    appbarLayout.setExpanded(true, true);
                }
            }
        });


        txtSeeAllComment = (TextView)findViewById(R.id.details_comment);
        textJoin = (TextView)findViewById(R.id.txt_join);
        layout_particition = (LinearLayout)findViewById(R.id.layout_particition);
        layout_invite = (LinearLayout)findViewById(R.id.layout_invite);
        layout_call = (LinearLayout)findViewById(R.id.layout_call);
        layout_remittance = (LinearLayout)findViewById(R.id.layout_remittance);

        layoutJoin = (RelativeLayout) findViewById(R.id.layout_join);
        layoutHor=(LinearLayout)findViewById(R.id.layoutHor);

        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        collapsingToolbar.setTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        sessionManager = new SessionManager(this);
        mEventDetailPresenter = new EventDetailPresenter(this);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        feedItems = new ArrayList<>();
        session = new SessionManager(this);
        idUser = AppAccountManager.getAppAccountUserId(this);

        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        if(b !=null) {
            idSc = b.getString(M_DETAILS_HOME_ACTIVITY);
            postItem = b.getString(POST_ITEM_HOME);

            String checkWatch = b.getString("clickNotification");
            if(checkWatch !=null){
                String typeNoti = b.getString(AppIntent.ACTION_TYPE_NOTI);
                String idNoti =  b.getString(AppIntent.ACTION_ID_NOTI);
                MyLog.d("watched: ", typeNoti+" idnoty: "+idNoti);
                if(checkWatch.equalsIgnoreCase("watched_2_3_6_5"))
                    postwatching(idUser,idNoti, AppConfig.URL_WATCHING_DETAIL,typeNoti);
                else{
                    postwatching(idUser,  idNoti, AppConfig.URL_WATCHING_TBLIKE, typeNoti);
                }
            }

        }

        locationManager = (LocationManager)this.getSystemService(LOCATION_SERVICE);
        client = new GoogleApiClient.Builder(EventDetailActivity.this).addApi(AppIndex.API).build();


        // iduser and idsc
        showProgress(getResources().getString(R.string.loading));

        mEventDetailPresenter.attemptGetEventDetail(this, idUser, idSc);

        txtSeeAllComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(EventDetailActivity.this, CommentActivity.class);
                i.putExtra("mDetailsHomeActivity", idSc);
                i.putExtra("countcomment", countComment);
                startActivity(i);
            }
        });
        btJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JoinOrNotJoin();
            }
        });
        layout_particition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityJoin();
            }
        });
        layout_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Invite();
            }
        });
        layout_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call();
            }
        });
        btn_accep_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click_accep_invite();
            }
        });
        btn_cancel_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click_cancel_invite();
            }
        });
        layout_remittance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickRemittance();
            }
        });
        initRecyclerView();
        // connect
        ReptryConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(getResources().getString(R.string.loading));
                mEventDetailPresenter.attemptGetEventDetail(EventDetailActivity.this, idUser, idSc);

            }
        });
    }

    private void initRecyclerView() {
        feedItems = new ArrayList<>();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(EventDetailActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.addItemDecoration(new DividerItemDecoration(EventDetailActivity.this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        listAdapter = new RecyclerAdapterComment(feedItems,EventDetailActivity.this, this);

        recyclerView.setAdapter(listAdapter);

        downloadComment(idSc);
    }

//    public void setToastJoin(String text) {
//        if(toastJoin!=null)
//            toastJoin.cancel();
//        toastJoin = Toast.makeText(this,"" , Toast.LENGTH_SHORT);
//        toastJoin.setText(getResources().getString(R.string.toast_joint)+" "+ text);
//        toastJoin.show();
//    }

    @Override
    public void onSuccess(String apiCode, final EventItem eventItem) {
        if (apiCode.equalsIgnoreCase(INFO_EVENT)) {
            hideProgress();
            layoutDetails.setVisibility(View.VISIBLE);
            layoutNotConnect.setVisibility(View.GONE);
            imEvent.setVisibility(View.VISIBLE);
            state = eventItem.state;
            String checkaccept  = eventItem.CheckInvite;
            if(idUser.equalsIgnoreCase(eventItem.idUser)){
                layoutJoin.setVisibility(View.GONE);
            }else{
                layoutJoin.setVisibility(View.VISIBLE);
            }
            //co loi moi tham gia su kien
            if (checkaccept.equalsIgnoreCase("1")) {
                btJoin.setVisibility(View.GONE);
                btn_accep_invite.setVisibility(View.VISIBLE);
                //image join blue
                btn_accep_invite.setImageResource(R.drawable.not_join_event);
                textJoin.setText(getResources().getString(R.string.accept));

            } else if(checkaccept.equalsIgnoreCase("0")) {
                btJoin.setVisibility(View.VISIBLE);
                btn_accep_invite.setVisibility(View.GONE);

                if (state.equalsIgnoreCase("1")) {
                    //hien thi da tham gia roi

                    btJoin.setImageResource(R.drawable.joint_event);
                    textJoin.setText(getResources().getString(R.string.not_join));
                    //    Toast.makeText(this, "da tham gia", Toast.LENGTH_SHORT).show();

                } else {
                    //hien thi chua tham gia
                    textJoin.setText(getResources().getString(R.string.join));
                    btJoin.setImageResource(R.drawable.not_join_event);
                    //      Toast.makeText(this, "chua tham gia", Toast.LENGTH_SHORT).show();
                }

            }

            userName.setText(getResources().getString(R.string.created_by)+" "+eventItem.userName);

            MyLog.d("image null" ,eventItem.image);
            if (!eventItem.image.isEmpty()) {
                imEvent.setVisibility(View.VISIBLE);
                Glide.with(this).load(eventItem.image).animate(android.R.anim.fade_in)
                        .error(R.drawable.background_hoa_tigo)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.background_hoa_tigo)
                        .into(imEvent);

                imEvent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(EventDetailActivity.this, ViewPagerImageActivity.class);
                        i.putExtra(RecyclerAdapterHome.PUT_IMAGE, eventItem.image);
                        startActivity(i);
                    }
                });
            } else {
                MyLog.d("image null" ,eventItem.image);
                imEvent.setVisibility(View.GONE);
            }


            nameEvents.setText(eventItem.status);
            toolbar.setTitle(eventItem.status);
            location.setText(eventItem.location);
            LOCATION_NAME  = eventItem.location;
            position.setText(eventItem.position);
            POSITION = eventItem.position;
            IMAGE_EVENT = eventItem.image;
            mEventColor = eventItem.color;
            mAllDay = eventItem.allDay;

            phone = eventItem.phone;
            details_inter_number.setText(eventItem.countJoin);

            mDayStart = Long.parseLong(eventItem.dayStart);
            SimpleDateFormat dft = new SimpleDateFormat("E, dd MMM yyyy", Locale.getDefault());
            SimpleDateFormat month = new SimpleDateFormat("MMM", Locale.getDefault());
            String strMonth = month.format(mDayStart);
            SimpleDateFormat date = new SimpleDateFormat("dd", Locale.getDefault());
            String stDay = date.format(mDayStart);

            mday.setText(stDay);
            mMonth.setText(strMonth);
            SimpleDateFormat tft = new SimpleDateFormat("HH:mm a", Locale.getDefault());

            mDayEnd = Long.parseLong(eventItem.dayFinish);
            Calendar curent = Calendar.getInstance();

            if(curent.getTimeInMillis()> mDayEnd){
                layout_particition.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(EventDetailActivity.this,getResources().getString(R.string.list_invite),Toast.LENGTH_SHORT).show();
                    }
                });
                layout_invite.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(EventDetailActivity.this,getResources().getString(R.string.function_invite),Toast.LENGTH_SHORT).show();
                    }
                });
                layout_call.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(EventDetailActivity.this,getResources().getString(R.string.function_call),Toast.LENGTH_SHORT).show();
                    }
                });
                layout_remittance.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(EventDetailActivity.this,getResources().getString(R.string.function_money),Toast.LENGTH_SHORT).show();
                    }
                });
                layoutJoin.setEnabled(false);
//                textEvented.setVisibility(View.VISIBLE);
                imEvent.setEnabled(false);
                btJoin.setEnabled(false);
                btn_accep_invite.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(EventDetailActivity.this,getResources().getString(R.string.list_invite_one),Toast.LENGTH_SHORT).show();
                    }
                });
                btn_cancel_invite.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(EventDetailActivity.this,getResources().getString(R.string.list_invite_two),Toast.LENGTH_SHORT).show();
                    }
                });

            }else{
                layout_particition.setEnabled(true);
                layout_invite.setEnabled(true);
                layout_call.setEnabled(true);
                layout_remittance.setEnabled(true);
                layoutJoin.setEnabled(true);
//                textEvented.setVisibility(View.GONE);
                imEvent.setEnabled(true);
                btJoin.setEnabled(true);
                btn_accep_invite.setEnabled(true);
                btn_cancel_invite.setEnabled(true);
            }

            MyLog.d("startDateTime=" + DateUtil.getDateTimeString(mDayStart) + ", endDateTime=" + DateUtil.getDateTimeString(mDayEnd));

            String de = dft.format(mDayStart) + " - " + dft.format(mDayEnd);
            String te = tft.format(mDayStart) + " - " + tft.format(mDayEnd);
            strEvent = eventItem.status + ", " + daystart.getText() + ": " + timeInterval.getText();

            daystart.setText(de);

            if(mAllDay) {
                timeInterval.setText(R.string.all_day);
            } else {
                timeInterval.setText(te);
            }
            MyLog.d("hhhhh",eventItem.position);

            if (!eventItem.position.isEmpty()) {
                MyLog.d("hhhhh1",eventItem.position);
                linearLayout_map.setVisibility(View.VISIBLE);
                lg1 = eventItem.lg1;
                lg2 = eventItem.lg2;

                initMap();
            } else {
                MyLog.d("hhhhh2",eventItem.position);
                linearLayout_map
                        .setVisibility(View.GONE);
            }




            mEventData = eventItem;

            if(AppAccountManager.getAppAccountUserId(this).equals(eventItem.idUser)) {
                // my post
                toolbar.getMenu().getItem(0).setVisible(true);
            }

        } else if (apiCode.equalsIgnoreCase(POST_JOIN_OR_NOT_JOIN)) {
            hideProgress();
            state = eventItem.state;

            if (state.equalsIgnoreCase("1")) {
                // Joined
                int co = Integer.parseInt(details_inter_number.getText().toString()) + 1;
                details_inter_number.setText("" + co);

                btJoin.setImageDrawable(getResources().getDrawable(R.drawable.joint_event));
                textJoin.setText(getResources().getString(R.string.not_join));
                //   setToastJoin(getResources().getString(R.string.join));

                MyLog.d("mAllDay= " + mAllDay);
                AppAccountManager.updateCalendarSync(this, idSc, mEventColor, mAllDay,
                        mDayStart+"", mDayEnd+"", nameEvents.getText().toString(), LOCATION_NAME);

            } else {
                // Not Joined
                int co = Integer.parseInt(details_inter_number.getText().toString()) - 1;
                details_inter_number.setText("" + co);
                textJoin.setText(getResources().getString(R.string.join));
                //      setToastJoin(getResources().getString(R.string.not_join));
                btJoin.setImageDrawable(getResources().getDrawable(R.drawable.not_join_event));

                AppAccountManager.deleteCalendarSync(this, idSc.split("-")[0]);
            }

            Intent a = new Intent();
            a.setAction(AppIntent.ACTION_RELOAD_COUNT_COMMENT);
            a.putExtra(RES_LOAD_DETAILS_HOME, true);
            a.putExtra(STATE_JOIN, state);
            a.putExtra(POST_ITEM_HOME, postItem);
            getApplication().sendBroadcast(a);



        }else if (apiCode.equalsIgnoreCase(POST_ACCEPT)) {
            hideProgress();

            String checkinvite = eventItem.state;

            if (checkinvite.equalsIgnoreCase("1")) {
                btn_accep_invite.setVisibility(View.GONE);
                btn_cancel_invite.setVisibility(View.VISIBLE);
                btJoin.setVisibility(View.GONE);
                textJoin.setText(getResources().getString(R.string.cancelnot));
                //     setToastJoin(getResources().getString(R.string.not_join));
                btn_cancel_invite.setImageResource(R.drawable.joint_event);

            } else {

                btn_cancel_invite.setVisibility(View.GONE);
                btn_accep_invite.setVisibility(View.GONE);
                btJoin.setVisibility(View.VISIBLE);
                btJoin.setImageResource(R.drawable.not_join_event);

                textJoin.setText(getResources().getString(R.string.join));
                //      setToastJoin(getResources().getString(R.string.accept));

            }
        }
    }

    @Override
    public void onError(String error, String errMessage, Throwable throwable) {
        hideProgress();
        showToast(errMessage);
        imEvent.setVisibility(View.GONE);
        layoutDetails.setVisibility(View.GONE);
        layoutNotConnect.setVisibility(View.VISIBLE);
    }

    // accept or rufust when invited.


    public void click_accep_invite(){
        mEventDetailPresenter.attemptaccepivite(this, idUser,idSc, ACCEPT);
        Toast.makeText(this, "click accept", Toast.LENGTH_SHORT).show();
        //  btn_cancel_invite.setImageResource(R.drawable.ic_dt_invite);
    }

    public void click_cancel_invite(){
        mEventDetailPresenter.attemptaccepivite(this, idUser,idSc, CANCEL);
        Toast.makeText(this, "click cancel", Toast.LENGTH_SHORT).show();
    }

    public void ActivityJoin() {
        Intent i = new Intent(EventDetailActivity.this, ListPaticipationActivity.class);
        i.putExtra("particition", idSc + "");
        startActivity(i);
    }


    public void JoinOrNotJoin() {
        if (state.equalsIgnoreCase("1")) {
            mEventDetailPresenter.attemptJoinOrNotJoin(this, idUser, idSc, "0", getResources().getString(R.string.not_join));
        } else {
            mEventDetailPresenter.attemptJoinOrNotJoin(this, idUser, idSc, "1", getResources().getString(R.string.join));
        }
    }

    public void Invite() {
        Intent intent = new Intent(EventDetailActivity.this, InviteActivity.class);
        intent.putExtra("detailsHomeActivity", idSc + "");
        intent.putExtra(PUT_EVENT, strEvent);
        startActivity(intent);
    }

    public void Call() {
        if (ContextCompat.checkSelfPermission(EventDetailActivity.this,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(EventDetailActivity.this,
                    Manifest.permission.CALL_PHONE)) {

                ActivityCompat.requestPermissions(EventDetailActivity.this,
                        new String[]{Manifest.permission.CALL_PHONE},
                        MY_PERMISSIONS_REQUEST_CALL_PHONE);
            } else {
                ActivityCompat.requestPermissions(EventDetailActivity.this,
                        new String[]{Manifest.permission.CALL_PHONE},
                        MY_PERMISSIONS_REQUEST_CALL_PHONE);
            }
        } else {
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
            startActivity(intent);
        }
    }

    public void clickRemittance() {
        Intent intent = new Intent(RemittanceBroadcastReceiver.ACTION_SEND_MONEY);
        intent.putExtra(RemittanceBroadcastReceiver.KEY_MONEY_AMOUNT, 50000);

        sendBroadcast(intent);
    }
//    map

    public void initMap(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        } else {

            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                showGPSDisabledAlertToUser();
            }

        }

        mapFragment = (MySupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.mapLocation);

//        mapFragment.getView().setVisibility(View.GONE);

        if(mapFragment != null) {
            mapFragment.setListener(new MySupportMapFragment.OnTouchListener() {
                @Override
                public void onTouch() {
                    if(!mapFragment.isFocused())
                        return;
                    nestedScrollView.requestDisallowInterceptTouchEvent(true);

                }
            });
        }

        if (mapFragment!= null) {
            mapFragment.getMapAsync(this);
        }

        if (client == null) {
            client = new GoogleApiClient.Builder(EventDetailActivity.this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            client.connect();
        }
    }


    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(EventDetailActivity.this);
        alertDialogBuilder.setMessage(getResources().getString(R.string.gps))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.setting), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(callGPSSettingIntent);

                        mapFragment.getMapAsync(EventDetailActivity.this);
                    }
                });
        alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancelnot), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }




    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(EventDetailActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        location_out = new LatLng(lg1, lg2);

        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location_out, 10f));
                return true;
            }
        });


        //Initialize Google Play Services
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(EventDetailActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
                Marker marker =   mMap.addMarker(new MarkerOptions()
                        .position(location_out)
                        .title(LOCATION_NAME)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location_out, 10f));
                mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                    @Override
                    public View getInfoWindow(Marker marker) {
//                        View view = EventDetailActivity.this.getLayoutInflater().inflate(R.layout.marker_layout, null);
//                        ImageView imMaker = (ImageView) view.findViewById(R.id.image_marker);
//                        TextView t1 = (TextView)view.findViewById(R.id.tv_location);
//                        TextView t2 = (TextView)view.findViewById(R.id.tv_position);
//                        t1.setText(LOCATION_NAME);
//                        t2.setText(POSITION);
//                        Picasso.with(EventDetailActivity.this).load(imageEvent)
//                                .error(R.drawable.background_hoa_tigo)
//                                .fit()
//                                .placeholder(R.drawable.background_hoa_tigo)
//                                .into(imMaker);


                        return null;
                    }

                    @Override
                    public View getInfoContents(Marker marker) {

                        return null;
                    }
                });
                marker.showInfoWindow();
                // Getting the name of the best provider
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location_out, 10f));

            }
        }
        else
        {
            buildGoogleApiClient();
            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(location_out)
                    .title(LOCATION_NAME)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                @Override
                public View getInfoWindow(Marker marker) {
                    // inflate view window

                    // set other views content


                    // set image view like this:


                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
//                    View view = EventDetailActivity.this.getLayoutInflater().inflate(R.layout.marker_layout, null);
//                    ImageView imMaker = (ImageView) view.findViewById(R.id.image_marker);
//                    TextView t1 = (TextView)view.findViewById(R.id.tv_location);
//                    TextView t2 = (TextView)view.findViewById(R.id.tv_position);
//                    t1.setText(LOCATION_NAME);
//                    t2.setText(POSITION);
//
////                    Glide.with(EventDetailActivity.this()).load(EventDetailActivity.IMAGE_EVENT).animate(android.R.anim.fade_in)
////                            .error(R.drawable.background_hoa_tigo)
////                            .diskCacheStrategy(DiskCacheStrategy.ALL)
////                            .placeholder(R.drawable.background_hoa_tigo)
////                            .into(imMaker);
//                    Picasso.with(EventDetailActivity.this)                   //Activity context
//                            .load(IMAGE_EVENT)
//                            .fit()                                //This avoided the OutOfMemoryError
//                            .centerCrop()                         //makes image to not stretch
//                            .into(imMaker);
                    return null;
                }
            });
            marker.showInfoWindow();
            // Getting the name of the best provider
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location_out, 10f));
            mMap.setMyLocationEnabled(true);
        }

    }

    protected synchronized void buildGoogleApiClient() {
        client = new GoogleApiClient.Builder(EventDetailActivity.this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        client.connect();
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(EventDetailActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(EventDetailActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(EventDetailActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(EventDetailActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(EventDetailActivity.this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        initMap();
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.permission_denied_location), Toast.LENGTH_LONG).show();
                }

            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }

    private void downloadComment(final String id_sc) {
        ApiExecuter.requestApiNoProgress(AppConfig.URL_TOP_COMMENT, new ApiExecuter.ApiResultListener() {
            @Override
            public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    JSONArray feedArray = jObj.getJSONArray("comments");
                    feedItems.clear();

                    if(feedArray.length() == 0) {
                        count_comment.setText("0");
                    } else {
                        for (int i = 0; i < feedArray.length(); i++) {
                            final JSONObject feedComment = (JSONObject) feedArray.get(i);
                            FeedItem item = new FeedItem();

                            item.setIdComment(feedComment.getString("ID_USER"));
                            item.setNamecomment(feedComment.getString("name"));
                            item.setAvatarComment(feedComment.getString("profilePic"));
                            item.setStatusComment(feedComment.getString("ContainerComment"));
                            item.setTimecomment(feedComment.getString("Currendate"));
                            item.setPhone(feedComment.getString("phone"));

                            item.setCountComment(feedComment.getString("count_comment"));

                            count_comment.setText(item.getCountComment());

                            // get reply comment
                            item.setCountcommentplay(feedComment.getString("CountComment"));
                            item.setIdRComment(feedComment.getString("ID_COMMENT"));

                            String idUserReplayComment = feedComment.isNull("iduserReplay") ? null : feedComment.getString("iduserReplay");
                            String nameReplay = feedComment.isNull("nameReplay") ? null : feedComment.getString("nameReplay");
                            String profilePic = feedComment.isNull("profilePicReplay") ? null : feedComment.getString("profilePicReplay");
                            String ContainerCommentRply = feedComment.isNull("ContainerCommentRply") ? null : feedComment.getString("ContainerCommentRply");
                            String currentdateRply = feedComment.isNull("currentdateRply") ? null : feedComment.getString("currentdateRply");

                            item.setIdUserReplayComment(idUserReplayComment);
                            item.setNameReplay(nameReplay);
                            item.setProfilePicReplay(profilePic);
                            item.setContainerCommentRply(ContainerCommentRply);
                            item.setCurrendateCommentRply(currentdateRply);

                            //Log.d("ccccc", "onLongClick:" + item.getId_sc());
                            feedItems.add(item);
                        }
                    }

                    doawloadsuccess();

                } catch (Exception ex) {
                    // JSON parsing error
                    //Log.d("tttttt", "error: "+ex.getMessage());

                    onFail(apiUrl, params, ex.getMessage(), ex);
                }
            }

            @Override
            public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {
                showToast(getString(R.string.error_load_event_comment));
            }
        }, "id_sc", id_sc);
    }

    private void doawloadsuccess() {
        recyclerView.setVisibility(View.VISIBLE);
//        progressBar.setVisibility(View.INVISIBLE);
        listAdapter.notifyDataSetChanged();

        Intent a = new Intent();
        a.setAction(AppIntent.ACTION_RELOAD_COUNT_COMMENT);
        a.putExtra("reloadcountcomment", true);
        a.putExtra("countcomment", count_comment.getText());
        a.putExtra(POST_ITEM_HOME, postItem);
        getApplication().sendBroadcast(a);
    }

// post watched notification
public void postwatching(final String idUser, final String id_noti, final String url, final String type_noti) {
    // Hide all views

    StringRequest strReq = new StringRequest(Request.Method.POST,
            url, new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            Intent intent = new Intent();
            intent.setAction(AppIntent.ACTION_PUSH_RECEIVED);
            intent.putExtra(AppIntent.ACTION_PUSH_WATCHED, "watched");
            intent.putExtra(AppIntent.ACTION_ID_NOTI, id_noti);
            intent.putExtra(AppIntent.ACTION_TYPE_NOTI, type_noti);
            sendBroadcast(intent);

        }
    }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            MyLog.d("error when postWatching", error.getMessage());
        }
    }) {
        @Override
        protected Map<String, String> getParams() {
            // Posting params to register url
            Map<String, String> params = new HashMap<>();
            params.put("id_user", idUser);
            params.put("id_noti", id_noti);

            return params;
        }

    };

    // Adding request to request queue

    AppController.getInstance().addToRequestQueue(strReq);
}

    @Override
    public void onStart() {
        super.onStart();

        client.connect();

        if(mRemittanceBroadcastReceiver == null)
            mRemittanceBroadcastReceiver = new RemittanceBroadcastReceiver();

        registerReceiver(mRemittanceBroadcastReceiver, new IntentFilter(RemittanceBroadcastReceiver.ACTION_SEND_MONEY));
    }

    @Override
    public void onStop() {
        super.onStop();
        if (client.isConnected()) {
            client.disconnect();
        }

        unregisterReceiver(mRemittanceBroadcastReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(client.isConnected()){
            client.disconnect();
        }
        client = null;
        Glide.clear(imEvent);
        imEvent = null;

    }

    @Override
    protected void onResume() {
        super.onResume();
        downloadComment(idSc);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_details_event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else if(item.getItemId() == R.id.menu_edit) {
            Calendar curent = Calendar.getInstance();
            if(curent.getTimeInMillis()> mDayEnd){
                Toast.makeText(this,getResources().getString(R.string.editEvent),Toast.LENGTH_SHORT).show();
            }
            else
            {
                Intent it = new Intent(this, UpdateEventActivity.class);
                it.putExtra("id_sc", idSc);
                it.putExtra("nameEvent", mEventData.status);
                it.putExtra("location", mEventData.location);
                it.putExtra("position", mEventData.position);
                it.putExtra("dateStart", mEventData.dayStart);
                it.putExtra("dateEnd", mEventData.dayFinish);
                it.putExtra("image", mEventData.image);
                it.putExtra("color", mEventData.color);
                it.putExtra("lg1", mEventData.lg1);
                it.putExtra("lg2", mEventData.lg2);
                it.putExtra("phone", mEventData.phone);
                it.putExtra("description",mEventData.desription);
                it.putExtra("allday", mEventData.allDay ? "1" : "0");
                startActivityForResult(it, AppIntent.REQUEST_UPDATE_EVENT);
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == AppIntent.REQUEST_UPDATE_EVENT) {
            if (resultCode == RESULT_OK) {
                mEventDetailPresenter.attemptGetEventDetail(this, idUser, idSc);
            }
        }
    }

    @Override
    public void onSuccessCommentAPI(String apiUrl) {
        if (AppConfig.URL_DELETECOMMENT.equals(apiUrl)) {
            // success comment delete from server
            // reload for comment count
            downloadComment(idSc);
        }
    }
}