package com.finger.tigo.friend;

/**
 * Created by Duong on 6/5/2017.
 */

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.finger.tigo.BaseActivity;
import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.addfriend.DetailFriendActivity;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.friend.adapter.ListFriendAdapter;
import com.finger.tigo.items.ItemNoti;
import com.finger.tigo.listener.RecyclerTouchListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Duong on 5/26/2017.
 */

public class TigonNewFriendActivity extends BaseActivity  {

    RecyclerView recyclerRequesTigon;
    private ListFriendAdapter listAdapter;
    String idUser;
    private List<ItemNoti> feedItems;
    TextView tvCountRequest;
    Toolbar toolbar;


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tigon_request);
        idUser = AppAccountManager.getAppAccountUserId(this);
        tvCountRequest = (TextView) findViewById(R.id.tv_count_request_tigon);
        recyclerRequesTigon = (RecyclerView) findViewById(R.id.list_tigon_request);
        toolbar = (Toolbar)findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitle("TiGon");

        initListNotificationfriend();
        showProgress();
        setNotificationLIstFriend();
    }

    private void initListNotificationfriend() {
        feedItems = new ArrayList<>();
        listAdapter = new ListFriendAdapter(TigonNewFriendActivity.this, feedItems);
        recyclerRequesTigon.setHasFixedSize(true);
        RecyclerView.LayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(TigonNewFriendActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerRequesTigon.setLayoutManager(horizontalLayoutManagaer);

        recyclerRequesTigon.setItemAnimator(new DefaultItemAnimator());
        recyclerRequesTigon.setAdapter(listAdapter);
        recyclerRequesTigon.addOnItemTouchListener(new RecyclerTouchListener(TigonNewFriendActivity.this, recyclerRequesTigon, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                ItemNoti item = feedItems.get(position);
                Intent i = new Intent(TigonNewFriendActivity.this, DetailFriendActivity.class);
                updateWatchedFriend(item.getIdFriend());

                i.putExtra(DetailFriendActivity.ID_USER, item.getIdFriend());
                i.putExtra(DetailFriendActivity.NAME,item.getName());
                i.putExtra(DetailFriendActivity.PROFILE_PIC, item.getProfilePic());
                i.putExtra(DetailFriendActivity.PHONE, item.getPhone());
                startActivity(i);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }
    // update watched
    private void updateWatchedFriend( final String repicient ) {
        // Tag used to cancel the request

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPDATE_WATCHED_FRIEND, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                hideDialog();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("onerror", "Registration Error: " + error.getMessage());

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("sender", idUser+"");
                params.put("repicient", repicient);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }

    public  void setNotificationLIstFriend() {

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_LIST_NEW_FRIEND, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response !=null)
                    parseJsonFeed(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                hideProgress();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("iduser", idUser);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
        // }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {//Write your logic here
            this.finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    public void parseJsonFeed(String response){
        hideProgress();
        try {
            JSONObject jObj = new JSONObject(response);
            JSONArray feedArray = jObj.getJSONArray("feed");
            feedItems.clear();
            for (int i = 0; i < feedArray.length(); i++) {

                JSONObject feedObj = (JSONObject) feedArray.get(i);
                ItemNoti item = new ItemNoti();
                item.setName(feedObj.getString("name"));
                item.setIdFriend(feedObj.getString("iduser"));
                item.setPhone(feedObj.getString("phone"));
                item.setTimeStamp(feedObj.getString("created_at"));
                item.setProfilePic(feedObj.getString("profilePic"));
                item.setSta(feedObj.getString("state"));
                feedItems.add(item);

            }

            if (!feedItems.isEmpty() && feedItems != null) {

                listAdapter.notifyDataSetChanged();
                int total = feedItems.size();

                tvCountRequest.setText(getResources().getString(R.string.new_tigon)+" ("+total+")");

            }

        } catch (JSONException e) {
            e.printStackTrace();
            hideProgress();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        setNotificationLIstFriend();
    }


}
