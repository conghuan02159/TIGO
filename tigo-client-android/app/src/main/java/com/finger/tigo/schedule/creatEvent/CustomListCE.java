package com.finger.tigo.schedule.creatEvent;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.finger.tigo.session.SessionManager;

import java.util.List;

import com.finger.tigo.R;
import com.finger.tigo.app.AppController;

public class CustomListCE extends BaseAdapter {
    private LayoutInflater inflater;
    private List<ItemFr> listItem;
    private Activity activity;
    private ProgressDialog pDialog;
    SessionManager session;
    String idUser;
    RadioGroup rgstate;

    ImageLoader imageLoader = AppController.getInstance().getImageLoader();



    public CustomListCE(Activity activity, List<ItemFr> listItem) {

        this.listItem = listItem;
        this.activity = activity;
        pDialog = new ProgressDialog(activity);
        pDialog.setCancelable(false);
        session = new SessionManager(activity);

    }

    @Override
    public int getCount() {
        return listItem.size();
    }

    @Override
    public Object getItem(int position) {
        return listItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        idUser = session.get_id_user_session() + "";
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.custom_event, null);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

        NetworkImageView profilePic = (NetworkImageView) convertView.findViewById(R.id.network);

        final TextView Name = (TextView) convertView.findViewById(R.id.name);
        final CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.check);
        final ItemFr item = listItem.get(position);


        Name.setText(item.getName());

        profilePic.setImageUrl(item.getProfilePic(), imageLoader);

        return convertView;
    }


    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}