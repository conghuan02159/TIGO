package com.finger.tigo.schedule.creatEvent;

import com.finger.tigo.items.FeedItem;

/**
 * Created by Conghuan on 4/11/2017.
 */

public interface ICreateEventView {
    void createSuccess(String idSc, FeedItem newEvent);
    void createFail(String message, Throwable cause);
    void checkInformation(String check);
}
