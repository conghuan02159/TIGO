package com.finger.tigo.account;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;

import com.finger.tigo.util.MyLog;

/**
 * Created by Finger-kjh on 2017-05-31.
 */

public class AppAccountManager {
    public static final String ACCOUNT_TYPE = "com.finger.tigo";
    public static final String USER_DATA_ID = "USER_DATA_ID";
    public static final String USER_DATA_USERNAME = "USER_DATA_USERNAME";
    public static final String USER_DATA_VERSION = "USER_DATA_VERSION";
    public static final String USER_DATA_CALENDAR = "USER_DATA_CALENDAR";
    public static final String CURRENT_USER_DATA_VERSION = "1";

    public static Account getAppAccount(Context context) {
        AccountManager am = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
        Account[] accountsFromFirstApp = am.getAccountsByType(AppAccountManager.ACCOUNT_TYPE);

        if(accountsFromFirstApp.length > 0) {
            return accountsFromFirstApp[0];
        }
        return null;
    }

    /**
     * retrieve Tigo App Account User Id
     */
    public static String getAppAccountUserId(Context context) {
        AccountManager am = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
        Account[] accountsFromFirstApp = am.getAccountsByType(AppAccountManager.ACCOUNT_TYPE);

        try {
            return am.getUserData(accountsFromFirstApp[0], AppAccountManager.USER_DATA_ID);
        } catch (IllegalArgumentException e) {
            MyLog.e("failed get Tigo App Account userId >> " + e.getMessage());
            return "";
        }
    }

    /**
     * create Tigo calendar and retrieve calendar id
     * @param accountName user account name
     * @return created calendar id
     */
    public static String createCalendarWithName(Context ctx, String accountName) {
        Uri target = Uri.parse(CalendarContract.Calendars.CONTENT_URI.toString());
        target = target.buildUpon().appendQueryParameter(CalendarContract.CALLER_IS_SYNCADAPTER, "true")
                .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_NAME, accountName)
                .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_TYPE, ACCOUNT_TYPE).build();

        ContentValues values = new ContentValues();
        values.put(CalendarContract.Calendars.ACCOUNT_NAME, accountName);
        values.put(CalendarContract.Calendars.ACCOUNT_TYPE, ACCOUNT_TYPE);
        values.put(CalendarContract.Calendars.NAME, "Tigo");
        values.put(CalendarContract.Calendars.CALENDAR_DISPLAY_NAME, "Tigo");
        values.put(CalendarContract.Calendars.CALENDAR_COLOR, 0x00FF00);
        values.put(CalendarContract.Calendars.CALENDAR_ACCESS_LEVEL, CalendarContract.Calendars.CAL_ACCESS_ROOT);
        values.put(CalendarContract.Calendars.OWNER_ACCOUNT, accountName);
        values.put(CalendarContract.Calendars.VISIBLE, 1);
        values.put(CalendarContract.Calendars.SYNC_EVENTS, 1);
        values.put(CalendarContract.Calendars.CALENDAR_TIME_ZONE, "calendar_timezone");
        values.put(CalendarContract.Calendars.CAN_PARTIALLY_UPDATE, 1);
        values.put(CalendarContract.Calendars.CAL_SYNC1, "https://www.google.com/calendar/feeds/" + accountName + "/private/full");
        values.put(CalendarContract.Calendars.CAL_SYNC2, "https://www.google.com/calendar/feeds/default/allcalendars/full/" + accountName);
        values.put(CalendarContract.Calendars.CAL_SYNC3, "https://www.google.com/calendar/feeds/default/allcalendars/full/" + accountName);
        values.put(CalendarContract.Calendars.CAL_SYNC4, 1);
        values.put(CalendarContract.Calendars.CAL_SYNC5, 0);
        values.put(CalendarContract.Calendars.CAL_SYNC8, System.currentTimeMillis());

        Uri newCalendar = ctx.getContentResolver().insert(target, values);
        String calendarid = newCalendar.getLastPathSegment();

        return calendarid;
    }

    public static void saveAccountUserDataAndSyncCalendar(Context context, Account account, String userId, String userName) {
        String calendarid = createCalendarWithName(context.getApplicationContext(), account.name);

        MyLog.d("calendaId = "+ calendarid);
        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
        accountManager.setUserData(account, AppAccountManager.USER_DATA_CALENDAR,calendarid);
        accountManager.setUserData(account, AppAccountManager.USER_DATA_ID, userId);
        accountManager.setUserData(account, AppAccountManager.USER_DATA_USERNAME, userName);
        accountManager.setUserData(account, AppAccountManager.USER_DATA_VERSION, AppAccountManager.CURRENT_USER_DATA_VERSION);

        ContentResolver.setSyncAutomatically(account, CalendarContract.AUTHORITY, true);
        ContentResolver.addPeriodicSync(account, CalendarContract.AUTHORITY, Bundle.EMPTY, 60);
    }

    public static void updateCalendarSync(Context context, Bundle extras) {
        Account account = getAppAccount(context);
        ContentResolver.requestSync(account, CalendarContract.AUTHORITY, extras);
    }

    public static void updateCalendarSync(Context context, String idSc, String color, boolean allDay,
                                          String daystart, String dayend, String eventName, String address) {

        MyLog.d("idSc=" + idSc + ", color=" + color + ", allDay=" + allDay + ", dayStart=" + daystart + ", dayEnd=" + dayend + ", evnetName=" + eventName + ", address=" + address);

        Bundle bundle = new Bundle();
        bundle.putString("idSc", idSc);
        bundle.putString("color", color);
        bundle.putBoolean("allDay", allDay);
        bundle.putString("startMillis", daystart);
        bundle.putString("endMillis", dayend);
        bundle.putString("eventName", eventName);
        bundle.putString("location", address);

        updateCalendarSync(context, bundle);
    }

    public static void deleteCalendarSync(Context context, String eventId) {
        Account account = getAppAccount(context);

        int removedNum = removeCalendarEntry(context, account, Long.parseLong(eventId));

        if(removedNum > 0) {
            context.getContentResolver().notifyChange(CalendarContract.CONTENT_URI, null);
        }
    }

    private static int removeCalendarEntry(Context context, Account account, long eventId) {
        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
        int calendarid = Integer.parseInt(accountManager.getUserData(account, AppAccountManager.USER_DATA_CALENDAR));

        MyLog.d("calendarId=" + calendarid + ", eventId=" + eventId);

        Uri eventsUri = Uri.parse("content://com.android.calendar/events");
        Uri eventUri = ContentUris.withAppendedId(eventsUri, eventId);

        int deletedRowNum = context.getContentResolver().delete(eventUri, null, null);

        MyLog.i("Deleted " + deletedRowNum + " calendar entry.");

        return deletedRowNum;
    }


}
