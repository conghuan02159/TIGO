package com.finger.tigo.register;

import com.finger.tigo.common.contact.Contact;
import com.finger.tigo.register.async.AsyncRegisterInteractor;
import com.finger.tigo.register.async.OnRegisFinishedListener;
import com.finger.tigo.register.model.ItemRegister;

import java.util.List;

/**
 * Created by Duong on 3/23/2017.
 */

public class RegisterPresenter  implements OnRegisFinishedListener{

    IRegisterView iRegisterView;
    AsyncRegisterInteractor AsyncRegis;

    public RegisterPresenter(IRegisterView iRegisterView ){
        this.iRegisterView = iRegisterView;
        AsyncRegis = new AsyncRegisterInteractor();

    }

    public void attemRegisterCreate(String name, String password, String rePassword){
        AsyncRegis.validateCredentailsAsync(this,name, password, rePassword);
    }

    public void attemRegisterPhone(String countryCode, String phone, String name, String password, String rePassword, int sex, long birthday){
        AsyncRegis.validatePhoneAsync(countryCode, this, phone, name, password, rePassword, sex, birthday);
    }

    public void attemRegisterVerifiCode(String code){
        AsyncRegis.validateVerificodeAsync(this, code);
    }

    public void attemRegisterSyncContact(List<Contact> listContact, String idUser){
        AsyncRegis.validateSyncContactAsync(this, listContact, idUser);
    }


    @Override
    public void listenerSuccess(String success, List<ItemRegister> listRegister) {
        iRegisterView.onSuccess(success, listRegister);
    }

    @Override
    public void listenerError(String error, int keyError) {
        iRegisterView.onError(error, keyError);
    }

}
