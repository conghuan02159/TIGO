package com.finger.tigo.schedule.creatEvent;

import com.finger.tigo.app.ApiExecuter;
import com.finger.tigo.items.FeedItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by Conghuan on 4/11/2017.
 */

public class CreateEventPresenter {
    public static final String KEY_ERROR_IDUSER = "iduser";
    public static final String STATUS = "status";
    public static final String LOCATION = "location";
    public static final String POSTION = "postion";
    public static final String DAYSTART = "daystar";
    public static final String DAYFINISH = "dayfinish";
    public static final String DESCRIPPTION = "description";
    public static final String CATEGORY = "category";
    public static final String BITMAP = "bitmap";
    private ICreateEventView view;
    private AsyncCreateEvent interactor;


    public CreateEventPresenter(ICreateEventView view) {
        this.view = view;
        this.interactor = new AsyncCreateEvent();
    }

    public void attemCreateEvent(ApiExecuter.ApiProgressListener apiProgressListener, String progressMsg, final String idSc, final String idUser, final String status, final String location,
                                 final String positions, final String daystart, final String dayfinish,
                                 final String mode, final String phone_event, final String lg1, final String lg2,
                                 final String color, final String category, final String allowinvite, final String alarm,
                                 final String description, final String imageFilePath, final int allDay) {

        if(idUser.isEmpty()){
            view.checkInformation(KEY_ERROR_IDUSER);
        } else if (status.isEmpty()) {
            view.checkInformation(STATUS);
        }else if (location.isEmpty()) {
            view.checkInformation(LOCATION);
        } else {
            interactor.validateCreateEvent(apiProgressListener, progressMsg,
                    new ApiExecuter.ApiResultListener() {
                        @Override
                        public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                            try {
                                JSONObject jObj = new JSONObject(response);
                                boolean error = jObj.getBoolean("error");

                                if (!error) {
                                    JSONObject user = jObj.getJSONObject("user");

                                    String id_sc = user.getString("id_sc");
                                    FeedItem item = new FeedItem();
                                    item.setdaystart(daystart);
                                    item.setDayend(dayfinish);
                                    item.setStatus(status);
                                    item.setAddress(location);
                                    item.setId_sc(id_sc);
                                    item.setColor(color);
                                    item.setAllDay(allDay + "");

                                    view.createSuccess(idSc, item);
                                }
                            } catch (JSONException e) {
                                onFail(apiUrl, params, e.getMessage(), e.getCause());
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {
                            view.createFail(errMessage, cause);
                        }
                    }, idSc, idUser, status, location, positions, daystart, dayfinish, mode, phone_event,
                    lg1, lg2, color, category, allowinvite, alarm, description, imageFilePath,allDay);
        }
    }


}
