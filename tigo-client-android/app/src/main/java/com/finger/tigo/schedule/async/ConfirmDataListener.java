package com.finger.tigo.schedule.async;

import com.finger.tigo.schedule.model.ItemSchedule;

import java.util.List;

/**
 * Created by Duong on 4/17/2017.
 */

public interface ConfirmDataListener {
    void onError(String error, int keyError);
    void onSuccess(String success,List<ItemSchedule> list);
}
