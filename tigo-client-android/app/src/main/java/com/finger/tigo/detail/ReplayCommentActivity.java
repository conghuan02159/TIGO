package com.finger.tigo.detail;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.finger.tigo.R;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppController;
import com.finger.tigo.detail.adapter.AdapterCommentRply;
import com.finger.tigo.items.FeedItem;
import com.finger.tigo.session.SessionManager;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class ReplayCommentActivity extends AppCompatActivity {

    private static final String TAG = "ActivityComment";
    ListView listViewCommentRply;
    String textMessage;
    private List<FeedItem> feedItems;
    private AdapterCommentRply listAdapter;
    private ProgressBar progressBar;
    private String idSc;
    EditText edtMesaage;
    ImageView btnPhotoCamera;
    ImageView tbnSend;
    private GoogleApiClient client;
    String id;
    String id_comment;
    String avartar;
    String textComment;
    String namecomment;
    String id_sc;
    Toolbar toolbar;
    CircleImageView image;
    TextView txtnameComment;
    TextView txtMessgeComment;
    public static String ID_COMMENT="id_comment";
    public static String AVARTAR="avartar";
    public static String TEXT_COMMENT="textComment";
    public static String NAME_COMMENT="namecomment";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment_rply);

        addControl();
        addEvent();
    }
    private void addControl() {
        toolbar=(Toolbar)findViewById(R.id.toolbarComment);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.reply_comment));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setTitleTextColor(Color.WHITE);

        txtnameComment=(TextView)findViewById(R.id.txt_comment_Name);
        image=(CircleImageView)findViewById(R.id.image_comment);
        txtMessgeComment=(TextView)findViewById(R.id.txtMessage);

        listViewCommentRply=(ListView)findViewById(R.id.listcommnet);
        edtMesaage=(EditText)findViewById(R.id.emojicon_edit_text);
        btnPhotoCamera=(ImageView) findViewById(R.id.btnPhotoCamera);
        tbnSend=(ImageView)findViewById(R.id.submit_btn);
        progressBar=(ProgressBar)findViewById(R.id.progressBar);

        feedItems = new ArrayList<FeedItem>();

        listViewCommentRply.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id_comment = bundle.getString(ID_COMMENT);
            avartar=bundle.getString(AVARTAR);
            textComment=bundle.getString(TEXT_COMMENT);
            namecomment=bundle.getString(NAME_COMMENT);
            downloadComment(id_comment);
        } else {
            Toast.makeText(this, "id_repcomment null!", Toast.LENGTH_SHORT).show();
        }
        Glide.with(this).load(avartar).into(image);
        txtMessgeComment.setText(textComment);
        txtnameComment.setText(namecomment);

    }
    private void addEvent()
    {
        tbnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textMessage=edtMesaage.getText().toString();
                if(!textMessage.isEmpty())
                {
                    clickcomment();
                    edtMesaage.setVisibility(View.VISIBLE);
                    edtMesaage.setText("");
                }
                else
                {
                    Toast.makeText(ReplayCommentActivity.this,"",Toast.LENGTH_LONG).show();
                }
            }
        });

        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

    }

    private void clickcomment() {
        // Hide all views
        String tag_string_req = "comments";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADDCOMMENTRPLY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                downloadComment(id_comment);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                SessionManager session = new SessionManager(getApplicationContext());
                String idUser = session.get_id_user_session() + "";
                Intent iin = getIntent();
                Bundle b = iin.getExtras();

                idSc = b.getString("id");
                if (idSc == null) {
                    idSc = b.getString("idd");
                }
                params.put("id_comment", id_comment+"");
                params.put("user_comment",idUser);
                params.put("containerCommentRply", textMessage);
                //postcomment(idSc, newText);
                return params;
            }
        };
        strReq.setTag(this.getClass().getName());
        AppController.getInstance().addToRequestQueue(strReq);


    }
    private void downloadComment(final String id_repcomment)
    {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_COMMENT_RPLY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    JSONArray feedArray = jObj.getJSONArray("comments");
                    feedItems.clear();
                    for (int i = 0; i < feedArray.length(); i++) {
                        final JSONObject feedComment = (JSONObject) feedArray.get(i);
                        FeedItem item = new FeedItem();
                        item.setNamecomment(feedComment.getString("name"));
                        item.setAvatarComment(feedComment.getString("profilePic"));
                        item.setPhone(feedComment.getString("phone"));
                        item.setIdRComment(feedComment.getString("ID_R_comment"));
                        item.setIdComment(feedComment.getString("ID_COMMENT"));
                        item.setId_sc(feedComment.getString("user_comment"));
                        item.setStatusComment(feedComment.getString("ContainerCommentRply"));
                        item.setTimecomment(feedComment.getString("CurrentdateRply"));

                     feedItems.add(item);
                    }
                    doawloadsuccess();

                } catch (Exception ex) {
                    // JSON parsing error
                    ex.printStackTrace();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("id_comment",id_repcomment);

                return params;
            }
        };
        strReq.setTag(this.getClass().getName());
        AppController.getInstance().addToRequestQueue(strReq);
    }

    private void doawloadsuccess()
    {
        listViewCommentRply.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
        listAdapter = new AdapterCommentRply(ReplayCommentActivity.this, feedItems);
        listViewCommentRply.setAdapter(listAdapter);
    }


    @Override
    protected void onResume() {
        downloadComment(id_comment);
        super.onResume();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {//NavUtils.navigateUpFromSameTask(this);
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
