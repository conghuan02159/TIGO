package com.finger.tigo.invite.adapter;

/**
 * Created by Duong on 11/21/2016.
 */


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.finger.tigo.R;
import com.finger.tigo.invite.fragment.InviteMessageFragment;
import com.finger.tigo.schedule.creatEvent.Person;

import java.util.List;


/**
 * Created by duong on 14/11/2016.
 */
public class MessageRecylerAdapter extends RecyclerView.Adapter<MessageRecylerAdapter.PersonViewHolder> implements Filterable {
    OnItemClickListener mItemClickListener;
    List<Person> people;
    private CustomFilter userFilter;

    public OnItemClickListener getmItemClickListener() {
        return mItemClickListener;
    }

    public void setmItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public MessageRecylerAdapter(List<Person> persons){
        this.people = persons;
        userFilter = new CustomFilter(MessageRecylerAdapter.this);
    }

    @Override
    public MessageRecylerAdapter.PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_sent_message, parent, false);
        return new PersonViewHolder(v);
    }


    @Override
    public void onBindViewHolder(MessageRecylerAdapter.PersonViewHolder holder, int position) {
        holder.personName.setText(people.get(position).name);
        holder.personAge.setText(people.get(position).id);
        holder.cbSelected.setChecked(people.get(position).isSelected());

    }

    public void setItemSelected(int position, boolean isSelected){
        if (position != -1) {
            people.get(position).setSelected(isSelected);
            notifyDataSetChanged();
        }
    }

    @Override
    public Filter getFilter() {

        return userFilter;
    }


    public interface OnItemClickListener{
        void onItemClick(View v, int position);
    }

    @Override
    public int getItemCount() {
        return people.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class PersonViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener{

        public final View mView;
        public TextView personName;
        public TextView personAge;
        public Person mItem;

        public CheckBox cbSelected;
        public PersonViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            itemView.setOnClickListener(this);
            personName = (TextView) itemView.findViewById(R.id.person_name);
            personAge = (TextView) itemView.findViewById(R.id.person_age_or_phone);
            cbSelected = (CheckBox) itemView.findViewById(R.id.cbSelected);


        }
        @Override
        public String toString(){
            return super.toString()+" '"+personName +"'";
        }

        @Override
        public void onClick(View v) {
            if(mItemClickListener != null){
                mItemClickListener.onItemClick(v, getPosition());

            }
        }
    }


    public class CustomFilter extends Filter {
        private MessageRecylerAdapter mAdapter;
        private CustomFilter(MessageRecylerAdapter mAdapter) {
            super();
            this.mAdapter = mAdapter;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            InviteMessageFragment.filteredList.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                InviteMessageFragment.filteredList.addAll(InviteMessageFragment.mPersonList);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final Person mWords : InviteMessageFragment.mPersonList) {
                    if (mWords.getName().toLowerCase().startsWith(filterPattern)) {
                        InviteMessageFragment.filteredList.add(mWords);
                    }
                }
            }
            System.out.println("Count Number " + InviteMessageFragment.filteredList.size());
            results.values = InviteMessageFragment.filteredList;
            results.count = InviteMessageFragment.filteredList.size();
            return results;
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            System.out.println("Count Number 2 " + ((List<Person>) results.values).size());
            this.mAdapter.notifyDataSetChanged();
        }
    }
}
