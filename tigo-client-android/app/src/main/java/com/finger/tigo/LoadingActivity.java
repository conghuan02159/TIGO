package com.finger.tigo;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.app.AppIntent;
import com.finger.tigo.libcountrypicker.fragments.CountryPicker;
import com.finger.tigo.libcountrypicker.models.Country;
import com.finger.tigo.login.LoginActivity;
import com.finger.tigo.register.RegisterActivity;
import com.finger.tigo.session.SessionManager;

/**
 * Created by Duong on 3/22/2017.
 */

public class LoadingActivity extends AppCompatActivity {


    SessionManager session;
    CountryPicker mCountryPicker;
    ImageView imFlowerTigon;
    ImageView imLoadingTigon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        session = new SessionManager(this);
//        if (session.getLoggedIn()) {
//
//
//        }

        registerReceiver(this.appendChatScreenMsgReceiver, new IntentFilter(AppIntent.ACTION_FINISH_ACTIVITY));
        AccountManager am =  (AccountManager)this.getSystemService(Context.ACCOUNT_SERVICE);
        Account[] accountsFromFirstApp = am.getAccountsByType(AppAccountManager.ACCOUNT_TYPE);
        if(accountsFromFirstApp.length !=0)
        {

//            // User is already logged in. Take him to main activity
            Intent intent = new Intent(this, SlideMenuActivity.class);
            startActivity(intent);
            finish();
        }

        setContentView(R.layout.activity_loading);
//        imFlowerTigon = (ImageView)findViewById(R.id.im_flower_tigon);
////        imLoadingTigon = (ImageView)findViewById(R.id.im_icon_tigo_loading);
////        Glide.with(this).load(R.drawable.icon_tigon_loading).into(imLoadingTigon);
//        Glide.with(this).load(R.drawable.hoa_tigon_tran).into(imFlowerTigon);

        session = new SessionManager(this);
        mCountryPicker = CountryPicker.newInstance(getResources().getString(R.string.select_country));
        Country country = mCountryPicker.getUserCountryInfo(this);
        session.setKeyCodeCountry(country.getDialCode());

        Button btn_signup=(Button) findViewById(R.id.btn_signup);
        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoadingActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });

        Button btn_login=(Button) findViewById(R.id.btn_login);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoadingActivity.this,LoginActivity.class);
                startActivity(intent);
            }
        });
    }

    BroadcastReceiver appendChatScreenMsgReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();
            if (b != null) {

               if(b.getBoolean("finishFromLogin")){
                    finish();
                }
            }
        }
    };
    @Override
    public void onResume(){
        registerReceiver(this.appendChatScreenMsgReceiver, new IntentFilter(AppIntent.ACTION_FINISH_ACTIVITY));
        super.onResume();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(appendChatScreenMsgReceiver != null)
            unregisterReceiver(appendChatScreenMsgReceiver);

    }
}