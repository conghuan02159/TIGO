package com.finger.tigo.menu.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.finger.tigo.BaseActivity;
import com.finger.tigo.R;
import com.finger.tigo.account.AppAccountManager;
import com.finger.tigo.app.ApiExecuter;
import com.finger.tigo.app.AppConfig;
import com.finger.tigo.app.AppIntent;
import com.finger.tigo.detail.EventDetailActivity;
import com.finger.tigo.items.FeedItem;
import com.finger.tigo.menu.UpdateEventActivity;
import com.finger.tigo.util.MyLog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.finger.tigo.detail.EventDetailActivity.M_DETAILS_HOME_ACTIVITY;


public class MenuRecyclerAdapter extends RecyclerView.Adapter {

    private List<FeedItem> feedItems;
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    // The minimum amount of items to have below your current scroll position
    // before loading more.
    private int visibleThreshold ;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    Activity activity;
    String idUser;

    public MenuRecyclerAdapter(Activity activity, List<FeedItem> persons, RecyclerView mRecyclerView) {
        this.feedItems = persons;
        this.activity = activity;
        this.idUser = AppAccountManager.getAppAccountUserId(activity);

        if (mRecyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    visibleThreshold = linearLayoutManager.getChildCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadmore();
                        }
                        loading = true;
                    }
                }

            });
        }


    }

    public interface OnLoadMoreListener {
        void onLoadmore();
    }

    @Override
    public int getItemCount() {
        return feedItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return feedItems.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_menu_detail_person, parent, false);
            vh = new MyViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_event_progess, parent, false);
            vh = new ProgressViewHolder(v);
        }


        return vh;
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {
        final FeedItem item = feedItems.get(position);

        if (viewHolder instanceof MyViewHolder) {
            long dayStart = Long.parseLong(item.getdaystart());
            long dayEnd =  Long.parseLong(item.getDateend());
            SimpleDateFormat dsft = new SimpleDateFormat("E, dd MMM yyyy", Locale.getDefault());
            final String strStDate = dsft.format(dayStart);
            ((MyViewHolder) viewHolder).dayStart.setText(strStDate);

            ((MyViewHolder) viewHolder).name.setText(item.getStatus());

            // checking for null feed Description
            if (item.getUrl() != null) {
                ((MyViewHolder) viewHolder).description.setText(item.getDesciption());
                ((MyViewHolder) viewHolder).description.setVisibility(View.VISIBLE);
            } else {
                // description is null, remove from the view
                ((MyViewHolder) viewHolder).description.setVisibility(View.GONE);
            }



            Glide.with(((MyViewHolder) viewHolder).imgEvent.getContext()).load(item.getImge()).animate(android.R.anim.fade_in)
                    .error(R.drawable.background_hoa_tigo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.background_hoa_tigo)
                    .into(((MyViewHolder) viewHolder).imgEvent);

            ((MyViewHolder) viewHolder).show_dialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final PopupMenu popup = new PopupMenu(activity, v);
                    //Inflating the Popup using xml file
                    popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());

                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem items) {
                            if (items.getItemId() == R.id.delete) {
                                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);

                                builder.setMessage(R.string.delete_event)
                                        .setCancelable(false)
                                        .setPositiveButton(R.string.notification_yes, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int idd) {

                                                deleteEvent(activity,item.getId_sc(), idUser, position);

                                            }
                                        })
                                        .setNegativeButton(R.string.notification_no, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                //  Action for 'NO' Button
                                                dialog.cancel();
                                            }
                                        });

                                //Creating dialog box
                                android.app.AlertDialog alert = builder.create();
                                //Setting the title manually
                                alert.setTitle(R.string.delete_title_event);
                                alert.show();
//
                            } else if (items.getItemId() == R.id.update) {

                                // Log.d("rrrrrrrr", "onMenuItemClick: "+item.getColor());
                                Intent it = new Intent(activity, UpdateEventActivity.class);
                                String idScc = item.getId_sc() + "";
                                it.putExtra("id_sc", idScc);
                                it.putExtra("nameEvent", item.getStatus());
                                it.putExtra("location", item.getAddress());
                                it.putExtra("position", item.getPosition());
                                it.putExtra("dateStart", item.getdaystart());
                                it.putExtra("dateEnd", item.getDateend());
                                it.putExtra("image", item.getImge());
                                it.putExtra("color", item.getColor());
                                it.putExtra("lg1", item.getLg1());
                                it.putExtra("phone", item.getPhoneEvent());
                                it.putExtra("lg2", item.getLg2());
                                it.putExtra("mode", item.getMode());
                                it.putExtra("alarm", item.getAlarm());
                                it.putExtra("allowinvite", item.getAllowInvite());
                                it.putExtra("description",item.getDesciption());
                                it.putExtra("allday", item.getAllDay());
                                it.putExtra("category", item.getCategory());
                                activity.startActivityForResult(it, AppIntent.REQUEST_UPDATE_EVENT);

                            } else if (items.getItemId() == R.id.share) {
                                String a = activity.getResources().getString(R.string.person_share);
                                String b = activity.getResources().getString(R.string.name_share);
                                String c = activity.getResources().getString(R.string.date_share);
                                String d = activity.getResources().getString(R.string.time_share);
                                String e = activity.getResources().getString(R.string.where_share);
                                String f = activity.getResources().getString(R.string.person_participation);

                                long dayStart = Long.parseLong(item.getdaystart());
                                SimpleDateFormat deft = new SimpleDateFormat("E, dd MMM yyyy", Locale.getDefault());
                                String strDate = deft.format(dayStart);

                                SimpleDateFormat timeStartSF = new SimpleDateFormat("HH:mm a", Locale.getDefault());
                                final String startTime = timeStartSF.format(dayStart);

                                String message = "\n" + a + feedItems.get(position).getName()
                                        + "\n" + b + feedItems.get(position).getStatus()
                                        + "\n" + c + strDate
                                        + "\n" + d + startTime
                                        + "\n" + e + feedItems.get(position).getAddress()
                                        + "\n" + f + feedItems.get(position).getCount_participation()
                                        + "\n " + feedItems.get(position).getImge();
                                shareAction(message);
                            }
                            return true;
                        }
                    });

                    popup.show();

                }
            });
            Calendar curent = Calendar.getInstance();
            if(curent.getTimeInMillis()> dayEnd){
//                ((MyViewHolder) viewHolder).layoutItem.setBackgroundColor(activity.getResources().getColor(R.color.colorBlackTran));
                ((MyViewHolder) viewHolder).img_oldevent.setVisibility(View.VISIBLE);

            }else{
//                ((MyViewHolder) viewHolder).layoutItem.setBackgroundColor(activity.getResources().getColor(R.color.white));
                ((MyViewHolder) viewHolder).img_oldevent.setVisibility(View.GONE);
            }
        } else {
            if (feedItems.size() >= 10) {
                ((ProgressViewHolder) viewHolder).progressBar.setIndeterminate(true);
                ((ProgressViewHolder) viewHolder).progressBar.setVisibility(View.VISIBLE);
            }
            else {

                ((ProgressViewHolder) viewHolder).progressBar.setIndeterminate(false);
                ((ProgressViewHolder) viewHolder).progressBar.setVisibility(View.GONE);
            }
        }

    }

    public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener listener) {
        this.onLoadMoreListener = listener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        public TextView description;
        public TextView dayStart;
        ImageView imgEvent;
        //        LinearLayout layoutDetail;
        TextView show_dialog;
        public FrameLayout layoutItem;
        public LinearLayout img_oldevent;

        public MyViewHolder(View rowView) {
            super(rowView);

                    name = (TextView)rowView.findViewById(R.id.name_event);
                    description = (TextView)rowView.findViewById(R.id.tv_description);
                    dayStart = (TextView)rowView.findViewById(R.id.tv_day_start);
                    imgEvent = (ImageView)rowView.findViewById(R.id.im_event);
//            layoutDetail = (LinearLayout)rowView.findViewById(R.id.layout_menu_detail);
                    show_dialog = (TextView)rowView.findViewById(R.id.show_dialog);
                    layoutItem = (FrameLayout) rowView.findViewById(R.id.layout_all_item_menu);
                    img_oldevent =(LinearLayout) rowView.findViewById(R.id.img_oldevent);
                    rowView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int position = getLayoutPosition();
                            FeedItem item = feedItems.get(position);
                            Intent i = new Intent(activity, EventDetailActivity.class);

                            i.putExtra(M_DETAILS_HOME_ACTIVITY, item.getId_sc() + "");
                            activity.startActivity(i);
                        }
                    });



        }
    }

    public  class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progress_more);
        }
    }

    private void deleteEvent(Context a,final String id_sc, final String id_user, final int position) {

        ApiExecuter.requestApi((BaseActivity) activity, AppConfig.URL_DELETE_EVENT,a.getString(R.string.deleting_event) ,
                new ApiExecuter.ApiResultListener() {
                    @Override
                    public void onSuccessApi(String apiUrl, Map<String, String> params, String response) {
                        MyLog.d("deleteEvent response=" + response);
                        AppAccountManager.deleteCalendarSync(activity, id_sc.split("-")[0]);

                        feedItems.remove(position);
                        notifyDataSetChanged();
                    }

                    @Override
                    public void onFail(String apiUrl, Map<String, String> params, String errMessage, Throwable cause) {
                        ((BaseActivity) activity).showToast(errMessage);
                    }
                }, "id_sc", id_sc, "id_user", id_user);


//        ((BaseActivity) activity).showProgress("deleting...");
//        StringRequest strReq = new StringRequest(Request.Method.POST,
//                AppConfig.URL_DELETE_EVENT, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                ((BaseActivity) activity).hideProgress();
//                MyLog.d("deleteEvent response=" + response);
//                AppAccountManager.deleteCalendarSync(activity, id_sc.split("-")[0]);
//
//                feedItems.remove(position);
//                notifyDataSetChanged();
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                ((BaseActivity) activity).hideProgress();
//                error.printStackTrace();
//                Log.e("yyyyyy", "Registration Error: " + error.getMessage());
//                //    hideDialog();
//
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() {
//                // Posting params to register url
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("id_sc", id_sc);
//                params.put("id_user", id_user);
//
//                return params;
//            }
//        };
//        // Adding request to request queue
//        strReq.setRetryPolicy(new DefaultRetryPolicy(
//                30000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        AppController.getInstance().addToRequestQueue(strReq);
    }

    public void shareAction(String message) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_TEXT, message);
        activity.startActivity(Intent.createChooser(share, activity.getResources().getString(R.string.title_share)));
    }

}