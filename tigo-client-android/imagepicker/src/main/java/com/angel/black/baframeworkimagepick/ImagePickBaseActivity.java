package com.angel.black.baframeworkimagepick;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.angel.black.baframeworkimagepick.dialog.AlertDialogFragment;
import com.angel.black.baframeworkimagepick.dialog.DialogClickListener;
import com.angel.black.baframeworkimagepick.util.BaLog;

/**
 * Created by Finger-kjh on 2017-05-26.
 */

public class ImagePickBaseActivity extends AppCompatActivity implements Toolbar.OnMenuItemClickListener {

    protected Toolbar mToolbar;
    protected ProgressBar mLoadingProgress;
    protected ViewGroup mRootLayout;
    protected ViewGroup mContentsLayout;

    public static boolean DEBUG = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_base);
        initToolbar();

        mRootLayout = (ViewGroup) findViewById(R.id.layout_activity_root);
        mContentsLayout = (ViewGroup) findViewById(R.id.layout_activity_contents);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        getLayoutInflater().inflate(layoutResID, mContentsLayout);
    }

    protected void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if(mToolbar != null) {
            setSupportActionBar(mToolbar);

            ActionBar actionBar = getSupportActionBar();

            actionBar.setDisplayHomeAsUpEnabled(true);
            mToolbar.setOnMenuItemClickListener(this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        BaLog.d();

        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                if(getParent() != null) {
                    NavUtils.navigateUpFromSameTask(this);
                    return true;
                }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if(getParent() != null) {
                NavUtils.navigateUpFromSameTask(this);
                return true;
            }
        }

        return false;
    }

    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public void showToast(int msgResId) {
        Toast.makeText(this, msgResId, Toast.LENGTH_LONG).show();
    }


    public void addFragment(int resId, Fragment fragment, String tag, boolean addToBackStack) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(resId, fragment, tag);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        if(addToBackStack)
            ft.addToBackStack(null);
        ft.commitAllowingStateLoss();
    }

    public void replaceFragment(int resId, Fragment fragment, String tag, boolean addToBackStack, boolean isAni) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if(isAni)
            ft.setCustomAnimations(R.anim.ip_slide_right_in, R.anim.ip_slide_left_out, R.anim.ip_slide_left_in, R.anim.ip_slide_right_out);
        if(addToBackStack)
            ft.addToBackStack(null);
        ft.replace(resId, fragment, tag);
        ft.commitAllowingStateLoss();
    }


    public void removeFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();

        if(fragment != null) {
            FragmentTransaction ft = fm.beginTransaction();
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            ft.remove(fragment);
            ft.commitAllowingStateLoss();
        }
    }


    public void showOkDialog(int strResId) {
        this.showOkDialog(getString(strResId));
    }

    public void showOkDialog(String message) {
        AlertDialogFragment dialogFragment = AlertDialogFragment.newInstance(null, message);
        showDialogFragment(dialogFragment, "okDlg");
    }

    public void showAlertDialog(int msgResId, DialogClickListener positiveClick) {
        this.showAlertDialog(getString(msgResId), positiveClick);
    }

    public void showAlertDialog(String message, DialogClickListener positiveClick) {
        AlertDialogFragment dialogFragment = AlertDialogFragment.newInstance(null, message, positiveClick);
        showDialogFragment(dialogFragment, "altDlgWithPosi");
    }

    public void showAlertDialogNotCancelable(int msgResId, DialogClickListener positiveClick) {
        AlertDialogFragment dialogFragment = AlertDialogFragment.newInstance(null, getString(msgResId), true, positiveClick);
        showDialogFragment(dialogFragment, "altDlgWithPosiNotCancel");
    }

    private void showDialogFragment(DialogFragment dialogFragment, String tag) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        ft.add(dialogFragment, tag);
        ft.commitAllowingStateLoss();
    }

    public void showProgress() {
        BaLog.i();
        Log.d(this.getClass().getSimpleName(), " showProgress!!");
        if(mLoadingProgress == null) {
            mLoadingProgress = (ProgressBar) findViewById(R.id.loading_progress);
        }

        if(mLoadingProgress != null) {
            mLoadingProgress.setVisibility(View.VISIBLE);
        }
    }

    public void hideProgress() {
        BaLog.i();
        Log.d(this.getClass().getSimpleName(), " hideProgress!!");
        if(mLoadingProgress != null) {
            mLoadingProgress.setVisibility(View.GONE);
        }
    }

    public ViewGroup getContentsLayout() {
        return mContentsLayout;
    }

    protected void initToolbarWithOnBackPressed() {
        initToolbar(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    protected void initToolbar(View.OnClickListener naviClick) {
        if(mToolbar != null) {
            mToolbar.setNavigationOnClickListener(naviClick);
        }
    }
}
