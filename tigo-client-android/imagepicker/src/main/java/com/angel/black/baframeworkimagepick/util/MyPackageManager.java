package com.angel.black.baframeworkimagepick.util;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.content.IntentCompat;

import com.angel.black.baframeworkimagepick.R;

import java.io.File;


/**
 * Created by KimJeongHun on 2016-05-02.
 */
public class MyPackageManager {
    public static final String TAG = MyPackageManager.class.getSimpleName();

    /**
     * 앱을 재시작시킨다.
     */
    public static void restartApp(Context context) {
        PackageManager packageManager = context.getPackageManager();
        Intent intent = packageManager.getLaunchIntentForPackage(context.getPackageName());
        ComponentName componentName = intent.getComponent();
        Intent mainIntent = IntentCompat.makeRestartActivityTask(componentName);
        context.startActivity(mainIntent);
        System.exit(0);
    }

    public static String getPublicAppAlbumPath(Context context) {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + "/" + getPublicAppAlbumName(context) + "/";
    }

    public static String getPublicAppAlbumName(Context mContext) {
        return mContext.getString(R.string.app_name);
    }

    public static String getTempImagePath(Context context) {
        String tempPath = context.getFilesDir().getAbsolutePath() + "/temp/";

        File tempDir = new File(tempPath);
        if(!tempDir.exists()) {
            tempDir.mkdir();
        }

        return tempDir.getAbsolutePath();
    }
}
