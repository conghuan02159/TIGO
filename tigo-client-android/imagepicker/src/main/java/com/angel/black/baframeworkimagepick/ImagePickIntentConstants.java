package com.angel.black.baframeworkimagepick;

/**
 * Created by KimJeongHun on 2016-09-18.
 */
public class ImagePickIntentConstants {
    public static final int REQUEST_IMAGE_PICK = 100;
    public static final int REQUEST_ZOOM_IMAGE = 200;
    public static final int REQUEST_EDIT_IMAGE = 300;
    public static final int REQUEST_TAKE_CAMERA = 400;

    public static final String KEY_IMAGE_PATH_LIST = "imagePathList";
    public static final String KEY_IMAGE_PICK_COUNT = "imagePickCount";
    public static final String KEY_IMAGE_COUNT = "imageCount";
    public static final String KEY_INITIAL_IMAGE_INDEX = "initialImageIndex";

    public static final String KEY_IMAGE_EDIT_HIDE_ORDERING = "hideOrdering";
    public static final String KEY_IMAGE_CHANGE_INDEX = "imageChangeIndex";
    public static final String KEY_SHOW_SELECTED_IMAGES_PREVIEW_BAR = "showSelImgPrvBar";
    public static final String KEY_CROP_SHAPE = "cropShape";

}
