package com.angel.black.baframeworkimagepick;

        import android.app.Activity;
import android.content.Intent;
        import android.provider.MediaStore;

        import com.angel.black.baframeworkimagepick.imagepick.ImagePickActivity;
import com.angel.black.baframeworkimagepick.imagepick.ImagePickWithCameraActivity;
import com.theartofdev.edmodo.cropper.CropImageView;

/**
 * Created by Finger-kjh on 2017-05-26.
 */

public class ImagePickExecuter {

    public static void executeGalleryPick(Activity activity) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        activity.startActivityForResult(Intent.createChooser(intent, "Select Picture"), ImagePickIntentConstants.REQUEST_IMAGE_PICK);
    }

    public static void executeImagePick(Activity activity, Class<? extends ImagePickActivity> imagePickActivity, int pickCount, boolean showPreviewBar) {
        Intent intent = new Intent(activity, imagePickActivity);
        intent.putExtra(ImagePickIntentConstants.KEY_IMAGE_PICK_COUNT, pickCount);
        intent.putExtra(ImagePickIntentConstants.KEY_SHOW_SELECTED_IMAGES_PREVIEW_BAR, showPreviewBar);
        activity.startActivityForResult(intent, ImagePickIntentConstants.REQUEST_IMAGE_PICK);
    }

    public static void executeImagePickWithCameraToOneImage(Activity activity,
                                                            boolean showPreviewBar) {
        executeImagePickWithCameraToOneImage(activity, showPreviewBar, CropImageView.CropShape.RECTANGLE);
    }

    public static void executeImagePickWithCameraToOneImage(Activity activity,
                                                            boolean showPreviewBar, CropImageView.CropShape cropShape) {
        Intent intent = new Intent(activity, ImagePickWithCameraActivity.class);
        intent.putExtra(ImagePickIntentConstants.KEY_IMAGE_PICK_COUNT, 1);
        intent.putExtra(ImagePickIntentConstants.KEY_SHOW_SELECTED_IMAGES_PREVIEW_BAR, showPreviewBar);
        intent.putExtra(ImagePickIntentConstants.KEY_CROP_SHAPE, cropShape);
        activity.startActivityForResult(intent, ImagePickIntentConstants.REQUEST_IMAGE_PICK);
    }

    public static void executeCamera(Activity activity) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        activity.startActivityForResult(intent, ImagePickIntentConstants.REQUEST_TAKE_CAMERA);
    }
}
