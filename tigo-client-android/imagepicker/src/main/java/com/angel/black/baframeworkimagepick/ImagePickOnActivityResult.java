package com.angel.black.baframeworkimagepick;

import android.content.Intent;

import com.angel.black.baframeworkimagepick.util.BaLog;
import com.angel.black.baframeworkimagepick.util.FileUtil;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Finger-kjh on 2017-05-29.
 */

public class ImagePickOnActivityResult {
    public static String onActivityResultAndGetOneImagePath(Intent data) {
        if (data == null)
            return "";

        ArrayList<String> imagePathList = data.getStringArrayListExtra(ImagePickIntentConstants.KEY_IMAGE_PATH_LIST);

        if (imagePathList == null || imagePathList.size() == 0)
            return "";

        String imagePath = imagePathList.get(0);

        File file = new File(imagePath);
        BaLog.w("picked image path = " + imagePath + ", size = " + FileUtil.getFileSize(file));

        return imagePathList.get(0);
    }
}
