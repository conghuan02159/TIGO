package com.angel.black.baframeworkimagepick.util;

import android.view.View;
import android.view.ViewGroup;

/**
 * Created by KimJeongHun on 2016-05-04.
 */
public class ViewUtil {

    public static void setVisibilityHierancy(ViewGroup vg, int visiblity) {
        vg.setVisibility(View.VISIBLE);

        for(int i = 0 ; i < vg.getChildCount(); i++) {
            View child = vg.getChildAt(i);
            child.setVisibility(visiblity);

            if(child instanceof ViewGroup) {
                setVisibilityHierancy((ViewGroup) child, visiblity);
            }
        }
    }
}
