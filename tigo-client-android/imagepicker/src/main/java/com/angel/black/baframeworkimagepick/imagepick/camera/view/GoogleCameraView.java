//package com.angel.black.baframeworkimagepick.imagepick.camera.view;
//
//import android.media.MediaActionSound;
//import android.os.Handler;
//import android.os.Message;
//import android.support.v4.app.Fragment;
//import android.view.View;
//import android.view.ViewGroup;
//
//import com.angel.black.baframeworkimagepick.ImagePickBaseActivity;
//import com.angel.black.baframeworkimagepick.imagepick.util.CameraPictureFileBuilder;
//import com.angel.black.baframeworkimagepick.util.BaLog;
//import com.angel.black.baframeworkimagepick.util.BuildUtil;
//import com.angel.black.baframeworkimagepick.util.MyPackageManager;
//import com.google.android.cameraview.CameraView;
//
//import org.apache.commons.lang3.ArrayUtils;
//
///**
// * Created by kimjeonghun on 2017. 6. 25..
// */
//
//public class GoogleCameraView extends CameraViewCompat {
//    private CameraView mCameraView;
//    private boolean mStoppedPreview;
//
//    protected GoogleCameraView(Fragment fragment, ViewGroup root) {
//        super(fragment, root);
//        initCameraPreview((ImagePickBaseActivity) fragment.getActivity());
//    }
//
//    private void initCameraPreview(ImagePickBaseActivity activity) {
//        mCameraView = new CameraView(activity);
//        mCameraView.addCallback(mCallback);
//        mCameraView.setFacing(CameraView.FACING_BACK);
//        mCameraView.setFlash(CameraView.FLASH_AUTO);
//        mCameraView.setAutoFocus(true);
//
//        mParentCameraPreview.addView(mCameraView, 0, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//
//        mBtnSwitchCamera.setVisibility(View.VISIBLE);
//
//        mCameraView.start();
//    }
//
//    CameraView.Callback mCallback = new CameraView.Callback() {
//        @Override
//        public void onCameraOpened(CameraView cameraView) {
//            BaLog.i();
//            ((CameraOpenCallback) mFragment).onSuccessCameraOpen();
//        }
//
//        @Override
//        public void onCameraClosed(CameraView cameraView) {
//            BaLog.i();
//            ((CameraOpenCallback) mFragment).onFailCameraOpen();
//        }
//
//        @Override
//        public void onPictureTaken(CameraView cameraView, byte[] data) {
//            BaLog.i();
//            super.onPictureTaken(cameraView, data);
//
//            ((CameraActionCallback) mFragment).onSuccessTakenPicture();
//
//            if(BuildUtil.isAboveJellyBean()) {
//                new MediaActionSound().play(MediaActionSound.SHUTTER_CLICK);
//            } else {
//
//            }
//
//            CameraPictureFileBuilder task = new CameraPictureFileBuilder(mActivity,
//                    MyPackageManager.getTempImagePath(mActivity), false,
//                    false, mHandler);
//            task.execute(ArrayUtils.toObject(data));
//        }
//    };
//
//    Handler mHandler = new Handler(mActivity.getMainLooper()) {
//        @Override
//        public void handleMessage(Message msg) {
//            if (msg.what == 0) {
//                final CameraPictureFileBuilder.BuildImageResult buildImageResult = (CameraPictureFileBuilder.BuildImageResult) msg.obj;
//                BaLog.d("build buildImageResult=" + buildImageResult);
//                ((CameraActionCallback) mFragment).onSuccessSavePictureImageToFile(buildImageResult);
//            } else if(msg.what == 1) {
//                ((CameraActionCallback) mFragment).onSuccessTakenPicture();
//            } else if(msg.what == 2) {
//                CameraPictureFileBuilder task = new CameraPictureFileBuilder(mActivity,
//                        MyPackageManager.getTempImagePath(mActivity), false,
//                        false, mHandler);
//                task.execute(ArrayUtils.toObject((byte[]) msg.obj));
//            }
//        }
//    };
//
//    @Override
//    public boolean isCreated() {
//        return mCameraView != null;
//    }
//
//    @Override
//    public void takePicture() throws Exception {
//        mCameraView.takePicture();
//    }
//
//    @Override
//    public void pauseCamera() {
//        BaLog.i();
//        mCameraView.removeCallback(mCallback);
//        mCameraView.stop();
//        mStoppedPreview = true;
//    }
//
//    @Override
//    public void resumePreview() {
//        mCameraView.resumePreview();
//        mCameraView.addCallback(mCallback);
//        mStoppedPreview = false;
//    }
//
//    @Override
//    public boolean isAliveCamera() {
//        return mCameraView.isCameraOpened() && !mStoppedPreview;
//    }
//
//    @Override
//    public boolean isShowingPreview() {
//        return !mStoppedPreview;
//    }
//
//    @Override
//    public void startPreview() {
//        mCameraView.start();
//        mCameraView.addCallback(mCallback);
//        mStoppedPreview = false;
//    }
//
//    @Override
//    public void stopPreview() {
//        BaLog.i();
//        mCameraView.removeCallback(mCallback);
//        mCameraView.stop();
//        mStoppedPreview = true;
//    }
//
//    @Override
//    public View getCameraPreview() {
//        return mCameraView;
//    }
//
//    @Override
//    public void openCameraAfterViewCreated() {
//        mCameraView.start();
//        mCameraView.addCallback(mCallback);
//        mStoppedPreview = false;
//    }
//
//    @Override
//    public int getFlashMode() {
//        return 0;
//    }
//
//    @Override
//    public boolean isFrontCamera() {
//        return mCameraView.getFacing() == CameraView.FACING_FRONT;
//    }
//
//    @Override
//    public void switchCamera(boolean front) {
//        if(front) {
//            mCameraView.setFacing(CameraView.FACING_FRONT);
//        } else {
//            mCameraView.setFacing(CameraView.FACING_BACK);
//        }
//    }
//
//    @Override
//    public void releaseCamera() {
//        BaLog.i();
//        mCameraView.removeCallback(mCallback);
//        mCameraView.stop();
//        mStoppedPreview = true;
//    }
//
//    @Override
//    public void determineCameraResumeAfterTakePicture() {
//
//    }
//
//    @Override
//    public void setDestFilePath(String destFilePath) {
//    }
//}
